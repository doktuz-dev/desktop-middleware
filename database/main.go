package database

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"github.com/spf13/viper"
	"go.elastic.co/apm/module/apmmongo"
	"go.elastic.co/apm/module/apmsql"
	_ "go.elastic.co/apm/module/apmsql/mysql"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var MiddlewareDB *mongo.Database
var MysqlDB *sql.DB

type AppDatabases struct {
	MiddlewareDB *mongo.Database
	MysqlDB      *sql.DB
}

func InitDatabases() *AppDatabases {
	mongoMiddleware := initMongoMiddlewareDB()
	MiddlewareDB = mongoMiddleware
	mysqlDB := initMysqlDB()
	MysqlDB = mysqlDB
	return &AppDatabases{
		MiddlewareDB: mongoMiddleware,
		MysqlDB:      mysqlDB,
	}
}

func initMongoMiddlewareDB() *mongo.Database {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx,
		options.Client().ApplyURI(viper.GetString("databaseMiddleware.uri")),
		options.Client().SetMonitor(apmmongo.CommandMonitor()))
	if err != nil {
		fmt.Println(err)
	}
	return client.Database(viper.GetString("databaseMiddleware.name"))
}

func initMysqlDB() *sql.DB {
	db, err := apmsql.Open("mysql", viper.GetString("databaseMediwebDB.uri"))
	if err != nil {
		fmt.Println(err)
	}
	db.SetConnMaxLifetime(time.Minute * 1)
	db.SetMaxOpenConns(100)
	db.SetMaxIdleConns(100)
	db.SetConnMaxIdleTime(time.Minute * 1)
	return db
}

func (dbs *AppDatabases) DisconnectDatabases() {
	err := dbs.MiddlewareDB.Client().Disconnect(context.TODO())
	if err != nil {
		panic(err)
	}
	err2 := dbs.MysqlDB.Close()
	if err2 != nil {
		panic(err2)
	}
}
