package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type PathologicalHistoryCollection struct {
	Collection *mongo.Collection
}

func GetPathologicalHistoryCollection() *PathologicalHistoryCollection {
	db := database.MiddlewareDB
	pathologicalHistory := db.Collection("transactions.attentions.pathologicalhistories")
	return &PathologicalHistoryCollection{Collection: pathologicalHistory}
}

func (wp *PathologicalHistoryCollection) AddPathologicalHistory(ctx context.Context, pathologicalHistory *models.PathologicalHistory) (*models.PathologicalHistory, error) {
	pathologicalHistory.CreatedDate = time.Now()
	pathologicalHistory.LastUpdatedDate = time.Now()
	pathologicalHistory.ExternalSystem = "MEDIWEB"
	if len(pathologicalHistory.ID) == 0 {
		pathologicalHistory.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, pathologicalHistory)
	if err != nil {
		return nil, err
	}
	return pathologicalHistory, nil
}

func (wp *PathologicalHistoryCollection) UpdatePathologicalHistory(ctx context.Context, id string, pathologicalHistory *models.PathologicalHistory) (bool, error) {
	pathologicalHistory.LastUpdatedDate = time.Now()
	pathologicalHistory.ExternalSystem = "MEDIWEB"
	filter := bson.M{"_id": id}

	vp := reflect.New(reflect.TypeOf(pathologicalHistory))
	vp.Elem().Set(reflect.ValueOf(pathologicalHistory))
	v := vp.Interface()
	update := bson.M{"$set": v}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *PathologicalHistoryCollection) GetPathologicalHistorys(ctx context.Context) ([]*models.PathologicalHistory, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrPathologicalHistorys := []*models.PathologicalHistory{}
	erro := res.All(ctx, &arrPathologicalHistorys)
	if erro != nil {
		return nil, erro
	}
	return arrPathologicalHistorys, nil
}

func (wp *PathologicalHistoryCollection) GetPathologicalHistoryByExternalId(ctx context.Context, id string) (*models.PathologicalHistory, error) {

	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models.PathologicalHistory{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *PathologicalHistoryCollection) DeletePathologicalHistoryByExternalId(ctx context.Context, id string) (bool, error) {
	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	_, err := wp.Collection.DeleteOne(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}
