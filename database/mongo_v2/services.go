package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models_v2"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type ServiceCollection struct {
	Collection *mongo.Collection
}

func GetServiceCollection() *ServiceCollection {
	db := database.MiddlewareDB
	service := db.Collection("registers.services")
	return &ServiceCollection{Collection: service}
}

func (wp *ServiceCollection) AddService(ctx context.Context, service *models_v2.Service) (*models_v2.Service, error) {
	service.CreatedDate = time.Now()
	service.LastUpdatedDate = time.Now()
	service.ExternalSystem = "MEDIWEB"
	if len(service.ID) == 0 {
		service.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, service)
	if err != nil {
		return nil, err
	}
	return service, nil
}

func (wp *ServiceCollection) UpdateService(ctx context.Context, id string, service *models_v2.Service) (bool, error) {
	service.LastUpdatedDate = time.Now()
	service.ExternalSystem = "MEDIWEB"
	filter := bson.M{"_id": id}

	vp := reflect.New(reflect.TypeOf(service))
	vp.Elem().Set(reflect.ValueOf(service))
	v := vp.Interface()
	update := bson.M{"$set": v}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *ServiceCollection) GetServices(ctx context.Context) ([]*models_v2.Service, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrServices := []*models_v2.Service{}
	erro := res.All(ctx, &arrServices)
	if erro != nil {
		return nil, erro
	}
	return arrServices, nil
}

func (wp *ServiceCollection) GetServiceByExternalId(ctx context.Context, id string) (*models_v2.Service, error) {

	filter := bson.M{"externalId": id, "status": 1, "externalSystem": "MEDIWEB"}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models_v2.Service{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *ServiceCollection) GetServiceById(ctx context.Context, id string, attentionTypeId string) (*models_v2.Service, error) {

	filter := bson.M{"externalId": id, "attentionTypeId": attentionTypeId, "externalSystem": "MEDIWEB"}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models_v2.Service{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *ServiceCollection) DeleteServiceByExternalId(ctx context.Context, id string, attentionTypeId string) (bool, error) {
	filter := bson.M{"externalId": id, "attentionTypeId": attentionTypeId, "externalSystem": "MEDIWEB"}
	_, err := wp.Collection.DeleteOne(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}
