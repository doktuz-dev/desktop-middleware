package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type ExamTypeCollection struct {
	Collection *mongo.Collection
}

func GetExamTypeCollection() *ExamTypeCollection {
	db := database.MiddlewareDB
	examType := db.Collection("registers.examtypes")
	return &ExamTypeCollection{Collection: examType}
}

func (wp *ExamTypeCollection) AddExamType(ctx context.Context, examType *models.ExamType) (*models.ExamType, error) {
	examType.CreatedDate = time.Now()
	examType.LastUpdatedDate = time.Now()
	examType.ExternalSystem = "MEDIWEB"
	if len(examType.ID) == 0 {
		examType.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, examType)
	if err != nil {
		return nil, err
	}
	return examType, nil
}

func (wp *ExamTypeCollection) UpdateExamType(ctx context.Context, id string, examType *models.ExamType) (bool, error) {
	examType.LastUpdatedDate = time.Now()
	examType.ExternalSystem = "MEDIWEB"
	filter := bson.M{"_id": id}

	vp := reflect.New(reflect.TypeOf(examType))
	vp.Elem().Set(reflect.ValueOf(examType))
	v := vp.Interface()
	update := bson.M{"$set": v}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *ExamTypeCollection) GetExamTypes(ctx context.Context) ([]*models.ExamType, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExamTypes := []*models.ExamType{}
	erro := res.All(ctx, &arrExamTypes)
	if erro != nil {
		return nil, erro
	}
	return arrExamTypes, nil
}

func (wp *ExamTypeCollection) GetExamTypeById(ctx context.Context, id string) (*models.ExamType, error) {

	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models.ExamType{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *ExamTypeCollection) DeleteExamTypeByExternalId(ctx context.Context, id string) (bool, error) {
	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	_, err := wp.Collection.DeleteOne(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}
