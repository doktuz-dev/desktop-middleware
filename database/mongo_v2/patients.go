package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models_v2"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type PatientCollection struct {
	Collection *mongo.Collection
}

func GetPatientCollection() *PatientCollection {
	db := database.MiddlewareDB
	patient := db.Collection("registers.patients2")
	return &PatientCollection{Collection: patient}
}

func (wp *PatientCollection) AddPatient(ctx context.Context, patient *models_v2.Patient) (*models_v2.Patient, error) {
	patient.CreatedDate = time.Now()
	patient.LastUpdatedDate = time.Now()
	if len(patient.ID) == 0 {
		patient.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, patient)
	if err != nil {
		return nil, err
	}
	return patient, nil
}

func (wp *PatientCollection) UpdatePatient(ctx context.Context, id string, patient *models_v2.Patient) (bool, error) {
	patient.LastUpdatedDate = time.Now()
	filter := bson.M{"_id": id}

	vp := reflect.New(reflect.TypeOf(patient))
	vp.Elem().Set(reflect.ValueOf(patient))
	v := vp.Interface()
	update := bson.M{"$set": v}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *PatientCollection) GetPatients(ctx context.Context) ([]*models_v2.Patient, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrPatients := []*models_v2.Patient{}
	erro := res.All(ctx, &arrPatients)
	if erro != nil {
		return nil, erro
	}
	return arrPatients, nil
}

func (wp *PatientCollection) GetPatientById(ctx context.Context, id string) (*models_v2.Patient, error) {
	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	res := wp.Collection.FindOne(ctx, filter)
	var patient models_v2.Patient
	if res.Err() != nil {
		return nil, res.Err()
	}
	err := res.Decode(&patient)
	if err != nil {
		return nil, err
	}
	return &patient, nil
}

func (wp *PatientCollection) GetPatientByDocument(ctx context.Context, doc string, docType int) (*models_v2.Patient, error) {
	filter := bson.M{"documentTypeId": docType, "documentNumber": doc}
	res := wp.Collection.FindOne(ctx, filter)
	var patient models_v2.Patient
	if res.Err() != nil {
		return nil, res.Err()
	}
	err := res.Decode(&patient)
	if err != nil {
		return nil, err
	}
	return &patient, nil
}

func (wp *PatientCollection) DeletePatientByExternalId(ctx context.Context, id string) (bool, error) {
	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	_, err := wp.Collection.DeleteOne(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}
