package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type ProvinceCollection struct {
	Collection *mongo.Collection
}

func GetProvinceCollection() *ProvinceCollection {
	db := database.MiddlewareDB
	province := db.Collection("registers.provinces")
	return &ProvinceCollection{Collection: province}
}

func (wp *ProvinceCollection) AddProvince(ctx context.Context, province *models.Province) (*models.Province, error) {
	province.CreatedDate = time.Now()
	province.LastUpdatedDate = time.Now()
	province.ExternalSystem = "MEDIWEB"
	if len(province.ID) == 0 {
		province.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, province)
	if err != nil {
		return nil, err
	}
	return province, nil
}

func (wp *ProvinceCollection) UpdateProvince(ctx context.Context, id string, province *models.Province) (bool, error) {
	province.LastUpdatedDate = time.Now()
	province.ExternalSystem = "MEDIWEB"
	filter := bson.M{"_id": id}

	vp := reflect.New(reflect.TypeOf(province))
	vp.Elem().Set(reflect.ValueOf(province))
	v := vp.Interface()
	update := bson.M{"$set": v}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *ProvinceCollection) GetProvinces(ctx context.Context) ([]*models.Province, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrProvinces := []*models.Province{}
	erro := res.All(ctx, &arrProvinces)
	if erro != nil {
		return nil, erro
	}
	return arrProvinces, nil
}

func (wp *ProvinceCollection) GetProvinceById(ctx context.Context, id string) (*models.Province, error) {

	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models.Province{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *ProvinceCollection) DeleteProvinceByExternalId(ctx context.Context, id string) (bool, error) {
	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	_, err := wp.Collection.DeleteOne(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}
