package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models_v2"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type PersonCollection struct {
	Collection *mongo.Collection
}

func GetPersonCollection() *PersonCollection {
	db := database.MiddlewareDB
	person := db.Collection("registers.persons.v2")
	return &PersonCollection{Collection: person}
}

func (wp *PersonCollection) AddPerson(ctx context.Context, person *models_v2.Person) (*models_v2.Person, error) {
	now := time.Now()
	person.CreatedDate = &now
	person.LastUpdatedDate = &now
	if person.ID == "" {
		person.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, person)
	if err != nil {
		return nil, err
	}
	return person, nil
}

func (wp *PersonCollection) UpdatePerson(ctx context.Context, id string, person *models_v2.Person) (bool, error) {
	now := time.Now()
	person.LastUpdatedDate = &now
	filter := bson.M{"_id": id}

	vp := reflect.New(reflect.TypeOf(person))
	vp.Elem().Set(reflect.ValueOf(person))
	v := vp.Interface()
	update := bson.M{"$set": v}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *PersonCollection) GetPersons(ctx context.Context) ([]*models_v2.Person, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrPersons := []*models_v2.Person{}
	erro := res.All(ctx, &arrPersons)
	if erro != nil {
		return nil, erro
	}
	return arrPersons, nil
}

func (wp *PersonCollection) GetPersonById(ctx context.Context, id string) (*models_v2.Person, error) {

	filter := bson.M{"_id": id}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models_v2.Person{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *PersonCollection) GetPersonByDocumentNumber(ctx context.Context, documentNumber string, documentTypeId int) (*models_v2.Person, error) {

	filter := bson.M{"documentNumber": documentNumber, "documentTypeId": documentTypeId}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models_v2.Person{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *PersonCollection) GetPersonByDocumentNumberAndCustomer(ctx context.Context, documentNumber string, documentTypeId int, customerId string) (*models_v2.Person, error) {

	filter := bson.M{"documentNumber": documentNumber, "documentTypeId": documentTypeId, "customerId": customerId}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models_v2.Person{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *PersonCollection) DeletePersonById(ctx context.Context, id string) (bool, error) {
	filter := bson.M{"_id": id}
	_, err := wp.Collection.DeleteOne(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *PersonCollection) CustomSetFieldById(ctx context.Context, indexFieldName, id string, fieldName string, body interface{}) (bool, error) {
	unset := bson.M{fieldName: body}

	filter := bson.M{indexFieldName: id}
	update := bson.M{"$set": unset}
	_, err := wp.Collection.UpdateMany(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}
