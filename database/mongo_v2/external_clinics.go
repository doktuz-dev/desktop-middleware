package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models_v2"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type ExternalClinicCollection struct {
	Collection *mongo.Collection
}

func GetExternalClinicCollection() *ExternalClinicCollection {
	db := database.MiddlewareDB
	examType := db.Collection("registers.externalclinics")
	return &ExternalClinicCollection{Collection: examType}
}

func (wp *ExternalClinicCollection) AddExternalClinic(ctx context.Context, examType *models_v2.ExternalClinic) (*models_v2.ExternalClinic, error) {
	examType.CreatedDate = time.Now()
	examType.LastUpdatedDate = time.Now()
	examType.ExternalSystem = "MEDIWEB"
	if len(examType.ID) == 0 {
		examType.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, examType)
	if err != nil {
		return nil, err
	}
	return examType, nil
}

func (wp *ExternalClinicCollection) UpdateExternalClinic(ctx context.Context, id string, examType *models_v2.ExternalClinic) (bool, error) {
	examType.LastUpdatedDate = time.Now()
	examType.ExternalSystem = "MEDIWEB"
	filter := bson.M{"_id": id}

	vp := reflect.New(reflect.TypeOf(examType))
	vp.Elem().Set(reflect.ValueOf(examType))
	v := vp.Interface()
	update := bson.M{"$set": v}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *ExternalClinicCollection) GetExternalClinics(ctx context.Context) ([]*models_v2.ExternalClinic, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExternalClinics := []*models_v2.ExternalClinic{}
	erro := res.All(ctx, &arrExternalClinics)
	if erro != nil {
		return nil, erro
	}
	return arrExternalClinics, nil
}

func (wp *ExternalClinicCollection) GetExternalClinicById(ctx context.Context, id int) (*models_v2.ExternalClinic, error) {

	filter := bson.M{"code": id, "externalSystem": "MEDIWEB"}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models_v2.ExternalClinic{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *ExternalClinicCollection) DeleteExternalClinicByExternalId(ctx context.Context, id int) (bool, error) {
	filter := bson.M{"code": id, "externalSystem": "MEDIWEB"}
	_, err := wp.Collection.DeleteOne(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}
