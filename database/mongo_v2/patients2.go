package mongo

import (
	"context"
	"desktop-middleware/database"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type Patient2Collection struct {
	Collection *mongo.Collection
}

func GetPatient2Collection() *Patient2Collection {
	db := database.MiddlewareDB
	patient := db.Collection("registers.patients")
	return &Patient2Collection{Collection: patient}
}

func (wp *Patient2Collection) UpdatePatients(ctx context.Context) (bool, error) {

	update := bson.M{"$set": bson.M{"phoneNumber": "0"}}
	_, err := wp.Collection.UpdateMany(ctx, bson.M{"phoneNumber": ""}, update)
	if err != nil {
		return false, err
	}
	return true, nil
}
