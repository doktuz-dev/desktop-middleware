package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models_v2"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type SpecialityCollection struct {
	Collection *mongo.Collection
}

func GetSpecialityCollection() *SpecialityCollection {
	db := database.MiddlewareDB
	speciality := db.Collection("registers.specialities")
	return &SpecialityCollection{Collection: speciality}
}

func (wp *SpecialityCollection) AddSpeciality(ctx context.Context, speciality *models_v2.Speciality) (*models_v2.Speciality, error) {
	speciality.CreatedDate = time.Now()
	speciality.LastUpdatedDate = time.Now()
	speciality.ExternalSystem = "MEDIWEB"
	if len(speciality.ID) == 0 {
		speciality.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, speciality)
	if err != nil {
		return nil, err
	}
	return speciality, nil
}

func (wp *SpecialityCollection) UpdateSpeciality(ctx context.Context, id string, speciality *models_v2.Speciality) (bool, error) {
	speciality.LastUpdatedDate = time.Now()
	speciality.ExternalSystem = "MEDIWEB"
	filter := bson.M{"_id": id}

	vp := reflect.New(reflect.TypeOf(speciality))
	vp.Elem().Set(reflect.ValueOf(speciality))
	v := vp.Interface()
	update := bson.M{"$set": v}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *SpecialityCollection) GetSpecialities(ctx context.Context) ([]*models_v2.Speciality, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrSpecialitys := []*models_v2.Speciality{}
	erro := res.All(ctx, &arrSpecialitys)
	if erro != nil {
		return nil, erro
	}
	return arrSpecialitys, nil
}

func (wp *SpecialityCollection) GetSpecialityByExternalId(ctx context.Context, id string) (*models_v2.Speciality, error) {

	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models_v2.Speciality{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *SpecialityCollection) DeleteSpecialityByExternalId(ctx context.Context, id string) (bool, error) {
	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	_, err := wp.Collection.DeleteOne(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}
