package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type TriageCollection struct {
	Collection *mongo.Collection
}

func GetTriageCollection() *TriageCollection {
	db := database.MiddlewareDB
	pathologicalHistory := db.Collection("transactions.attentions.triages")
	return &TriageCollection{Collection: pathologicalHistory}
}

func (wp *TriageCollection) AddTriage(ctx context.Context, pathologicalHistory *models.Triage) (*models.Triage, error) {
	pathologicalHistory.CreatedDate = time.Now()
	pathologicalHistory.LastUpdatedDate = time.Now()
	pathologicalHistory.ExternalSystem = "MEDIWEB"
	if len(pathologicalHistory.ID) == 0 {
		pathologicalHistory.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, pathologicalHistory)
	if err != nil {
		return nil, err
	}
	return pathologicalHistory, nil
}

func (wp *TriageCollection) UpdateTriage(ctx context.Context, id string, pathologicalHistory *models.Triage) (bool, error) {
	pathologicalHistory.LastUpdatedDate = time.Now()
	pathologicalHistory.ExternalSystem = "MEDIWEB"
	filter := bson.M{"_id": id}

	vp := reflect.New(reflect.TypeOf(pathologicalHistory))
	vp.Elem().Set(reflect.ValueOf(pathologicalHistory))
	v := vp.Interface()
	update := bson.M{"$set": v}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *TriageCollection) GetTriages(ctx context.Context) ([]*models.Triage, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrTriages := []*models.Triage{}
	erro := res.All(ctx, &arrTriages)
	if erro != nil {
		return nil, erro
	}
	return arrTriages, nil
}

func (wp *TriageCollection) GetTriageByExternalId(ctx context.Context, id string) (*models.Triage, error) {

	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models.Triage{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *TriageCollection) DeleteTriageByExternalId(ctx context.Context, id string) (bool, error) {
	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	_, err := wp.Collection.DeleteOne(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}
