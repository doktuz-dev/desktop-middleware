package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type MaritalStatusCollection struct {
	Collection *mongo.Collection
}

func GetMaritalStatusCollection() *MaritalStatusCollection {
	db := database.MiddlewareDB
	department := db.Collection("registers.maritalstatus")
	return &MaritalStatusCollection{Collection: department}
}

func (wp *MaritalStatusCollection) AddMaritalStatus(ctx context.Context, department *models.MaritalStatus) (*models.MaritalStatus, error) {
	department.CreatedDate = time.Now()
	department.LastUpdatedDate = time.Now()
	if len(department.ID) == 0 {
		department.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, department)
	if err != nil {
		return nil, err
	}
	return department, nil
}

func (wp *MaritalStatusCollection) UpdateMaritalStatus(ctx context.Context, id string, department *models.MaritalStatus) (bool, error) {
	department.LastUpdatedDate = time.Now()
	filter := bson.M{"_id": id}

	vp := reflect.New(reflect.TypeOf(department))
	vp.Elem().Set(reflect.ValueOf(department))
	v := vp.Interface()
	update := bson.M{"$set": v}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *MaritalStatusCollection) GetMaritalStatuss(ctx context.Context) ([]*models.MaritalStatus, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrMaritalStatuss := []*models.MaritalStatus{}
	erro := res.All(ctx, &arrMaritalStatuss)
	if erro != nil {
		return nil, erro
	}
	return arrMaritalStatuss, nil
}
