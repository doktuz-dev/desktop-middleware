package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models_v2"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type AttentionCollection struct {
	Collection *mongo.Collection
}

func GetAttentionCollection() *AttentionCollection {
	db := database.MiddlewareDB
	attention := db.Collection("transactions.attentions.v2")
	return &AttentionCollection{Collection: attention}
}

var externalSystem = "MEDIWEB"

func (wp *AttentionCollection) AddAttention(ctx context.Context, attention *models_v2.Attention) (*models_v2.Attention, error) {
	now := time.Now()
	var healthcentreName = fmt.Sprintf("Doktuz - %s", *attention.HealthCentreName)

	attention.CreatedDate = &now
	attention.LastUpdatedDate = &now
	attention.ExternalSystem = &externalSystem
	attention.HealthCentreName = &healthcentreName
	if len(attention.ID) == 0 {
		attention.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, attention)
	if err != nil {
		return nil, err
	}
	return attention, nil
}

func (wp *AttentionCollection) UpdateAttention(ctx context.Context, id string, attention *models_v2.Attention) (bool, error) {
	now := time.Now()
	var healthcentreName = fmt.Sprintf("Doktuz - %s", *attention.HealthCentreName)
	attention.LastUpdatedDate = &now
	attention.ExternalSystem = &externalSystem
	attention.HealthCentreName = &healthcentreName

	filter := bson.M{"_id": id}
	update := bson.M{
		"$set": bson.M{
			"customerId":         attention.CustomerId,
			"customerCode":       attention.CustomerCode,
			"customerName":       attention.CustomerName,
			"personId":           attention.PersonId,
			"patientId":          attention.PatientId,
			"attentionTypeId":    attention.AttentionTypeId,
			"attentionTypeName":  attention.AttentionTypeName,
			"deliveryTypeId":     attention.DeliveryTypeId,
			"deliveryTypeName":   attention.DeliveryTypeName,
			"healthCentreId":     attention.HealthCentreId,
			"healthCentreName":   attention.HealthCentreName,
			"workStationId":      attention.WorkStationId,
			"workStationName":    attention.WorkStationName,
			"workProfileId":      attention.WorkProfileId,
			"workProfileName":    attention.WorkProfileName,
			"workingArea":        attention.WorkingArea,
			"examTypeId":         attention.ExamTypeId,
			"examTypeName":       attention.ExamTypeName,
			"JobTitle":           attention.JobTitle,
			"attentionDate":      attention.AttentionDate,
			"mailStatus":         attention.MailStatus,
			"mailSentDateTime":   attention.MailSentDateTime,
			"mailOpenedDateTime": attention.MailOpenedDateTime,
			"wasAttended":        attention.WasAttended,
			"externalClinicFlag": attention.ExternalClinicFlag,
			"externalClinicId":   attention.ExternalClinicID,
			"externalClinicName": attention.ExternalClinicName,
			"departmentId":       attention.DepartmentId,
			"departmentName":     attention.DepartmentName,
			"districtId":         attention.DistrictId,
			"districtName":       attention.DistrictName,
			"status":             attention.Status,
			"externalId":         attention.ExternalId,
			"externalSystem":     attention.ExternalSystem,
			"createdDate":        attention.CreatedDate,
			"lastUpdatedDate":    attention.LastUpdatedDate,
			"customer":           attention.Customer,
			"patient":            attention.Patient,
			"person":             attention.Person,
			"healthcentre":       attention.Healthcentre,
			"workStation":        attention.WorkStation,
			"workProfile":        attention.WorkProfile,
			"externalClinic":     attention.ExternalClinic,
		},
	}

	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *AttentionCollection) UpdatePersonIdToAttentions(ctx context.Context, patientId, personId string) (bool, error) {

	filter := bson.M{"patientId": patientId}
	update := bson.M{"$set": bson.M{"personId": personId}}
	_, err := wp.Collection.UpdateMany(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *AttentionCollection) UnsetPersonIdAttention(ctx context.Context, id string) (bool, error) {
	unset := bson.M{"personId": ""}

	filter := bson.M{"_id": id}
	update := bson.M{"$unset": unset}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}
func (wp *AttentionCollection) GetLastEmoAttentionByPatientId(ctx context.Context, personId, customerId, typeAttention string) (*models_v2.Attention, error) {
	matchStage := bson.M{"personId": personId, "customer._id": customerId}
	switch typeAttention {
	case "emo":
		matchStage["attentionTypeName"] = "Ocupacional"
		matchStage["aptitude"] = bson.M{"$ne": nil}
		matchStage["aptitude.resultCode"] = bson.M{"$in": []string{"1", "2", "8"}}
	case "no_emo":
		matchStage["attentionTypeName"] = "Asistencial"
	case "observed":
		matchStage["attentionTypeName"] = "Ocupacional"
		matchStage["aptitude"] = bson.M{"$ne": nil}
		matchStage["aptitude.resultCode"] = bson.M{"$in": []string{"3"}}
	case "any":
		matchStage["attentionTypeName"] = "Ocupacional"
		matchStage["wasAttended"] = true
	default:
		// Do not modify the matchStage
	}

	pipeline := []bson.M{
		{"$match": matchStage},
		{"$project": bson.M{"attentionLines": 0}},
		{"$sort": bson.M{"attentionDate": -1}},
		{"$limit": 1},
	}
	res, err := wp.Collection.Aggregate(ctx, pipeline)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	var results []interface{}
	errDecode := res.All(ctx, &results)
	if errDecode != nil {
		return nil, errDecode
	}
	var attentionFounded *models_v2.Attention
	for _, attention := range results {
		if val, ok := attention.(primitive.D); ok {
			data, err := bson.Marshal(val)
			if err != nil {
				// handle error during marshaling
				return nil, err
			}
			var attentionActive *models_v2.Attention
			err = bson.Unmarshal(data, &attentionActive)
			if err != nil {
				// handle error, could not unmarshal
				return nil, err
			}
			attentionFounded = attentionActive
			attentionFounded.Person = nil
			attentionFounded.AttentionLines = nil
			attentionFounded.Exams = nil
		} else {
			return nil, err
		}
	}
	return attentionFounded, err
}

func (wp *AttentionCollection) GetAttentions(ctx context.Context) ([]*models_v2.Attention, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrAttentions := []*models_v2.Attention{}
	erro := res.All(ctx, &arrAttentions)
	if erro != nil {
		return nil, erro
	}
	return arrAttentions, nil
}

func (wp *AttentionCollection) GetAttentionById(ctx context.Context, id string) (*models_v2.Attention, error) {

	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models_v2.Attention{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *AttentionCollection) GetAttentionByExternalId(ctx context.Context, id string) (*models_v2.Attention, error) {
	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models_v2.Attention{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *AttentionCollection) DeleteAttentionByExternalId(ctx context.Context, id string) (bool, error) {
	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	_, err := wp.Collection.DeleteOne(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *AttentionCollection) UnsetFieldByExternalId(ctx context.Context, id string, fieldName string) (bool, error) {
	unset := bson.M{fieldName: 1}

	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	update := bson.M{"$unset": unset}
	_, err := wp.Collection.UpdateMany(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *AttentionCollection) SetFieldByID(ctx context.Context, id string, fieldName string, body interface{}) (bool, error) {
	unset := bson.M{fieldName: body}

	filter := bson.M{"_id": id, "externalSystem": "MEDIWEB"}
	update := bson.M{"$set": unset}
	_, err := wp.Collection.UpdateMany(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *AttentionCollection) CustomSetFieldById(ctx context.Context, indexFieldName, id string, fieldName string, body interface{}) (bool, error) {
	unset := bson.M{fieldName: body}

	filter := bson.M{indexFieldName: id}
	update := bson.M{"$set": unset}
	_, err := wp.Collection.UpdateMany(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *AttentionCollection) SetArrayFieldByExternalId(ctx context.Context, id string, fieldName string, body []interface{}) (bool, error) {
	unset := bson.M{fieldName: body}

	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	update := bson.M{"$set": unset}
	_, err := wp.Collection.UpdateMany(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *AttentionCollection) PullFieldByExternalId(ctx context.Context, attentionId string, fieldName string, externalId string) (bool, error) {
	pull := bson.M{fieldName: bson.M{"externalId": externalId}}

	filter := bson.M{"externalId": attentionId, "externalSystem": "MEDIWEB"}
	update := bson.M{"$pull": pull}
	_, err := wp.Collection.UpdateMany(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *AttentionCollection) PushFieldByID(ctx context.Context, attentionId string, fieldName string, body interface{}) (bool, error) {
	push := bson.M{fieldName: body}

	filter := bson.M{"_id": attentionId}
	update := bson.M{"$push": push}
	_, err := wp.Collection.UpdateMany(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *AttentionCollection) PullBodyFromExamsByExternalId(ctx context.Context, attentionId string, externalId string, serviceAreaId string) (bool, error) {
	pull := bson.M{"exams": bson.M{"externalId": externalId, "serviceAreaId": serviceAreaId}}

	filter := bson.M{"_id": attentionId}
	update := bson.M{"$pull": pull}
	_, err := wp.Collection.UpdateMany(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *AttentionCollection) PushBodyToExamsByExternalId(ctx context.Context, attentionId string, exam interface{}) (bool, error) {
	push := bson.M{"exams": exam}

	filter := bson.M{"_id": attentionId}
	update := bson.M{"$push": push}
	_, err := wp.Collection.UpdateMany(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *AttentionCollection) PullDiagnosticsByExternalId(ctx context.Context, attentionId string, serviceAreaId string) (bool, error) {
	pull := bson.M{"diagnostics": bson.M{"serviceAreaId": serviceAreaId}}

	filter := bson.M{"_id": attentionId}
	update := bson.M{"$pull": pull}
	_, err := wp.Collection.UpdateMany(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *AttentionCollection) PushDiagnosticsByExternalId(ctx context.Context, attentionId string, diagnostics []interface{}) (bool, error) {
	push := bson.M{"diagnostics": bson.M{"$each": diagnostics}}

	filter := bson.M{"_id": attentionId}
	update := bson.M{"$push": push}
	_, err := wp.Collection.UpdateMany(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *AttentionCollection) PullRecommendationsByExternalId(ctx context.Context, attentionId string, serviceAreaId string) (bool, error) {
	pull := bson.M{"recommendations": bson.M{"serviceAreaId": serviceAreaId}}

	filter := bson.M{"_id": attentionId}
	update := bson.M{"$pull": pull}
	_, err := wp.Collection.UpdateMany(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *AttentionCollection) PushRecommendationsByExternalId(ctx context.Context, attentionId string, recommendations []interface{}) (bool, error) {
	push := bson.M{"recommendations": bson.M{"$each": recommendations}}

	filter := bson.M{"_id": attentionId}
	update := bson.M{"$push": push}
	_, err := wp.Collection.UpdateMany(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *AttentionCollection) UpdateServicesInAttentionLines(ctx context.Context, serviceId string, service interface{}) (bool, error) {
	pipeline := bson.A{
		bson.M{"$match": bson.M{"attentionLines.serviceId": serviceId}},
		bson.M{
			"$set": bson.M{
				"attentionLines": bson.M{
					"$map": bson.M{
						"input": "$attentionLines",
						"as":    "line",
						"in": bson.M{
							"$cond": bson.M{
								"if": bson.M{"$eq": []interface{}{"$$line.serviceId", serviceId}},
								"then": bson.M{
									"$mergeObjects": bson.A{
										"$$line",
										bson.M{"service": service},
									},
								},
								"else": "$$line",
							},
						},
					},
				},
			},
		},
		bson.M{
			"$merge": bson.M{
				"into":           "transactions.attentions.v2",
				"on":             "_id",
				"whenMatched":    "merge",
				"whenNotMatched": "discard",
			},
		},
	}

	value, err := wp.Collection.Aggregate(ctx, pipeline)
	if err != nil {
		return false, err
	}

	defer value.Close(ctx)

	return true, nil
}

func (wp *AttentionCollection) UpdateServiceAreasInAttentionLines(ctx context.Context, id string, serviceArea interface{}) (bool, error) {
	pipeline := bson.A{
		bson.M{"$match": bson.M{"attentionLines.serviceArea._id": id}},
		bson.M{
			"$set": bson.M{
				"attentionLines": bson.M{
					"$map": bson.M{
						"input": "$attentionLines",
						"as":    "line",
						"in": bson.M{
							"$cond": bson.M{
								"if": bson.M{"$eq": []interface{}{"$$line.serviceArea._id", id}},
								"then": bson.M{
									"$mergeObjects": bson.A{
										"$$line",
										bson.M{"serviceArea": serviceArea},
									},
								},
								"else": "$$line",
							},
						},
					},
				},
			},
		},
		bson.M{
			"$merge": bson.M{
				"into":           "transactions.attentions.v2",
				"on":             "_id",
				"whenMatched":    "merge",
				"whenNotMatched": "discard",
			},
		},
	}

	value, err := wp.Collection.Aggregate(ctx, pipeline)
	if err != nil {
		return false, err
	}

	defer value.Close(ctx)

	return true, nil
}

func (wp *AttentionCollection) UpdateServiceAreasInDiagnostics(ctx context.Context, id string, serviceArea interface{}) (bool, error) {
	pipeline := bson.A{
		bson.M{"$match": bson.M{"diagnostics.serviceAreaId": id}},
		bson.M{
			"$set": bson.M{
				"diagnostics": bson.M{
					"$map": bson.M{
						"input": "$diagnostics",
						"as":    "line",
						"in": bson.M{
							"$cond": bson.M{
								"if": bson.M{"$eq": []interface{}{"$$line.serviceAreaId", id}},
								"then": bson.M{
									"$mergeObjects": bson.A{
										"$$line",
										bson.M{"serviceArea": serviceArea},
									},
								},
								"else": "$$line",
							},
						},
					},
				},
			},
		},
		bson.M{
			"$merge": bson.M{
				"into":           "transactions.attentions.v2",
				"on":             "_id",
				"whenMatched":    "merge",
				"whenNotMatched": "discard",
			},
		},
	}

	value, err := wp.Collection.Aggregate(ctx, pipeline)
	if err != nil {
		return false, err
	}

	defer value.Close(ctx)

	return true, nil
}

func (wp *AttentionCollection) UpdateServiceAreasInExamResults(ctx context.Context, id string, serviceArea interface{}) (bool, error) {
	pipeline := bson.A{
		bson.M{"$match": bson.M{"examResults.serviceAreaId": id}},
		bson.M{
			"$set": bson.M{
				"examResults": bson.M{
					"$map": bson.M{
						"input": "$examResults",
						"as":    "line",
						"in": bson.M{
							"$cond": bson.M{
								"if": bson.M{"$eq": []interface{}{"$$line.serviceAreaId", id}},
								"then": bson.M{
									"$mergeObjects": bson.A{
										"$$line",
										bson.M{"serviceArea": serviceArea},
									},
								},
								"else": "$$line",
							},
						},
					},
				},
			},
		},
		bson.M{
			"$merge": bson.M{
				"into":           "transactions.attentions.v2",
				"on":             "_id",
				"whenMatched":    "merge",
				"whenNotMatched": "discard",
			},
		},
	}

	value, err := wp.Collection.Aggregate(ctx, pipeline)
	if err != nil {
		return false, err
	}

	defer value.Close(ctx)

	return true, nil
}

func (wp *AttentionCollection) UpdateServiceAreasInExams(ctx context.Context, id string, serviceArea interface{}) (bool, error) {
	pipeline := bson.A{
		bson.M{"$match": bson.M{"exams.serviceAreaId": id}},
		bson.M{
			"$set": bson.M{
				"exams": bson.M{
					"$map": bson.M{
						"input": "$exams",
						"as":    "line",
						"in": bson.M{
							"$cond": bson.M{
								"if": bson.M{"$eq": []interface{}{"$$line.serviceAreaId", id}},
								"then": bson.M{
									"$mergeObjects": bson.A{
										"$$line",
										bson.M{"serviceArea": serviceArea},
									},
								},
								"else": "$$line",
							},
						},
					},
				},
			},
		},
		bson.M{
			"$merge": bson.M{
				"into":           "transactions.attentions.v2",
				"on":             "_id",
				"whenMatched":    "merge",
				"whenNotMatched": "discard",
			},
		},
	}

	value, err := wp.Collection.Aggregate(ctx, pipeline)
	if err != nil {
		return false, err
	}

	defer value.Close(ctx)

	return true, nil
}

func (wp *AttentionCollection) UpdateSpecialityInConsultations(ctx context.Context, id string, speciality interface{}) (bool, error) {
	pipeline := bson.A{
		bson.M{"$match": bson.M{"consultation.specialityId": id}},
		bson.M{
			"$set": bson.M{
				"consultation": bson.M{
					"$map": bson.M{
						"input": "$consultation",
						"as":    "line",
						"in": bson.M{
							"$cond": bson.M{
								"if": bson.M{"$eq": []interface{}{"$$line.specialityId", id}},
								"then": bson.M{
									"$mergeObjects": bson.A{
										"$$line",
										bson.M{"speciality": speciality},
									},
								},
								"else": "$$line",
							},
						},
					},
				},
			},
		},
		bson.M{
			"$merge": bson.M{
				"into":           "transactions.attentions.v2",
				"on":             "_id",
				"whenMatched":    "merge",
				"whenNotMatched": "discard",
			},
		},
	}

	value, err := wp.Collection.Aggregate(ctx, pipeline)
	if err != nil {
		return false, err
	}

	defer value.Close(ctx)

	return true, nil
}
