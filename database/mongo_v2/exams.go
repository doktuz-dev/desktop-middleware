package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type ExamCollection struct {
	Collection *mongo.Collection
}

func GetExamCollection() *ExamCollection {
	db := database.MiddlewareDB
	test := db.Collection("transactions.attentions.exams")
	return &ExamCollection{Collection: test}
}

func (wp *ExamCollection) AddExam(ctx context.Context, test *models.Exam) (*models.Exam, error) {
	test.CreatedDate = time.Now()
	test.LastUpdatedDate = time.Now()
	test.ExternalSystem = "MEDIWEB"
	if len(test.ID) == 0 {
		test.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, test)
	if err != nil {
		return nil, err
	}
	return test, nil
}

func (wp *ExamCollection) UpdateExam(ctx context.Context, id string, test *models.Exam) (bool, error) {
	filter := bson.M{"_id": id}
	query := bson.M{}
	if len(test.AttentionID) > 0 {
		query["attentionId"] = test.AttentionID
	}
	if len(test.ServiceAreaID) > 0 {
		query["serviceAreaId"] = test.ServiceAreaID
	}
	if len(test.Filenames) > 0 {
		query["filenames"] = test.Filenames
	}
	if len(test.Result) > 0 {
		query["result"] = test.Result
	}
	if test.Status > 0 {
		query["status"] = test.Status
	}
	if len(test.Quality) > 0 {
		query["quality"] = test.Quality
	}
	query["reviewed"] = test.Reviewed
	query["lastUpdatedDate"] = time.Now()
	update := bson.M{"$set": query}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *ExamCollection) GetExams(ctx context.Context) ([]*models.Exam, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models.Exam{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	return arrExams, nil
}

func (wp *ExamCollection) GetExamById(ctx context.Context, id string, attentionId string, serviceAreaId string) (*models.Exam, error) {

	filter := bson.M{"externalId": id, "attentionId": attentionId, "serviceAreaId": serviceAreaId, "externalSystem": "MEDIWEB"}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models.Exam{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *ExamCollection) DeleteExamByExternalId(ctx context.Context, id string, attentionId string, serviceAreaId string) (bool, error) {
	filter := bson.M{"externalId": id, "attentionId": attentionId, "serviceAreaId": serviceAreaId, "externalSystem": "MEDIWEB"}
	_, err := wp.Collection.DeleteOne(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}
