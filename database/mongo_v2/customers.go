package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models_v2"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type CustomerCollection struct {
	Collection *mongo.Collection
}

func GetCustomerCollection() *CustomerCollection {
	db := database.MiddlewareDB
	customer := db.Collection("registers.customers")
	return &CustomerCollection{Collection: customer}
}

func (wp *CustomerCollection) AddCustomer(ctx context.Context, customer *models_v2.Customer) (*models_v2.Customer, error) {
	customer.CreatedDate = time.Now()
	customer.LastUpdatedDate = time.Now()
	customer.ExternalSystem = "MEDIWEB"
	if len(customer.ID) == 0 {
		customer.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, customer)
	if err != nil {
		return nil, err
	}
	return customer, nil
}

func (wp *CustomerCollection) UpdateCustomer(ctx context.Context, id string, customer *models_v2.Customer) (bool, error) {
	customer.LastUpdatedDate = time.Now()
	customer.ExternalSystem = "MEDIWEB"
	filter := bson.M{"_id": id}

	vp := reflect.New(reflect.TypeOf(customer))
	vp.Elem().Set(reflect.ValueOf(customer))
	v := vp.Interface()
	update := bson.M{"$set": v}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *CustomerCollection) GetCustomers(ctx context.Context) ([]*models_v2.Customer, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrCustomers := []*models_v2.Customer{}
	erro := res.All(ctx, &arrCustomers)
	if erro != nil {
		return nil, erro
	}
	return arrCustomers, nil
}

func (wp *CustomerCollection) GetCustomerById(ctx context.Context, id string) (*models_v2.Customer, error) {

	filter := bson.M{"externalId": id}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models_v2.Customer{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *CustomerCollection) GetCustomerByCode(ctx context.Context, code string) (*models_v2.Customer, error) {

	filter := bson.M{"code": code}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models_v2.Customer{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *CustomerCollection) DeleteCustomerByExternalId(ctx context.Context, id string) (bool, error) {
	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	_, err := wp.Collection.DeleteOne(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}
