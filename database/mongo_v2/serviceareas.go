package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models_v2"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type ServiceAreaCollection struct {
	Collection *mongo.Collection
}

func GetServiceAreaCollection() *ServiceAreaCollection {
	db := database.MiddlewareDB
	serviceArea := db.Collection("registers.serviceareas")
	return &ServiceAreaCollection{Collection: serviceArea}
}

func (wp *ServiceAreaCollection) AddServiceArea(ctx context.Context, serviceArea *models_v2.ServiceArea) (*models_v2.ServiceArea, error) {
	serviceArea.CreatedDate = time.Now()
	serviceArea.LastUpdatedDate = time.Now()
	serviceArea.ExternalSystem = "MEDIWEB"
	if len(serviceArea.ID) == 0 {
		serviceArea.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, serviceArea)
	if err != nil {
		return nil, err
	}
	return serviceArea, nil
}

func (wp *ServiceAreaCollection) UpdateServiceArea(ctx context.Context, id string, serviceArea *models_v2.ServiceArea) (bool, error) {
	serviceArea.LastUpdatedDate = time.Now()
	serviceArea.ExternalSystem = "MEDIWEB"
	filter := bson.M{"_id": id}

	vp := reflect.New(reflect.TypeOf(serviceArea))
	vp.Elem().Set(reflect.ValueOf(serviceArea))
	v := vp.Interface()
	update := bson.M{"$set": v}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *ServiceAreaCollection) GetServiceAreas(ctx context.Context) ([]*models_v2.ServiceArea, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrServiceAreas := []*models_v2.ServiceArea{}
	erro := res.All(ctx, &arrServiceAreas)
	if erro != nil {
		return nil, erro
	}
	return arrServiceAreas, nil
}

func (wp *ServiceAreaCollection) GetServiceAreaByExternalId(ctx context.Context, id string) (*models_v2.ServiceArea, error) {

	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models_v2.ServiceArea{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *ServiceAreaCollection) GetServiceAreaById(ctx context.Context, id string) (*models_v2.ServiceArea, error) {

	filter := bson.M{"_id": id}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models_v2.ServiceArea{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *ServiceAreaCollection) DeleteServiceAreaByExternalId(ctx context.Context, id string) (bool, error) {
	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	_, err := wp.Collection.DeleteOne(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}
