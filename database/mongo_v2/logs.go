package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type LogsCollection struct {
	Collection *mongo.Collection
}

func GetLogsCollection() *LogsCollection {
	db := database.MiddlewareDB
	logs := db.Collection("log.cdc.logs")
	return &LogsCollection{Collection: logs}
}

func (wp *LogsCollection) AddLogs(ctx context.Context, value *models.Log) (*models.Log, error) {
	value.FromSystem = "MySQL"
	value.ToSystem = "MongoDB"
	value.CreatedDate = time.Now()
	value.LastUpdatedDate = time.Now()

	if len(value.ID) == 0 {
		value.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, value)
	if err != nil {
		return nil, err
	}
	return value, nil
}
func (wp *LogsCollection) UpdateLogs(ctx context.Context, id string, log *models.Log) (bool, error) {

	log.LastUpdatedDate = time.Now()

	filter := bson.M{"_id": id}
	operation := bson.M{"status": log.Status, "body": log.Body, "operation": log.Operation, "lastUpdatedDate": log.LastUpdatedDate}
	if log.Error != "" {
		operation["error"] = log.Error
	}
	if log.Collection != "" {
		operation["collection"] = log.Collection
	}
	update := bson.M{"$set": operation}

	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil

}
