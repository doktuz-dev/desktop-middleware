package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type DiagnosticCollection struct {
	Collection *mongo.Collection
}

func GetDiagnosticCollection() *DiagnosticCollection {
	db := database.MiddlewareDB
	test := db.Collection("transactions.attentions.exams.diagnostics")
	return &DiagnosticCollection{Collection: test}
}

func (wp *DiagnosticCollection) AddDiagnostic(ctx context.Context, test *models.Diagnostic) (*models.Diagnostic, error) {
	test.CreatedDate = time.Now()
	test.LastUpdatedDate = time.Now()
	test.ExternalSystem = "MEDIWEB"
	if len(test.ID) == 0 {
		test.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, test)
	if err != nil {
		return nil, err
	}
	return test, nil
}

func (wp *DiagnosticCollection) UpdateDiagnostic(ctx context.Context, id string, test *models.Diagnostic) (bool, error) {
	test.LastUpdatedDate = time.Now()
	test.ExternalSystem = "MEDIWEB"
	filter := bson.M{"_id": id}

	vp := reflect.New(reflect.TypeOf(test))
	vp.Elem().Set(reflect.ValueOf(test))
	v := vp.Interface()
	update := bson.M{"$set": v}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *DiagnosticCollection) GetDiagnostics(ctx context.Context) ([]*models.Diagnostic, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrDiagnostics := []*models.Diagnostic{}
	erro := res.All(ctx, &arrDiagnostics)
	if erro != nil {
		return nil, erro
	}
	return arrDiagnostics, nil
}

func (wp *DiagnosticCollection) DeleteDiagnostics(ctx context.Context, attentionId string, serviceAreaId string, examId string) (bool, error) {
	filter := bson.M{"attentionId": attentionId, "serviceAreaId": serviceAreaId, "examId": examId, "externalSystem": "MEDIWEB"}
	_, err := wp.Collection.DeleteMany(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *DiagnosticCollection) AddManyDiagnostics(ctx context.Context, test []interface{}) ([]interface{}, error) {
	result, err := wp.Collection.InsertMany(ctx, test)
	if err != nil {
		return nil, err
	}
	return result.InsertedIDs, nil
}
