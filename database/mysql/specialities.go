package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetSpecialities(ctx context.Context) ([]*models.Speciality, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from especialidad_inter")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var specialities []*models.Speciality

	for results.Next() {
		var speciality models.Speciality

		err := results.Scan(&speciality.ExternalId, &speciality.Name, &speciality.Status)
		if err != nil {
			return nil, err
		}

		specialities = append(specialities, &speciality)
	}
	return specialities, nil
}
