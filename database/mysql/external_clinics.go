package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetExternalClinics(ctx context.Context) ([]*models.ExternalClinic, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select idclinica, descripcion, estado from clinica")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var examTypes []*models.ExternalClinic

	for results.Next() {
		var examType models.ExternalClinic

		err := results.Scan(&examType.Code, &examType.Name, &examType.Status)
		if err != nil {
			return nil, err
		}

		examTypes = append(examTypes, &examType)
	}
	return examTypes, nil
}
