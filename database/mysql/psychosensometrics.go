package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetPsychosensometrics(ctx context.Context) ([]*models.Psychosensometric, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from psychosensometrics")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var tests []*models.Psychosensometric

	for results.Next() {
		var test models.Psychosensometric

		err := results.Scan(
			&test.ID,
			&test.AttentionID,
			&test.ServiceAreaID,
			&test.Result,
			&test.Status,
		)
		if err != nil {
			return nil, err
		}

		tests = append(tests, &test)
	}
	return tests, nil
}
