package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetLaboratorys(ctx context.Context) ([]*models.Laboratory, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from laboratories")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var tests []*models.Laboratory

	for results.Next() {
		var test models.Laboratory

		err := results.Scan(
			&test.ID,
			&test.AttentionId,
			&test.ServiceAreaID,
			&test.Diagnostic1,
			&test.Diagnostic2,
			&test.Diagnostic3,
			&test.Diagnostic4,
			&test.Diagnostic5,
			&test.Status,
			&test.Reviewed,
		)
		if err != nil {
			return nil, err
		}

		tests = append(tests, &test)
	}
	return tests, nil
}
