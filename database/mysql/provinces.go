package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetProvinces(ctx context.Context) ([]*models.Province, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select idprovincia, nombre, iddepartamento from dbmediweb.gralprovincias")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var provinces []*models.Province

	for results.Next() {
		var province models.Province

		err := results.Scan(&province.ExternalId, &province.Name, &province.DepartmentID)
		if err != nil {
			return nil, err
		}

		provinces = append(provinces, &province)
	}
	return provinces, nil
}
