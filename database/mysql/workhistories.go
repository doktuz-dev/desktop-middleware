package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
	"fmt"
	"strings"
)

func GetWorkHistorys(ctx context.Context) ([]*models.WorkHistory, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from workhistories")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var workHistorys []*models.WorkHistory

	for results.Next() {
		var workHistory models.WorkHistory
		var dangers models.Dangers
		var eppTypes models.EppType
		file := ""

		err := results.Scan(
			&workHistory.ExternalId,
			&workHistory.AttentionId,
			&workHistory.PatientId,
			&file,
			&workHistory.Company,
			&workHistory.JobTitle,
			&dangers.Dust,
			&dangers.Noise,
			&dangers.RepetitiveMovement,
			&dangers.SegmentalVibration,
			&dangers.HeavyMetals,
			&dangers.Ergonomic,
			&dangers.TotalVibrations,
			&dangers.Carcinogenic,
			&dangers.Mutagenic,
			&dangers.Shifts,
			&dangers.Disergonomic,
			&dangers.Positions,
			&dangers.Falls,
			&dangers.Solvent,
			&dangers.NightShifts,
			&dangers.HighTemperature,
			&dangers.Biological,
			&dangers.Loads,
			&dangers.Pvd,
			&dangers.Chemical,
			&dangers.Others,
			&eppTypes.Gloves,
			&eppTypes.Earplug,
			&eppTypes.Earmuffs,
			&eppTypes.Helmet,
			&eppTypes.Mask,
			&eppTypes.Boots,
			&eppTypes.Harness,
			&eppTypes.Glasses,
			&eppTypes.Clothes,
			&eppTypes.Respirators,
			&eppTypes.Others,
			&workHistory.SequenceNumber,
			&workHistory.Status,
		)
		if err != nil {
			return nil, err
		}

		if file != "" {
			img := strings.Split(file, ".")
			workHistory.Filename = append(workHistory.Filename, fmt.Sprintf("1.%s", img[1]))
		}

		workHistory.Dangers = &dangers
		workHistory.EppType = &eppTypes

		workHistorys = append(workHistorys, &workHistory)
	}
	return workHistorys, nil
}
