package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetHealthCentres(ctx context.Context) ([]*models.HealthCentre, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select idlocal, descripcion, estado from dbmediweb.local")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var healthCentres []*models.HealthCentre

	for results.Next() {
		var healthCentre models.HealthCentre

		err := results.Scan(&healthCentre.ExternalId, &healthCentre.Name, &healthCentre.Status)
		if err != nil {
			return nil, err
		}

		healthCentres = append(healthCentres, &healthCentre)
	}
	return healthCentres, nil
}
