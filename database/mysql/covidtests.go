package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetCovidTests(ctx context.Context) ([]*models.CovidTest, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from covidtests")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var tests []*models.CovidTest

	for results.Next() {
		var test models.CovidTest

		err := results.Scan(
			&test.ExternalId,
			&test.AttentionId,
			&test.ServiceAreaID,
			&test.SerologicalTestIgmResult,
			&test.SerologicalTestIggResult,
			&test.MolecularTestResult,
			&test.QuantitativeAntigenTestResult,
			&test.EliseTestIgaResult,
			&test.EliseTestIggResult,
			&test.QualitativeAntigenTestResult,
			&test.RocheTestResult,
			&test.CovneuTestResult,
			&test.SalivaAntigenTestResult,
			&test.Status,
		)
		if err != nil {
			return nil, err
		}

		tests = append(tests, &test)
	}
	return tests, nil
}
