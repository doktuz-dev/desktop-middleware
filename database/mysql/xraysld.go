package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetXrayLDs(ctx context.Context) ([]*models.XrayLD, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from x_rayld")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var tests []*models.XrayLD

	for results.Next() {
		var test models.XrayLD

		err := results.Scan(
			&test.ID,
			&test.AttentionID,
			&test.ServiceAreaID,
			&test.Description1,
			&test.Description2,
			&test.Description3,
			&test.Description4,
			&test.Description5,
			&test.Description6,
			&test.Description7,
			&test.Description8,
			&test.Description9,
			&test.Description10,
			&test.Description11,
			&test.Description12,
			&test.Description13,
			&test.Description14,
			&test.Description15,
			&test.Result,
			&test.Diagnostic1,
			&test.Diagnostic2,
			&test.Diagnostic3,
			&test.Status,
		)
		if err != nil {
			return nil, err
		}

		tests = append(tests, &test)
	}
	return tests, nil
}
