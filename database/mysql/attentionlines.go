package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetAttentionLines(ctx context.Context) ([]*models.AttentionLine, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select iddet_comprobante, idcomprobante, idexamenocu, estado from det_comprobante")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var attentionLines []*models.AttentionLine

	for results.Next() {
		var attentionLine models.AttentionLine

		err := results.Scan(
			&attentionLine.ExternalId,
			&attentionLine.AttentionId,
			&attentionLine.ServiceID,
			&attentionLine.Status,
		)
		if err != nil {
			return nil, err
		}

		attentionLines = append(attentionLines, &attentionLine)
	}
	return attentionLines, nil
}
