package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetWorkStations(ctx context.Context) ([]*models.WorkStation, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select idproyecto, descripcion, estado, idempresa from proyecto")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var workStations []*models.WorkStation

	for results.Next() {
		var workStation models.WorkStation

		err := results.Scan(&workStation.ExternalId, &workStation.Name, &workStation.Status, &workStation.CustomerId)
		if err != nil {
			return nil, err
		}

		workStations = append(workStations, &workStation)
	}
	return workStations, nil
}
