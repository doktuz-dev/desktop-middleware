package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetStressTests(ctx context.Context) ([]*models.StressTest, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from stresstests")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var tests []*models.StressTest

	for results.Next() {
		var test models.StressTest

		err := results.Scan(
			&test.ID,
			&test.AttentionID,
			&test.ServiceAreaID,
			&test.Result,
			&test.Status,
		)
		if err != nil {
			return nil, err
		}

		tests = append(tests, &test)
	}
	return tests, nil
}
