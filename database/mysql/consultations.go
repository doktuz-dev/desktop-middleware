package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetConsultations(ctx context.Context) ([]*models.Consultation, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select iddet_interconsulta, idcomprobante, iddet_interconsulta printId, respondido, estado, idespecialidad from det_interconsulta")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var consultations []*models.Consultation

	for results.Next() {
		var consultation models.Consultation

		err := results.Scan(
			&consultation.ExternalId,
			&consultation.AttentionId,
			&consultation.PrintId,
			&consultation.Replied,
			&consultation.Status,
			&consultation.SpecialityID,
		)
		if err != nil {
			return nil, err
		}

		consultations = append(consultations, &consultation)
	}
	return consultations, nil
}
