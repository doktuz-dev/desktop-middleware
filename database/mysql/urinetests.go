package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetUrineTests(ctx context.Context) ([]*models.UrineTest, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from urinetests")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var tests []*models.UrineTest

	for results.Next() {
		var test models.UrineTest

		err := results.Scan(
			&test.ExternalId,
			&test.AttentionId,
			&test.ServiceAreaID,
			&test.Result,
			&test.Status,
		)
		if err != nil {
			return nil, err
		}

		tests = append(tests, &test)
	}
	return tests, nil
}
