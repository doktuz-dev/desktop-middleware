package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetMedicalHistorys(ctx context.Context) ([]*models.MedicalHistory, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from medicalhistories")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var workHistorys []*models.MedicalHistory

	for results.Next() {
		var workHistory models.MedicalHistory

		err := results.Scan(
			&workHistory.ExternalId,
			&workHistory.AttentionId,
			&workHistory.Anamnesis,
			&workHistory.Skin,
			&workHistory.Head,
			&workHistory.Neck,
			&workHistory.Nose,
			&workHistory.Mouth,
			&workHistory.Pharynx,
			&workHistory.Larynx,
			&workHistory.Heart,
			&workHistory.Lungs,
			&workHistory.Belly,
			&workHistory.Miemsd,
			&workHistory.Miemid,
			&workHistory.Backbone,
			&workHistory.Breast,
			&workHistory.Genitals,
			&workHistory.Glands,
			&workHistory.March,
			&workHistory.OsteotendinousReflexes,
			&workHistory.Language,
			&workHistory.Hernia,
			&workHistory.VaricoseVein,
			&workHistory.NervousSistem,
			&workHistory.LymphaticSistem,
			&workHistory.Status,
		)
		if err != nil {
			return nil, err
		}

		workHistorys = append(workHistorys, &workHistory)
	}
	return workHistorys, nil
}
