package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetBiochemistryTests(ctx context.Context) ([]*models.BiochemistryTest, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from biochemistrytests")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var tests []*models.BiochemistryTest

	for results.Next() {
		var test models.BiochemistryTest

		err := results.Scan(
			&test.ExternalId,
			&test.AttentionId,
			&test.ServiceAreaID,
			&test.TgoCount,
			&test.TgpCount,
			&test.UreaCount,
			&test.CreatinineCount,
			&test.GlucoseCount,
			&test.CholesterolCount,
			&test.TriglycerideCount,
			&test.HdlCount,
			&test.LdlCount,
			&test.Status,
		)
		if err != nil {
			return nil, err
		}

		tests = append(tests, &test)
	}
	return tests, nil
}
