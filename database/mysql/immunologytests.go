package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetImmunologyTests(ctx context.Context) ([]*models.ImmunologyTest, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from immunologytests")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var tests []*models.ImmunologyTest

	for results.Next() {
		var test models.ImmunologyTest

		err := results.Scan(
			&test.ExternalId,
			&test.AttentionId,
			&test.ServiceAreaID,
			&test.HaigmResult,
			&test.HaiggResult,
			&test.HbsagResult,
			&test.HepatitisCResult,
			&test.TificooResult,
			&test.TificohCResult,
			&test.ParatificoaResult,
			&test.ParatificobResult,
			&test.BrucellasSspResult,
			&test.PsaResult,
			&test.VdrlResult,
			&test.RprResult,
			&test.Status,
		)
		if err != nil {
			return nil, err
		}

		tests = append(tests, &test)
	}
	return tests, nil
}
