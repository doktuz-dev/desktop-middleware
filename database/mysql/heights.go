package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetHeights(ctx context.Context) ([]*models.Height, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from heights")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var tests []*models.Height

	for results.Next() {
		var test models.Height

		err := results.Scan(
			&test.ID,
			&test.AttentionID,
			&test.ServiceAreaID,
			&test.Result,
			&test.Status,
		)
		if err != nil {
			return nil, err
		}

		tests = append(tests, &test)
	}
	return tests, nil
}
