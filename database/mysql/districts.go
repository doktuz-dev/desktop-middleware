package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetDistricts(ctx context.Context) ([]*models.District, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select iddistrito, nombre, idprovincia, iddepartamento from dbmediweb.graldistritos")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var districts []*models.District

	for results.Next() {
		var district models.District

		err := results.Scan(&district.ExternalId, &district.Name, &district.ProvinceID, &district.DepartmentID)
		if err != nil {
			return nil, err
		}

		districts = append(districts, &district)
	}
	return districts, nil
}
