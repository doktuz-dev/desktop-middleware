package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"

	"github.com/araddon/dateparse"
)

func GetEvaluations(ctx context.Context) ([]*models.Evaluation, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from nps")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var evaluations []*models.Evaluation

	for results.Next() {
		var evaluation models.Evaluation

		evaluationDate := ""

		err := results.Scan(
			&evaluation.ExternalId,
			&evaluation.AttentionId,
			&evaluationDate,
			&evaluation.Result,
			&evaluation.Comments,
			&evaluation.WasRejected,
		)
		if err != nil {
			return nil, err
		}
		evaluationDateTime, _ := dateparse.ParseLocal(evaluationDate)

		evaluation.EvaluationDate = evaluationDateTime

		evaluations = append(evaluations, &evaluation)
	}
	return evaluations, nil
}
