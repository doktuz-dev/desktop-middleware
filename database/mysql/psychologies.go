package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetPsychologys(ctx context.Context) ([]*models.Psychology, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from psychologies")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var tests []*models.Psychology

	for results.Next() {
		var test models.Psychology

		err := results.Scan(
			&test.ID,
			&test.AttentionID,
			&test.ServiceAreaID,
			&test.Result,
			&test.AcrophobiaTest,
			&test.ClaustrophobiaTest,
			&test.FatigueTest,
			&test.Recommendation1,
			&test.Recommendation2,
			&test.Recommendation3,
			&test.Status,
		)
		if err != nil {
			return nil, err
		}

		tests = append(tests, &test)
	}
	return tests, nil
}
