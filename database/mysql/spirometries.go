package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetSpirometries(ctx context.Context) ([]*models.Spirometry, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from spirometries")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var tests []*models.Spirometry

	for results.Next() {
		var test models.Spirometry

		err := results.Scan(
			&test.ID,
			&test.AttentionID,
			&test.ServiceAreaID,
			&test.FileName1,
			&test.Result,
			&test.Quality,
			&test.Diagnostic1,
			&test.Diagnostic2,
			&test.Diagnostic3,
			&test.Status,
		)
		if err != nil {
			return nil, err
		}

		tests = append(tests, &test)
	}
	return tests, nil
}
