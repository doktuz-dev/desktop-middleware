package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetExamResults(ctx context.Context) ([]*models.ExamResult, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from tests")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var tests []*models.ExamResult

	for results.Next() {
		var test models.ExamResult

		err := results.Scan(
			&test.ExternalId,
			&test.AttentionId,
			&test.ServiceAreaID,
			&test.Filename,
			&test.PrintId,
			&test.Reviewed,
			&test.Status,
		)
		if err != nil {
			return nil, err
		}

		tests = append(tests, &test)
	}
	return tests, nil
}
