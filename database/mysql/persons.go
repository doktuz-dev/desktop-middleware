package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"

	"github.com/araddon/dateparse"
)

func GetPersons(ctx context.Context) ([]*models.Person, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select idpaciente, ifnull(estadocivil, ''), ifnull(gradoins, ''), dni2, idtipodocumento, nombres, apellidos, ifnull(sexo, ''), ifnull(correo, ''), ifnull(fechanacimiento, ''), ifnull(telefono, ''), ifnull(iddistrito, '') from paciente")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var persons []*models.Person

	for results.Next() {
		var person models.Person

		birthDate := ""

		err := results.Scan(&person.ID, &person.MaritalStatusId, &person.DegreeInstructionId, &person.DocumentNumber, &person.DocumentTypeID, &person.FirstName, &person.LastName, &person.Sex, &person.Email, &birthDate, &person.PhoneNumber, &person.DistrictId)
		if err != nil {
			return nil, err
		}

		birthDateDa, _ := dateparse.ParseLocal(birthDate)

		person.BirthDate = birthDateDa

		persons = append(persons, &person)
	}
	return persons, nil
}
