package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetOphtalmologys(ctx context.Context) ([]*models.Ophtalmology, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from ophtalmologies")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var tests []*models.Ophtalmology

	for results.Next() {
		var test models.Ophtalmology
		var distanceNoCorrector models.VisualAcuity
		var closeNoCorrector models.VisualAcuity
		var distanceWithCorrector models.VisualAcuity
		var closeWithCorrector models.VisualAcuity

		err := results.Scan(
			&test.ID,
			&test.AttentionId,
			&test.ServiceAreaID,
			&distanceNoCorrector.Right,
			&distanceNoCorrector.Left,
			&closeNoCorrector.Right,
			&closeNoCorrector.Left,
			&distanceWithCorrector.Right,
			&distanceWithCorrector.Left,
			&closeWithCorrector.Right,
			&closeWithCorrector.Left,
			&test.Stereopsis,
			&test.Diagnostic1,
			&test.Diagnostic2,
			&test.Diagnostic3,
			&test.Diagnostic4,
			&test.Diagnostic5,
			&test.Status,
		)
		if err != nil {
			return nil, err
		}

		test.CloseNoCorrector = &closeNoCorrector
		test.CloseWithCorrector = &closeWithCorrector
		test.DistanceNoCorrector = &distanceNoCorrector
		test.DistanceWithCorrector = &distanceWithCorrector

		tests = append(tests, &test)
	}
	return tests, nil
}
