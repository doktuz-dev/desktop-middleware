package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetHematologyTests(ctx context.Context) ([]*models.HematologyTest, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from hematologytests")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var tests []*models.HematologyTest

	for results.Next() {
		var test models.HematologyTest

		err := results.Scan(
			&test.ExternalId,
			&test.AttentionId,
			&test.ServiceAreaID,
			&test.BloodCountResult,
			&test.LeukocyteCount,
			&test.HemoglobinCount,
			&test.HematocritCount,
			&test.RedCellsCount,
			&test.BandNeutrophilsCount,
			&test.SegmentedCount,
			&test.EosinophilCount,
			&test.BasophilsCount,
			&test.MonocytesCount,
			&test.LymphocytesCount,
			&test.VcmCount,
			&test.HcmCount,
			&test.PlateletsCount,
			&test.BloodType,
			&test.BloodFactor,
			&test.Status,
		)
		if err != nil {
			return nil, err
		}

		tests = append(tests, &test)
	}
	return tests, nil
}
