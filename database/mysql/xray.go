package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetXrays(ctx context.Context) ([]*models.Xray, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from x_ray")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var tests []*models.Xray

	for results.Next() {
		var test models.Xray

		err := results.Scan(
			&test.ID,
			&test.AttentionID,
			&test.ServiceAreaID,
			&test.Description1,
			&test.Description2,
			&test.Description3,
			&test.Conclusion,
			&test.Diagnostic1,
			&test.Diagnostic2,
			&test.Diagnostic3,
			&test.Status,
		)
		if err != nil {
			return nil, err
		}

		tests = append(tests, &test)
	}
	return tests, nil
}
