package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetCustomers(ctx context.Context) ([]*models.Customer, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select idempresa, ruc, descripcion, estado, direccion, telefono from dbmediweb.empresa")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var companies []*models.Customer

	for results.Next() {
		var company models.Customer

		err := results.Scan(&company.ExternalId, &company.Code, &company.Name, &company.Status, &company.Address, &company.PhoneNumber)
		if err != nil {
			return nil, err
		}

		companies = append(companies, &company)
	}
	return companies, nil
}
