package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetConfinedSpaces(ctx context.Context) ([]*models.ConfinedSpace, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from confinedspaces")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var tests []*models.ConfinedSpace

	for results.Next() {
		var test models.ConfinedSpace

		err := results.Scan(
			&test.ID,
			&test.AttentionID,
			&test.ServiceAreaID,
			&test.Result,
			&test.Status,
		)
		if err != nil {
			return nil, err
		}

		tests = append(tests, &test)
	}
	return tests, nil
}
