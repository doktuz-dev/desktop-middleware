package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetServiceAreas(ctx context.Context) ([]*models.ServiceArea, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from servicearea")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var serviceAreas []*models.ServiceArea

	for results.Next() {
		var serviceArea models.ServiceArea

		err := results.Scan(&serviceArea.ExternalId, &serviceArea.Name, &serviceArea.Description, &serviceArea.Status)
		if err != nil {
			return nil, err
		}

		serviceAreas = append(serviceAreas, &serviceArea)
	}
	return serviceAreas, nil
}
