package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetTriages(ctx context.Context) ([]*models.Triage, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from triages")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var triages []*models.Triage

	for results.Next() {
		var triage models.Triage

		err := results.Scan(
			&triage.ExternalId,
			&triage.AttentionId,
			&triage.SystolicPressure,
			&triage.DiastolicPressure,
			&triage.HeartRate,
			&triage.RespiratoryRate,
			&triage.Weight,
			&triage.Height,
			&triage.Bmi,
			&triage.Abdominal,
			&triage.Status,
		)
		if err != nil {
			return nil, err
		}

		triages = append(triages, &triage)
	}
	return triages, nil
}
