package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetDermatologys(ctx context.Context) ([]*models.Dermatology, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from dermatologies")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var tests []*models.Dermatology

	for results.Next() {
		var test models.Dermatology

		err := results.Scan(
			&test.ID,
			&test.AttentionID,
			&test.ServiceAreaID,
			&test.Diagnostic1,
			&test.Diagnostic2,
			&test.Diagnostic3,
			&test.Status,
		)
		if err != nil {
			return nil, err
		}

		tests = append(tests, &test)
	}
	return tests, nil
}
