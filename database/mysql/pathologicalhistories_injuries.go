package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetPathologicalHistoryInjuriess(ctx context.Context) ([]*models.PathologicalHistoryInjuries, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from pathologicalhistories_injuries")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var pathologicalHistorys []*models.PathologicalHistoryInjuries

	for results.Next() {
		var pathologicalHistory models.PathologicalHistoryInjuries

		err := results.Scan(
			&pathologicalHistory.ExternalId,
			&pathologicalHistory.AttentionId,
			&pathologicalHistory.Description,
			&pathologicalHistory.RelatedWithWork,
			&pathologicalHistory.Year,
			&pathologicalHistory.RestDays,
			&pathologicalHistory.SequenceNumber,
			&pathologicalHistory.Status,
		)
		if err != nil {
			return nil, err
		}

		pathologicalHistorys = append(pathologicalHistorys, &pathologicalHistory)
	}
	return pathologicalHistorys, nil
}
