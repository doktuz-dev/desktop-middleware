package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetDrugTests(ctx context.Context) ([]*models.DrugTest, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from drugstests")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var tests []*models.DrugTest

	for results.Next() {
		var test models.DrugTest

		err := results.Scan(
			&test.ExternalId,
			&test.AttentionId,
			&test.ServiceAreaID,
			&test.CocaineResult,
			&test.MarijuanaResult,
			&test.AmphetamineResult,
			&test.MethamphetamineResult,
			&test.BenzodiazepineResult,
			&test.EcstasyResult,
			&test.AlcoholResult,
			&test.LeadResult,
			&test.Status,
		)
		if err != nil {
			return nil, err
		}

		tests = append(tests, &test)
	}
	return tests, nil
}
