package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetNeurologys(ctx context.Context) ([]*models.Neurology, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from neurologies")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var tests []*models.Neurology

	for results.Next() {
		var test models.Neurology

		err := results.Scan(
			&test.ID,
			&test.AttentionID,
			&test.ServiceAreaID,
			&test.Diagnostic1,
			&test.Diagnostic2,
			&test.Diagnostic3,
			&test.Status,
		)
		if err != nil {
			return nil, err
		}

		tests = append(tests, &test)
	}
	return tests, nil
}
