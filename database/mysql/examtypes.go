package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetExamTypes(ctx context.Context) ([]*models.ExamType, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select idtipo, descripcion, estado from tipo_examen")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var examTypes []*models.ExamType

	for results.Next() {
		var examType models.ExamType

		err := results.Scan(&examType.ExternalId, &examType.Name, &examType.Status)
		if err != nil {
			return nil, err
		}

		examTypes = append(examTypes, &examType)
	}
	return examTypes, nil
}
