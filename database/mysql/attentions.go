package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"

	"github.com/araddon/dateparse"
)

func GetAttentions(ctx context.Context) ([]*models.Attention, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from attentions")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var attentions []*models.Attention

	for results.Next() {
		var attention models.Attention

		attentionDate := ""
		mailSentDateTime := ""
		mailOpenedDateTime := ""

		err := results.Scan(
			&attention.ExternalId,
			&attention.WorkStationId,
			&attention.WorkProfileId,
			&attention.ExamTypeId,
			&attention.WorkingArea,
			&attention.JobTitle,
			&attention.HealthCentreId,
			&attention.PatientId,
			&attention.CustomerId,
			&attention.AttentionTypeId,
			&attention.DeliveryTypeId,
			&attention.DepartmentId,
			&attention.DistrictId,
			&attentionDate,
			&attention.MailStatus,
			&mailSentDateTime,
			&mailOpenedDateTime,
			&attention.Status,
			&attention.WasAttended,
		)
		if err != nil {
			return nil, err
		}
		attentionDateTime, _ := dateparse.ParseLocal(attentionDate)
		mailSentDateTimeP, _ := dateparse.ParseLocal(mailSentDateTime)
		mailOpenedDateTimeP, _ := dateparse.ParseLocal(mailOpenedDateTime)

		attention.AttentionDate = attentionDateTime
		attention.MailSentDateTime = &mailSentDateTimeP
		attention.MailOpenedDateTime = &mailOpenedDateTimeP

		attentions = append(attentions, &attention)
	}
	return attentions, nil
}
