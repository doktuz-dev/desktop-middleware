package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetMicrobiologyTests(ctx context.Context) ([]*models.MicrobiologyTest, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from microbiologytests")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var tests []*models.MicrobiologyTest

	for results.Next() {
		var test models.MicrobiologyTest
		var sputum1 string
		var sputum2 string
		var sputum3 string
		var parasite1 string
		var parasite2 string
		var parasite3 string

		err := results.Scan(
			&test.ExternalId,
			&test.AttentionId,
			&test.ServiceAreaID,
			&sputum1,
			&sputum2,
			&sputum3,
			&parasite1,
			&parasite2,
			&parasite3,
			&test.StoolCultureResult,
			&test.UrineCultureResult,
			&test.PharyngealSecretionCultureResult,
			&test.Status,
		)
		if err != nil {
			return nil, err
		}
		test.SputumSampleResult = []string{}
		test.ParasiteResult = []string{}
		if sputum1 != "" {
			test.SputumSampleResult = append(test.SputumSampleResult, sputum1)
		}
		if sputum2 != "" {
			test.SputumSampleResult = append(test.SputumSampleResult, sputum2)
		}
		if sputum3 != "" {
			test.SputumSampleResult = append(test.SputumSampleResult, sputum1)
		}
		if parasite1 != "" {
			test.ParasiteResult = append(test.ParasiteResult, parasite1)
		}
		if parasite2 != "" {
			test.ParasiteResult = append(test.ParasiteResult, parasite2)
		}
		if parasite3 != "" {
			test.ParasiteResult = append(test.ParasiteResult, parasite3)
		}

		tests = append(tests, &test)
	}
	return tests, nil
}
