package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetDepartments(ctx context.Context) ([]*models.Department, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select iddepartamento, nombre from dbmediweb.graldepartamentos")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var departments []*models.Department

	for results.Next() {
		var department models.Department

		err := results.Scan(&department.ExternalId, &department.Name)
		if err != nil {
			return nil, err
		}

		departments = append(departments, &department)
	}
	return departments, nil
}
