package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetSkeletalMuscles(ctx context.Context) ([]*models.SkeletalMuscle, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from skeletal_muscle")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var tests []*models.SkeletalMuscle

	for results.Next() {
		var test models.SkeletalMuscle

		err := results.Scan(
			&test.ID,
			&test.AttentionID,
			&test.ServiceAreaID,
			&test.Diagnostic1,
			&test.Diagnostic2,
			&test.Diagnostic3,
			&test.Diagnostic4,
			&test.Diagnostic5,
			&test.Diagnostic6,
			&test.Diagnostic7,
			&test.Diagnostic8,
			&test.Diagnostic9,
			&test.Diagnostic10,
			&test.Diagnostic11,
			&test.Status,
		)
		if err != nil {
			return nil, err
		}

		tests = append(tests, &test)
	}
	return tests, nil
}
