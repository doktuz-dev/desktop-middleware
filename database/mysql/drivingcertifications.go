package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetDrivingCertifications(ctx context.Context) ([]*models.DrivingCertification, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from drivingcertifications")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var tests []*models.DrivingCertification

	for results.Next() {
		var test models.DrivingCertification

		err := results.Scan(
			&test.ID,
			&test.AttentionID,
			&test.ServiceAreaID,
			&test.Result,
			&test.Status,
		)
		if err != nil {
			return nil, err
		}

		tests = append(tests, &test)
	}
	return tests, nil
}
