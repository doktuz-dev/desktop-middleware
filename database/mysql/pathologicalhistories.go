package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetPathologicalHistorys(ctx context.Context) ([]*models.PathologicalHistory, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from pathologicalhistories")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var pathologicalHistorys []*models.PathologicalHistory

	for results.Next() {
		var pathologicalHistory models.PathologicalHistory
		var fatherDisease models.PathologicalDisease
		var motherDisease models.PathologicalDisease
		var brotherDisease models.PathologicalDisease
		var grandParentDisease models.PathologicalDisease
		var spouseDisease models.PathologicalDisease
		var cigarrete models.PathologicalConsume
		var alcohol models.PathologicalConsume
		var drug models.PathologicalConsume
		var vaccines models.Vaccines

		err := results.Scan(
			&pathologicalHistory.ExternalId,
			&pathologicalHistory.AttentionId,
			&pathologicalHistory.MedicalHistoryDescription,
			&pathologicalHistory.MedicalHistoryAntecedent,
			&fatherDisease.HTA,
			&fatherDisease.DM,
			&fatherDisease.Cancer,
			&fatherDisease.CancerDescription,
			&fatherDisease.RheumatoidDisease,
			&fatherDisease.TBC,
			&fatherDisease.NR,
			&fatherDisease.Other,
			&fatherDisease.OtherDescription,
			&motherDisease.HTA,
			&motherDisease.DM,
			&motherDisease.Cancer,
			&motherDisease.CancerDescription,
			&motherDisease.RheumatoidDisease,
			&motherDisease.TBC,
			&motherDisease.NR,
			&motherDisease.Other,
			&motherDisease.OtherDescription,
			&brotherDisease.HTA,
			&brotherDisease.DM,
			&brotherDisease.Cancer,
			&brotherDisease.CancerDescription,
			&brotherDisease.RheumatoidDisease,
			&brotherDisease.TBC,
			&brotherDisease.NR,
			&brotherDisease.Other,
			&brotherDisease.OtherDescription,
			&grandParentDisease.HTA,
			&grandParentDisease.DM,
			&grandParentDisease.Cancer,
			&grandParentDisease.CancerDescription,
			&grandParentDisease.RheumatoidDisease,
			&grandParentDisease.TBC,
			&grandParentDisease.NR,
			&grandParentDisease.Other,
			&grandParentDisease.OtherDescription,
			&spouseDisease.HTA,
			&spouseDisease.DM,
			&spouseDisease.Cancer,
			&spouseDisease.CancerDescription,
			&spouseDisease.RheumatoidDisease,
			&spouseDisease.TBC,
			&spouseDisease.NR,
			&spouseDisease.Other,
			&spouseDisease.OtherDescription,
			&pathologicalHistory.Alergies,
			&pathologicalHistory.AlergiesDescription,
			&alcohol.Consume,
			&alcohol.Type,
			&alcohol.Amount,
			&alcohol.ConsumptionLevel,
			&cigarrete.Consume,
			&cigarrete.Type,
			&cigarrete.Amount,
			&cigarrete.ConsumptionLevel,
			&drug.Consume,
			&drug.Type,
			&drug.Amount,
			&drug.ConsumptionLevel,
			&pathologicalHistory.CurrentMedicine,
			&pathologicalHistory.JobTitle,
			&vaccines.Tetanus,
			&vaccines.YellowFever,
			&vaccines.Flu,
			&vaccines.Hepatitisb,
			&vaccines.Mmr,
			&vaccines.Polio,
			&vaccines.Rabies,
			&vaccines.Influenza,
			&vaccines.Hepatitisa,
			&vaccines.Meningococcus,
			&vaccines.HumanPapilloma,
			&vaccines.Bcg,
			&vaccines.Ppd,
			&vaccines.Infancy,
			&vaccines.Others,
			&pathologicalHistory.Status,
		)
		if err != nil {
			return nil, err
		}

		pathologicalHistory.FatherDisease = &fatherDisease
		pathologicalHistory.MotherDisease = &motherDisease
		pathologicalHistory.BrotherDisease = &brotherDisease
		pathologicalHistory.GrandParentDisease = &grandParentDisease
		pathologicalHistory.SpouseDisease = &spouseDisease
		pathologicalHistory.Cigarrete = &cigarrete
		pathologicalHistory.Alcohol = &alcohol
		pathologicalHistory.Drug = &drug
		pathologicalHistory.Vaccines = &vaccines

		pathologicalHistorys = append(pathologicalHistorys, &pathologicalHistory)
	}
	return pathologicalHistorys, nil
}
