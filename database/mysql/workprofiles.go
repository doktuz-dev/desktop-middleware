package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"

	"github.com/araddon/dateparse"
)

func GetWorkProfiles(ctx context.Context) ([]*models.WorkProfile, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from workprofiles")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var workProfiles []*models.WorkProfile

	for results.Next() {
		var workProfile models.WorkProfile

		date := ""

		err := results.Scan(&workProfile.ExternalId, &workProfile.Name, &workProfile.CustomerId, &workProfile.Status, &date, &workProfile.Version)
		if err != nil {
			return nil, err
		}
		dateTime, _ := dateparse.ParseLocal(date)

		workProfile.EndDate = dateTime

		workProfiles = append(workProfiles, &workProfile)
	}
	return workProfiles, nil
}
