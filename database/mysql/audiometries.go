package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetAudiometrys(ctx context.Context) ([]*models.Audiometry, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from audiometries")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var tests []*models.Audiometry

	for results.Next() {
		var test models.Audiometry
		var rightAir models.Conduction
		var leftAir models.Conduction
		var rightBone models.Conduction
		var leftBone models.Conduction

		err := results.Scan(
			&test.ID,
			&test.AttentionId,
			&test.ServiceAreaID,
			&rightAir.R250,
			&rightAir.R500,
			&rightAir.R1000,
			&rightAir.R2000,
			&rightAir.R3000,
			&rightAir.R4000,
			&rightAir.R6000,
			&rightAir.R8000,
			&leftAir.R250,
			&leftAir.R500,
			&leftAir.R1000,
			&leftAir.R2000,
			&leftAir.R3000,
			&leftAir.R4000,
			&leftAir.R6000,
			&leftAir.R8000,
			&rightBone.R250,
			&rightBone.R500,
			&rightBone.R1000,
			&rightBone.R2000,
			&rightBone.R3000,
			&rightBone.R4000,
			&rightBone.R6000,
			&rightBone.R8000,
			&leftBone.R250,
			&leftBone.R500,
			&leftBone.R1000,
			&leftBone.R2000,
			&leftBone.R3000,
			&leftBone.R4000,
			&leftBone.R6000,
			&leftBone.R8000,
			&test.OtoscopyRight,
			&test.OtoscopyLeft,
			&test.ExposureHour,
			&test.WorkingYears,
			&test.Earplug,
			&test.Earmuffs,
			&test.Result,
			&test.Diagnostic1,
			&test.Diagnostic2,
			&test.Status,
		)
		if err != nil {
			return nil, err
		}

		test.RightAirConduction = &rightAir
		test.LeftAirConduction = &leftAir
		test.RightBoneConduction = &rightBone
		test.LeftBoneConduction = &leftBone

		tests = append(tests, &test)
	}
	return tests, nil
}
