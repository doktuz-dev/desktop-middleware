package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
	"strings"

	"github.com/araddon/dateparse"
)

func GetAptitudes(ctx context.Context) ([]*models.Aptitude, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from aptitudes")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var aptitudes []*models.Aptitude

	for results.Next() {
		var aptitude models.Aptitude
		var restrictions []string
		var recommendations []string

		startDate := ""
		endDate := ""
		var restriction1 string
		var restriction2 string
		var restriction3 string
		var restriction4 string
		var restriction5 string
		var restriction6 string
		var restriction7 string
		var restriction8 string
		var restriction9 string
		var restriction10 string
		var restriction11 string
		var restriction12 string
		var restriction13 string
		var restriction14 string
		var restriction15 string
		var restriction16 string
		var restriction17 string
		var restriction18 string
		var restriction19 string
		var restriction20 string
		var restriction21 string
		var restriction22 string

		var recommendation1 string
		var recommendation2 string
		var recommendation3 string
		var recommendation4 string
		var recommendation5 string
		var recommendation6 string
		var recommendation7 string
		var recommendation8 string
		var recommendation9 string
		var recommendation10 string
		var recommendation11 string
		var recommendation12 string
		var recommendation13 string
		var recommendation14 string
		var recommendation15 string
		var recommendation16 string
		var recommendation17 string
		var recommendation18 string
		var recommendation19 string
		var recommendation20 string
		var recommendation21 string
		var recommendation22 string
		var recommendation23 string
		var recommendation24 string
		var recommendation25 string
		var recommendation26 string
		var recommendation27 string
		var recommendation28 string
		var recommendation29 string
		var recommendation30 string
		var recommendation31 string
		var recommendation32 string
		var recommendation33 string
		var recommendation34 string
		var recommendation35 string
		var recommendation36 string
		var recommendation37 string
		var recommendation38 string
		var recommendation39 string
		var recommendation40 string
		var recommendation41 string
		var recommendation42 string
		var recommendation43 string
		var recommendation44 string
		var recommendation45 string
		var recommendation46 string
		var recommendation47 string
		var recommendation48 string
		var recommendation49 string
		var recommendation50 string
		var recommendation51 string
		var recommendation52 string
		var recommendation53 string
		var recommendation54 string
		var recommendation55 string
		var recommendation56 string
		var recommendation57 string
		var recommendation58 string
		var recommendation59 string
		var recommendation60 string
		var recommendation61 string
		var recommendation62 string
		var recommendation63 string
		var recommendation64 string
		var recommendation65 string
		var recommendation66 string
		var recommendation67 string
		var recommendation68 string
		var recommendation69 string
		var recommendation70 string
		var recommendation71 string
		var recommendation72 string
		var recommendation73 string
		var recommendation74 string
		var recommendation75 string
		var recommendation76 string
		var recommendation77 string
		var recommendation78 string
		var recommendation79 string
		var recommendation80 string
		var recommendation81 string
		var recommendation82 string
		var recommendation83 string
		var recommendation84 string
		var recommendation85 string

		err := results.Scan(
			&aptitude.ExternalId,
			&aptitude.AttentionId,
			&startDate,
			&endDate,
			&aptitude.ResultCode,
			&aptitude.ObservedReason,
			&aptitude.AptitudeDetail,
			&restriction1,
			&restriction2,
			&restriction3,
			&restriction4,
			&restriction5,
			&restriction6,
			&restriction7,
			&restriction8,
			&restriction9,
			&restriction10,
			&restriction11,
			&restriction12,
			&restriction13,
			&restriction14,
			&restriction15,
			&restriction16,
			&restriction17,
			&restriction18,
			&restriction19,
			&restriction20,
			&restriction21,
			&restriction22,
			&aptitude.ResultDescription,
			&aptitude.Reviewed,
			&aptitude.Audited,
			&aptitude.Status,
			&recommendation1,
			&recommendation2,
			&recommendation3,
			&recommendation4,
			&recommendation5,
			&recommendation6,
			&recommendation7,
			&recommendation8,
			&recommendation9,
			&recommendation10,
			&recommendation11,
			&recommendation12,
			&recommendation13,
			&recommendation14,
			&recommendation15,
			&recommendation16,
			&recommendation17,
			&recommendation18,
			&recommendation19,
			&recommendation20,
			&recommendation21,
			&recommendation22,
			&recommendation23,
			&recommendation24,
			&recommendation25,
			&recommendation26,
			&recommendation27,
			&recommendation28,
			&recommendation29,
			&recommendation30,
			&recommendation31,
			&recommendation32,
			&recommendation33,
			&recommendation34,
			&recommendation35,
			&recommendation36,
			&recommendation37,
			&recommendation38,
			&recommendation39,
			&recommendation40,
			&recommendation41,
			&recommendation42,
			&recommendation43,
			&recommendation44,
			&recommendation45,
			&recommendation46,
			&recommendation47,
			&recommendation48,
			&recommendation49,
			&recommendation50,
			&recommendation51,
			&recommendation52,
			&recommendation53,
			&recommendation54,
			&recommendation55,
			&recommendation56,
			&recommendation57,
			&recommendation58,
			&recommendation59,
			&recommendation60,
			&recommendation61,
			&recommendation62,
			&recommendation63,
			&recommendation64,
			&recommendation65,
			&recommendation66,
			&recommendation67,
			&recommendation68,
			&recommendation69,
			&recommendation70,
			&recommendation71,
			&recommendation72,
			&recommendation73,
			&recommendation74,
			&recommendation75,
			&recommendation76,
			&recommendation77,
			&recommendation78,
			&recommendation79,
			&recommendation80,
			&recommendation81,
			&recommendation82,
			&recommendation83,
			&recommendation84,
			&recommendation85,
		)
		if err != nil {
			return nil, err
		}
		startDateP, _ := dateparse.ParseLocal(startDate)
		endDateP, _ := dateparse.ParseLocal(endDate)

		aptitude.StartDate = startDateP
		aptitude.EndDate = endDateP

		if restriction1 != "" {
			restrictions = append(restrictions, restriction1)
		}

		if restriction2 != "" {
			restrictions = append(restrictions, restriction2)
		}

		if restriction3 != "" {
			restrictions = append(restrictions, restriction3)
		}

		if restriction4 != "" {
			restrictions = append(restrictions, restriction4)
		}

		if restriction5 != "" {
			restrictions = append(restrictions, restriction5)
		}

		if restriction6 != "" {
			restrictions = append(restrictions, restriction6)
		}

		if restriction7 != "" {
			restrictions = append(restrictions, restriction7)
		}

		if restriction8 != "" {
			restrictions = append(restrictions, restriction8)
		}

		if restriction9 != "" {
			restrictions = append(restrictions, restriction9)
		}

		if restriction10 != "" {
			restrictions = append(restrictions, restriction10)
		}

		if restriction11 != "" {
			restrictions = append(restrictions, restriction11)
		}

		if restriction12 != "" {
			restrictions = append(restrictions, restriction12)
		}

		if restriction13 != "" {
			restrictions = append(restrictions, restriction13)
		}

		if restriction14 != "" {
			restrictions = append(restrictions, restriction14)
		}

		if restriction15 != "" {
			restrictions = append(restrictions, restriction15)
		}

		if restriction16 != "" {
			restrictions = append(restrictions, restriction16)
		}

		if restriction17 != "" {
			restrictions = append(restrictions, restriction17)
		}

		if restriction18 != "" {
			restrictions = append(restrictions, restriction18)
		}

		if restriction19 != "" {
			restrictions = append(restrictions, restriction19)
		}

		if restriction20 != "" {
			restrictions = append(restrictions, restriction20)
		}

		if restriction21 != "" {
			restrictions = append(restrictions, restriction21)
		}

		if restriction22 != "" {
			restrictions = append(restrictions, restriction22)
		}

		if strings.TrimSpace(recommendation1) != "" {
			recommendations = append(recommendations, recommendation1)
		}

		if strings.TrimSpace(recommendation2) != "" {
			recommendations = append(recommendations, recommendation2)
		}

		if strings.TrimSpace(recommendation3) != "" {
			recommendations = append(recommendations, recommendation3)
		}

		if strings.TrimSpace(recommendation4) != "" {
			recommendations = append(recommendations, recommendation4)
		}

		if strings.TrimSpace(recommendation5) != "" {
			recommendations = append(recommendations, recommendation5)
		}

		if strings.TrimSpace(recommendation6) != "" {
			recommendations = append(recommendations, recommendation6)
		}

		if strings.TrimSpace(recommendation7) != "" {
			recommendations = append(recommendations, recommendation7)
		}

		if strings.TrimSpace(recommendation8) != "" {
			recommendations = append(recommendations, recommendation8)
		}

		if strings.TrimSpace(recommendation9) != "" {
			recommendations = append(recommendations, recommendation9)
		}

		if strings.TrimSpace(recommendation10) != "" {
			recommendations = append(recommendations, recommendation10)
		}

		if strings.TrimSpace(recommendation11) != "" {
			recommendations = append(recommendations, recommendation11)
		}

		if strings.TrimSpace(recommendation12) != "" {
			recommendations = append(recommendations, recommendation12)
		}

		if strings.TrimSpace(recommendation13) != "" {
			recommendations = append(recommendations, recommendation13)
		}

		if strings.TrimSpace(recommendation14) != "" {
			recommendations = append(recommendations, recommendation14)
		}

		if strings.TrimSpace(recommendation15) != "" {
			recommendations = append(recommendations, recommendation15)
		}

		if strings.TrimSpace(recommendation16) != "" {
			recommendations = append(recommendations, recommendation16)
		}

		if strings.TrimSpace(recommendation17) != "" {
			recommendations = append(recommendations, recommendation17)
		}

		if strings.TrimSpace(recommendation18) != "" {
			recommendations = append(recommendations, recommendation18)
		}

		if strings.TrimSpace(recommendation19) != "" {
			recommendations = append(recommendations, recommendation19)
		}

		if strings.TrimSpace(recommendation20) != "" {
			recommendations = append(recommendations, recommendation20)
		}

		if strings.TrimSpace(recommendation21) != "" {
			recommendations = append(recommendations, recommendation21)
		}

		if strings.TrimSpace(recommendation22) != "" {
			recommendations = append(recommendations, recommendation22)
		}

		if strings.TrimSpace(recommendation23) != "" {
			recommendations = append(recommendations, recommendation23)
		}

		if strings.TrimSpace(recommendation24) != "" {
			recommendations = append(recommendations, recommendation24)
		}

		if strings.TrimSpace(recommendation25) != "" {
			recommendations = append(recommendations, recommendation25)
		}

		if strings.TrimSpace(recommendation26) != "" {
			recommendations = append(recommendations, recommendation26)
		}

		if strings.TrimSpace(recommendation27) != "" {
			recommendations = append(recommendations, recommendation27)
		}

		if strings.TrimSpace(recommendation28) != "" {
			recommendations = append(recommendations, recommendation28)
		}

		if strings.TrimSpace(recommendation29) != "" {
			recommendations = append(recommendations, recommendation29)
		}

		if strings.TrimSpace(recommendation30) != "" {
			recommendations = append(recommendations, recommendation30)
		}

		if strings.TrimSpace(recommendation31) != "" {
			recommendations = append(recommendations, recommendation31)
		}

		if strings.TrimSpace(recommendation32) != "" {
			recommendations = append(recommendations, recommendation32)
		}

		if strings.TrimSpace(recommendation33) != "" {
			recommendations = append(recommendations, recommendation33)
		}

		if strings.TrimSpace(recommendation34) != "" {
			recommendations = append(recommendations, recommendation34)
		}

		if strings.TrimSpace(recommendation35) != "" {
			recommendations = append(recommendations, recommendation35)
		}

		if strings.TrimSpace(recommendation36) != "" {
			recommendations = append(recommendations, recommendation36)
		}

		if strings.TrimSpace(recommendation37) != "" {
			recommendations = append(recommendations, recommendation37)
		}

		if strings.TrimSpace(recommendation38) != "" {
			recommendations = append(recommendations, recommendation38)
		}

		if strings.TrimSpace(recommendation39) != "" {
			recommendations = append(recommendations, recommendation39)
		}

		if strings.TrimSpace(recommendation40) != "" {
			recommendations = append(recommendations, recommendation40)
		}

		if strings.TrimSpace(recommendation41) != "" {
			recommendations = append(recommendations, recommendation41)
		}

		if strings.TrimSpace(recommendation42) != "" {
			recommendations = append(recommendations, recommendation42)
		}

		if strings.TrimSpace(recommendation43) != "" {
			recommendations = append(recommendations, recommendation43)
		}

		if strings.TrimSpace(recommendation44) != "" {
			recommendations = append(recommendations, recommendation44)
		}

		if strings.TrimSpace(recommendation45) != "" {
			recommendations = append(recommendations, recommendation45)
		}

		if strings.TrimSpace(recommendation46) != "" {
			recommendations = append(recommendations, recommendation46)
		}

		if strings.TrimSpace(recommendation47) != "" {
			recommendations = append(recommendations, recommendation47)
		}

		if strings.TrimSpace(recommendation48) != "" {
			recommendations = append(recommendations, recommendation48)
		}

		if strings.TrimSpace(recommendation49) != "" {
			recommendations = append(recommendations, recommendation49)
		}

		if strings.TrimSpace(recommendation50) != "" {
			recommendations = append(recommendations, recommendation50)
		}

		if strings.TrimSpace(recommendation51) != "" {
			recommendations = append(recommendations, recommendation51)
		}

		if strings.TrimSpace(recommendation52) != "" {
			recommendations = append(recommendations, recommendation52)
		}

		if strings.TrimSpace(recommendation53) != "" {
			recommendations = append(recommendations, recommendation53)
		}

		if strings.TrimSpace(recommendation54) != "" {
			recommendations = append(recommendations, recommendation54)
		}

		if strings.TrimSpace(recommendation55) != "" {
			recommendations = append(recommendations, recommendation55)
		}

		if strings.TrimSpace(recommendation56) != "" {
			recommendations = append(recommendations, recommendation56)
		}

		if strings.TrimSpace(recommendation57) != "" {
			recommendations = append(recommendations, recommendation57)
		}

		if strings.TrimSpace(recommendation58) != "" {
			recommendations = append(recommendations, recommendation58)
		}

		if strings.TrimSpace(recommendation59) != "" {
			recommendations = append(recommendations, recommendation59)
		}

		if strings.TrimSpace(recommendation60) != "" {
			recommendations = append(recommendations, recommendation60)
		}

		if strings.TrimSpace(recommendation61) != "" {
			recommendations = append(recommendations, recommendation61)
		}

		if strings.TrimSpace(recommendation62) != "" {
			recommendations = append(recommendations, recommendation62)
		}

		if strings.TrimSpace(recommendation63) != "" {
			recommendations = append(recommendations, recommendation63)
		}

		if strings.TrimSpace(recommendation64) != "" {
			recommendations = append(recommendations, recommendation64)
		}

		if strings.TrimSpace(recommendation65) != "" {
			recommendations = append(recommendations, recommendation65)
		}

		if strings.TrimSpace(recommendation66) != "" {
			recommendations = append(recommendations, recommendation66)
		}

		if strings.TrimSpace(recommendation67) != "" {
			recommendations = append(recommendations, recommendation67)
		}

		if strings.TrimSpace(recommendation68) != "" {
			recommendations = append(recommendations, recommendation68)
		}

		if strings.TrimSpace(recommendation69) != "" {
			recommendations = append(recommendations, recommendation69)
		}

		if strings.TrimSpace(recommendation70) != "" {
			recommendations = append(recommendations, recommendation70)
		}

		if strings.TrimSpace(recommendation71) != "" {
			recommendations = append(recommendations, recommendation71)
		}

		if strings.TrimSpace(recommendation72) != "" {
			recommendations = append(recommendations, recommendation72)
		}

		if strings.TrimSpace(recommendation73) != "" {
			recommendations = append(recommendations, recommendation73)
		}

		if strings.TrimSpace(recommendation74) != "" {
			recommendations = append(recommendations, recommendation74)
		}

		if strings.TrimSpace(recommendation75) != "" {
			recommendations = append(recommendations, recommendation75)
		}

		if strings.TrimSpace(recommendation76) != "" {
			recommendations = append(recommendations, recommendation76)
		}

		if strings.TrimSpace(recommendation77) != "" {
			recommendations = append(recommendations, recommendation77)
		}

		if strings.TrimSpace(recommendation78) != "" {
			recommendations = append(recommendations, recommendation78)
		}

		if strings.TrimSpace(recommendation79) != "" {
			recommendations = append(recommendations, recommendation79)
		}

		if strings.TrimSpace(recommendation80) != "" {
			recommendations = append(recommendations, recommendation80)
		}

		if strings.TrimSpace(recommendation81) != "" {
			recommendations = append(recommendations, recommendation81)
		}

		if strings.TrimSpace(recommendation82) != "" {
			recommendations = append(recommendations, recommendation82)
		}

		if strings.TrimSpace(recommendation83) != "" {
			recommendations = append(recommendations, recommendation83)
		}

		if strings.TrimSpace(recommendation84) != "" {
			recommendations = append(recommendations, recommendation84)
		}

		if strings.TrimSpace(recommendation85) != "" {
			recommendations = append(recommendations, recommendation85)
		}

		aptitude.Restriction = restrictions
		aptitude.Recommendations = recommendations
		aptitudes = append(aptitudes, &aptitude)
	}
	return aptitudes, nil
}
