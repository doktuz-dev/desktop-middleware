package mysql

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
)

func GetServices(ctx context.Context) ([]*models.Service, error) {
	db := database.MysqlDB
	results, err := db.QueryContext(ctx, "select * from services")
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var services []*models.Service

	for results.Next() {
		var service models.Service

		err := results.Scan(&service.ExternalId, &service.ServiceAreaId, &service.AttentionTypeId, &service.Code, &service.Name, &service.Status)
		if err != nil {
			return nil, err
		}

		services = append(services, &service)
	}
	return services, nil
}
