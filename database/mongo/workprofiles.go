package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type WorkProfileCollection struct {
	Collection *mongo.Collection
}

func GetWorkProfileCollection() *WorkProfileCollection {
	db := database.MiddlewareDB
	workProfile := db.Collection("registers.workprofiles")
	return &WorkProfileCollection{Collection: workProfile}
}

func (wp *WorkProfileCollection) AddWorkProfile(ctx context.Context, workProfile *models.WorkProfile) (*models.WorkProfile, error) {
	workProfile.CreatedDate = time.Now()
	workProfile.LastUpdatedDate = time.Now()
	workProfile.ExternalSystem = "MEDIWEB"
	if len(workProfile.ID) == 0 {
		workProfile.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, workProfile)
	if err != nil {
		return nil, err
	}
	return workProfile, nil
}

func (wp *WorkProfileCollection) UpdateProtocolo(ctx context.Context, id string, workProfile *models.WorkProfile) (bool, error) {
	workProfile.LastUpdatedDate = time.Now()
	workProfile.ExternalSystem = "MEDIWEB"
	filter := bson.M{"_id": id}
	update := bson.M{"$set": bson.M{
		"endDate":            workProfile.EndDate,
		"version":            workProfile.Version,
		"status":             workProfile.Status,
		"customerId":         workProfile.CustomerId,
		"healthcentres":      workProfile.Healthcentres,
		"externalProtocolId": workProfile.ExternalProtocolId,
		"lastUpdatedDate":    workProfile.LastUpdatedDate,
		"externalSystem":     workProfile.ExternalSystem,
	}}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *WorkProfileCollection) UpdateObra(ctx context.Context, id string, workProfile *models.WorkProfile) (bool, error) {
	workProfile.LastUpdatedDate = time.Now()
	workProfile.ExternalSystem = "MEDIWEB"
	filter := bson.M{"_id": id}
	update := bson.M{"$set": bson.M{
		"name":    workProfile.Name,
		"blocked": workProfile.Blocked,
	}}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *WorkProfileCollection) GetWorkProfiles(ctx context.Context) ([]*models.WorkProfile, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrWorkProfiles := []*models.WorkProfile{}
	erro := res.All(ctx, &arrWorkProfiles)
	if erro != nil {
		return nil, erro
	}
	return arrWorkProfiles, nil
}

func (wp *WorkProfileCollection) GetWorkProfileById(ctx context.Context, id string) (*models.WorkProfile, error) {
	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	res := wp.Collection.FindOne(ctx, filter)
	var patient models.WorkProfile
	if res.Err() != nil {
		return nil, res.Err()
	}
	err := res.Decode(&patient)
	if err != nil {
		return nil, err
	}
	return &patient, nil
}

func (wp *WorkProfileCollection) GetWorkProfileByProtocolId(ctx context.Context, id string) (*models.WorkProfile, error) {
	filter := bson.M{"externalProtocolId": id, "externalSystem": "MEDIWEB"}
	res := wp.Collection.FindOne(ctx, filter)
	var patient models.WorkProfile
	if res.Err() != nil {
		return nil, res.Err()
	}
	err := res.Decode(&patient)
	if err != nil {
		return nil, err
	}
	return &patient, nil
}

func (wp *WorkProfileCollection) DeleteWorkProfileByExternalId(ctx context.Context, id string) (bool, error) {
	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	_, err := wp.Collection.DeleteOne(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}
