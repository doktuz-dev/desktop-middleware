package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type DegreeInstructionCollection struct {
	Collection *mongo.Collection
}

func GetDegreeInstructionCollection() *DegreeInstructionCollection {
	db := database.MiddlewareDB
	department := db.Collection("registers.degreeinstructions")
	return &DegreeInstructionCollection{Collection: department}
}

func (wp *DegreeInstructionCollection) AddDegreeInstruction(ctx context.Context, department *models.DegreeInstruction) (*models.DegreeInstruction, error) {
	department.CreatedDate = time.Now()
	department.LastUpdatedDate = time.Now()
	if len(department.ID) == 0 {
		department.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, department)
	if err != nil {
		return nil, err
	}
	return department, nil
}

func (wp *DegreeInstructionCollection) UpdateDegreeInstruction(ctx context.Context, id string, department *models.DegreeInstruction) (bool, error) {
	department.LastUpdatedDate = time.Now()
	filter := bson.M{"_id": id}

	vp := reflect.New(reflect.TypeOf(department))
	vp.Elem().Set(reflect.ValueOf(department))
	v := vp.Interface()
	update := bson.M{"$set": v}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *DegreeInstructionCollection) GetDegreeInstructions(ctx context.Context) ([]*models.DegreeInstruction, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrDegreeInstructions := []*models.DegreeInstruction{}
	erro := res.All(ctx, &arrDegreeInstructions)
	if erro != nil {
		return nil, erro
	}
	return arrDegreeInstructions, nil
}
