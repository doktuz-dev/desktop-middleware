package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type AttentionCollection struct {
	Collection *mongo.Collection
}

func GetAttentionCollection() *AttentionCollection {
	db := database.MiddlewareDB
	attention := db.Collection("transactions.attentions")
	return &AttentionCollection{Collection: attention}
}

func (wp *AttentionCollection) AddAttention(ctx context.Context, attention *models.Attention) (*models.Attention, error) {
	attention.CreatedDate = time.Now()
	attention.LastUpdatedDate = time.Now()
	attention.ExternalSystem = "MEDIWEB"
	attention.HealthCentreName = fmt.Sprintf("Doktuz - %s", attention.HealthCentreName)
	if len(attention.ID) == 0 {
		attention.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, attention)
	if err != nil {
		return nil, err
	}
	return attention, nil
}

func (wp *AttentionCollection) UpdateAttention(ctx context.Context, id string, attention *models.Attention) (bool, error) {
	attention.LastUpdatedDate = time.Now()
	attention.ExternalSystem = "MEDIWEB"
	attention.HealthCentreName = fmt.Sprintf("Doktuz - %s", attention.HealthCentreName)

	filter := bson.M{"_id": id}
	update := bson.M{"$set": attention}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *AttentionCollection) UpdatePersonIdToAttentions(ctx context.Context, patientId, personId string) (bool, error) {

	filter := bson.M{"patientId": patientId}
	update := bson.M{"$set": bson.M{"personId": personId}}
	_, err := wp.Collection.UpdateMany(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *AttentionCollection) UnsetPersonIdAttention(ctx context.Context, id string) (bool, error) {
	unset := bson.M{"personId": ""}

	filter := bson.M{"_id": id}
	update := bson.M{"$unset": unset}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *AttentionCollection) GetAttentions(ctx context.Context) ([]*models.Attention, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrAttentions := []*models.Attention{}
	erro := res.All(ctx, &arrAttentions)
	if erro != nil {
		return nil, erro
	}
	return arrAttentions, nil
}

func (wp *AttentionCollection) GetAttentionById(ctx context.Context, id string) (*models.Attention, error) {

	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models.Attention{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *AttentionCollection) GetAttentionByExternalId(ctx context.Context, id string) (*models.Attention, error) {
	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models.Attention{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *AttentionCollection) DeleteAttentionByExternalId(ctx context.Context, id string) (bool, error) {
	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	_, err := wp.Collection.DeleteOne(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}
