package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
	"reflect"
	"strings"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type HealthCentreCollection struct {
	Collection *mongo.Collection
}

func GetHealthCentreCollection() *HealthCentreCollection {
	db := database.MiddlewareDB
	healthCentre := db.Collection("registers.healthcentres")
	return &HealthCentreCollection{Collection: healthCentre}
}

func (wp *HealthCentreCollection) AddHealthCentre(ctx context.Context, healthCentre *models.HealthCentre) (*models.HealthCentre, error) {
	healthCentre.CreatedDate = time.Now()
	healthCentre.LastUpdatedDate = time.Now()
	healthCentre.ClinicName = "Doktuz"
	healthCentre.ExternalSystem = "MEDIWEB"
	if len(healthCentre.ID) == 0 {
		healthCentre.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, healthCentre)
	if err != nil {
		return nil, err
	}
	return healthCentre, nil
}

func (wp *HealthCentreCollection) UpdateHealthCentre(ctx context.Context, id string, healthCentre *models.HealthCentre) (bool, error) {
	healthCentre.LastUpdatedDate = time.Now()
	healthCentre.ClinicName = "Doktuz"
	healthCentre.ExternalSystem = "MEDIWEB"
	filter := bson.M{"_id": id}

	vp := reflect.New(reflect.TypeOf(healthCentre))
	vp.Elem().Set(reflect.ValueOf(healthCentre))
	v := vp.Interface()
	update := bson.M{"$set": v}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *HealthCentreCollection) GetHealthCentres(ctx context.Context) ([]*models.HealthCentre, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrHealthCentres := []*models.HealthCentre{}
	erro := res.All(ctx, &arrHealthCentres)
	if erro != nil {
		return nil, erro
	}
	return arrHealthCentres, nil
}

func (wp *HealthCentreCollection) GetHealthCentreById(ctx context.Context, id string) (*models.HealthCentre, error) {

	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models.HealthCentre{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *HealthCentreCollection) GetHealthCentreByClinicId(ctx context.Context, clinicId string) ([]*models.HealthCentre, error) {

	filter := bson.M{"clinicId": clinicId, "externalSystem": "MEDIWEB"}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models.HealthCentre{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	return arrExams, nil
}

func (wp *HealthCentreCollection) GetHealthCentreByName(ctx context.Context, clinicName string) (*models.HealthCentre, error) {
	regex := primitive.Regex{Pattern: strings.TrimSpace(clinicName), Options: "i"}
	filter := bson.M{"clinicName": regex, "externalSystem": "MEDIWEB"}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models.HealthCentre{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *HealthCentreCollection) DeleteHealthCentreByExternalId(ctx context.Context, id string) (bool, error) {
	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	_, err := wp.Collection.DeleteOne(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}
