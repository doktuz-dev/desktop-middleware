package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type ConsultationCollection struct {
	Collection *mongo.Collection
}

func GetConsultationCollection() *ConsultationCollection {
	db := database.MiddlewareDB
	consultation := db.Collection("transactions.attentions.consultations")
	return &ConsultationCollection{Collection: consultation}
}

func (wp *ConsultationCollection) AddConsultation(ctx context.Context, consultation *models.Consultation) (*models.Consultation, error) {
	consultation.CreatedDate = time.Now()
	consultation.LastUpdatedDate = time.Now()
	consultation.ExternalSystem = "MEDIWEB"
	if len(consultation.ID) == 0 {
		consultation.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, consultation)
	if err != nil {
		return nil, err
	}
	return consultation, nil
}

func (wp *ConsultationCollection) UpdateConsultation(ctx context.Context, id string, consultation *models.Consultation) (bool, error) {
	consultation.LastUpdatedDate = time.Now()
	consultation.ExternalSystem = "MEDIWEB"
	filter := bson.M{"_id": id}

	vp := reflect.New(reflect.TypeOf(consultation))
	vp.Elem().Set(reflect.ValueOf(consultation))
	v := vp.Interface()
	update := bson.M{"$set": v}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *ConsultationCollection) GetConsultations(ctx context.Context) ([]*models.Consultation, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrConsultations := []*models.Consultation{}
	erro := res.All(ctx, &arrConsultations)
	if erro != nil {
		return nil, erro
	}
	return arrConsultations, nil
}

func (wp *ConsultationCollection) GetConsultationByExternalId(ctx context.Context, id string) (*models.Consultation, error) {

	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models.Consultation{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *ConsultationCollection) DeleteConsultationByExternalId(ctx context.Context, id string) (bool, error) {
	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	_, err := wp.Collection.DeleteOne(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}
