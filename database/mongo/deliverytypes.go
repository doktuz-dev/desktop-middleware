package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type DeliveryTypeCollection struct {
	Collection *mongo.Collection
}

func GetDeliveryTypeCollection() *DeliveryTypeCollection {
	db := database.MiddlewareDB
	department := db.Collection("registers.deliverytypes")
	return &DeliveryTypeCollection{Collection: department}
}

func (wp *DeliveryTypeCollection) AddDeliveryType(ctx context.Context, department *models.DeliveryType) (*models.DeliveryType, error) {
	department.CreatedDate = time.Now()
	department.LastUpdatedDate = time.Now()
	if len(department.ID) == 0 {
		department.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, department)
	if err != nil {
		return nil, err
	}
	return department, nil
}

func (wp *DeliveryTypeCollection) UpdateDeliveryType(ctx context.Context, id string, department *models.DeliveryType) (bool, error) {
	department.LastUpdatedDate = time.Now()
	filter := bson.M{"_id": id}

	vp := reflect.New(reflect.TypeOf(department))
	vp.Elem().Set(reflect.ValueOf(department))
	v := vp.Interface()
	update := bson.M{"$set": v}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *DeliveryTypeCollection) GetDeliveryTypes(ctx context.Context) ([]*models.DeliveryType, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrDeliveryTypes := []*models.DeliveryType{}
	erro := res.All(ctx, &arrDeliveryTypes)
	if erro != nil {
		return nil, erro
	}
	return arrDeliveryTypes, nil
}

func (wp *DeliveryTypeCollection) GetDeliveryTypeTypesById(ctx context.Context, id string) (*models.DeliveryType, error) {

	filter := bson.M{"code": id}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models.DeliveryType{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}
