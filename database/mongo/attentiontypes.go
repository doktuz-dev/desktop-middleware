package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type AttentionTypeCollection struct {
	Collection *mongo.Collection
}

func GetAttentionTypeCollection() *AttentionTypeCollection {
	db := database.MiddlewareDB
	department := db.Collection("registers.attentiontypes")
	return &AttentionTypeCollection{Collection: department}
}

func (wp *AttentionTypeCollection) AddAttentionType(ctx context.Context, department *models.AttentionType) (*models.AttentionType, error) {
	department.CreatedDate = time.Now()
	department.LastUpdatedDate = time.Now()
	if len(department.ID) == 0 {
		department.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, department)
	if err != nil {
		return nil, err
	}
	return department, nil
}

func (wp *AttentionTypeCollection) UpdateAttentionType(ctx context.Context, id string, department *models.AttentionType) (bool, error) {
	department.LastUpdatedDate = time.Now()
	filter := bson.M{"_id": id}

	vp := reflect.New(reflect.TypeOf(department))
	vp.Elem().Set(reflect.ValueOf(department))
	v := vp.Interface()
	update := bson.M{"$set": v}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *AttentionTypeCollection) GetAttentionTypes(ctx context.Context) ([]*models.AttentionType, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrAttentionTypes := []*models.AttentionType{}
	erro := res.All(ctx, &arrAttentionTypes)
	if erro != nil {
		return nil, erro
	}
	return arrAttentionTypes, nil
}

func (wp *AttentionTypeCollection) GetAttentionTypesById(ctx context.Context, id string) (*models.AttentionType, error) {

	filter := bson.M{"code": id}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models.AttentionType{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}
