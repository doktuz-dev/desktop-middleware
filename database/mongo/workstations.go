package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type WorkStationCollection struct {
	Collection *mongo.Collection
}

func GetWorkStationCollection() *WorkStationCollection {
	db := database.MiddlewareDB
	workStation := db.Collection("registers.customers.workstations")
	return &WorkStationCollection{Collection: workStation}
}

func (wp *WorkStationCollection) AddWorkStation(ctx context.Context, workStation *models.WorkStation) (*models.WorkStation, error) {
	workStation.CreatedDate = time.Now()
	workStation.LastUpdatedDate = time.Now()
	workStation.ExternalSystem = "MEDIWEB"
	if len(workStation.ID) == 0 {
		workStation.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, workStation)
	if err != nil {
		return nil, err
	}
	return workStation, nil
}

func (wp *WorkStationCollection) UpdateWorkStation(ctx context.Context, id string, workStation *models.WorkStation) (bool, error) {
	workStation.LastUpdatedDate = time.Now()
	workStation.ExternalSystem = "MEDIWEB"
	filter := bson.M{"_id": id}

	vp := reflect.New(reflect.TypeOf(workStation))
	vp.Elem().Set(reflect.ValueOf(workStation))
	v := vp.Interface()
	update := bson.M{"$set": v}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *WorkStationCollection) GetWorkStations(ctx context.Context) ([]*models.WorkStation, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrWorkStations := []*models.WorkStation{}
	erro := res.All(ctx, &arrWorkStations)
	if erro != nil {
		return nil, erro
	}
	return arrWorkStations, nil
}

func (wp *WorkStationCollection) GetWorkStationById(ctx context.Context, id string) (*models.WorkStation, error) {

	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models.WorkStation{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *WorkStationCollection) DeleteWorkStationByExternalId(ctx context.Context, id string) (bool, error) {
	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	_, err := wp.Collection.DeleteOne(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}
