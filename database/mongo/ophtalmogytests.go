package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type OphtalmologyTestCollection struct {
	Collection *mongo.Collection
}

func GetOphtalmologyTestCollection() *OphtalmologyTestCollection {
	db := database.MiddlewareDB
	test := db.Collection("transactions.attentions.exams.ophtalmologytests")
	return &OphtalmologyTestCollection{Collection: test}
}

func (wp *OphtalmologyTestCollection) AddOphtalmologyTest(ctx context.Context, test *models.OphtalmologyTest) (*models.OphtalmologyTest, error) {
	test.CreatedDate = time.Now()
	test.LastUpdatedDate = time.Now()
	test.ExternalSystem = "MEDIWEB"
	if len(test.ID) == 0 {
		test.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, test)
	if err != nil {
		return nil, err
	}
	return test, nil
}

func (wp *OphtalmologyTestCollection) UpdateOphtalmologyTest(ctx context.Context, id string, test *models.OphtalmologyTest) (bool, error) {
	test.LastUpdatedDate = time.Now()
	test.ExternalSystem = "MEDIWEB"
	filter := bson.M{"_id": id}

	vp := reflect.New(reflect.TypeOf(test))
	vp.Elem().Set(reflect.ValueOf(test))
	v := vp.Interface()
	update := bson.M{"$set": v}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *OphtalmologyTestCollection) GetOphtalmologyTests(ctx context.Context) ([]*models.OphtalmologyTest, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrOphtalmologyTests := []*models.OphtalmologyTest{}
	erro := res.All(ctx, &arrOphtalmologyTests)
	if erro != nil {
		return nil, erro
	}
	return arrOphtalmologyTests, nil
}

func (wp *OphtalmologyTestCollection) DeleteOphtalmologyTests(ctx context.Context, attentionId string, serviceAreaId string, examId string) (bool, error) {
	filter := bson.M{"attentionId": attentionId, "serviceAreaId": serviceAreaId, "examId": examId, "externalSystem": "MEDIWEB"}
	_, err := wp.Collection.DeleteMany(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *OphtalmologyTestCollection) GetOphtalmologyTestByExternalId(ctx context.Context, id string) (*models.OphtalmologyTest, error) {

	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models.OphtalmologyTest{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *OphtalmologyTestCollection) DeleteOphtalmologyTestByExternalId(ctx context.Context, id string) (bool, error) {
	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	_, err := wp.Collection.DeleteOne(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}
