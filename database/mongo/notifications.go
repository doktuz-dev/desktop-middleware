package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type NotificationsCollection struct {
	Collection *mongo.Collection
}

func GetNotificationsCollection() *NotificationsCollection {
	db := database.MiddlewareDB
	notification := db.Collection("registers.notifications")
	return &NotificationsCollection{Collection: notification}
}

func (wp *NotificationsCollection) AddNotifications(ctx context.Context, notification *models.Notification) (*models.Notification, error) {
	notification.CreatedDate = time.Now()
	notification.LastUpdatedDate = time.Now()
	if len(notification.ID) == 0 {
		notification.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, notification)
	if err != nil {
		return nil, err
	}
	return notification, nil
}

func (wp *NotificationsCollection) GetNotifications(ctx context.Context, customerId, code, platform string) ([]*models.Notification, error) {
	filter := bson.M{}
	if len(customerId) > 0 {
		filter["customerId"] = customerId
	}
	if len(code) > 0 {
		filter["code"] = code
	}
	if len(platform) > 0 {
		filter["platform"] = platform
	}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrNotifications := []*models.Notification{}
	erro := res.All(ctx, &arrNotifications)
	if erro != nil {
		return nil, erro
	}
	return arrNotifications, nil
}

func (wp *NotificationsCollection) UpdateNotifications(ctx context.Context, id string, notification *models.Notification) (bool, error) {
	filter := bson.M{"_id": id}
	vp := reflect.New(reflect.TypeOf(notification))
	vp.Elem().Set(reflect.ValueOf(notification))
	v := vp.Interface()
	update := bson.M{"$set": v}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *NotificationsCollection) DisableNotificationsById(ctx context.Context, id string) (bool, error) {
	filter := bson.M{"_id": id}
	update := bson.M{"$set": bson.M{"enabled": false}}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *NotificationsCollection) EnableNotificationsById(ctx context.Context, id string) (bool, error) {
	filter := bson.M{"_id": id}
	update := bson.M{"$set": bson.M{"enabled": true}}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *NotificationsCollection) GetNotificationByCode(ctx context.Context, code int) (*models.Notification, error) {
	filter := bson.M{"code": code}
	res := wp.Collection.FindOne(ctx, filter)
	var notification models.Notification
	if res.Err() != nil {
		return nil, res.Err()
	}
	err := res.Decode(&notification)
	if err != nil {
		return nil, err
	}
	return &notification, nil
}

func (wp *NotificationsCollection) DeleteNotificationsByItemId(ctx context.Context, customerId string) (bool, error) {
	filter := bson.M{"customerId": customerId}
	res, err := wp.Collection.DeleteMany(ctx, filter)
	if err != nil {
		return false, err
	}
	if res.DeletedCount > 0 {
		return true, nil
	}
	return false, nil
}

func (wp *NotificationsCollection) AddManyNotifications(ctx context.Context, test []interface{}) ([]interface{}, error) {
	result, err := wp.Collection.InsertMany(ctx, test)
	if err != nil {
		return nil, err
	}
	return result.InsertedIDs, nil
}
