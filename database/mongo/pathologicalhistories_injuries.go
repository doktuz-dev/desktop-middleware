package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type PathologicalHistoryInjuriesCollection struct {
	Collection *mongo.Collection
}

func GetPathologicalHistoryInjuriesCollection() *PathologicalHistoryInjuriesCollection {
	db := database.MiddlewareDB
	pathologicalHistory := db.Collection("transactions.attentions.pathologicalhistories.injuries")
	return &PathologicalHistoryInjuriesCollection{Collection: pathologicalHistory}
}

func (wp *PathologicalHistoryInjuriesCollection) AddPathologicalHistoryInjuries(ctx context.Context, pathologicalHistory *models.PathologicalHistoryInjuries) (*models.PathologicalHistoryInjuries, error) {
	pathologicalHistory.CreatedDate = time.Now()
	pathologicalHistory.LastUpdatedDate = time.Now()
	pathologicalHistory.ExternalSystem = "MEDIWEB"
	if len(pathologicalHistory.ID) == 0 {
		pathologicalHistory.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, pathologicalHistory)
	if err != nil {
		return nil, err
	}
	return pathologicalHistory, nil
}

func (wp *PathologicalHistoryInjuriesCollection) UpdatePathologicalHistoryInjuries(ctx context.Context, id string, pathologicalHistory *models.PathologicalHistoryInjuries) (bool, error) {
	pathologicalHistory.LastUpdatedDate = time.Now()
	pathologicalHistory.ExternalSystem = "MEDIWEB"
	filter := bson.M{"_id": id}

	vp := reflect.New(reflect.TypeOf(pathologicalHistory))
	vp.Elem().Set(reflect.ValueOf(pathologicalHistory))
	v := vp.Interface()
	update := bson.M{"$set": v}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *PathologicalHistoryInjuriesCollection) GetPathologicalHistoryInjuries(ctx context.Context) ([]*models.PathologicalHistoryInjuries, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrPathologicalHistoryInjuriess := []*models.PathologicalHistoryInjuries{}
	erro := res.All(ctx, &arrPathologicalHistoryInjuriess)
	if erro != nil {
		return nil, erro
	}
	return arrPathologicalHistoryInjuriess, nil
}

func (wp *PathologicalHistoryInjuriesCollection) DeletePathologicalHistoryInjuriesByExternalId(ctx context.Context, id string, seq int) (bool, error) {
	filter := bson.M{"externalId": id, "sequenceNumber": seq, "externalSystem": "MEDIWEB"}
	_, err := wp.Collection.DeleteMany(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *PathologicalHistoryInjuriesCollection) DeletePathologicalHistoryInjuriesById(ctx context.Context, id string) (bool, error) {
	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	_, err := wp.Collection.DeleteMany(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *PathologicalHistoryInjuriesCollection) AddManyPathologicalHistoryInjuries(ctx context.Context, test []interface{}) ([]interface{}, error) {
	result, err := wp.Collection.InsertMany(ctx, test)
	if err != nil {
		return nil, err
	}
	return result.InsertedIDs, nil
}
