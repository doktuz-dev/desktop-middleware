package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type JobTitleCollection struct {
	Collection *mongo.Collection
}

func GetJobTitleCollection() *JobTitleCollection {
	db := database.MiddlewareDB
	jobTitle := db.Collection("registers.jobtitles")
	return &JobTitleCollection{Collection: jobTitle}
}

func (wp *JobTitleCollection) AddJobTitle(ctx context.Context, jobTitle *models.JobTitle) (*models.JobTitle, error) {
	jobTitle.CreatedDate = time.Now()
	jobTitle.LastUpdatedDate = time.Now()
	jobTitle.ExternalSystem = "MEDIWEB"
	if len(jobTitle.ID) == 0 {
		jobTitle.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, jobTitle)
	if err != nil {
		return nil, err
	}
	return jobTitle, nil
}

func (wp *JobTitleCollection) UpdateJobTitle(ctx context.Context, id string, jobTitle *models.JobTitle) (bool, error) {
	jobTitle.LastUpdatedDate = time.Now()
	jobTitle.ExternalSystem = "MEDIWEB"
	filter := bson.M{"_id": id}

	vp := reflect.New(reflect.TypeOf(jobTitle))
	vp.Elem().Set(reflect.ValueOf(jobTitle))
	v := vp.Interface()
	update := bson.M{"$set": v}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *JobTitleCollection) GetJobTitles(ctx context.Context) ([]*models.JobTitle, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrJobTitles := []*models.JobTitle{}
	erro := res.All(ctx, &arrJobTitles)
	if erro != nil {
		return nil, erro
	}
	return arrJobTitles, nil
}

func (wp *JobTitleCollection) GetJobTitleByExternalId(ctx context.Context, id string) (*models.JobTitle, error) {

	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models.JobTitle{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *JobTitleCollection) GetJobTitleByWorkingArea(ctx context.Context, id string) ([]*models.JobTitle, error) {

	filter := bson.M{"workingAreaId": id}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models.JobTitle{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	return arrExams, nil
}

func (wp *JobTitleCollection) DeleteJobTitleByExternalId(ctx context.Context, id string) (bool, error) {
	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	_, err := wp.Collection.DeleteOne(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}
