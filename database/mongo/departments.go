package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type DepartmentCollection struct {
	Collection *mongo.Collection
}

func GetDepartmentCollection() *DepartmentCollection {
	db := database.MiddlewareDB
	department := db.Collection("registers.departments")
	return &DepartmentCollection{Collection: department}
}

func (wp *DepartmentCollection) AddDepartment(ctx context.Context, department *models.Department) (*models.Department, error) {
	department.CreatedDate = time.Now()
	department.LastUpdatedDate = time.Now()
	department.ExternalSystem = "MEDIWEB"
	if len(department.ID) == 0 {
		department.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, department)
	if err != nil {
		return nil, err
	}
	return department, nil
}

func (wp *DepartmentCollection) UpdateDepartment(ctx context.Context, id string, department *models.Department) (bool, error) {
	department.LastUpdatedDate = time.Now()
	department.ExternalSystem = "MEDIWEB"
	filter := bson.M{"_id": id}

	vp := reflect.New(reflect.TypeOf(department))
	vp.Elem().Set(reflect.ValueOf(department))
	v := vp.Interface()
	update := bson.M{"$set": v}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *DepartmentCollection) GetDepartments(ctx context.Context) ([]*models.Department, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrDepartments := []*models.Department{}
	erro := res.All(ctx, &arrDepartments)
	if erro != nil {
		return nil, erro
	}
	return arrDepartments, nil
}

func (wp *DepartmentCollection) GetDepartmentById(ctx context.Context, id string) (*models.Department, error) {

	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models.Department{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *DepartmentCollection) DeleteDepartmentByExternalId(ctx context.Context, id string) (bool, error) {
	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	_, err := wp.Collection.DeleteOne(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}
