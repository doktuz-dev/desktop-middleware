package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type WorkingAreaCollection struct {
	Collection *mongo.Collection
}

func GetWorkingAreaCollection() *WorkingAreaCollection {
	db := database.MiddlewareDB
	workingArea := db.Collection("registers.workingareas")
	return &WorkingAreaCollection{Collection: workingArea}
}

func (wp *WorkingAreaCollection) AddWorkingArea(ctx context.Context, workingArea *models.WorkingArea) (*models.WorkingArea, error) {
	workingArea.CreatedDate = time.Now()
	workingArea.LastUpdatedDate = time.Now()
	workingArea.ExternalSystem = "MEDIWEB"
	if len(workingArea.ID) == 0 {
		workingArea.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, workingArea)
	if err != nil {
		return nil, err
	}
	return workingArea, nil
}

func (wp *WorkingAreaCollection) UpdateWorkingArea(ctx context.Context, id string, workingArea *models.WorkingArea) (bool, error) {
	workingArea.LastUpdatedDate = time.Now()
	workingArea.ExternalSystem = "MEDIWEB"
	filter := bson.M{"_id": id}

	vp := reflect.New(reflect.TypeOf(workingArea))
	vp.Elem().Set(reflect.ValueOf(workingArea))
	v := vp.Interface()
	update := bson.M{"$set": v}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *WorkingAreaCollection) GetWorkingareas(ctx context.Context) ([]*models.WorkingArea, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrWorkingareas := []*models.WorkingArea{}
	erro := res.All(ctx, &arrWorkingareas)
	if erro != nil {
		return nil, erro
	}
	return arrWorkingareas, nil
}

func (wp *WorkingAreaCollection) GetWorkingAreaByExternalId(ctx context.Context, id string) (*models.WorkingArea, error) {

	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models.WorkingArea{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *WorkingAreaCollection) GetWorkingAreaByCustomerID(ctx context.Context, id string) ([]*models.WorkingArea, error) {

	filter := bson.M{"customerId": id}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models.WorkingArea{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	return arrExams, nil
}

func (wp *WorkingAreaCollection) DeleteWorkingAreaByExternalId(ctx context.Context, id string) (bool, error) {
	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	_, err := wp.Collection.DeleteOne(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}
