package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type AptitudeCollection struct {
	Collection *mongo.Collection
}

func GetAptitudeCollection() *AptitudeCollection {
	db := database.MiddlewareDB
	aptitude := db.Collection("transactions.attentions.aptitudes")
	return &AptitudeCollection{Collection: aptitude}
}

func (wp *AptitudeCollection) AddAptitude(ctx context.Context, aptitude *models.Aptitude) (*models.Aptitude, error) {
	aptitude.CreatedDate = time.Now()
	aptitude.LastUpdatedDate = time.Now()
	aptitude.ExternalSystem = "MEDIWEB"
	if len(aptitude.ID) == 0 {
		aptitude.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, aptitude)
	if err != nil {
		return nil, err
	}
	return aptitude, nil
}

func (wp *AptitudeCollection) UpdateAptitude(ctx context.Context, id string, aptitude *models.Aptitude) (bool, error) {
	aptitude.LastUpdatedDate = time.Now()
	aptitude.ExternalSystem = "MEDIWEB"
	filter := bson.M{"_id": id}

	vp := reflect.New(reflect.TypeOf(aptitude))
	vp.Elem().Set(reflect.ValueOf(aptitude))
	v := vp.Interface()
	update := bson.M{"$set": v}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *AptitudeCollection) GetAptitudes(ctx context.Context) ([]*models.Aptitude, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrAptitudes := []*models.Aptitude{}
	erro := res.All(ctx, &arrAptitudes)
	if erro != nil {
		return nil, erro
	}
	return arrAptitudes, nil
}

func (wp *AptitudeCollection) GetAptitudeByExternalId(ctx context.Context, id string) (*models.Aptitude, error) {
	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models.Aptitude{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *AptitudeCollection) DeleteAptitudeByExternalId(ctx context.Context, id string) (bool, error) {
	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	_, err := wp.Collection.DeleteOne(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}
