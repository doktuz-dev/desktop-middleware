package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type EvaluationCollection struct {
	Collection *mongo.Collection
}

func GetEvaluationCollection() *EvaluationCollection {
	db := database.MiddlewareDB
	evaluation := db.Collection("transactions.evaluations")
	return &EvaluationCollection{Collection: evaluation}
}

func (wp *EvaluationCollection) AddEvaluation(ctx context.Context, evaluation *models.Evaluation) (*models.Evaluation, error) {
	evaluation.CreatedDate = time.Now()
	evaluation.LastUpdatedDate = time.Now()
	evaluation.ExternalSystem = "MEDIWEB"
	if len(evaluation.ID) == 0 {
		evaluation.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, evaluation)
	if err != nil {
		return nil, err
	}
	return evaluation, nil
}

func (wp *EvaluationCollection) UpdateEvaluation(ctx context.Context, id string, evaluation *models.Evaluation) (bool, error) {
	evaluation.LastUpdatedDate = time.Now()
	evaluation.ExternalSystem = "MEDIWEB"
	filter := bson.M{"_id": id}

	vp := reflect.New(reflect.TypeOf(evaluation))
	vp.Elem().Set(reflect.ValueOf(evaluation))
	v := vp.Interface()
	update := bson.M{"$set": v}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *EvaluationCollection) GetEvaluations(ctx context.Context) ([]*models.Evaluation, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrEvaluations := []*models.Evaluation{}
	erro := res.All(ctx, &arrEvaluations)
	if erro != nil {
		return nil, erro
	}
	return arrEvaluations, nil
}

func (wp *EvaluationCollection) GetEvaluationById(ctx context.Context, id string) (*models.Evaluation, error) {
	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	res := wp.Collection.FindOne(ctx, filter)
	var patient models.Evaluation
	if res.Err() != nil {
		return nil, res.Err()
	}
	err := res.Decode(&patient)
	if err != nil {
		return nil, err
	}
	return &patient, nil
}

func (wp *EvaluationCollection) GetEvaluationByExternalId(ctx context.Context, id string) (*models.Evaluation, error) {

	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models.Evaluation{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *EvaluationCollection) DeleteEvaluationByExternalId(ctx context.Context, id string) (bool, error) {
	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	_, err := wp.Collection.DeleteOne(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}
