package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type WorkHistoryCollection struct {
	Collection *mongo.Collection
}

func GetWorkHistoryCollection() *WorkHistoryCollection {
	db := database.MiddlewareDB
	workHistory := db.Collection("transactions.attentions.workhistories")
	return &WorkHistoryCollection{Collection: workHistory}
}

func (wp *WorkHistoryCollection) AddWorkHistory(ctx context.Context, workHistory *models.WorkHistory) (*models.WorkHistory, error) {
	workHistory.CreatedDate = time.Now()
	workHistory.LastUpdatedDate = time.Now()
	workHistory.ExternalSystem = "MEDIWEB"
	if len(workHistory.ID) == 0 {
		workHistory.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, workHistory)
	if err != nil {
		return nil, err
	}
	return workHistory, nil
}

func (wp *WorkHistoryCollection) UpdateWorkHistory(ctx context.Context, id string, workHistory *models.WorkHistory) (bool, error) {
	workHistory.LastUpdatedDate = time.Now()
	workHistory.ExternalSystem = "MEDIWEB"
	filter := bson.M{"_id": id}

	vp := reflect.New(reflect.TypeOf(workHistory))
	vp.Elem().Set(reflect.ValueOf(workHistory))
	v := vp.Interface()
	update := bson.M{"$set": v}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *WorkHistoryCollection) GetWorkHistorys(ctx context.Context) ([]*models.WorkHistory, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrWorkHistorys := []*models.WorkHistory{}
	erro := res.All(ctx, &arrWorkHistorys)
	if erro != nil {
		return nil, erro
	}
	return arrWorkHistorys, nil
}

func (wp *WorkHistoryCollection) DeleteWorkHistoriesById(ctx context.Context, id string) (bool, error) {
	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	_, err := wp.Collection.DeleteMany(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *WorkHistoryCollection) DeleteWorkHistories(ctx context.Context, id string, seq int) (bool, error) {
	filter := bson.M{"externalId": id, "sequenceNumber": seq, "externalSystem": "MEDIWEB"}
	_, err := wp.Collection.DeleteMany(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *WorkHistoryCollection) AddManyWorkHistories(ctx context.Context, test []interface{}) ([]interface{}, error) {
	result, err := wp.Collection.InsertMany(ctx, test)
	if err != nil {
		return nil, err
	}
	return result.InsertedIDs, nil
}
