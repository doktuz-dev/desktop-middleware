package mongo

import (
	"context"
	"desktop-middleware/database"
	"desktop-middleware/models"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type DistrictCollection struct {
	Collection *mongo.Collection
}

func GetDistrictCollection() *DistrictCollection {
	db := database.MiddlewareDB
	district := db.Collection("registers.districts")
	return &DistrictCollection{Collection: district}
}

func (wp *DistrictCollection) AddDistrict(ctx context.Context, district *models.District) (*models.District, error) {
	district.CreatedDate = time.Now()
	district.LastUpdatedDate = time.Now()
	district.ExternalSystem = "MEDIWEB"
	if len(district.ID) == 0 {
		district.ID = primitive.NewObjectID().Hex()
	}
	_, err := wp.Collection.InsertOne(ctx, district)
	if err != nil {
		return nil, err
	}
	return district, nil
}

func (wp *DistrictCollection) UpdateDistrict(ctx context.Context, id string, district *models.District) (bool, error) {
	district.LastUpdatedDate = time.Now()
	district.ExternalSystem = "MEDIWEB"
	filter := bson.M{"_id": id}

	vp := reflect.New(reflect.TypeOf(district))
	vp.Elem().Set(reflect.ValueOf(district))
	v := vp.Interface()
	update := bson.M{"$set": v}
	_, err := wp.Collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wp *DistrictCollection) GetDistricts(ctx context.Context) ([]*models.District, error) {
	filter := bson.M{}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrDistricts := []*models.District{}
	erro := res.All(ctx, &arrDistricts)
	if erro != nil {
		return nil, erro
	}
	return arrDistricts, nil
}

func (wp *DistrictCollection) GetDistrictById(ctx context.Context, id string) (*models.District, error) {

	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	res, err := wp.Collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if res.Err() != nil {
		return nil, res.Err()
	}
	arrExams := []*models.District{}
	erro := res.All(ctx, &arrExams)
	if erro != nil {
		return nil, erro
	}
	if len(arrExams) > 0 {
		return arrExams[0], nil
	} else {
		return nil, nil
	}
}

func (wp *DistrictCollection) DeleteDistrictByExternalId(ctx context.Context, id string) (bool, error) {
	filter := bson.M{"externalId": id, "externalSystem": "MEDIWEB"}
	_, err := wp.Collection.DeleteOne(ctx, filter)
	if err != nil {
		return false, err
	}
	return true, nil
}
