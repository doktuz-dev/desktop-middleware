package models_v2

import "time"

type Aptitude struct {
	ID                string    `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionId       string    `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	StartDate         time.Time `json:"startDate,omitempty" bson:"startDate,omitempty"`
	EndDate           time.Time `json:"endDate,omitempty" bson:"endDate,omitempty"`
	ResultCode        string    `json:"resultCode,omitempty" bson:"resultCode,omitempty"`
	ObservedReason    string    `json:"observedReason,omitempty" bson:"observedReason,omitempty"`
	AptitudeDetail    string    `json:"aptitudeDetail,omitempty" bson:"aptitudeDetail,omitempty"`
	Restriction       []string  `json:"restriction,omitempty" bson:"restriction,omitempty"`
	Recommendations   []string  `json:"recommendations,omitempty" bson:"recommendations,omitempty"`
	ResultDescription string    `json:"resultDescription,omitempty" bson:"resultDescription,omitempty"`
	Reviewed          bool      `json:"reviewed,omitempty" bson:"reviewed,omitempty"`
	Audited           bool      `json:"audited,omitempty" bson:"audited,omitempty"`
	Status            int       `json:"status" bson:"status"`
	ExternalId        string    `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem    string    `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate       time.Time `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate   time.Time `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}
