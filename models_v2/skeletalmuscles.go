package models_v2

import "time"

type SkeletalMuscle struct {
	ID              string `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionID     string `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	ServiceAreaID   string `json:"serviceAreaId,omitempty" bson:"serviceAreaId,omitempty"`
	ServiceAreaCode string `json:"serviceAreaCode,omitempty" bson:"serviceAreaCode,omitempty"`
	Diagnostic1     string `json:"diagnostic1,omitempty" bson:"diagnostic1,omitempty"`
	Diagnostic2     string `json:"diagnostic2,omitempty" bson:"diagnostic2,omitempty"`
	Diagnostic3     string `json:"diagnostic3,omitempty" bson:"diagnostic3,omitempty"`
	Diagnostic4     string `json:"diagnostic4,omitempty" bson:"diagnostic4,omitempty"`
	Diagnostic5     string `json:"diagnostic5,omitempty" bson:"diagnostic5,omitempty"`
	Diagnostic6     string `json:"diagnostic6,omitempty" bson:"diagnostic6,omitempty"`
	Diagnostic7     string `json:"diagnostic7,omitempty" bson:"diagnostic7,omitempty"`
	Diagnostic8     string `json:"diagnostic8,omitempty" bson:"diagnostic8,omitempty"`
	Diagnostic9     string `json:"diagnostic9,omitempty" bson:"diagnostic9,omitempty"`
	Diagnostic10    string `json:"diagnostic10,omitempty" bson:"diagnostic10,omitempty"`
	Diagnostic11    string `json:"diagnostic11,omitempty" bson:"diagnostic11,omitempty"`
	Status          int    `json:"status" bson:"status"`
}
type SkeletalMuscleTest struct {
	ID                string             `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionID       string             `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	ServiceAreaID     string             `json:"serviceAreaId,omitempty" bson:"serviceAreaId,omitempty"`
	ServiceAreaCode   string             `json:"serviceAreaCode,omitempty" bson:"serviceAreaCode,omitempty"`
	ExamID            string             `json:"examId,omitempty" bson:"examId,omitempty"`
	GeneralPain       string             `json:"generalPain,omitempty" bson:"generalPain,omitempty"`
	StaticEvaluation  *StaticEvaluation  `json:"staticEvaluation,omitempty" bson:"staticEvaluation,omitempty"`
	PainMobility      *PainMobility      `json:"painMobility,omitempty" bson:"painMobility,omitempty"`
	BackExploration   *BackExploration   `json:"backExploration,omitempty" bson:"backExploration,omitempty"`
	BackPalpation     *BackPalpation     `json:"backPalpation,omitempty" bson:"backPalpation,omitempty"`
	JointsExploration *JointsExploration `json:"jointsExploration,omitempty" bson:"jointsExploration,omitempty"`
	BackFindings      string             `json:"backFindings,omitempty" bson:"backFindings,omitempty"`
	Status            int                `json:"status" bson:"status"`
	ExternalId        string             `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem    string             `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate       time.Time          `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate   time.Time          `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}

type StaticEvaluation struct {
	PhysiologicalCurvatures *PhysiologicalCurvatures `json:"physiologicalCurvatures,omitempty" bson:"physiologicalCurvatures,omitempty"`
	Feet                    *Feet                    `json:"feet,omitempty" bson:"feet,omitempty"`
	AdamsForwardBendTest    string                   `json:"adamsForwardBendTest,omitempty" bson:"adamsForwardBendTest,omitempty"`
}

type PhysiologicalCurvatures struct {
	Cervical string `json:"cervical,omitempty" bson:"cervical,omitempty"`
	Dorsal   string `json:"dorsal,omitempty" bson:"dorsal,omitempty"`
	Lumbar   string `json:"lumbar,omitempty" bson:"lumbar,omitempty"`
}

type Feet struct {
	FeetDiagnostic string `json:"feetDiagnostic,omitempty" bson:"feetDiagnostic,omitempty"`
	PesCavus       string `json:"pesCavus,omitempty" bson:"pesCavus,omitempty"`
	FlatFoot       string `json:"flatFoot,omitempty" bson:"flatFoot,omitempty"`
}

type PainMobility struct {
	Cervical *PainMobilityFields `json:"cervical,omitempty" bson:"cervical,omitempty"`
	Dorsal   *PainMobilityFields `json:"dorsal,omitempty" bson:"dorsal,omitempty"`
	Lumbar   *PainMobilityFields `json:"lumbar,omitempty" bson:"lumbar,omitempty"`
}

type PainMobilityFields struct {
	Flexion              string `json:"flexion,omitempty" bson:"flexion,omitempty"`
	Extension            string `json:"extension,omitempty" bson:"extension,omitempty"`
	RightLateralization  string `json:"rightLateralization,omitempty" bson:"rightLateralization,omitempty"`
	LeftLateralization   string `json:"leftLateralization,omitempty" bson:"leftLateralization,omitempty"`
	RightRotation        string `json:"rightRotation,omitempty" bson:"rightRotation,omitempty"`
	LeftRotation         string `json:"leftRotation,omitempty" bson:"leftRotation,omitempty"`
	FunctionalLimitation string `json:"functionalLimitation,omitempty" bson:"functionalLimitation,omitempty"`
	RadiatingPain        string `json:"radiatingPain,omitempty" bson:"radiatingPain,omitempty"`
}

type BackExploration struct {
	LassegueTest *LassegueTest `json:"lassegueTest,omitempty" bson:"lassegueTest,omitempty"`
	SchoberTest  string        `json:"schoberTest,omitempty" bson:"schoberTest,omitempty"`
}

type LassegueTest struct {
	Right string `json:"right,omitempty" bson:"right,omitempty"`
	Left  string `json:"left,omitempty" bson:"left,omitempty"`
}

type BackPalpation struct {
	Cervical *CervicalBackPalpation `json:"cervical,omitempty" bson:"cervical,omitempty"`
	Dorsal   *DorsalBackPalpation   `json:"dorsal,omitempty" bson:"dorsal,omitempty"`
	Lumbar   *LumbarBackPalpation   `json:"lumbar,omitempty" bson:"lumbar,omitempty"`
}

type CervicalBackPalpation struct {
	PainfulSpinousApophysis string `json:"painfulSpinousApophysis,omitempty" bson:"painfulSpinousApophysis,omitempty"`
	MuscleContracture       string `json:"muscleContracture,omitempty" bson:"muscleContracture,omitempty"`
}

type DorsalBackPalpation struct {
	PainfulSpinousApophysis string `json:"painfulSpinousApophysis,omitempty" bson:"painfulSpinousApophysis,omitempty"`
	MuscleContracture       string `json:"muscleContracture,omitempty" bson:"muscleContracture,omitempty"`
}

type LumbarBackPalpation struct {
	PainfulSpinousApophysis string `json:"painfulSpinousApophysis,omitempty" bson:"painfulSpinousApophysis,omitempty"`
	MuscleContracture       string `json:"muscleContracture,omitempty" bson:"muscleContracture,omitempty"`
}

type JointsExploration struct {
	Shoulder        *JointsExplorationDetail `json:"shoulder,omitempty" bson:"shoulder,omitempty"`
	Elbow           *JointsExplorationDetail `json:"elbow,omitempty" bson:"elbow,omitempty"`
	Wrist           *JointsExplorationDetail `json:"wrist,omitempty" bson:"wrist,omitempty"`
	HandsAndFingers *JointsExplorationDetail `json:"handsAndFingers,omitempty" bson:"handsAndFingers,omitempty"`
	Hips            *JointsExplorationDetail `json:"hips,omitempty" bson:"hips,omitempty"`
	Knee            *JointsExplorationDetail `json:"knee,omitempty" bson:"knee,omitempty"`
	Ankle           *JointsExplorationDetail `json:"ankle,omitempty" bson:"ankle,omitempty"`
}

type JointsExplorationDetail struct {
	Right *JointsExplorationFields `json:"right,omitempty" bson:"right,omitempty"`
	Left  *JointsExplorationFields `json:"left,omitempty" bson:"left,omitempty"`
}

type JointsExplorationFields struct {
	Abduction             string `json:"abduction,omitempty" bson:"abduction,omitempty"`
	Adduction             string `json:"adduction,omitempty" bson:"adduction,omitempty"`
	Flexion               string `json:"flexion,omitempty" bson:"flexion,omitempty"`
	Extension             string `json:"extension,omitempty" bson:"extension,omitempty"`
	ExternalRotation      string `json:"externalRotation,omitempty" bson:"externalRotation,omitempty"`
	InternalRotation      string `json:"internalRotation,omitempty" bson:"internalRotation,omitempty"`
	FunctionalLimitation  string `json:"functionalLimitation,omitempty" bson:"functionalLimitation,omitempty"`
	RadiatingPain         string `json:"radiatingPain,omitempty" bson:"radiatingPain,omitempty"`
	MuscleMassAlterations string `json:"muscleMassAlterations,omitempty" bson:"muscleMassAlterations,omitempty"`
}

func (newTest *SkeletalMuscleTest) SetBackFindings() string {
	painMobilityFindings := newTest.PainMobilityFindings()
	backExplorationFindings := newTest.BackExplorationFindings()
	backPalpationFindings := newTest.BackPalpationFindings()
	jointsExplorationFindings := newTest.JointsExplorationFindings()

	if painMobilityFindings || backExplorationFindings || backPalpationFindings || jointsExplorationFindings {
		return "Con hallazgos"
	} else {
		return "Sin hallazgos"
	}

}
func (newTest *SkeletalMuscleTest) PainMobilityFindings() bool {

	if newTest.PainMobility.Cervical.Flexion == "Dolor" ||
		newTest.PainMobility.Cervical.Extension == "Dolor" ||
		newTest.PainMobility.Cervical.RightLateralization == "Dolor" ||
		newTest.PainMobility.Cervical.LeftLateralization == "Dolor" ||
		newTest.PainMobility.Cervical.RightRotation == "Dolor" ||
		newTest.PainMobility.Cervical.LeftRotation == "Dolor" ||
		newTest.PainMobility.Cervical.FunctionalLimitation == "Si" ||
		newTest.PainMobility.Cervical.RadiatingPain == "Si" ||
		newTest.PainMobility.Dorsal.Flexion == "Dolor" ||
		newTest.PainMobility.Dorsal.Extension == "Dolor" ||
		newTest.PainMobility.Dorsal.RightLateralization == "Dolor" ||
		newTest.PainMobility.Dorsal.LeftLateralization == "Dolor" ||
		newTest.PainMobility.Dorsal.RightRotation == "Dolor" ||
		newTest.PainMobility.Dorsal.LeftRotation == "Dolor" ||
		newTest.PainMobility.Dorsal.FunctionalLimitation == "Si" ||
		newTest.PainMobility.Dorsal.RadiatingPain == "Si" ||
		newTest.PainMobility.Lumbar.Flexion == "Dolor" ||
		newTest.PainMobility.Lumbar.Extension == "Dolor" ||
		newTest.PainMobility.Lumbar.RightLateralization == "Dolor" ||
		newTest.PainMobility.Lumbar.LeftLateralization == "Dolor" ||
		newTest.PainMobility.Lumbar.RightRotation == "Dolor" ||
		newTest.PainMobility.Lumbar.LeftRotation == "Dolor" ||
		newTest.PainMobility.Lumbar.FunctionalLimitation == "Si" ||
		newTest.PainMobility.Lumbar.RadiatingPain == "Si" {
		return true

	} else {
		return false
	}

}
func (newTest *SkeletalMuscleTest) BackExplorationFindings() bool {

	if newTest.BackExploration.SchoberTest == "Positivo" || newTest.BackExploration.LassegueTest.Right == "Positivo" || newTest.BackExploration.LassegueTest.Left == "Positivo" {
		return true

	} else {
		return false
	}

}
func (newTest *SkeletalMuscleTest) BackPalpationFindings() bool {

	if newTest.BackPalpation.Cervical.MuscleContracture == "Positivo" ||
		newTest.BackPalpation.Cervical.PainfulSpinousApophysis == "Positivo" ||
		newTest.BackPalpation.Dorsal.MuscleContracture == "Positivo" ||
		newTest.BackPalpation.Dorsal.PainfulSpinousApophysis == "Positivo" ||
		newTest.BackPalpation.Lumbar.MuscleContracture == "Positivo" ||
		newTest.BackPalpation.Lumbar.PainfulSpinousApophysis == "Positivo" {
		return true
	} else {
		return false
	}

}
func (newTest *SkeletalMuscleTest) JointsExplorationFindings() bool {

	if (newTest.JointsExploration.Shoulder.Right.Abduction != "0" && newTest.JointsExploration.Shoulder.Right.Abduction != "") ||
		(newTest.JointsExploration.Shoulder.Right.Adduction != "0" && newTest.JointsExploration.Shoulder.Right.Adduction != "") ||
		(newTest.JointsExploration.Shoulder.Right.Flexion != "0" && newTest.JointsExploration.Shoulder.Right.Flexion != "") ||
		(newTest.JointsExploration.Shoulder.Right.Extension != "0" && newTest.JointsExploration.Shoulder.Right.Extension != "") ||
		(newTest.JointsExploration.Shoulder.Right.ExternalRotation != "0" && newTest.JointsExploration.Shoulder.Right.ExternalRotation != "") ||
		(newTest.JointsExploration.Shoulder.Right.InternalRotation != "0" && newTest.JointsExploration.Shoulder.Right.InternalRotation != "") ||
		newTest.JointsExploration.Shoulder.Right.FunctionalLimitation == "Si" ||
		newTest.JointsExploration.Shoulder.Right.RadiatingPain == "Si" ||
		newTest.JointsExploration.Shoulder.Right.MuscleMassAlterations == "Si" ||
		(newTest.JointsExploration.Shoulder.Left.Abduction != "0" && newTest.JointsExploration.Shoulder.Left.Abduction != "") ||
		(newTest.JointsExploration.Shoulder.Left.Adduction != "0" && newTest.JointsExploration.Shoulder.Left.Adduction != "") ||
		(newTest.JointsExploration.Shoulder.Left.Flexion != "0" && newTest.JointsExploration.Shoulder.Left.Flexion != "") ||
		(newTest.JointsExploration.Shoulder.Left.Extension != "0" && newTest.JointsExploration.Shoulder.Left.Extension != "") ||
		(newTest.JointsExploration.Shoulder.Left.ExternalRotation != "0" && newTest.JointsExploration.Shoulder.Left.ExternalRotation != "") ||
		(newTest.JointsExploration.Shoulder.Left.InternalRotation != "0" && newTest.JointsExploration.Shoulder.Left.InternalRotation != "") ||
		newTest.JointsExploration.Shoulder.Left.FunctionalLimitation == "Si" ||
		newTest.JointsExploration.Shoulder.Left.RadiatingPain == "Si" ||
		newTest.JointsExploration.Shoulder.Left.MuscleMassAlterations == "Si" ||
		(newTest.JointsExploration.Elbow.Right.Abduction != "0" && newTest.JointsExploration.Elbow.Right.Abduction != "") ||
		(newTest.JointsExploration.Elbow.Right.Adduction != "0" && newTest.JointsExploration.Elbow.Right.Adduction != "") ||
		(newTest.JointsExploration.Elbow.Right.Flexion != "0" && newTest.JointsExploration.Elbow.Right.Flexion != "") ||
		(newTest.JointsExploration.Elbow.Right.Extension != "0" && newTest.JointsExploration.Elbow.Right.Extension != "") ||
		(newTest.JointsExploration.Elbow.Right.ExternalRotation != "0" && newTest.JointsExploration.Elbow.Right.ExternalRotation != "") ||
		(newTest.JointsExploration.Elbow.Right.InternalRotation != "0" && newTest.JointsExploration.Elbow.Right.InternalRotation != "") ||
		newTest.JointsExploration.Elbow.Right.FunctionalLimitation == "Si" ||
		newTest.JointsExploration.Elbow.Right.RadiatingPain == "Si" ||
		newTest.JointsExploration.Elbow.Right.MuscleMassAlterations == "Si" ||
		(newTest.JointsExploration.Elbow.Left.Abduction != "0" && newTest.JointsExploration.Elbow.Left.Abduction != "") ||
		(newTest.JointsExploration.Elbow.Left.Adduction != "0" && newTest.JointsExploration.Elbow.Left.Adduction != "") ||
		(newTest.JointsExploration.Elbow.Left.Flexion != "0" && newTest.JointsExploration.Elbow.Left.Flexion != "") ||
		(newTest.JointsExploration.Elbow.Left.Extension != "0" && newTest.JointsExploration.Elbow.Left.Extension != "") ||
		(newTest.JointsExploration.Elbow.Left.ExternalRotation != "0" && newTest.JointsExploration.Elbow.Left.ExternalRotation != "") ||
		(newTest.JointsExploration.Elbow.Left.InternalRotation != "0" && newTest.JointsExploration.Elbow.Left.InternalRotation != "") ||
		newTest.JointsExploration.Elbow.Left.FunctionalLimitation == "Si" ||
		newTest.JointsExploration.Elbow.Left.RadiatingPain == "Si" ||
		newTest.JointsExploration.Elbow.Left.MuscleMassAlterations == "Si" ||
		(newTest.JointsExploration.Wrist.Right.Abduction != "0" && newTest.JointsExploration.Wrist.Right.Abduction != "") ||
		(newTest.JointsExploration.Wrist.Right.Adduction != "0" && newTest.JointsExploration.Wrist.Right.Adduction != "") ||
		(newTest.JointsExploration.Wrist.Right.Flexion != "0" && newTest.JointsExploration.Wrist.Right.Flexion != "") ||
		(newTest.JointsExploration.Wrist.Right.Extension != "0" && newTest.JointsExploration.Wrist.Right.Extension != "") ||
		(newTest.JointsExploration.Wrist.Right.ExternalRotation != "0" && newTest.JointsExploration.Wrist.Right.ExternalRotation != "") ||
		(newTest.JointsExploration.Wrist.Right.InternalRotation != "0" && newTest.JointsExploration.Wrist.Right.InternalRotation != "") ||
		newTest.JointsExploration.Wrist.Right.FunctionalLimitation == "Si" ||
		newTest.JointsExploration.Wrist.Right.RadiatingPain == "Si" ||
		newTest.JointsExploration.Wrist.Right.MuscleMassAlterations == "Si" ||
		(newTest.JointsExploration.Wrist.Left.Abduction != "0" && newTest.JointsExploration.Wrist.Left.Abduction != "") ||
		(newTest.JointsExploration.Wrist.Left.Adduction != "0" && newTest.JointsExploration.Wrist.Left.Adduction != "") ||
		(newTest.JointsExploration.Wrist.Left.Flexion != "0" && newTest.JointsExploration.Wrist.Left.Flexion != "") ||
		(newTest.JointsExploration.Wrist.Left.Extension != "0" && newTest.JointsExploration.Wrist.Left.Extension != "") ||
		(newTest.JointsExploration.Wrist.Left.ExternalRotation != "0" && newTest.JointsExploration.Wrist.Left.ExternalRotation != "") ||
		(newTest.JointsExploration.Wrist.Left.InternalRotation != "0" && newTest.JointsExploration.Wrist.Left.InternalRotation != "") ||
		newTest.JointsExploration.Wrist.Left.FunctionalLimitation == "Si" ||
		newTest.JointsExploration.Wrist.Left.RadiatingPain == "Si" ||
		newTest.JointsExploration.Wrist.Left.MuscleMassAlterations == "Si" ||
		(newTest.JointsExploration.HandsAndFingers.Right.Abduction != "0" && newTest.JointsExploration.HandsAndFingers.Right.Abduction != "") ||
		(newTest.JointsExploration.HandsAndFingers.Right.Adduction != "0" && newTest.JointsExploration.HandsAndFingers.Right.Adduction != "") ||
		(newTest.JointsExploration.HandsAndFingers.Right.Flexion != "0" && newTest.JointsExploration.HandsAndFingers.Right.Flexion != "") ||
		(newTest.JointsExploration.HandsAndFingers.Right.Extension != "0" && newTest.JointsExploration.HandsAndFingers.Right.Extension != "") ||
		(newTest.JointsExploration.HandsAndFingers.Right.ExternalRotation != "0" && newTest.JointsExploration.HandsAndFingers.Right.ExternalRotation != "") ||
		(newTest.JointsExploration.HandsAndFingers.Right.InternalRotation != "0" && newTest.JointsExploration.HandsAndFingers.Right.InternalRotation != "") ||
		newTest.JointsExploration.HandsAndFingers.Right.FunctionalLimitation == "Si" ||
		newTest.JointsExploration.HandsAndFingers.Right.RadiatingPain == "Si" ||
		newTest.JointsExploration.HandsAndFingers.Right.MuscleMassAlterations == "Si" ||
		(newTest.JointsExploration.HandsAndFingers.Left.Abduction != "0" && newTest.JointsExploration.HandsAndFingers.Left.Abduction != "") ||
		(newTest.JointsExploration.HandsAndFingers.Left.Adduction != "0" && newTest.JointsExploration.HandsAndFingers.Left.Adduction != "") ||
		(newTest.JointsExploration.HandsAndFingers.Left.Flexion != "0" && newTest.JointsExploration.HandsAndFingers.Left.Flexion != "") ||
		(newTest.JointsExploration.HandsAndFingers.Left.Extension != "0" && newTest.JointsExploration.HandsAndFingers.Left.Extension != "") ||
		(newTest.JointsExploration.HandsAndFingers.Left.ExternalRotation != "0" && newTest.JointsExploration.HandsAndFingers.Left.ExternalRotation != "") ||
		(newTest.JointsExploration.HandsAndFingers.Left.InternalRotation != "0" && newTest.JointsExploration.HandsAndFingers.Left.InternalRotation != "") ||
		newTest.JointsExploration.HandsAndFingers.Left.FunctionalLimitation == "Si" ||
		newTest.JointsExploration.HandsAndFingers.Left.RadiatingPain == "Si" ||
		newTest.JointsExploration.HandsAndFingers.Left.MuscleMassAlterations == "Si" ||
		(newTest.JointsExploration.Hips.Right.Abduction != "0" && newTest.JointsExploration.Hips.Right.Abduction != "") ||
		(newTest.JointsExploration.Hips.Right.Adduction != "0" && newTest.JointsExploration.Hips.Right.Adduction != "") ||
		(newTest.JointsExploration.Hips.Right.Flexion != "0" && newTest.JointsExploration.Hips.Right.Flexion != "") ||
		(newTest.JointsExploration.Hips.Right.Extension != "0" && newTest.JointsExploration.Hips.Right.Extension != "") ||
		(newTest.JointsExploration.Hips.Right.ExternalRotation != "0" && newTest.JointsExploration.Hips.Right.ExternalRotation != "") ||
		(newTest.JointsExploration.Hips.Right.InternalRotation != "0" && newTest.JointsExploration.Hips.Right.InternalRotation != "") ||
		newTest.JointsExploration.Hips.Right.FunctionalLimitation == "Si" ||
		newTest.JointsExploration.Hips.Right.RadiatingPain == "Si" ||
		newTest.JointsExploration.Hips.Right.MuscleMassAlterations == "Si" ||
		(newTest.JointsExploration.Hips.Left.Abduction != "0" && newTest.JointsExploration.Hips.Left.Abduction != "") ||
		(newTest.JointsExploration.Hips.Left.Adduction != "0" && newTest.JointsExploration.Hips.Left.Adduction != "") ||
		(newTest.JointsExploration.Hips.Left.Flexion != "0" && newTest.JointsExploration.Hips.Left.Flexion != "") ||
		(newTest.JointsExploration.Hips.Left.Extension != "0" && newTest.JointsExploration.Hips.Left.Extension != "") ||
		(newTest.JointsExploration.Hips.Left.ExternalRotation != "0" && newTest.JointsExploration.Hips.Left.ExternalRotation != "") ||
		(newTest.JointsExploration.Hips.Left.InternalRotation != "0" && newTest.JointsExploration.Hips.Left.InternalRotation != "") ||
		newTest.JointsExploration.Hips.Left.FunctionalLimitation == "Si" ||
		newTest.JointsExploration.Hips.Left.RadiatingPain == "Si" ||
		newTest.JointsExploration.Hips.Left.MuscleMassAlterations == "Si" ||
		(newTest.JointsExploration.Knee.Right.Abduction != "0" && newTest.JointsExploration.Knee.Right.Abduction != "") ||
		(newTest.JointsExploration.Knee.Right.Adduction != "0" && newTest.JointsExploration.Knee.Right.Adduction != "") ||
		(newTest.JointsExploration.Knee.Right.Flexion != "0" && newTest.JointsExploration.Knee.Right.Flexion != "") ||
		(newTest.JointsExploration.Knee.Right.Extension != "0" && newTest.JointsExploration.Knee.Right.Extension != "") ||
		(newTest.JointsExploration.Knee.Right.ExternalRotation != "0" && newTest.JointsExploration.Knee.Right.ExternalRotation != "") ||
		(newTest.JointsExploration.Knee.Right.InternalRotation != "0" && newTest.JointsExploration.Knee.Right.InternalRotation != "") ||
		newTest.JointsExploration.Knee.Right.FunctionalLimitation == "Si" ||
		newTest.JointsExploration.Knee.Right.RadiatingPain == "Si" ||
		newTest.JointsExploration.Knee.Right.MuscleMassAlterations == "Si" ||
		(newTest.JointsExploration.Knee.Left.Abduction != "0" && newTest.JointsExploration.Knee.Left.Abduction != "") ||
		(newTest.JointsExploration.Knee.Left.Adduction != "0" && newTest.JointsExploration.Knee.Left.Adduction != "") ||
		(newTest.JointsExploration.Knee.Left.Flexion != "0" && newTest.JointsExploration.Knee.Left.Flexion != "") ||
		(newTest.JointsExploration.Knee.Left.Extension != "0" && newTest.JointsExploration.Knee.Left.Extension != "") ||
		(newTest.JointsExploration.Knee.Left.ExternalRotation != "0" && newTest.JointsExploration.Knee.Left.ExternalRotation != "") ||
		(newTest.JointsExploration.Knee.Left.InternalRotation != "0" && newTest.JointsExploration.Knee.Left.InternalRotation != "") ||
		newTest.JointsExploration.Knee.Left.FunctionalLimitation == "Si" ||
		newTest.JointsExploration.Knee.Left.RadiatingPain == "Si" ||
		newTest.JointsExploration.Knee.Left.MuscleMassAlterations == "Si" ||
		(newTest.JointsExploration.Ankle.Right.Abduction != "0" && newTest.JointsExploration.Ankle.Right.Abduction != "") ||
		(newTest.JointsExploration.Ankle.Right.Adduction != "0" && newTest.JointsExploration.Ankle.Right.Adduction != "") ||
		(newTest.JointsExploration.Ankle.Right.Flexion != "0" && newTest.JointsExploration.Ankle.Right.Flexion != "") ||
		(newTest.JointsExploration.Ankle.Right.Extension != "0" && newTest.JointsExploration.Ankle.Right.Extension != "") ||
		(newTest.JointsExploration.Ankle.Right.ExternalRotation != "0" && newTest.JointsExploration.Ankle.Right.ExternalRotation != "") ||
		(newTest.JointsExploration.Ankle.Right.InternalRotation != "0" && newTest.JointsExploration.Ankle.Right.InternalRotation != "") ||
		newTest.JointsExploration.Ankle.Right.FunctionalLimitation == "Si" ||
		newTest.JointsExploration.Ankle.Right.RadiatingPain == "Si" ||
		newTest.JointsExploration.Ankle.Right.MuscleMassAlterations == "Si" ||
		(newTest.JointsExploration.Ankle.Left.Abduction != "0" && newTest.JointsExploration.Ankle.Left.Abduction != "") ||
		(newTest.JointsExploration.Ankle.Left.Adduction != "0" && newTest.JointsExploration.Ankle.Left.Adduction != "") ||
		(newTest.JointsExploration.Ankle.Left.Flexion != "0" && newTest.JointsExploration.Ankle.Left.Flexion != "") ||
		(newTest.JointsExploration.Ankle.Left.Extension != "0" && newTest.JointsExploration.Ankle.Left.Extension != "") ||
		(newTest.JointsExploration.Ankle.Left.ExternalRotation != "0" && newTest.JointsExploration.Ankle.Left.ExternalRotation != "") ||
		(newTest.JointsExploration.Ankle.Left.InternalRotation != "0" && newTest.JointsExploration.Ankle.Left.InternalRotation != "") ||
		newTest.JointsExploration.Ankle.Left.FunctionalLimitation == "Si" ||
		newTest.JointsExploration.Ankle.Left.RadiatingPain == "Si" ||
		newTest.JointsExploration.Ankle.Left.MuscleMassAlterations == "Si" {
		return true
	} else {
		return false
	}

}
