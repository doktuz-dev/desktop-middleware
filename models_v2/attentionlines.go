package models_v2

import "time"

type AttentionLine struct {
	ID              string       `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionId     string       `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	ServiceID       string       `json:"serviceId,omitempty" bson:"serviceId,omitempty"`
	Status          int          `json:"status" bson:"status"`
	ExternalId      string       `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem  string       `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate     time.Time    `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate time.Time    `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
	Service         *Service     `json:"service,omitempty" bson:"service,omitempty"`
	ServiceArea     *ServiceArea `json:"serviceArea,omitempty" bson:"serviceArea,omitempty"`
}
