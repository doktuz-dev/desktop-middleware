package models_v2

import "time"

type Scheduling struct {
	ID                              string              `json:"_id" bson:"_id,omitempty"`
	ExternalSchedulingId            int                 `json:"externalSchedulingId,omitempty" bson:"externalSchedulingId,omitempty"`
	ExternalSchedulingSystem        string              `json:"externalSchedulingSystem,omitempty" bson:"externalSchedulingSystem,omitempty"`
	ExternalSchedulingStatusMessage string              `json:"externalSchedulingStatusMessage,omitempty" bson:"externalSchedulingStatusMessage,omitempty"`
	AttentionDate                   string              `json:"attentionDate" bson:"attentionDate,omitempty"`
	PatientID                       string              `json:"patientId" bson:"patientId,omitempty"`
	FirstName                       string              `json:"firstName" bson:"firstName,omitempty"`
	LastName                        string              `json:"lastName" bson:"lastName,omitempty"`
	DocumentType                    string              `json:"documentType" bson:"documentType,omitempty"`
	DocumentNumber                  string              `json:"documentNumber" bson:"documentNumber,omitempty"`
	BirthDate                       time.Time           `json:"birthDate" bson:"birthDate,omitempty"`
	Sex                             string              `json:"sex" bson:"sex,omitempty"`
	Email                           string              `json:"email" bson:"email,omitempty"`
	Phone                           string              `json:"phone" bson:"phone,omitempty"`
	ProtocolID                      string              `json:"protocolId" bson:"protocolId,omitempty"`
	ProtocolDescription             string              `json:"protocolDescription" bson:"protocolDescription,omitempty"`
	TestTypeID                      string              `json:"testTypeId" bson:"testTypeId,omitempty"`
	TestTypeDescription             string              `json:"testTypeDescription" bson:"testTypeDescription,omitempty"`
	HeadquarterId                   string              `json:"headquarterId" bson:"headquarterId,omitempty"`
	HeadquarterCode                 string              `json:"headquarterCode" bson:"headquarterCode,omitempty"`
	CustomerID                      string              `json:"companyId,omitempty" bson:"companyId,omitempty"`
	WorkPlace                       string              `json:"workPlace,omitempty" bson:"workPlace,omitempty"`
	WorkingAreaName                 string              `json:"workingAreaName,omitempty" bson:"workingAreaName,omitempty"`
	ProjectID                       string              `json:"projectId,omitempty" bson:"projectId,omitempty"`
	ProjectDescription              string              `json:"projectDescription,omitempty" bson:"projectDescription,omitempty"`
	UserID                          string              `json:"userId,omitempty" bson:"userId,omitempty"`
	ExternalId                      string              `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem                  string              `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	NotificationStatusID            string              `json:"notificationStatusId,omitempty" bson:"notificationStatusId,omitempty"`
	User                            *LoginUser          `json:"user,omitempty" bson:"user,omitempty"`
	NotificationStatus              *NotificationStatus `json:"notificationStatus,omitempty" bson:"notificationStatus,omitempty"`
	CreatedDate                     time.Time           `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate                 time.Time           `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}

type NoEmoScheduling struct {
	ID                              string                     `json:"_id" bson:"_id,omitempty"`
	ExternalSchedulingId            int                        `json:"externalSchedulingId,omitempty" bson:"externalSchedulingId,omitempty"`
	ExternalSchedulingSystem        string                     `json:"externalSchedulingSystem,omitempty" bson:"externalSchedulingSystem,omitempty"`
	ExternalSchedulingStatusMessage string                     `json:"externalSchedulingStatusMessage,omitempty" bson:"externalSchedulingStatusMessage,omitempty"`
	AttentionDate                   string                     `json:"attentionDate" bson:"attentionDate,omitempty"`
	PatientID                       string                     `json:"patientId" bson:"patientId,omitempty"`
	FirstName                       string                     `json:"firstName" bson:"firstName,omitempty"`
	LastName                        string                     `json:"lastName" bson:"lastName,omitempty"`
	DocumentType                    string                     `json:"documentType" bson:"documentType,omitempty"`
	DocumentNumber                  string                     `json:"documentNumber" bson:"documentNumber,omitempty"`
	BirthDate                       time.Time                  `json:"birthDate" bson:"birthDate,omitempty"`
	Sex                             string                     `json:"sex" bson:"sex,omitempty"`
	Email                           string                     `json:"email" bson:"email,omitempty"`
	Phone                           string                     `json:"phone" bson:"phone,omitempty"`
	HeadquarterId                   string                     `json:"headquarterId" bson:"headquarterId,omitempty"`
	HeadquarterCode                 string                     `json:"headquarterCode" bson:"headquarterCode,omitempty"`
	HeadquarterDescription          string                     `json:"healthcentreDescription" bson:"healthcentreDescription,omitempty"`
	CustomerID                      string                     `json:"companyId,omitempty" bson:"companyId,omitempty"`
	UserID                          string                     `json:"userId,omitempty" bson:"userId,omitempty"`
	WorkPlace                       string                     `json:"workPlace,omitempty" bson:"workPlace,omitempty"`
	WorkingAreaName                 string                     `json:"workingAreaName,omitempty" bson:"workingAreaName,omitempty"`
	ProjectID                       string                     `json:"projectId,omitempty" bson:"projectId,omitempty"`
	ProjectDescription              string                     `json:"projectDescription,omitempty" bson:"projectDescription,omitempty"`
	ScheduleStatus                  string                     `json:"scheduleStatus" bson:"-"`
	NoEmoSchedulingServices         []*NoEmoSchedulingServices `json:"noEmoSchedulingServices,omitempty" bson:"noEmoSchedulingServices,omitempty"`
	NotificationStatusID            string                     `json:"notificationStatusId,omitempty" bson:"notificationStatusId,omitempty"`
	TestID                          string                     `json:"testId" bson:"testId,omitempty"`
	TestName                        string                     `json:"testName" bson:"testName,omitempty"`
	User                            *LoginUser                 `json:"user,omitempty" bson:"user,omitempty"`
	NotificationStatus              *NotificationStatus        `json:"notificationStatus,omitempty" bson:"notificationStatus,omitempty"`
	CreatedDate                     time.Time                  `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate                 time.Time                  `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}

type NotificationStatusMessage struct {
	SSID          string `json:"ssid" bson:"ssid,omitempty"`
	Status        string `json:"status" bson:"status,omitempty"`
	StatusMessage string `json:"statusMessage" bson:"statusMessage,omitempty"`
}

type NoEmoSchedulingServices struct {
	ServiceID          string `json:"serviceId" bson:"serviceId,omitempty"`
	ServiceDescription string `json:"serviceDescription" bson:"serviceDescription,omitempty"`
}

type NotificationStatus struct {
	ID              string                     `json:"_id" bson:"_id,omitempty"`
	CollectionName  string                     `json:"collectionName,omitempty" bson:"collectionName,omitempty"`
	Email           *NotificationStatusMessage `json:"email,omitempty" bson:"email,omitempty"`
	Whatsapp        *NotificationStatusMessage `json:"whatsapp,omitempty" bson:"whatsapp,omitempty"`
	CreatedDate     time.Time                  `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate time.Time                  `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}

type ReadResultScheduling struct {
	ID                          string              `json:"_id" bson:"_id,omitempty"`
	AttentionID                 string              `json:"attentionId" bson:"attentionId,omitempty"`
	PersonID                    string              `json:"personId" bson:"personId,omitempty"`
	UserID                      string              `json:"userId" bson:"userId,omitempty"`
	CustomerId                  string              `json:"customerId,omitempty" bson:"customerId,omitempty"`
	Type                        string              `json:"type" bson:"type,omitempty"`
	SchedulingDate              string              `json:"schedulingDate" bson:"schedulingDate,omitempty"`
	SchedulingHour              string              `json:"schedulingHour" bson:"schedulingHour,omitempty"`
	Duration                    float64             `json:"duration" bson:"duration,omitempty"`
	Address                     string              `json:"address" bson:"address,omitempty"`
	Platform                    string              `json:"platform" bson:"platform,omitempty"`
	Token                       string              `json:"token" bson:"token,omitempty"`
	Link                        string              `json:"link" bson:"link,omitempty"`
	SendNotification            bool                `json:"sendNotification" bson:"sendNotification"`
	AttachMedicalChart          bool                `json:"attachMedicalChart" bson:"attachMedicalChart"`
	AttachMedicalResult         bool                `json:"attachMedicalResult" bson:"attachMedicalResult"`
	AttachCompetencyCertificate bool                `json:"attachCompetencyCertificate" bson:"attachCompetencyCertificate"`
	Status                      string              `json:"status" bson:"status,omitempty"`
	NotificationStatusID        string              `json:"notificationStatusId,omitempty" bson:"notificationStatusId,omitempty"`
	User                        *LoginUser          `json:"user,omitempty" bson:"user,omitempty"`
	NotificationStatus          *NotificationStatus `json:"notificationStatus,omitempty" bson:"notificationStatus,omitempty"`
	CreatedDate                 time.Time           `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate             time.Time           `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}
