package models_v2

import "time"

type JobTitle struct {
	ID                      string    `json:"_id" bson:"_id"`
	WorkingAreaID           string    `json:"workingAreaId,omitempty" bson:"workingAreaId,omitempty"`
	Description             string    `json:"description,omitempty" bson:"description,omitempty"`
	Enabled                 bool      `json:"enabled" bson:"enabled"`
	OccupationalHazards     []string  `json:"occupationalHazards,omitempty" bson:"occupationalHazards,omitempty"`
	OccupationalHazardNames []string  `json:"occupationalHazardNames,omitempty" bson:"occupationalHazardNames,omitempty"`
	ExternalId              string    `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem          string    `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate             time.Time `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate         time.Time `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}
