package models_v2

import "time"

type Laboratory struct {
	ID              string `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionId     string `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	ServiceAreaID   string `json:"serviceAreaId,omitempty" bson:"serviceAreaId,omitempty"`
	ServiceAreaCode string `json:"serviceAreaCode,omitempty" bson:"serviceAreaCode,omitempty"`
	Diagnostic1     string `json:"diagnostic1,omitempty" bson:"diagnostic1,omitempty"`
	Diagnostic2     string `json:"diagnostic2,omitempty" bson:"diagnostic2,omitempty"`
	Diagnostic3     string `json:"diagnostic3,omitempty" bson:"diagnostic3,omitempty"`
	Diagnostic4     string `json:"diagnostic4,omitempty" bson:"diagnostic4,omitempty"`
	Diagnostic5     string `json:"diagnostic5,omitempty" bson:"diagnostic5,omitempty"`
	Status          int    `json:"status" bson:"status"`
	Reviewed        bool   `json:"reviewed,omitempty" bson:"reviewed,omitempty"`
}

type HematologyTest struct {
	ID                   string    `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionId          string    `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	ServiceAreaID        string    `json:"serviceAreaId,omitempty" bson:"serviceAreaId,omitempty"`
	ServiceAreaCode      string    `json:"serviceAreaCode,omitempty" bson:"serviceAreaCode,omitempty"`
	ExamID               string    `json:"examId,omitempty" bson:"examId,omitempty"`
	BloodCountResult     string    `json:"bloodCountResult,omitempty" bson:"bloodCountResult,omitempty"`
	LeukocyteCount       float64   `json:"leukocyteCount,omitempty" bson:"leukocyteCount,omitempty"`
	HemoglobinCount      float64   `json:"hemoglobinCount,omitempty" bson:"hemoglobinCount,omitempty"`
	HematocritCount      float64   `json:"hematocritCount,omitempty" bson:"hematocritCount,omitempty"`
	RedCellsCount        float64   `json:"redCellsCount,omitempty" bson:"redCellsCount,omitempty"`
	BandNeutrophilsCount string    `json:"bandNeutrophilsCount,omitempty" bson:"bandNeutrophilsCount,omitempty"`
	SegmentedCount       float64   `json:"segmentedCount,omitempty" bson:"segmentedCount,omitempty"`
	EosinophilCount      float64   `json:"eosinophilCount,omitempty" bson:"eosinophilCount,omitempty"`
	BasophilsCount       float64   `json:"basophilsCount,omitempty" bson:"basophilsCount,omitempty"`
	MonocytesCount       float64   `json:"monocytesCount,omitempty" bson:"monocytesCount,omitempty"`
	LymphocytesCount     float64   `json:"lymphocytesCount,omitempty" bson:"lymphocytesCount,omitempty"`
	VcmCount             float64   `json:"vcmCount,omitempty" bson:"vcmCount,omitempty"`
	HcmCount             float64   `json:"hcmCount,omitempty" bson:"hcmCount,omitempty"`
	PlateletsCount       float64   `json:"plateletsCount,omitempty" bson:"plateletsCount,omitempty"`
	BloodType            string    `json:"bloodType,omitempty" bson:"bloodType,omitempty"`
	BloodFactor          string    `json:"bloodFactor,omitempty" bson:"bloodFactor,omitempty"`
	Status               int       `json:"status" bson:"status"`
	ExternalId           string    `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem       string    `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate          time.Time `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate      time.Time `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}

type UrineTest struct {
	ID              string    `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionId     string    `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	ServiceAreaID   string    `json:"serviceAreaId,omitempty" bson:"serviceAreaId,omitempty"`
	ServiceAreaCode string    `json:"serviceAreaCode,omitempty" bson:"serviceAreaCode,omitempty"`
	ExamID          string    `json:"examId,omitempty" bson:"examId,omitempty"`
	Result          string    `json:"result,omitempty" bson:"result,omitempty"`
	Status          int       `json:"status" bson:"status"`
	ExternalId      string    `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem  string    `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate     time.Time `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate time.Time `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}

type DrugTest struct {
	ID                    string    `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionId           string    `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	ServiceAreaID         string    `json:"serviceAreaId,omitempty" bson:"serviceAreaId,omitempty"`
	ServiceAreaCode       string    `json:"serviceAreaCode,omitempty" bson:"serviceAreaCode,omitempty"`
	ExamID                string    `json:"examId,omitempty" bson:"examId,omitempty"`
	CocaineResult         string    `json:"cocaineResult,omitempty" bson:"cocaineResult,omitempty"`
	MarijuanaResult       string    `json:"marijuanaResult,omitempty" bson:"marijuanaResult,omitempty"`
	AmphetamineResult     string    `json:"amphetamineResult,omitempty" bson:"amphetamineResult,omitempty"`
	MethamphetamineResult string    `json:"methamphetamineResult,omitempty" bson:"methamphetamineResult,omitempty"`
	BenzodiazepineResult  string    `json:"benzodiazepineResult,omitempty" bson:"benzodiazepineResult,omitempty"`
	EcstasyResult         string    `json:"ecstasyResult,omitempty" bson:"ecstasyResult,omitempty"`
	AlcoholResult         string    `json:"alcoholResult,omitempty" bson:"alcoholResult,omitempty"`
	LeadResult            string    `json:"leadResult,omitempty" bson:"leadResult,omitempty"`
	Status                int       `json:"status" bson:"status"`
	ExternalId            string    `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem        string    `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate           time.Time `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate       time.Time `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}

type MicrobiologyTest struct {
	ID                               string    `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionId                      string    `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	ServiceAreaID                    string    `json:"serviceAreaId,omitempty" bson:"serviceAreaId,omitempty"`
	ServiceAreaCode                  string    `json:"serviceAreaCode,omitempty" bson:"serviceAreaCode,omitempty"`
	ExamID                           string    `json:"examId,omitempty" bson:"examId,omitempty"`
	SputumSampleResult               []string  `json:"sputumSampleResult,omitempty" bson:"sputumSampleResult,omitempty"`
	ParasiteResult                   []string  `json:"parasiteResult,omitempty" bson:"parasiteResult,omitempty"`
	StoolCultureResult               string    `json:"stoolCultureResult,omitempty" bson:"stoolCultureResult,omitempty"`
	UrineCultureResult               string    `json:"urineCultureResult,omitempty" bson:"urineCultureResult,omitempty"`
	PharyngealSecretionCultureResult string    `json:"pharyngealSecretionCultureResult,omitempty" bson:"pharyngealSecretionCultureResult,omitempty"`
	Status                           int       `json:"status" bson:"status"`
	ExternalId                       string    `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem                   string    `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate                      time.Time `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate                  time.Time `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}

type BiochemistryTest struct {
	ID                string    `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionId       string    `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	ServiceAreaID     string    `json:"serviceAreaId,omitempty" bson:"serviceAreaId,omitempty"`
	ServiceAreaCode   string    `json:"serviceAreaCode,omitempty" bson:"serviceAreaCode,omitempty"`
	ExamID            string    `json:"examId,omitempty" bson:"examId,omitempty"`
	TgoCount          float64   `json:"tgoCount,omitempty" bson:"tgoCount,omitempty"`
	TgpCount          float64   `json:"tgpCount,omitempty" bson:"tgpCount,omitempty"`
	UreaCount         float64   `json:"ureaCount,omitempty" bson:"ureaCount,omitempty"`
	CreatinineCount   float64   `json:"creatinineCount,omitempty" bson:"creatinineCount,omitempty"`
	GlucoseCount      float64   `json:"glucoseCount,omitempty" bson:"glucoseCount,omitempty"`
	CholesterolCount  float64   `json:"cholesterolCount,omitempty" bson:"cholesterolCount,omitempty"`
	TriglycerideCount float64   `json:"triglycerideCount,omitempty" bson:"triglycerideCount,omitempty"`
	HdlCount          float64   `json:"hdlCount,omitempty" bson:"hdlCount,omitempty"`
	LdlCount          float64   `json:"ldlCount,omitempty" bson:"ldlCount,omitempty"`
	Status            int       `json:"status" bson:"status"`
	ExternalId        string    `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem    string    `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate       time.Time `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate   time.Time `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}

type ImmunologyTest struct {
	ID                 string    `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionId        string    `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	ServiceAreaID      string    `json:"serviceAreaId,omitempty" bson:"serviceAreaId,omitempty"`
	ServiceAreaCode    string    `json:"serviceAreaCode,omitempty" bson:"serviceAreaCode,omitempty"`
	ExamID             string    `json:"examId,omitempty" bson:"examId,omitempty"`
	HaigmResult        string    `json:"haigmResult,omitempty" bson:"haigmResult,omitempty"`
	HaiggResult        string    `json:"haiggResult,omitempty" bson:"haiggResult,omitempty"`
	HbsagResult        string    `json:"hbsagResult,omitempty" bson:"hbsagResult,omitempty"`
	HepatitisCResult   string    `json:"hepatitisCResult,omitempty" bson:"hepatitisCResult,omitempty"`
	TificooResult      string    `json:"tificooResult,omitempty" bson:"tificooResult,omitempty"`
	TificohCResult     string    `json:"tificohCResult,omitempty" bson:"tificohCResult,omitempty"`
	ParatificoaResult  string    `json:"paratificoaResult,omitempty" bson:"paratificoaResult,omitempty"`
	ParatificobResult  string    `json:"paratificobResult,omitempty" bson:"paratificobResult,omitempty"`
	BrucellasSspResult string    `json:"brucellasSspResult,omitempty" bson:"brucellasSspResult,omitempty"`
	PsaResult          string    `json:"psaResult,omitempty" bson:"psaResult,omitempty"`
	VdrlResult         string    `json:"vdrlResult,omitempty" bson:"vdrlResult,omitempty"`
	RprResult          string    `json:"rprResult,omitempty" bson:"rprResult,omitempty"`
	Status             int       `json:"status" bson:"status"`
	ExternalId         string    `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem     string    `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate        time.Time `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate    time.Time `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}

type CovidTest struct {
	ID                            string    `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionId                   string    `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	ServiceAreaID                 string    `json:"serviceAreaId,omitempty" bson:"serviceAreaId,omitempty"`
	ServiceAreaCode               string    `json:"serviceAreaCode,omitempty" bson:"serviceAreaCode,omitempty"`
	ExamID                        string    `json:"examId,omitempty" bson:"examId,omitempty"`
	SerologicalTestIgmResult      string    `json:"serologicalTestIgmResult,omitempty" bson:"serologicalTestIgmResult,omitempty"`
	SerologicalTestIggResult      string    `json:"serologicalTestIggResult,omitempty" bson:"serologicalTestIggResult,omitempty"`
	MolecularTestResult           string    `json:"molecularTestResult,omitempty" bson:"molecularTestResult,omitempty"`
	QuantitativeAntigenTestResult string    `json:"quantitativeAntigenTestResult,omitempty" bson:"quantitativeAntigenTestResult,omitempty"`
	EliseTestIgaResult            string    `json:"eliseTestIgaResult,omitempty" bson:"eliseTestIgaResult,omitempty"`
	EliseTestIggResult            string    `json:"eliseTestIggResult,omitempty" bson:"eliseTestIggResult,omitempty"`
	QualitativeAntigenTestResult  string    `json:"qualitativeAntigenTestResult,omitempty" bson:"qualitativeAntigenTestResult,omitempty"`
	RocheTestResult               string    `json:"rocheTestResult,omitempty" bson:"rocheTestResult,omitempty"`
	CovneuTestResult              string    `json:"covneuTestResult,omitempty" bson:"covneuTestResult,omitempty"`
	SalivaAntigenTestResult       string    `json:"salivaAntigenTestResult,omitempty" bson:"salivaAntigenTestResult,omitempty"`
	Status                        int       `json:"status" bson:"status"`
	ExternalId                    string    `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem                string    `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate                   time.Time `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate               time.Time `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}
