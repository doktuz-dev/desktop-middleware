package models_v2

import "time"

type HealthCentre struct {
	ID              string    `json:"_id,omitempty" bson:"_id,omitempty"`
	Code            string    `json:"code,omitempty" bson:"code,omitempty"`
	ClinicName      string    `json:"clinicName,omitempty" bson:"clinicName,omitempty"`
	Name            string    `json:"name,omitempty" bson:"name,omitempty"`
	Address         string    `json:"address,omitempty" bson:"address,omitempty"`
	FirstEmail      string    `json:"firstEmail,omitempty" bson:"firstEmail,omitempty"`
	SecondEmail     string    `json:"secondEmail,omitempty" bson:"secondEmail,omitempty"`
	PhoneNumber     string    `json:"phoneNumber,omitempty" bson:"phoneNumber,omitempty"`
	OfficeHours     string    `json:"officeHours,omitempty" bson:"officeHours,omitempty"`
	Status          int       `json:"status" bson:"status"`
	ExternalId      string    `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem  string    `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate     time.Time `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate time.Time `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}
