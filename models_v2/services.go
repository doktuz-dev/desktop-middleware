package models_v2

import "time"

type Service struct {
	ID              string    `json:"_id,omitempty" bson:"_id,omitempty"`
	ServiceAreaId   string    `json:"serviceAreaId,omitempty" bson:"serviceAreaId,omitempty"`
	AttentionTypeId string    `json:"attentionTypeId,omitempty" bson:"attentionTypeId,omitempty"`
	Code            string    `json:"code,omitempty" bson:"code,omitempty"`
	Name            string    `json:"name,omitempty" bson:"name,omitempty"`
	EndDate         time.Time `json:"endDate,omitempty" bson:"endDate,omitempty"`
	Blocked         int       `json:"blocked" bson:"blocked"`
	Status          int       `json:"status" bson:"status"`
	ExternalId      string    `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem  string    `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate     time.Time `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate time.Time `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}
