package models_v2

import "time"

type Diagnostic struct {
	ID              string       `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionID     string       `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	ServiceAreaID   string       `json:"serviceAreaId,omitempty" bson:"serviceAreaId,omitempty"`
	ServiceAreaCode string       `json:"serviceAreaCode,omitempty" bson:"serviceAreaCode,omitempty"`
	ExamID          string       `json:"examId,omitempty" bson:"examId,omitempty"`
	SequenceNumber  int          `json:"sequenceNumber,omitempty" bson:"sequenceNumber,omitempty"`
	Diagnostic      string       `json:"diagnostic,omitempty" bson:"diagnostic,omitempty"`
	Status          int          `json:"status" bson:"status"`
	ExternalId      string       `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem  string       `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate     time.Time    `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate time.Time    `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
	ServiceArea     *ServiceArea `json:"serviceArea,omitempty" bson:"serviceArea,omitempty"`
}
