package models_v2

import "time"

type Department struct {
	ID              string    `json:"_id,omitempty" bson:"_id,omitempty"`
	Name            string    `json:"name,omitempty" bson:"name,omitempty"`
	ExternalId      string    `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem  string    `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate     time.Time `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate time.Time `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}
