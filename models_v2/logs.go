package models_v2

import "time"

type Log struct {
	ID              string      `json:"_id" bson:"_id"`
	FromSystem      string      `json:"fromSystem" bson:"fromSystem"`
	ToSystem        string      `json:"toSystem" bson:"toSystem"`
	Collection      string      `json:"collection" bson:"collection"`
	Body            interface{} `json:"body" bson:"body"`
	Operation       string      `json:"operation" bson:"operation"`
	Error           string      `json:"error,omitempty" bson:"error,omitempty"`
	Status          string      `json:"status" bson:"status"`
	CreatedDate     time.Time   `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate time.Time   `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}
