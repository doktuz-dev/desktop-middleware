package models_v2

import "time"

type DeliveryType struct {
	ID              string    `json:"_id,omitempty" bson:"_id,omitempty"`
	Code            string    `json:"code,omitempty" bson:"code,omitempty"`
	Name            string    `json:"name,omitempty" bson:"name,omitempty"`
	CreatedDate     time.Time `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate time.Time `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}
