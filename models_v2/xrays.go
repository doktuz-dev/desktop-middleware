package models_v2

type Xray struct {
	ID              string `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionID     string `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	ServiceAreaID   string `json:"serviceAreaId,omitempty" bson:"serviceAreaId,omitempty"`
	ServiceAreaCode string `json:"serviceAreaCode,omitempty" bson:"serviceAreaCode,omitempty"`
	Description1    string `json:"description1,omitempty" bson:"description1,omitempty"`
	Description2    string `json:"description2,omitempty" bson:"description2,omitempty"`
	Description3    string `json:"description3,omitempty" bson:"description3,omitempty"`
	Conclusion      string `json:"conslusion,omitempty" bson:"conslusion,omitempty"`
	Diagnostic1     string `json:"diagnostic1,omitempty" bson:"diagnostic1,omitempty"`
	Diagnostic2     string `json:"diagnostic2,omitempty" bson:"diagnostic2,omitempty"`
	Diagnostic3     string `json:"diagnostic3,omitempty" bson:"diagnostic3,omitempty"`
	Status          int    `json:"status" bson:"status"`
}
