package models_v2

import "time"

type Psychology struct {
	ID                 string `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionID        string `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	ServiceAreaID      string `json:"serviceAreaId,omitempty" bson:"serviceAreaId,omitempty"`
	ServiceAreaCode    string `json:"serviceAreaCode,omitempty" bson:"serviceAreaCode,omitempty"`
	Result             string `json:"diagnostic1,omitempty" bson:"diagnostic1,omitempty"`
	AcrophobiaTest     string `json:"acrophobiaTest,omitempty" bson:"acrophobiaTest,omitempty"`
	ClaustrophobiaTest string `json:"claustrophobiaTest,omitempty" bson:"claustrophobiaTest,omitempty"`
	FatigueTest        string `json:"fatigueTest,omitempty" bson:"fatigueTest,omitempty"`
	Recommendation1    string `json:"recommendation1,omitempty" bson:"recommendation1,omitempty"`
	Recommendation2    string `json:"recommendation2,omitempty" bson:"recommendation2,omitempty"`
	Recommendation3    string `json:"recommendation3,omitempty" bson:"recommendation3,omitempty"`
	Status             int    `json:"status" bson:"status"`
}

type PsychologyTest struct {
	ID                 string    `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionID        string    `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	ServiceAreaID      string    `json:"serviceAreaId,omitempty" bson:"serviceAreaId,omitempty"`
	ServiceAreaCode    string    `json:"serviceAreaCode,omitempty" bson:"serviceAreaCode,omitempty"`
	ExamID             string    `json:"examId,omitempty" bson:"examId,omitempty"`
	AcrophobiaTest     string    `json:"acrophobiaTest,omitempty" bson:"acrophobiaTest,omitempty"`
	ClaustrophobiaTest string    `json:"claustrophobiaTest,omitempty" bson:"claustrophobiaTest,omitempty"`
	FatigueTest        string    `json:"fatigueTest,omitempty" bson:"fatigueTest,omitempty"`
	Status             int       `json:"status" bson:"status"`
	ExternalId         string    `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem     string    `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate        time.Time `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate    time.Time `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}
