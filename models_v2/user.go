package models_v2

import "time"

type LoginUser struct {
	ID               string          `json:"_id" bson:"_id"`
	Username         string          `json:"username" bson:"username"`
	FirstName        string          `json:"firstName" bson:"firstName"`
	LastName         string          `json:"lastName" bson:"lastName"`
	Gender           string          `json:"gender" bson:"gender"`
	Signature        string          `json:"signature,omitempty" bson:"signature,omitempty"`
	ProfileImageUrl  string          `json:"profileImageUrl" bson:"profileImageUrl"`
	Status           int             `json:"status,omitempty" bson:"status"`
	Phone            string          `json:"phone,omitempty" bson:"phone,omitempty"`
	ConsultationMail string          `json:"consultationMail,omitempty" bson:"consultationMail,omitempty"`
	Customers        []*UserCustomer `json:"customers,omitempty" bson:"customers,omitempty"`
	IsAdmin          bool            `json:"isAdmin" bson:"isAdmin"`
	CreatedDate      time.Time       `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate  time.Time       `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}

type UserCustomer struct {
	CustomerID string `json:"customerId" bson:"customerId"`
	Role       string `json:"role" bson:"role"`
	IsOwner    bool   `json:"isOwner" bson:"isOwner"`
	Status     int    `json:"status" bson:"status"`
}
