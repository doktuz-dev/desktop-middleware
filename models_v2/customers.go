package models_v2

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Customer struct {
	ID                     string    `json:"_id,omitempty" bson:"_id,omitempty"`
	Name                   string    `json:"name,omitempty" bson:"name,omitempty"`
	Code                   string    `json:"code,omitempty" bson:"code,omitempty"`
	Status                 int       `json:"status" bson:"status"`
	Address                string    `json:"address,omitempty" bson:"address,omitempty"`
	PhoneNumber            string    `json:"phoneNumber,omitempty" bson:"phoneNumber,omitempty"`
	ExternalId             string    `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem         string    `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	Membership             string    `json:"membership" bson:"-"`
	MembershipDays         int       `json:"membershipDays" bson:"-"`
	StartMembershipDate    time.Time `json:"startMembershipDate,omitempty" bson:"-"`
	HasImportedPatient     bool      `json:"hasImportedPatient" bson:"hasImportedPatient"`
	HasSchedulings         bool      `json:"hasSchedulings" bson:"hasSchedulings"`
	CreatedDate            time.Time `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate        time.Time `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
	ConsultationMail       string    `json:"consultationMail,omitempty" bson:"consultationMail,omitempty"`
	HasLoginThisWeek       bool      `json:"hasLoginThisWeek" bson:"hasLoginThisWeek"`
	HasLoginThisMonth      bool      `json:"hasLoginThisMonth" bson:"hasLoginThisMonth"`
	HasAttentionsThisMonth bool      `json:"hasAttentionsThisMonth" bson:"hasAttentionsThisMonth"`
	LastAddedPatientDate   time.Time `json:"lastAddedPatientDate" bson:"lastAddedPatientDate"`
	ReadResultReportCode   string    `json:"readResultReportCode,omitempty" bson:"readResultReportCode,omitempty"`
	ReportFormCode         string    `json:"reportFormCode,omitempty" bson:"reportFormCode,omitempty"`
	ReportFormVersion      string    `json:"reportFormVersion,omitempty" bson:"reportFormVersion,omitempty"`
	ReportFormApprovedDate string    `json:"reportFormApprovedDate,omitempty" bson:"reportFormApprovedDate,omitempty"`
	Logo                   string    `json:"logo,omitempty" bson:"logo,omitempty"`
	HighRisk               *bool     `json:"highRisk" bson:"highRisk"`
	WorkRegime             *int      `json:"workRegime" bson:"workRegime"`
}

type Notification struct {
	ID              string        `json:"_id" bson:"_id"`
	Code            string        `json:"code,omitempty" bson:"code,omitempty"`
	CustomerID      string        `json:"customerId,omitempty" bson:"customerId,omitempty"`
	Description     string        `json:"description,omitempty" bson:"description,omitempty"`
	Status          string        `json:"status,omitempty" bson:"status,omitempty"`
	Platforms       *PlatformList `json:"platforms,omitempty" bson:"platforms,omitempty"`
	Sort            int           `json:"sort" bson:"sort"`
	CreatedDate     time.Time     `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate time.Time     `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}

type PlatformList struct {
	Whatsapp *Platform `json:"whatsapp,omitempty" bson:"whatsapp,omitempty"`
	Email    *Platform `json:"email,omitempty" bson:"email,omitempty"`
	SMS      *Platform `json:"sms,omitempty" bson:"sms,omitempty"`
	Web      *Platform `json:"web,omitempty" bson:"web,omitempty"`
}

type Platform struct {
	Subject        string `json:"subject,omitempty" bson:"subject,omitempty"`
	Message        string `json:"message,omitempty" bson:"message,omitempty"`
	Description    string `json:"description,omitempty" bson:"description,omitempty"`
	Enabled        bool   `json:"enabled" bson:"enabled"`
	EnabledForFree bool   `json:"enabledForFree" bson:"enabledForFree"`
}

func (c *Customer) SetNotifications() []interface{} {
	var results []interface{}
	list := []*Notification{
		{
			ID:          primitive.NewObjectID().Hex(),
			Code:        "CONGRAT_FIRST_PERSON",
			CustomerID:  c.ID,
			Description: "Notificación cuando la empresa ha cargado colaborador o agendado por primera vez",
			Status:      "INFO",
			Platforms: &PlatformList{
				Web: &Platform{
					Subject:     "Información",
					Message:     "Felicidades!, acabas de cargar colaborador por primera vez. El periodo de prueba empieza a partir de hoy!",
					Description: "Notificación cuando la empresa ha cargado colaborador o agendado por primera vez",
					Enabled:     true,
				},
			},
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
		},
		{
			ID:          primitive.NewObjectID().Hex(),
			Code:        "CONGRAT_FIRST_SCHEDULE",
			CustomerID:  c.ID,
			Description: "Notificación cuando la empresa ha cargado colaborador o agendado por primera vez",
			Status:      "INFO",
			Platforms: &PlatformList{
				Web: &Platform{
					Subject:     "Información",
					Message:     "Felicidades!, acabas de agendar por primera vez). El periodo de prueba empieza a partir de hoy!",
					Description: "Notificación cuando la empresa ha cargado colaborador o agendado por primera vez",
					Enabled:     true,
				},
			},
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
		},
		{
			ID:          primitive.NewObjectID().Hex(),
			Code:        "REQUEST_LOAD_PERSONS",
			CustomerID:  c.ID,
			Description: "Notificacion cuando la empresa aun no ha cargado colaboradores",
			Status:      "INFO",
			Platforms: &PlatformList{
				Web: &Platform{
					Subject:     "Recomendación",
					Message:     "La empresa no cuenta con colaboradores. Por favor añadir o importar colaboradores",
					Description: "Notificacion cuando la empresa aun no ha cargado colaboradores",
					Enabled:     true,
				},
			},
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
		},
		{
			ID:          primitive.NewObjectID().Hex(),
			Code:        "MEMBERSHIP_WILL_EXPIRE_SOON",
			CustomerID:  c.ID,
			Description: "Notificacion cuando expirara pronto la membresia de la empresa",
			Status:      "WARNING",
			Platforms: &PlatformList{
				Web: &Platform{
					Subject:     "Comunicado",
					Message:     "La membresía de la empresa ({{NOMBRE_EMPRESA}}) expirará en {{REMAINING_DAYS}} días ({{DATE_EXPIRED}}), si desea mantener su membresía puede contactar a nuestro ejecutivo comercial a través del chat de support.",
					Description: "Notificacion cuando expirara pronto la membresia de la empresa",
					Enabled:     true,
				},
			},
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
		},
		{
			ID:          primitive.NewObjectID().Hex(),
			Code:        "EMO_MANAGEMENT_SUMMARY",
			CustomerID:  c.ID,
			Description: "6. Enviar un correo automático de manera mensual con la estimación de multas según SUNAFIL",
			Status:      "INFO",
			Platforms: &PlatformList{
				Email: &Platform{
					Description: "6. Enviar un correo automático de manera mensual con la estimación de multas según SUNAFIL",
					Enabled:     false,
				},
			},
			Sort:            6,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
		},
		{
			ID:          primitive.NewObjectID().Hex(),
			Code:        "PERSON_MANAGEMENT_UPDATES",
			CustomerID:  c.ID,
			Description: "1. Enviar un correo cuando alguien agregue a un colaborador de manera individual o masiva y cuando alguien da de baja a un colaborador",
			Status:      "INFO",
			Platforms: &PlatformList{
				Email: &Platform{
					Description: "1. Enviar un correo cuando alguien agregue a un colaborador de manera individual o masiva y cuando alguien da de baja a un colaborador",
					Enabled:     true,
				},
			},
			Sort:            1,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
		},
		{
			ID:          primitive.NewObjectID().Hex(),
			Code:        "SCHEDULE_MANAGEMENT_UPDATES",
			CustomerID:  c.ID,
			Description: "2. Enviar un correo cuando alguien realice un agendamiento de un EMO o prueba COVID",
			Status:      "INFO",
			Platforms: &PlatformList{
				Email: &Platform{
					Description:    "2. Enviar un correo cuando alguien realice un agendamiento de un EMO o prueba COVID",
					Enabled:        true,
					EnabledForFree: true,
				},
				Whatsapp: &Platform{
					Description: "2. Enviar un correo cuando alguien realice un agendamiento de un EMO o prueba COVID",
					Enabled:     true,
				},
				SMS: &Platform{
					Description: "2. Enviar un correo cuando alguien realice un agendamiento de un EMO o prueba COVID",
					Enabled:     false,
				},
				Web: &Platform{
					Description: "2. Enviar un correo cuando alguien realice un agendamiento de un EMO o prueba COVID",
					Enabled:     true,
				},
			},
			Sort:            2,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
		},
		{
			ID:          primitive.NewObjectID().Hex(),
			Code:        "CONSULTATION_MANAGEMENT_UPDATES",
			CustomerID:  c.ID,
			Description: "3. Enviar notificación automática al colaborador cuando se genera una orden de interconsulta.",
			Status:      "INFO",
			Platforms: &PlatformList{
				Email: &Platform{
					Description: "3. Enviar notificación automática al colaborador cuando se genera una orden de interconsulta.",
					Enabled:     true,
				},
				Whatsapp: &Platform{
					Description: "3. Enviar notificación automática al colaborador cuando se genera una orden de interconsulta.",
					Enabled:     true,
				},
				SMS: &Platform{
					Description: "3. Enviar notificación automática al colaborador cuando se genera una orden de interconsulta.",
					Enabled:     false,
				},
				Web: &Platform{
					Description: "3. Enviar notificación automática al colaborador cuando se genera una orden de interconsulta.",
					Enabled:     false,
				},
			},
			Sort:            3,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
		},
		{
			ID:          primitive.NewObjectID().Hex(),
			Code:        "RESULTS_DELIVERY_REGISTRATION",
			CustomerID:  c.ID,
			Description: "5. Enviar notificación al colaborador cuando se genera una constancia de entrega de resultados del EMO.",
			Status:      "INFO",
			Platforms: &PlatformList{
				Email: &Platform{
					Description: "5. Enviar notificación al colaborador cuando se genera una constancia de entrega de resultados del EMO.",
					Enabled:     true,
				},
				Whatsapp: &Platform{
					Description: "5. Enviar notificación al colaborador cuando se genera una constancia de entrega de resultados del EMO.",
					Enabled:     true,
				},
				SMS: &Platform{
					Description: "5. Enviar notificación al colaborador cuando se genera una constancia de entrega de resultados del EMO.",
					Enabled:     false,
				},
				Web: &Platform{
					Description: "5. Enviar notificación al colaborador cuando se genera una constancia de entrega de resultados del EMO.",
					Enabled:     false,
				},
			},
			Sort:            5,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
		},
		{
			ID:          primitive.NewObjectID().Hex(),
			Code:        "SCHEDULING_REMINDER_TO_PATIENT",
			CustomerID:  c.ID,
			Description: "7. Enviar notificación de recordatorio automático al colaborador 1 y 3 días antes de la fecha agendada para un EMO, prueba COVID o Lectura de resultados.",
			Status:      "INFO",
			Platforms: &PlatformList{
				Email: &Platform{
					Description: "7. Enviar notificación de recordatorio automático al colaborador 1 y 3 días antes de la fecha agendada para un EMO, prueba COVID o Lectura de resultados.",
					Enabled:     true,
				},
				Whatsapp: &Platform{
					Description: "7. Enviar notificación de recordatorio automático al colaborador 1 y 3 días antes de la fecha agendada para un EMO, prueba COVID o Lectura de resultados.",
					Enabled:     true,
				},
				SMS: &Platform{
					Description: "7. Enviar notificación de recordatorio automático al colaborador 1 y 3 días antes de la fecha agendada para un EMO, prueba COVID o Lectura de resultados.",
					Enabled:     false,
				},
				Web: &Platform{
					Description: "7. Enviar notificación de recordatorio automático al colaborador 1 y 3 días antes de la fecha agendada para un EMO, prueba COVID o Lectura de resultados.",
					Enabled:     false,
				},
			},
			Sort:            7,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
		},
		{
			ID:          primitive.NewObjectID().Hex(),
			Code:        "PREMIUM_REMINDER",
			CustomerID:  c.ID,
			Description: "5. Recibir un correo automático cuando el periodo de prueba está por terminar 1 y 3 días antes de la fecha de vencimiento.",
			Status:      "INFO",
			Platforms: &PlatformList{
				Email: &Platform{
					Description: "5. Recibir un correo automático cuando el periodo de prueba está por terminar 1 y 3 días antes de la fecha de vencimiento.",
					Enabled:     true,
				},
				Whatsapp: &Platform{
					Description: "5. Recibir un correo automático cuando el periodo de prueba está por terminar 1 y 3 días antes de la fecha de vencimiento.",
					Enabled:     true,
				},
				SMS: &Platform{
					Description: "5. Recibir un correo automático cuando el periodo de prueba está por terminar 1 y 3 días antes de la fecha de vencimiento.",
					Enabled:     false,
				},
				Web: &Platform{
					Description: "5. Recibir un correo automático cuando el periodo de prueba está por terminar 1 y 3 días antes de la fecha de vencimiento.",
					Enabled:     false,
				},
			},
			Sort:            11,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
		},
		{
			ID:          primitive.NewObjectID().Hex(),
			Code:        "READRESULT_SCHEDULING_MANAGEMENT_UPDATES",
			CustomerID:  c.ID,
			Description: "4. Enviar notificación al colaborador cuando se realice un agendamiento para la lectura de su EMO.",
			Status:      "INFO",
			Platforms: &PlatformList{
				Email: &Platform{
					Description: "4. Enviar notificación al colaborador cuando se realice un agendamiento para la lectura de su EMO.",
					Enabled:     true,
				},
				Whatsapp: &Platform{
					Description: "4. Enviar notificación al colaborador cuando se realice un agendamiento para la lectura de su EMO.",
					Enabled:     true,
				},
				SMS: &Platform{
					Description: "4. Enviar notificación al colaborador cuando se realice un agendamiento para la lectura de su EMO.",
					Enabled:     false,
				},
				Web: &Platform{
					Description: "4. Enviar notificación al colaborador cuando se realice un agendamiento para la lectura de su EMO.",
					Enabled:     false,
				},
			},
			Sort:            4,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
		},
		{
			ID:          primitive.NewObjectID().Hex(),
			Code:        "EMO_MANAGEMENT_SUMMARY_TO_USER",
			CustomerID:  c.ID,
			Description: "3. Recibir un correo automático de manera mensual con la estimación de multas según SUNAFIL",
			Status:      "INFO",
			Platforms: &PlatformList{
				Email: &Platform{
					Description: "3. Recibir un correo automático de manera mensual con la estimación de multas según SUNAFIL",
					Enabled:     false,
				},
			},
			Sort:            9,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
		},
		{
			ID:          primitive.NewObjectID().Hex(),
			Code:        "EMO_OBSERVED_CHANGES",
			CustomerID:  c.ID,
			Description: "4. Recibir un correo cuando alguien suba un Certificado de aptitud y un EMO observado cambie a apto, apto con restricción o no apto",
			Status:      "INFO",
			Platforms: &PlatformList{
				Email: &Platform{
					Description: "4. Recibir un correo cuando alguien suba un Certificado de aptitud y un EMO observado cambie a apto, apto con restricción o no apto",
					Enabled:     true,
				},
			},
			Sort:            10,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
		},
		{
			ID:          primitive.NewObjectID().Hex(),
			Code:        "PERSON_MANAGEMENT_UPDATES_TO_USER",
			CustomerID:  c.ID,
			Description: "1. Recibir un correo cuando alguien agregue a un colaborador de manera individual o masiva y cuando alguien da de baja a un colaborador",
			Status:      "INFO",
			Platforms: &PlatformList{
				Email: &Platform{
					Description: "1. Recibir un correo cuando alguien agregue a un colaborador de manera individual o masiva y cuando alguien da de baja a un colaborador",
					Enabled:     true,
				},
			},
			Sort:            7,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
		},
		{
			ID:          primitive.NewObjectID().Hex(),
			Code:        "SCHEDULE_MANAGEMENT_UPDATES_TO_USER",
			CustomerID:  c.ID,
			Description: "2. Recibir un correo cuando alguien realice un agendamiento de un EMO o prueba COVID",
			Status:      "INFO",
			Platforms: &PlatformList{
				Email: &Platform{
					Description:    "2. Recibir un correo cuando alguien realice un agendamiento de un EMO o prueba COVID",
					Enabled:        true,
					EnabledForFree: true,
				},
				Whatsapp: &Platform{
					Description: "2. Recibir un correo cuando alguien realice un agendamiento de un EMO o prueba COVID",
					Enabled:     true,
				},
				SMS: &Platform{
					Description: "2. Recibir un correo cuando alguien realice un agendamiento de un EMO o prueba COVID",
					Enabled:     false,
				},
				Web: &Platform{
					Description: "2. Recibir un correo cuando alguien realice un agendamiento de un EMO o prueba COVID",
					Enabled:     true,
				},
			},
			Sort:            8,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
		},
	}

	for _, val := range list {
		results = append(results, val)
	}
	return results
}
