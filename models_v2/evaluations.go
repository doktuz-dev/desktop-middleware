package models_v2

import "time"

type Evaluation struct {
	ID              string    `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionId     string    `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	PersonId        string    `json:"personId,omitempty" bson:"personId,omitempty"`
	PatientId       string    `json:"patientId,omitempty" bson:"patientId,omitempty"`
	EvaluationType  string    `json:"evaluationType,omitempty" bson:"evaluationType,omitempty"`
	EvaluationDate  time.Time `json:"evaluationDate,omitempty" bson:"evaluationDate,omitempty"`
	Result          int       `json:"result,omitempty" bson:"result,omitempty"`
	Comments        string    `json:"comments,omitempty" bson:"comments,omitempty"`
	WasRejected     bool      `json:"wasRejected" bson:"wasRejected"`
	ExternalId      string    `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem  string    `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate     time.Time `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate time.Time `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}

type Question struct {
	ID              string    `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionId     string    `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	EvaluationId    string    `json:"evaluationId,omitempty" bson:"evaluationId,omitempty"`
	Question        string    `json:"question,omitempty" bson:"question,omitempty"`
	Answer          int       `json:"answer,omitempty" bson:"answer,omitempty"`
	Comments        string    `json:"comments,omitempty" bson:"comments,omitempty"`
	ExternalId      string    `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem  string    `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate     time.Time `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate time.Time `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}
