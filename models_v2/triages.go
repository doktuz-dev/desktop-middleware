package models_v2

import "time"

type Triage struct {
	ID                string    `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionId       string    `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	PersonId          string    `json:"personId,omitempty" bson:"personId,omitempty"`
	PatientId         string    `json:"patientId,omitempty" bson:"patientId,omitempty"`
	SystolicPressure  float64   `json:"systolicPressure,omitempty" bson:"systolicPressure,omitempty"`
	DiastolicPressure float64   `json:"diastolicPressure,omitempty" bson:"diastolicPressure,omitempty"`
	HeartRate         float64   `json:"heartRate,omitempty" bson:"heartRate,omitempty"`
	RespiratoryRate   float64   `json:"respiratoryRate,omitempty" bson:"respiratoryRate,omitempty"`
	Weight            float64   `json:"weight,omitempty" bson:"weight,omitempty"`
	Height            float64   `json:"height,omitempty" bson:"height,omitempty"`
	Bmi               float64   `json:"bmi,omitempty" bson:"bmi,omitempty"`
	Abdominal         float64   `json:"abdominal,omitempty" bson:"abdominal,omitempty"`
	Status            int       `json:"status" bson:"status"`
	ExternalId        string    `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem    string    `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate       time.Time `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate   time.Time `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}
