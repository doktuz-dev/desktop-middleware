package models_v2

import "time"

type Patient struct {
	ID                    string    `json:"_id" bson:"_id,omitempty"`
	DocumentTypeID        int       `json:"documentTypeId,omitempty" bson:"documentTypeId,omitempty"`
	DocumentTypeName      string    `json:"documentTypeName,omitempty" bson:"documentTypeName,omitempty"`
	DocumentNumber        string    `json:"documentNumber,omitempty" bson:"documentNumber,omitempty"`
	PersonID              string    `json:"personId,omitempty" bson:"personId,omitempty"`
	FirstName             string    `json:"firstName,omitempty" bson:"firstName,omitempty"`
	LastName              string    `json:"lastName,omitempty" bson:"lastName,omitempty"`
	BirthDate             time.Time `json:"birthDate,omitempty" bson:"birthDate,omitempty"`
	Sex                   string    `json:"sex,omitempty" bson:"sex,omitempty"`
	Email                 string    `json:"email,omitempty" bson:"email,omitempty"`
	PhoneNumber           string    `json:"phoneNumber,omitempty" bson:"phoneNumber,omitempty"`
	MaritalStatusId       string    `json:"maritalStatusId,omitempty" bson:"maritalStatusId,omitempty"`
	MaritalStatusName     string    `json:"maritalStatusName,omitempty" bson:"maritalStatusName,omitempty"`
	DegreeInstructionId   string    `json:"degreeInstructionId,omitempty" bson:"degreeInstructionId,omitempty"`
	DegreeInstructionName string    `json:"degreeInstructionName,omitempty" bson:"degreeInstructionName,omitempty"`
	DistrictId            string    `json:"districtId,omitempty" bson:"districtId,omitempty"`
	DistrictName          string    `json:"districtName,omitempty" bson:"districtName,omitempty"`
	Status                int       `json:"status" bson:"status"`
	ExternalId            string    `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem        string    `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate           time.Time `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate       time.Time `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}
