package models_v2

import "time"

type Person struct {
	ID                       string           `json:"_id" bson:"_id,omitempty"`
	EMOstatus                *string          `json:"emoStatus,omitempty" bson:"emoStatus,omitempty"`
	DocumentTypeID           *int             `json:"documentTypeId,omitempty" bson:"documentTypeId,omitempty"`
	DocumentTypeName         *string          `json:"documentTypeName,omitempty" bson:"documentTypeName,omitempty"`
	DocumentNumber           *string          `json:"documentNumber,omitempty" bson:"documentNumber,omitempty"`
	PatientId                *string          `json:"patientId,omitempty" bson:"patientId,omitempty"`
	FirstName                *string          `json:"firstName,omitempty" bson:"firstName,omitempty"`
	LastName                 *string          `json:"lastName,omitempty" bson:"lastName,omitempty"`
	BirthDateString          *string          `json:"birthDateString,omitempty" bson:"-"`
	BirthDate                *time.Time       `json:"birthDate,omitempty" bson:"birthDate,omitempty"`
	Sex                      *string          `json:"sex,omitempty" bson:"sex,omitempty"`
	MaritalStatusId          *string          `json:"maritalStatusId,omitempty" bson:"maritalStatusId,omitempty"`
	MaritalStatusName        *string          `json:"maritalStatusName,omitempty" bson:"maritalStatusName,omitempty"`
	DegreeInstructionId      *string          `json:"degreeInstructionId,omitempty" bson:"degreeInstructionId,omitempty"`
	DegreeInstructionName    *string          `json:"degreeInstructionName,omitempty" bson:"degreeInstructionName,omitempty"`
	Email                    *string          `json:"email,omitempty" bson:"email,omitempty"`
	ZipCode                  *string          `json:"zipCode,omitempty" bson:"zipCode,omitempty"`
	PhoneNumber              *string          `json:"phoneNumber,omitempty" bson:"phoneNumber,omitempty"`
	CustomerID               *string          `json:"customerId,omitempty" bson:"customerId,omitempty"`
	JobTitleName             *string          `json:"jobTitleName,omitempty" bson:"jobTitleName,omitempty"`
	AdmissionDateString      *string          `json:"admissionDateString,omitempty" bson:"-"`
	AdmissionDate            *time.Time       `json:"admissionDate,omitempty" bson:"admissionDate,omitempty"`
	WorkStationId            *string          `json:"workStationId,omitempty" bson:"workStationId,omitempty"`
	WorkStationName          *string          `json:"workStationName,omitempty" bson:"workStationName,omitempty"`
	DistrictId               *string          `json:"districtId,omitempty" bson:"districtId,omitempty"`
	DistrictName             *string          `json:"districtName,omitempty" bson:"districtName,omitempty"`
	CreatedDate              *time.Time       `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate          *time.Time       `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
	Customer                 *Customer        `json:"customer,omitempty" bson:"customer,omitempty"`
	Patient                  *Patient         `json:"patient,omitempty" bson:"patient,omitempty"`
	LastEmoAttention         *Attention       `json:"lastEmoAttention" bson:"lastEmoAttention,omitempty"`
	LastNoEmoAttention       *Attention       `json:"lastNoEmoAttention,omitempty" bson:"lastNoEmoAttention,omitempty"`
	LastObservedEmoAttention *Attention       `json:"lastObservedEmoAttention" bson:"lastObservedEmoAttention,omitempty"`
	LastAnyEmoAttention      *Attention       `json:"lastAnyEmoAttention,omitempty" bson:"lastAnyEmoAttention,omitempty"`
	LastEmoScheduling        *Scheduling      `json:"lastEmoScheduling,omitempty" bson:"lastEmoScheduling,omitempty"`
	LastNoEmoScheduling      *NoEmoScheduling `json:"lastNoEmoScheduling,omitempty" bson:"lastNoEmoScheduling,omitempty"`
	PersonStatus             *string          `json:"personStatus,omitempty" bson:"personStatus,omitempty"`
	TemporalDeletedFlag      *bool            `json:"temporalDeletedFlag,omitempty" bson:"temporalDeletedFlag,omitempty"`
	DeletedBy                *string          `json:"deletedBy,omitempty" bson:"deletedBy,omitempty"`
	WorkingAreaId            *string          `json:"workingAreaId,omitempty" bson:"workingAreaId,omitempty"`
	WorkingAreaName          *string          `json:"workingAreaName,omitempty" bson:"workingAreaName,omitempty"`
	JobTitleID               *string          `json:"jobTitleId,omitempty" bson:"jobTitleId,omitempty"`
	VIP                      *int             `json:"vip,omitempty" bson:"vip,omitempty"`
	EmosCount                *int             `json:"emosCount,omitempty" bson:"emosCount,omitempty"`
}
