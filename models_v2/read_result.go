package models_v2

import (
	"time"
)

type ReadResult struct {
	ID                          string              `json:"_id" bson:"_id"`
	AttentionID                 string              `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	OpenedDate                  time.Time           `json:"openedDate,omitempty" bson:"openedDate,omitempty"`
	ClosedDate                  time.Time           `json:"closedDate,omitempty" bson:"closedDate,omitempty"`
	UserId                      string              `json:"userId,omitempty" bson:"userId,omitempty"`
	NotificationStatusId        string              `json:"notificationStatusId,omitempty" bson:"notificationStatusId,omitempty"`
	Filename                    string              `json:"filename,omitempty" bson:"filename,omitempty"`
	AttachMedicalChart          bool                `json:"attachMedicalChart" bson:"attachMedicalChart"`
	AttachMedicalResult         bool                `json:"attachMedicalResult" bson:"attachMedicalResult"`
	AttachCompetencyCertificate bool                `json:"attachCompetencyCertificate" bson:"attachCompetencyCertificate"`
	IsSigned                    bool                `json:"isSigned" bson:"isSigned"`
	CreatedDate                 time.Time           `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate             time.Time           `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
	User                        *LoginUser          `json:"user,omitempty" bson:"user,omitempty"`
	NotificationStatus          *NotificationStatus `json:"notificationStatus,omitempty" bson:"notificationStatus,omitempty"`
}
