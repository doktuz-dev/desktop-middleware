package functions

import (
	"desktop-middleware/consumer_models"
	"desktop-middleware/models"
	"strconv"
	"strings"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

func GetCustomer(id string, values []*models.Customer) *models.Customer {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetDepartment(id string, values []*models.Department) *models.Department {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}
func GetProvince(id string, values []*models.Province) *models.Province {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetHealthCentre(id string, values []*models.HealthCentre) *models.HealthCentre {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetWorkStation(id string, values []*models.WorkStation) *models.WorkStation {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetServiceArea(id string, values []*models.ServiceArea) *models.ServiceArea {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetSpeciality(id string, values []*models.Speciality) *models.Speciality {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetService(id string, attentionTypeId string, values []*models.Service) *models.Service {
	for _, val := range values {
		if val.ExternalId == id && val.AttentionTypeId == attentionTypeId {
			return val
		}
	}
	return nil
}

func GetWorkProfile(id string, values []*models.WorkProfile) *models.WorkProfile {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetExamType(id string, values []*models.ExamType) *models.ExamType {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetExternalClinic(id int, values []*models.ExternalClinic) *models.ExternalClinic {
	for _, val := range values {
		if val.Code == id {
			return val
		}
	}
	return nil
}

func GetPerson(documentNumber string, documentType int, values []*models.Person) *models.Person {
	for _, val := range values {
		if val.DocumentNumber == documentNumber && val.DocumentTypeID == documentType {
			return val
		}
	}
	return nil
}

func GetPatient(documentTypeID int, documentNumber string, values []*models.Patient) *models.Patient {
	for _, val := range values {
		if val.DocumentTypeID == documentTypeID && val.DocumentNumber == documentNumber {
			return val
		}
	}
	return nil
}

func GetAttentionType(id string, values []*models.AttentionType) *models.AttentionType {
	for _, val := range values {
		if val.Code == id {
			return val
		}
	}
	return nil
}

func GetDeliveryType(id string, values []*models.DeliveryType) *models.DeliveryType {
	for _, val := range values {
		if val.Code == id {
			return val
		}
	}
	return nil
}

func GetMaritalStatus(id string, values []*models.MaritalStatus) *models.MaritalStatus {
	for _, val := range values {
		if val.Code == id {
			return val
		}
	}
	return nil
}

func GetDegreeInstructions(id string, values []*models.DegreeInstruction) *models.DegreeInstruction {
	for _, val := range values {
		if val.Code == id {
			return val
		}
	}
	return nil
}

func GetMaritalStatusById(id string, values []*models.MaritalStatus) *models.MaritalStatus {
	for _, val := range values {
		if val.ID == id {
			return val
		}
	}
	return nil
}

func GetDegreeInstructionsById(id string, values []*models.DegreeInstruction) *models.DegreeInstruction {
	for _, val := range values {
		if val.ID == id {
			return val
		}
	}
	return nil
}

func GetAttention(id string, values []*models.Attention) *models.Attention {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetAptitude(id string, values []*models.Aptitude) *models.Aptitude {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetExamResult(id string, values []*models.ExamResult) *models.ExamResult {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetConsultation(id string, values []*models.Consultation) *models.Consultation {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetAttentionLine(id string, values []*models.AttentionLine) *models.AttentionLine {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetEvaluation(id string, values []*models.Evaluation) *models.Evaluation {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetQuestion(id string, values []*models.Question) *models.Question {
	for _, val := range values {
		if val.EvaluationId == id {
			return val
		}
	}
	return nil
}

func GetExam(id string, attentionId string, serviceAreaId string, values []*models.Exam) *models.Exam {
	for _, val := range values {
		if val.ExternalId == id && val.ServiceAreaID == serviceAreaId && val.AttentionID == attentionId {
			return val
		}
	}
	return nil
}

func GetDiagnosticsFromSkeletalMuscle(id string, value *models.SkeletalMuscle) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic4) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  4,
			Diagnostic:      strings.TrimSpace(value.Diagnostic4),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic5) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  5,
			Diagnostic:      strings.TrimSpace(value.Diagnostic5),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic6) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  6,
			Diagnostic:      strings.TrimSpace(value.Diagnostic6),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic7) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  7,
			Diagnostic:      strings.TrimSpace(value.Diagnostic7),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic8) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  8,
			Diagnostic:      strings.TrimSpace(value.Diagnostic8),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic9) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  9,
			Diagnostic:      strings.TrimSpace(value.Diagnostic9),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic10) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  10,
			Diagnostic:      strings.TrimSpace(value.Diagnostic10),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic11) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  11,
			Diagnostic:      strings.TrimSpace(value.Diagnostic11),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromSpirometry(id string, value *models.Spirometry) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromXray(id string, value *models.Xray) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromXrayLD(id string, value *models.XrayLD) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromDermatology(id string, value *models.Dermatology) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromNeurology(id string, value *models.Neurology) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromDentistry(id string, value *models.Dentistry) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	return results
}

func GetRecommendationsFromPsychology(id string, value *models.Psychology) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Recommendation1) != "" {
		result := models.Recommendation{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Recommendation:  strings.TrimSpace(value.Recommendation1),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Recommendation2) != "" {
		result := models.Recommendation{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Recommendation:  strings.TrimSpace(value.Recommendation2),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Recommendation3) != "" {
		result := models.Recommendation{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Recommendation:  strings.TrimSpace(value.Recommendation3),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	return results
}

func GetPsychologyTest(id string, values []*models.PsychologyTest) *models.PsychologyTest {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetPathologicalHistory(id string, values []*models.PathologicalHistory) *models.PathologicalHistory {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetPathologicalHistoryInjuries(id string, values []*models.PathologicalHistoryInjuries) *models.PathologicalHistoryInjuries {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetWorkHistory(id string, company string, values []*models.WorkHistory) *models.WorkHistory {
	for _, val := range values {
		if val.ExternalId == id && val.Company == company {
			return val
		}
	}
	return nil
}

func GetMedicalHistory(id string, values []*models.MedicalHistory) *models.MedicalHistory {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetTriage(id string, values []*models.Triage) *models.Triage {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetDiagnosticsFromAudiometry(id string, value *models.Audiometry) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionId,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionId,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	return results
}

func GetAudiometryTest(id string, values []*models.AudiometryTest) *models.AudiometryTest {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetDiagnosticsFromOphtalmology(id string, value *models.Ophtalmology) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionId,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionId,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionId,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic4) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionId,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  4,
			Diagnostic:      strings.TrimSpace(value.Diagnostic4),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic5) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionId,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  5,
			Diagnostic:      strings.TrimSpace(value.Diagnostic5),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	return results
}

func GetOphtalmologyTest(id string, values []*models.OphtalmologyTest) *models.OphtalmologyTest {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetDiagnosticsFromCardiovascular(id string, value *models.Cardiovascular) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromLaboratory(id string, value *models.Laboratory) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionId,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionId,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionId,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic4) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionId,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  4,
			Diagnostic:      strings.TrimSpace(value.Diagnostic4),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic5) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionId,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  5,
			Diagnostic:      strings.TrimSpace(value.Diagnostic5),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	return results
}

func GetHematologyTest(id string, values []*models.HematologyTest) *models.HematologyTest {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetDrugTest(id string, values []*models.DrugTest) *models.DrugTest {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetMicrobiologyTest(id string, values []*models.MicrobiologyTest) *models.MicrobiologyTest {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetBiochemistryTest(id string, values []*models.BiochemistryTest) *models.BiochemistryTest {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetImmunologyTest(id string, values []*models.ImmunologyTest) *models.ImmunologyTest {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetCovidTest(id string, values []*models.CovidTest) *models.CovidTest {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetUrineTest(id string, values []*models.UrineTest) *models.UrineTest {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetDiagnosticsFromCardiovascularConsumer(id string, attencionId string, serviceAreaId string, serviceAreaCode string, value *consumer_models.CardiovascularConsumer) []interface{} {
	var results []interface{}
	status, _ := strconv.Atoi(value.Status)
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			Status:          status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			Status:          status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			Status:          status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromDentistryConsumer(id string, attencionId string, serviceAreaId string, serviceAreaCode string, value *consumer_models.DentistryConsumer) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromDermatologyConsumer(id string, attencionId string, serviceAreaId string, serviceAreaCode string, value *consumer_models.DermatologyConsumer) []interface{} {
	var results []interface{}
	status, _ := strconv.Atoi(value.Status)
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			Status:          status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			Status:          status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			Status:          status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromNeurologyConsumer(id string, attencionId string, serviceAreaId string, serviceAreaCode string, value *consumer_models.NeurologyConsumer) []interface{} {
	var results []interface{}
	status, _ := strconv.Atoi(value.Status)
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			Status:          status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			Status:          status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			Status:          status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromSkeletalMuscleConsumer(id string, attencionId string, serviceAreaId string, serviceAreaCode string, value *consumer_models.SkeletalMuscleConsumer) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic4) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  4,
			Diagnostic:      strings.TrimSpace(value.Diagnostic4),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic5) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  5,
			Diagnostic:      strings.TrimSpace(value.Diagnostic5),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic6) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  6,
			Diagnostic:      strings.TrimSpace(value.Diagnostic6),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic7) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  7,
			Diagnostic:      strings.TrimSpace(value.Diagnostic7),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic8) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  8,
			Diagnostic:      strings.TrimSpace(value.Diagnostic8),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic9) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  9,
			Diagnostic:      strings.TrimSpace(value.Diagnostic9),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic10) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  10,
			Diagnostic:      strings.TrimSpace(value.Diagnostic10),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic11) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  11,
			Diagnostic:      strings.TrimSpace(value.Diagnostic11),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromSpirometryConsumer(id string, attencionId string, serviceAreaId string, serviceAreaCode string, value *consumer_models.SpirometryConsumer) []interface{} {
	var results []interface{}
	status, _ := strconv.Atoi(value.Status)
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			Status:          status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			Status:          status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			Status:          status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromXRayConsumer(id string, attencionId string, serviceAreaId string, serviceAreaCode string, value *consumer_models.XRayConsumer) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromXRayLDConsumer(id string, attencionId string, serviceAreaId string, serviceAreaCode string, value *consumer_models.XRayLDConsumer) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			Status:          value.Status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	return results
}

func GetRecommendationsFromPsychologyConsumer(id string, attentionId string, serviceAreaId string, serviceAreaCode string, value *consumer_models.PsychologyConsumer) []interface{} {
	var results []interface{}
	status, _ := strconv.Atoi(value.Status)
	if strings.TrimSpace(value.Recommendation1) != "" {
		result := models.Recommendation{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attentionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Recommendation:  strings.TrimSpace(value.Recommendation1),
			Status:          status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Recommendation2) != "" {
		result := models.Recommendation{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attentionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Recommendation:  strings.TrimSpace(value.Recommendation2),
			Status:          status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Recommendation3) != "" {
		result := models.Recommendation{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attentionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Recommendation:  strings.TrimSpace(value.Recommendation3),
			Status:          status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromAudiometryConsumer(id string, attentionId string, serviceAreaId string, serviceAreaCode string, value *consumer_models.AudiometryConsumer) []interface{} {
	var results []interface{}
	status, _ := strconv.Atoi(value.Status)
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attentionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			Status:          status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attentionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			Status:          status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromOphtalmologyConsumer(id string, attentionId string, serviceAreaId string, serviceAreaCode string, value *consumer_models.OphtalmologyConsumer) []interface{} {
	var results []interface{}
	status, _ := strconv.Atoi(value.Status)
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attentionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			Status:          status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attentionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			Status:          status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attentionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			Status:          status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic4) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attentionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  4,
			Diagnostic:      strings.TrimSpace(value.Diagnostic4),
			Status:          status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic5) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attentionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  5,
			Diagnostic:      strings.TrimSpace(value.Diagnostic5),
			Status:          status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromLaboratoryConsumer(id string, attencionId string, serviceAreaId string, serviceAreaCode string, value *consumer_models.LaboratoryConsumer) []interface{} {
	var results []interface{}
	status, _ := strconv.Atoi(value.Status)
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			Status:          status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			Status:          status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			Status:          status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic4) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  4,
			Diagnostic:      strings.TrimSpace(value.Diagnostic4),
			Status:          status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic5) != "" {
		result := models.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  5,
			Diagnostic:      strings.TrimSpace(value.Diagnostic5),
			Status:          status,
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
			ExternalSystem:  "MEDIWEB",
		}
		results = append(results, &result)
	}
	return results
}

func GetDistrict(externalId string, values []*models.District) *models.District {
	for _, val := range values {
		if val.ExternalId == externalId {
			return val
		}
	}
	return nil
}

func GetDistrictById(id string, values []*models.District) *models.District {
	for _, val := range values {
		if val.ID == id {
			return val
		}
	}
	return nil
}

func GetWorkingArea(description string, values []*models.WorkingArea) *models.WorkingArea {
	for _, val := range values {
		if strings.TrimSpace(strings.ToUpper(val.Description)) == strings.TrimSpace(strings.ToUpper(description)) {
			return val
		}
	}
	return nil
}

func GetWorkingAreaByID(externalId string, values []*models.WorkingArea) *models.WorkingArea {
	for _, val := range values {
		if val.ExternalId == externalId {
			return val
		}
	}
	return nil
}

func GetJobTitle(description string, values []*models.JobTitle) *models.JobTitle {
	for _, val := range values {
		if strings.TrimSpace(strings.ToUpper(val.Description)) == strings.TrimSpace(strings.ToUpper(description)) {
			return val
		}
	}
	return nil
}

func GetJobTitleByID(externalId string, values []*models.JobTitle) *models.JobTitle {
	for _, val := range values {
		if val.ExternalId == externalId {
			return val
		}
	}
	return nil
}
