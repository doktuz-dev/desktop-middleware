package config

import (
	"os"
	"strings"

	"github.com/spf13/viper"
)

func SetConfiguration() {
	filename := os.Getenv("CONFIG_FILENAME")
	if filename == "" {
		filename = "config_local.json"
	}
	arrfilename := strings.Split(filename, ".")
	viper.SetConfigName(arrfilename[0])
	viper.SetConfigType(arrfilename[1])
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}
