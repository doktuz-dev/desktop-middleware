package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"desktop-middleware/database/mysql"
	functions "desktop-middleware/functions"
	"sync"

	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	db := database.InitDatabases()
	results, err := mysql.GetImmunologyTests(context.TODO())
	if err != nil {
		panic(err)
	}
	defer db.DisconnectDatabases()
	col := mongo.GetImmunologyTestCollection()
	hematologyTests, errGet := col.GetImmunologyTests(context.Background())
	if errGet != nil {
		panic(errGet)
	}
	serviceAreas, errGet2 := mongo.GetServiceAreaCollection().GetServiceAreas(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}
	numCPU := viper.GetInt("kafka.threadCount")
	length := len(results)
	wg := sync.WaitGroup{}

	chunkSize := (length + numCPU - 1) / numCPU

	for i := 0; i < length; i += chunkSize {

		wg.Add(1)
		go func(i int, chunkSize int) {
			end := i + chunkSize
			defer wg.Done()

			if end > length {
				end = length
			}
			slice := results[i:end]

			for _, val := range slice {
				att, errGet2 := mongo.GetAttentionCollection().GetAttentionById(context.Background(), val.AttentionId)
				if errGet2 != nil {
					panic(errGet2)
				}
				if att != nil {
					val.AttentionId = att.ID
				}
				srv := functions.GetServiceArea(val.ServiceAreaID, serviceAreas)
				if srv != nil {
					val.ServiceAreaID = srv.ID
					val.ServiceAreaCode = srv.Code
				}
				exam, _ := mongo.GetExamCollection().GetExamById(context.TODO(), val.ExternalId, val.AttentionId, val.ServiceAreaID)
				if exam != nil {
					val.ExamID = exam.ID
				}
				hematologyTest := functions.GetImmunologyTest(val.ExternalId, hematologyTests)
				if hematologyTest != nil {
					val.ID = hematologyTest.ID
					val.ExternalSystem = "MEDIWEB"
					val.CreatedDate = hematologyTest.CreatedDate
					_, errUpt := col.UpdateImmunologyTest(context.Background(), val.ID, val)
					if errUpt != nil {
						panic(errUpt)
					}
				} else {
					val.ExternalSystem = "MEDIWEB"
					_, err2 := col.AddImmunologyTest(context.Background(), val)
					if err2 != nil {
						panic(err2)
					}
				}
			}
		}(i, chunkSize)
	}
	wg.Wait()
}
