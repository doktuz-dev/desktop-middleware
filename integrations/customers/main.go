package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"sync"

	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	db := database.InitDatabases()
	defer db.DisconnectDatabases()
	col := mongo.GetCustomerCollection()
	customers, errGet := col.GetCustomers(context.Background())
	if errGet != nil {
		panic(errGet)
	}
	numCPU := viper.GetInt("kafka.threadCount")
	length := len(customers)
	wg := sync.WaitGroup{}

	chunkSize := (length + numCPU - 1) / numCPU

	for i := 0; i < length; i += chunkSize {

		wg.Add(1)
		go func(i int, chunkSize int) {
			end := i + chunkSize
			defer wg.Done()

			if end > length {
				end = length
			}
			slice := customers[i:end]

			for _, val := range slice {
				list := val.SetNotifications()
				if len(list) > 0 {
					mongo.GetNotificationsCollection().AddManyNotifications(context.Background(), list)
				}
			}
		}(i, chunkSize)
	}
	wg.Wait()
}
