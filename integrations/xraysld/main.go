package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"desktop-middleware/database/mysql"
	functions "desktop-middleware/functions"
	"desktop-middleware/models"
	"fmt"
	"strings"
	"sync"

	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	db := database.InitDatabases()
	results, err := mysql.GetXrayLDs(context.TODO())
	if err != nil {
		panic(err)
	}
	defer db.DisconnectDatabases()
	col := mongo.GetExamCollection()

	serviceAreas, errGet2 := mongo.GetServiceAreaCollection().GetServiceAreas(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}
	numCPU := viper.GetInt("kafka.threadCount")
	length := len(results)
	wg := sync.WaitGroup{}

	chunkSize := (length + numCPU - 1) / numCPU

	for i := 0; i < length; i += chunkSize {

		wg.Add(1)
		go func(i int, chunkSize int) {
			end := i + chunkSize
			defer wg.Done()

			if end > length {
				end = length
			}
			slice := results[i:end]

			for _, val := range slice {
				var newexam models.Exam

				var filenames []string
				if val.Description1 != "" {
					img := strings.Split(val.Description1, ".")
					filenames = append(filenames, fmt.Sprintf("1.%s", img[1]))
				}
				if val.Description2 != "" {
					img := strings.Split(val.Description2, ".")
					filenames = append(filenames, fmt.Sprintf("2.%s", img[1]))
				}
				if val.Description3 != "" {
					img := strings.Split(val.Description3, ".")
					filenames = append(filenames, fmt.Sprintf("3.%s", img[1]))
				}
				if val.Description4 != "" {
					img := strings.Split(val.Description4, ".")
					filenames = append(filenames, fmt.Sprintf("4.%s", img[1]))
				}
				if val.Description5 != "" {
					img := strings.Split(val.Description5, ".")
					filenames = append(filenames, fmt.Sprintf("5.%s", img[1]))
				}
				if val.Description6 != "" {
					img := strings.Split(val.Description6, ".")
					filenames = append(filenames, fmt.Sprintf("6.%s", img[1]))
				}
				if val.Description7 != "" {
					img := strings.Split(val.Description7, ".")
					filenames = append(filenames, fmt.Sprintf("7.%s", img[1]))
				}
				if val.Description8 != "" {
					img := strings.Split(val.Description8, ".")
					filenames = append(filenames, fmt.Sprintf("8.%s", img[1]))
				}
				if val.Description9 != "" {
					img := strings.Split(val.Description9, ".")
					filenames = append(filenames, fmt.Sprintf("9.%s", img[1]))
				}
				if val.Description10 != "" {
					img := strings.Split(val.Description10, ".")
					filenames = append(filenames, fmt.Sprintf("10.%s", img[1]))
				}
				if val.Description11 != "" {
					img := strings.Split(val.Description11, ".")
					filenames = append(filenames, fmt.Sprintf("11.%s", img[1]))
				}
				if val.Description12 != "" {
					img := strings.Split(val.Description12, ".")
					filenames = append(filenames, fmt.Sprintf("12.%s", img[1]))
				}
				if val.Description13 != "" {
					img := strings.Split(val.Description13, ".")
					filenames = append(filenames, fmt.Sprintf("13.%s", img[1]))
				}
				if val.Description14 != "" {
					img := strings.Split(val.Description14, ".")
					filenames = append(filenames, fmt.Sprintf("14.%s", img[1]))
				}
				if val.Description15 != "" {
					img := strings.Split(val.Description15, ".")
					filenames = append(filenames, fmt.Sprintf("15.%s", img[1]))
				}
				att, errGet2 := mongo.GetAttentionCollection().GetAttentionById(context.Background(), val.AttentionID)
				if errGet2 != nil {
					panic(errGet2)
				}
				if att != nil {
					val.AttentionID = att.ID
				}
				srv := functions.GetServiceArea(val.ServiceAreaID, serviceAreas)
				if srv != nil {
					val.ServiceAreaID = srv.ID
					val.ServiceAreaCode = srv.Code
				}
				exam, errGet := col.GetExamById(context.Background(), val.ID, val.AttentionID, val.ServiceAreaID)
				if errGet != nil {
					panic(errGet)
				}
				if exam != nil {
					newexam.ID = exam.ID
					newexam.AttentionID = val.AttentionID
					newexam.ServiceAreaID = val.ServiceAreaID
					newexam.ServiceAreaCode = val.ServiceAreaCode
					newexam.Filenames = filenames
					newexam.Result = val.Result
					newexam.Status = val.Status
					newexam.ExternalSystem = "MEDIWEB"
					newexam.CreatedDate = exam.CreatedDate
					_, errUpt := col.UpdateExam(context.Background(), newexam.ID, &newexam)
					if errUpt != nil {
						panic(errUpt)
					}

					mongo.GetDiagnosticCollection().DeleteDiagnostics(context.Background(), newexam.AttentionID, newexam.ServiceAreaID, newexam.ID)

					diagnostics := functions.GetDiagnosticsFromXrayLD(newexam.ID, val)
					if len(diagnostics) > 0 {
						_, err2 := mongo.GetDiagnosticCollection().AddManyDiagnostics(context.Background(), diagnostics)
						if err2 != nil {
							panic(err2)
						}
					}
				} else {
					newexam.AttentionID = val.AttentionID
					newexam.ServiceAreaID = val.ServiceAreaID
					newexam.ServiceAreaCode = val.ServiceAreaCode
					newexam.Filenames = filenames
					newexam.Result = val.Result
					newexam.Status = val.Status
					newexam.ExternalId = val.ID
					newexam.ExternalSystem = "MEDIWEB"
					value, err2 := col.AddExam(context.Background(), &newexam)
					if err2 != nil {
						panic(err2)
					}

					mongo.GetDiagnosticCollection().DeleteDiagnostics(context.Background(), value.AttentionID, value.ServiceAreaID, value.ID)

					diagnostics := functions.GetDiagnosticsFromXrayLD(value.ID, val)
					if len(diagnostics) > 0 {
						_, err2 := mongo.GetDiagnosticCollection().AddManyDiagnostics(context.Background(), diagnostics)
						if err2 != nil {
							panic(err2)
						}
					}
				}
			}
		}(i, chunkSize)
	}
	wg.Wait()
}
