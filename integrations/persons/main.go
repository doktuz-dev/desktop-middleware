package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"desktop-middleware/functions"
	"strconv"
	"sync"

	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	db := database.InitDatabases()
	defer db.DisconnectDatabases()
	col := mongo.GetPersonCollection()
	persons, errGet := col.GetPersons(context.Background())
	if errGet != nil {
		panic(errGet)
	}

	customers, errGet2 := mongo.GetCustomerCollection().GetCustomers(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}

	workStations, errGet2 := mongo.GetWorkStationCollection().GetWorkStations(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}

	maritalStatus, errGet2 := mongo.GetMaritalStatusCollection().GetMaritalStatuss(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}

	degreeInstruction, errGet2 := mongo.GetDegreeInstructionCollection().GetDegreeInstructions(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}

	districts, errGet2 := mongo.GetDistrictCollection().GetDistricts(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}

	numCPU := viper.GetInt("kafka.threadCount")
	length := len(persons)
	wg := sync.WaitGroup{}

	chunkSize := (length + numCPU - 1) / numCPU

	for i := 0; i < length; i += chunkSize {

		wg.Add(1)
		go func(i int, chunkSize int) {
			end := i + chunkSize
			defer wg.Done()

			if end > length {
				end = length
			}
			slice := persons[i:end]

			for _, person := range slice {
				cust := functions.GetCustomer(strconv.Itoa(person.CompanyID), customers)
				work := functions.GetWorkStation(strconv.Itoa(person.ProjectID), workStations)
				if cust != nil {
					person.CustomerId = cust.ID
				}
				if work != nil {
					person.WorkStationId = work.ID
				}
				pat, _ := mongo.GetPatientCollection().GetPatientByDocument(context.Background(), person.DocumentNumber, person.DocumentTypeID)
				if pat != nil {
					degree := functions.GetDegreeInstructionsById(pat.DegreeInstructionId, degreeInstruction)
					mar := functions.GetMaritalStatusById(pat.MaritalStatusId, maritalStatus)
					district := functions.GetDistrictById(pat.DistrictId, districts)
					if mar != nil {
						person.MaritalStatusId = mar.ID
						person.MaritalStatusName = mar.Name
					}
					if degree != nil {
						person.DegreeInstructionId = degree.ID
						person.DegreeInstructionName = degree.Name
					}
					if district != nil {
						person.DistrictId = district.ID
						person.DistrictName = district.Name
					}
					person.PatientId = pat.ID
				}
				_, errUpt := col.UpdatePerson(context.Background(), person.ID, person)
				if errUpt != nil {
					panic(errUpt)
				}
			}
		}(i, chunkSize)
	}
	wg.Wait()
}
