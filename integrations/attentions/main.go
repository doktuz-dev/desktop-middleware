package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"desktop-middleware/database/mysql"
	functions "desktop-middleware/functions"
	"fmt"
	"sync"

	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	db := database.InitDatabases()
	results, err := mysql.GetAttentions(context.TODO())
	if err != nil {
		panic(err)
	}
	defer db.DisconnectDatabases()
	col := mongo.GetAttentionCollection()
	attentions, errGet := col.GetAttentions(context.Background())
	if errGet != nil {
		panic(errGet)
	}

	customers, errGet2 := mongo.GetCustomerCollection().GetCustomers(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}

	departments, errGet2 := mongo.GetDepartmentCollection().GetDepartments(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}

	districts, errGet2 := mongo.GetDistrictCollection().GetDistricts(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}

	attentionTypes, errGet2 := mongo.GetAttentionTypeCollection().GetAttentionTypes(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}

	deliveryTypes, errGet2 := mongo.GetDeliveryTypeCollection().GetDeliveryTypes(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}

	healthCentres, errGet2 := mongo.GetHealthCentreCollection().GetHealthCentres(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}

	workStations, errGet2 := mongo.GetWorkStationCollection().GetWorkStations(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}

	workProfiles, errGet2 := mongo.GetWorkProfileCollection().GetWorkProfiles(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}

	examTypes, errGet2 := mongo.GetExamTypeCollection().GetExamTypes(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}
	numCPU := viper.GetInt("kafka.threadCount")
	length := len(results)
	wg := sync.WaitGroup{}

	chunkSize := (length + numCPU - 1) / numCPU

	for i := 0; i < length; i += chunkSize {

		wg.Add(1)
		go func(i int, chunkSize int) {
			end := i + chunkSize
			defer wg.Done()

			if end > length {
				end = length
			}
			slice := results[i:end]

			for _, val := range slice {
				cust := functions.GetCustomer(val.CustomerId, customers)
				if cust != nil {
					val.CustomerId = cust.ID
					val.CustomerCode = cust.Code
					val.CustomerName = cust.Name
					pat, _ := mongo.GetPatientCollection().GetPatientById(context.Background(), val.PatientId)
					if pat != nil {
						val.PatientId = pat.ID
						per, _ := mongo.GetPersonCollection().GetPersonByDocumentNumberAndCustomer(context.Background(), pat.DocumentNumber, pat.DocumentTypeID, cust.ID)
						if per != nil {
							val.PersonId = per.ID
						} else {
							val.PersonId = ""
						}
					}
				} else {
					pat, _ := mongo.GetPatientCollection().GetPatientById(context.Background(), val.PatientId)
					if pat != nil {
						val.PatientId = pat.ID
					}
				}
				depa := functions.GetDepartment(val.DepartmentId, departments)
				if depa != nil {
					val.DepartmentId = depa.ID
					val.DepartmentName = depa.Name
				}
				attType := functions.GetAttentionType(val.AttentionTypeId, attentionTypes)
				if attType != nil {
					val.AttentionTypeId = attType.ID
					val.AttentionTypeName = attType.Name
				}
				delType := functions.GetDeliveryType(val.DeliveryTypeId, deliveryTypes)
				if delType != nil {
					val.DeliveryTypeId = delType.ID
					val.DeliveryTypeName = delType.Name
				}
				healthCentr := functions.GetHealthCentre(val.HealthCentreId, healthCentres)
				if healthCentr != nil {
					val.HealthCentreId = healthCentr.ID
					val.HealthCentreName = healthCentr.Name
				}
				workSta := functions.GetWorkStation(val.WorkStationId, workStations)
				if workSta != nil {
					val.WorkStationId = workSta.ID
					val.WorkStationName = workSta.Name
				}
				workPro := functions.GetWorkProfile(val.WorkProfileId, workProfiles)
				if workPro != nil {
					val.WorkProfileId = workPro.ID
					val.WorkProfileName = workPro.Name
				}
				exa := functions.GetExamType(val.ExamTypeId, examTypes)
				if exa != nil {
					val.ExamTypeId = exa.ID
					val.ExamTypeName = exa.Name
				}
				district := functions.GetDistrict(val.DistrictId, districts)
				if district != nil {
					val.DistrictId = district.ID
					val.DistrictName = district.Name
				}
				attention := functions.GetAttention(val.ExternalId, attentions)
				if attention != nil {
					val.ID = attention.ID
					val.ExternalSystem = "MEDIWEB"
					val.CreatedDate = attention.CreatedDate
					_, errUpt := col.UpdateAttention(context.Background(), val.ID, val)
					if errUpt != nil {
						panic(errUpt)
					}
					fmt.Println("updated")
				} else {
					val.ExternalSystem = "MEDIWEB"
					_, err2 := col.AddAttention(context.Background(), val)
					if err2 != nil {
						panic(err2)
					}
					fmt.Println("created")
				}
			}
		}(i, chunkSize)
	}
	wg.Wait()
}
