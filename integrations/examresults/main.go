package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"desktop-middleware/database/mysql"
	functions "desktop-middleware/functions"
	"fmt"
	"sync"

	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	db := database.InitDatabases()
	results, err := mysql.GetExamResults(context.TODO())
	if err != nil {
		panic(err)
	}
	defer db.DisconnectDatabases()
	col := mongo.GetExamResultCollection()
	tests, errGet := col.GetExamResults(context.Background())
	if errGet != nil {
		panic(errGet)
	}

	attentions, errGet2 := mongo.GetAttentionCollection().GetAttentions(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}
	serviceAreas, errGet2 := mongo.GetServiceAreaCollection().GetServiceAreas(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}
	numCPU := viper.GetInt("kafka.threadCount")
	length := len(results)
	wg := sync.WaitGroup{}

	chunkSize := (length + numCPU - 1) / numCPU

	for i := 0; i < length; i += chunkSize {

		wg.Add(1)
		go func(i int, chunkSize int) {
			end := i + chunkSize
			defer wg.Done()

			if end > length {
				end = length
			}
			slice := results[i:end]

			for _, val := range slice {
				att := functions.GetAttention(val.AttentionId, attentions)
				if att != nil {
					val.AttentionId = att.ID
				}
				serv := functions.GetServiceArea(val.ServiceAreaID, serviceAreas)
				if serv != nil {
					val.ServiceAreaID = serv.ID
					val.ServiceAreaCode = serv.Code
				}
				test := functions.GetExamResult(val.ExternalId, tests)
				if test != nil {
					val.ID = test.ID
					val.ExternalSystem = "MEDIWEB"
					val.CreatedDate = test.CreatedDate
					_, errUpt := col.UpdateExamResult(context.Background(), val.ID, val)
					if errUpt != nil {
						panic(errUpt)
					}
					fmt.Println("Updated")
				} else {
					val.ExternalSystem = "MEDIWEB"
					_, err2 := col.AddExamResult(context.Background(), val)
					if err2 != nil {
						panic(err2)
					}
					fmt.Println("Created")
				}
			}
		}(i, chunkSize)
	}
	wg.Wait()
}
