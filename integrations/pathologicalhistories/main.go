package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"desktop-middleware/database/mysql"
	functions "desktop-middleware/functions"
	"sync"

	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	db := database.InitDatabases()
	results, err := mysql.GetPathologicalHistorys(context.TODO())
	if err != nil {
		panic(err)
	}
	defer db.DisconnectDatabases()
	col := mongo.GetPathologicalHistoryCollection()
	pathologicalHistorys, errGet := col.GetPathologicalHistorys(context.Background())
	if errGet != nil {
		panic(errGet)
	}

	attentions, errGet2 := mongo.GetAttentionCollection().GetAttentions(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}
	numCPU := viper.GetInt("kafka.threadCount")
	length := len(results)
	wg := sync.WaitGroup{}

	chunkSize := (length + numCPU - 1) / numCPU

	for i := 0; i < length; i += chunkSize {

		wg.Add(1)
		go func(i int, chunkSize int) {
			end := i + chunkSize
			defer wg.Done()

			if end > length {
				end = length
			}
			slice := results[i:end]

			for _, val := range slice {
				att := functions.GetAttention(val.AttentionId, attentions)
				if att != nil {
					val.AttentionId = att.ID
					val.PatientId = att.PatientId
					val.PersonId = att.PersonId
				}
				pathologicalHistory := functions.GetPathologicalHistory(val.ExternalId, pathologicalHistorys)
				if pathologicalHistory != nil {
					val.ID = pathologicalHistory.ID
					val.ExternalSystem = "MEDIWEB"
					val.CreatedDate = pathologicalHistory.CreatedDate
					_, errUpt := col.UpdatePathologicalHistory(context.Background(), val.ID, val)
					if errUpt != nil {
						panic(errUpt)
					}
				} else {
					val.ExternalSystem = "MEDIWEB"
					_, err2 := col.AddPathologicalHistory(context.Background(), val)
					if err2 != nil {
						panic(err2)
					}
				}
			}
		}(i, chunkSize)
	}
	wg.Wait()
}
