package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"desktop-middleware/database/mysql"
	functions "desktop-middleware/functions"
	"desktop-middleware/models"
	"sync"

	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	db := database.InitDatabases()
	results, err := mysql.GetSkeletalMuscles(context.TODO())
	if err != nil {
		panic(err)
	}
	defer db.DisconnectDatabases()
	col := mongo.GetExamCollection()

	serviceAreas, errGet2 := mongo.GetServiceAreaCollection().GetServiceAreas(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}
	numCPU := viper.GetInt("kafka.threadCount")
	length := len(results)
	wg := sync.WaitGroup{}

	chunkSize := (length + numCPU - 1) / numCPU

	for i := 0; i < length; i += chunkSize {

		wg.Add(1)
		go func(i int, chunkSize int) {
			end := i + chunkSize
			defer wg.Done()

			if end > length {
				end = length
			}
			slice := results[i:end]

			for _, val := range slice {
				var newexam models.Exam
				att, errGet2 := mongo.GetAttentionCollection().GetAttentionById(context.Background(), val.AttentionID)
				if errGet2 != nil {
					panic(errGet2)
				}
				if att != nil {
					val.AttentionID = att.ID
				}
				srv := functions.GetServiceArea(val.ServiceAreaID, serviceAreas)
				if srv != nil {
					val.ServiceAreaID = srv.ID
					val.ServiceAreaCode = srv.Code
				}
				exam, errGet := col.GetExamById(context.Background(), val.ID, val.AttentionID, val.ServiceAreaID)
				if errGet != nil {
					panic(errGet)
				}
				if exam != nil {
					newexam.ID = exam.ID
					newexam.AttentionID = val.AttentionID
					newexam.ServiceAreaID = val.ServiceAreaID
					newexam.ServiceAreaCode = val.ServiceAreaCode
					newexam.Status = val.Status
					newexam.ExternalSystem = "MEDIWEB"
					newexam.CreatedDate = exam.CreatedDate
					_, errUpt := col.UpdateExam(context.Background(), newexam.ID, &newexam)
					if errUpt != nil {
						panic(errUpt)
					}

					mongo.GetDiagnosticCollection().DeleteDiagnostics(context.Background(), newexam.AttentionID, newexam.ServiceAreaID, newexam.ID)

					diagnostics := functions.GetDiagnosticsFromSkeletalMuscle(newexam.ID, val)
					if len(diagnostics) > 0 {
						_, err2 := mongo.GetDiagnosticCollection().AddManyDiagnostics(context.Background(), diagnostics)
						if err2 != nil {
							panic(err2)
						}
					}
				} else {
					newexam.AttentionID = val.AttentionID
					newexam.ServiceAreaID = val.ServiceAreaID
					newexam.ServiceAreaCode = val.ServiceAreaCode
					newexam.Status = val.Status
					newexam.ExternalId = val.ID
					newexam.ExternalSystem = "MEDIWEB"
					value, err2 := col.AddExam(context.Background(), &newexam)
					if err2 != nil {
						panic(err2)
					}

					mongo.GetDiagnosticCollection().DeleteDiagnostics(context.Background(), value.AttentionID, value.ServiceAreaID, value.ID)

					diagnostics := functions.GetDiagnosticsFromSkeletalMuscle(value.ID, val)
					if len(diagnostics) > 0 {
						_, err2 := mongo.GetDiagnosticCollection().AddManyDiagnostics(context.Background(), diagnostics)
						if err2 != nil {
							panic(err2)
						}
					}
				}
			}
		}(i, chunkSize)
	}
	wg.Wait()
}
