package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"desktop-middleware/database/mysql"
	functions "desktop-middleware/functions"
	"sync"

	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	db := database.InitDatabases()
	results, err := mysql.GetWorkStations(context.TODO())
	if err != nil {
		panic(err)
	}
	defer db.DisconnectDatabases()
	col := mongo.GetWorkStationCollection()
	workStations, errGet := col.GetWorkStations(context.Background())
	if errGet != nil {
		panic(errGet)
	}

	customers, errGet2 := mongo.GetCustomerCollection().GetCustomers(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}

	numCPU := viper.GetInt("kafka.threadCount")
	length := len(results)
	wg := sync.WaitGroup{}

	chunkSize := (length + numCPU - 1) / numCPU

	for i := 0; i < length; i += chunkSize {

		wg.Add(1)
		go func(i int, chunkSize int) {
			end := i + chunkSize
			defer wg.Done()

			if end > length {
				end = length
			}
			slice := results[i:end]

			for _, val := range slice {
				cust := functions.GetCustomer(val.CustomerId, customers)
				val.Code = val.ExternalId
				if cust != nil {
					val.CustomerId = cust.ID
				}
				workStation := functions.GetWorkStation(val.ExternalId, workStations)
				if workStation != nil {
					val.ID = workStation.ID
					val.ExternalSystem = "MEDIWEB"
					val.CreatedDate = workStation.CreatedDate
					_, errUpt := col.UpdateWorkStation(context.Background(), val.ID, val)
					if errUpt != nil {
						panic(errUpt)
					}
				} else {
					val.ExternalSystem = "MEDIWEB"
					_, err2 := col.AddWorkStation(context.Background(), val)
					if err2 != nil {
						panic(err2)
					}
				}
			}
		}(i, chunkSize)
	}
	wg.Wait()
}
