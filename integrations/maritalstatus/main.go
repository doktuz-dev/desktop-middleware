package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"desktop-middleware/models"
	"sync"

	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	db := database.InitDatabases()
	defer db.DisconnectDatabases()
	col := mongo.GetMaritalStatusCollection()
	results := []*models.MaritalStatus{
		{
			Code: "1",
			Name: "SOLTERO",
		},
		{
			Code: "2",
			Name: "CASADO",
		},
		{
			Code: "3",
			Name: "DIVORCIADO",
		},
		{
			Code: "4",
			Name: "VIUDO",
		},
		{
			Code: "5",
			Name: "CONVIVIENTE",
		},
	}

	numCPU := viper.GetInt("kafka.threadCount")
	length := len(results)
	wg := sync.WaitGroup{}

	chunkSize := (length + numCPU - 1) / numCPU

	for i := 0; i < length; i += chunkSize {

		wg.Add(1)
		go func(i int, chunkSize int) {
			end := i + chunkSize
			defer wg.Done()

			if end > length {
				end = length
			}
			slice := results[i:end]

			for _, val := range slice {
				_, err2 := col.AddMaritalStatus(context.Background(), val)
				if err2 != nil {
					panic(err2)
				}
			}
		}(i, chunkSize)
	}
	wg.Wait()
}
