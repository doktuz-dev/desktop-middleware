package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"desktop-middleware/database/mysql"
	functions "desktop-middleware/functions"
	"sync"

	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	db := database.InitDatabases()
	results, err := mysql.GetConsultations(context.TODO())
	if err != nil {
		panic(err)
	}
	defer db.DisconnectDatabases()
	col := mongo.GetConsultationCollection()

	specialities, errGet2 := mongo.GetSpecialityCollection().GetSpecialities(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}
	numCPU := viper.GetInt("kafka.threadCount")
	length := len(results)
	wg := sync.WaitGroup{}

	chunkSize := (length + numCPU - 1) / numCPU

	for i := 0; i < length; i += chunkSize {

		wg.Add(1)
		go func(i int, chunkSize int) {
			end := i + chunkSize
			defer wg.Done()

			if end > length {
				end = length
			}
			slice := results[i:end]

			for _, val := range slice {
				att, _ := mongo.GetAttentionCollection().GetAttentionByExternalId(context.Background(), val.AttentionId)
				if att != nil {
					val.AttentionId = att.ID
					val.PersonID = att.PersonId
				}
				spe := functions.GetSpeciality(val.SpecialityID, specialities)
				if spe != nil {
					val.SpecialityID = spe.ID
				}
				consultation, _ := col.GetConsultationByExternalId(context.Background(), val.ExternalId)
				if consultation != nil {
					val.ID = consultation.ID
					val.ExternalSystem = "MEDIWEB"
					val.CreatedDate = consultation.CreatedDate
					_, errUpt := col.UpdateConsultation(context.Background(), val.ID, val)
					if errUpt != nil {
						panic(errUpt)
					}
				} else {
					val.ExternalSystem = "MEDIWEB"
					_, err2 := col.AddConsultation(context.Background(), val)
					if err2 != nil {
						panic(err2)
					}
				}
			}
		}(i, chunkSize)
	}
	wg.Wait()
}
