package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"desktop-middleware/database/mysql"
	functions "desktop-middleware/functions"
	"sync"

	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	db := database.InitDatabases()
	results, err := mysql.GetWorkProfiles(context.TODO())
	if err != nil {
		panic(err)
	}
	defer db.DisconnectDatabases()
	col := mongo.GetWorkProfileCollection()

	customers, errGet2 := mongo.GetCustomerCollection().GetCustomers(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}

	workProfiles, errGet2 := col.GetWorkProfiles(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}

	numCPU := viper.GetInt("kafka.threadCount")
	length := len(results)
	wg := sync.WaitGroup{}

	chunkSize := (length + numCPU - 1) / numCPU

	for i := 0; i < length; i += chunkSize {

		wg.Add(1)
		go func(i int, chunkSize int) {
			end := i + chunkSize
			defer wg.Done()

			if end > length {
				end = length
			}
			slice := results[i:end]

			for _, val := range slice {
				cust := functions.GetCustomer(val.CustomerId, customers)
				if cust != nil {
					val.CustomerId = cust.ID
				}
				workProfile := functions.GetWorkProfile(val.ExternalId, workProfiles)
				if workProfile != nil {
					val.ID = workProfile.ID
					val.ExternalSystem = "MEDIWEB"
					val.CreatedDate = workProfile.CreatedDate
					_, errUpt := col.UpdateWorkProfile(context.Background(), val.ID, val)
					if errUpt != nil {
						panic(errUpt)
					}
				} else {
					val.ExternalSystem = "MEDIWEB"
					_, err2 := col.AddWorkProfile(context.Background(), val)
					if err2 != nil {
						panic(err2)
					}
				}
			}
		}(i, chunkSize)
	}
	wg.Wait()
}
