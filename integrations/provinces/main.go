package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"desktop-middleware/database/mysql"
	functions "desktop-middleware/functions"
	"fmt"
	"sync"

	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	db := database.InitDatabases()
	results, err := mysql.GetProvinces(context.TODO())
	if err != nil {
		panic(err)
	}
	defer db.DisconnectDatabases()
	col := mongo.GetProvinceCollection()
	provinces, errGet := mongo.GetProvinceCollection().GetProvinces(context.Background())
	if errGet != nil {
		panic(errGet)
	}
	departments, errGet := mongo.GetDepartmentCollection().GetDepartments(context.Background())
	if errGet != nil {
		panic(errGet)
	}

	numCPU := viper.GetInt("kafka.threadCount")
	length := len(results)
	wg := sync.WaitGroup{}

	chunkSize := (length + numCPU - 1) / numCPU

	for i := 0; i < length; i += chunkSize {

		wg.Add(1)
		go func(i int, chunkSize int) {
			end := i + chunkSize
			defer wg.Done()

			if end > length {
				end = length
			}
			slice := results[i:end]

			for _, val := range slice {
				dept := functions.GetDepartment(val.DepartmentID, departments)
				if dept != nil {
					val.DepartmentID = dept.ID
				}
				cust := functions.GetProvince(val.ExternalId, provinces)
				if cust != nil {
					val.ID = cust.ID
					val.ExternalSystem = "MEDIWEB"
					val.CreatedDate = cust.CreatedDate
					_, errUpt := col.UpdateProvince(context.Background(), val.ID, val)
					if errUpt != nil {
						fmt.Println(errUpt)
					}
				} else {
					val.ExternalSystem = "MEDIWEB"
					_, err2 := col.AddProvince(context.Background(), val)
					if err2 != nil {
						fmt.Println(err2)
					}
				}
			}
		}(i, chunkSize)
	}
	wg.Wait()
}
