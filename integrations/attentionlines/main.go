package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"desktop-middleware/database/mysql"
	functions "desktop-middleware/functions"
	"sync"

	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	db := database.InitDatabases()
	results, err := mysql.GetAttentionLines(context.TODO())
	if err != nil {
		panic(err)
	}
	defer db.DisconnectDatabases()
	col := mongo.GetAttentionLineCollection()
	attentionLines, errGet := col.GetAttentionLines(context.Background())
	if errGet != nil {
		panic(errGet)
	}

	attentions, errGet2 := mongo.GetAttentionCollection().GetAttentions(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}

	services, errGet2 := mongo.GetServiceCollection().GetServices(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}
	numCPU := viper.GetInt("kafka.threadCount")
	length := len(results)
	wg := sync.WaitGroup{}

	chunkSize := (length + numCPU - 1) / numCPU

	for i := 0; i < length; i += chunkSize {

		wg.Add(1)
		go func(i int, chunkSize int) {
			end := i + chunkSize
			defer wg.Done()

			if end > length {
				end = length
			}
			slice := results[i:end]

			for _, val := range slice {
				attentionType := "1"
				att := functions.GetAttention(val.AttentionId, attentions)
				if att != nil {
					val.AttentionId = att.ID
					if att.AttentionTypeName == "Asistencial" {
						attentionType = "2"
					}
				}
				srv := functions.GetService(val.ServiceID, attentionType, services)
				if srv != nil {
					val.ServiceID = srv.ID
				}
				attentionLine := functions.GetAttentionLine(val.ExternalId, attentionLines)
				if attentionLine != nil {
					val.ID = attentionLine.ID
					val.ExternalSystem = "MEDIWEB"
					val.CreatedDate = attentionLine.CreatedDate
					_, errUpt := col.UpdateAttentionLine(context.Background(), val.ID, val)
					if errUpt != nil {
						panic(errUpt)
					}
				} else {
					val.ExternalSystem = "MEDIWEB"
					_, err2 := col.AddAttentionLine(context.Background(), val)
					if err2 != nil {
						panic(err2)
					}
				}
			}
		}(i, chunkSize)
	}
	wg.Wait()
}
