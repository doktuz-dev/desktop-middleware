package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"desktop-middleware/database/mysql"
	functions "desktop-middleware/functions"
	"sync"

	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	db := database.InitDatabases()
	results, err := mysql.GetServiceAreas(context.TODO())
	if err != nil {
		panic(err)
	}
	defer db.DisconnectDatabases()
	col := mongo.GetServiceAreaCollection()
	serviceAreas, errGet := col.GetServiceAreas(context.Background())
	if errGet != nil {
		panic(errGet)
	}

	numCPU := viper.GetInt("kafka.threadCount")
	length := len(results)
	wg := sync.WaitGroup{}

	chunkSize := (length + numCPU - 1) / numCPU

	for i := 0; i < length; i += chunkSize {

		wg.Add(1)
		go func(i int, chunkSize int) {
			end := i + chunkSize
			defer wg.Done()

			if end > length {
				end = length
			}
			slice := results[i:end]

			for _, val := range slice {
				cust := functions.GetServiceArea(val.ExternalId, serviceAreas)
				if cust != nil {
					val.ID = cust.ID
					switch val.ExternalId {
					case "1":
						val.Code = "OPHTALMOLOGY"
					case "2":
						val.Code = "GEOGRAPHICALHEIGHT"
					case "3":
						val.Code = "AUDIOMETRY"
					case "4":
						val.Code = "HEIGHT"
					case "5":
						val.Code = "XRAY"
					case "6":
						val.Code = "PSYCHOLOGY"
					case "7":
						val.Code = "SPIROMETRY"
					case "8":
						val.Code = "EKG"
					case "9":
						val.Code = "DENTISTRY"
					case "10":
						val.Code = "PATHOLOGICALHISTORY"
					case "11":
						val.Code = "DRIVINGCERTIFICATION"
					case "12":
						val.Code = "TRIAGE"
					case "13":
						val.Code = "MEDICINE"
					case "14":
						val.Code = "XRAYLD"
					case "18":
						val.Code = "SKELETALMUSCLE"
					case "19":
						val.Code = "NEUROLOGY"
					case "21":
						val.Code = "DIAGNOSTIC"
					case "22":
						val.Code = "WORKHISTORY"
					case "23":
						val.Code = "DERMATOLOGY"
					case "24":
						val.Code = "STRESSTEST"
					case "26":
						val.Code = "LABORATORY"
					case "27":
						val.Code = "DOCUMENT"
					case "28":
						val.Code = "PSYCHOSENSOMETRIC"
					case "29":
						val.Code = "CONSULTATION"
					case "30":
						val.Code = "APTITUDE"
					case "31":
						val.Code = "MEDICALREPORT"
					case "32":
						val.Code = "NUTRITION"
					case "33":
						val.Code = "CONFINEDSPACE"
					case "34":
						val.Code = "PSP"
					case "38":
						val.Code = "AUXILIARYEXAM"
					}
					val.ExternalSystem = "MEDIWEB"
					val.CreatedDate = cust.CreatedDate
					_, errUpt := col.UpdateServiceArea(context.Background(), val.ID, val)
					if errUpt != nil {
						panic(errUpt)
					}
				} else {
					switch val.ExternalId {
					case "1":
						val.Code = "OPHTALMOLOGY"
					case "2":
						val.Code = "GEOGRAPHICALHEIGHT"
					case "3":
						val.Code = "AUDIOMETRY"
					case "4":
						val.Code = "HEIGHT"
					case "5":
						val.Code = "XRAY"
					case "6":
						val.Code = "PSYCHOLOGY"
					case "7":
						val.Code = "SPIROMETRY"
					case "8":
						val.Code = "EKG"
					case "9":
						val.Code = "DENTISTRY"
					case "10":
						val.Code = "PATHOLOGICALHISTORY"
					case "11":
						val.Code = "DRIVINGCERTIFICATION"
					case "12":
						val.Code = "TRIAGE"
					case "13":
						val.Code = "MEDICINE"
					case "14":
						val.Code = "XRAYLD"
					case "18":
						val.Code = "SKELETALMUSCLE"
					case "19":
						val.Code = "NEUROLOGY"
					case "21":
						val.Code = "DIAGNOSTIC"
					case "22":
						val.Code = "WORKHISTORY"
					case "23":
						val.Code = "DERMATOLOGY"
					case "24":
						val.Code = "STRESSTEST"
					case "26":
						val.Code = "LABORATORY"
					case "27":
						val.Code = "DOCUMENT"
					case "28":
						val.Code = "PSYCHOSENSOMETRIC"
					case "29":
						val.Code = "CONSULTATION"
					case "30":
						val.Code = "APTITUDE"
					case "31":
						val.Code = "MEDICALREPORT"
					case "32":
						val.Code = "NUTRITION"
					case "33":
						val.Code = "CONFINEDSPACE"
					case "34":
						val.Code = "PSP"
					case "38":
						val.Code = "AUXILIARYEXAM"
					}
					val.ExternalSystem = "MEDIWEB"
					_, err2 := col.AddServiceArea(context.Background(), val)
					if err2 != nil {
						panic(err2)
					}
				}
			}
		}(i, chunkSize)
	}
	wg.Wait()
}
