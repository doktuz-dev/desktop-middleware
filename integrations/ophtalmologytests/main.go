package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"desktop-middleware/database/mysql"
	functions "desktop-middleware/functions"
	"desktop-middleware/models"
	"sync"

	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	db := database.InitDatabases()
	results, err := mysql.GetOphtalmologys(context.TODO())
	if err != nil {
		panic(err)
	}
	defer db.DisconnectDatabases()
	col := mongo.GetExamCollection()

	serviceAreas, errGet2 := mongo.GetServiceAreaCollection().GetServiceAreas(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}

	psychologyTests, errGet2 := mongo.GetOphtalmologyTestCollection().GetOphtalmologyTests(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}
	numCPU := viper.GetInt("kafka.threadCount")
	length := len(results)
	wg := sync.WaitGroup{}

	chunkSize := (length + numCPU - 1) / numCPU

	for i := 0; i < length; i += chunkSize {

		wg.Add(1)
		go func(i int, chunkSize int) {
			end := i + chunkSize
			defer wg.Done()

			if end > length {
				end = length
			}
			slice := results[i:end]

			for _, val := range slice {
				var newexam models.Exam
				var newTest models.OphtalmologyTest
				att, errGet2 := mongo.GetAttentionCollection().GetAttentionById(context.Background(), val.AttentionId)
				if errGet2 != nil {
					panic(errGet2)
				}
				if att != nil {
					val.AttentionId = att.ID
				}
				srv := functions.GetServiceArea(val.ServiceAreaID, serviceAreas)
				if srv != nil {
					val.ServiceAreaID = srv.ID
					val.ServiceAreaCode = srv.Code
				}
				test := functions.GetOphtalmologyTest(val.ID, psychologyTests)
				exam, errGet := col.GetExamById(context.Background(), val.ID, val.AttentionId, val.ServiceAreaID)
				if errGet != nil {
					panic(errGet)
				}
				if exam != nil {
					newexam.ID = exam.ID
					newexam.AttentionID = val.AttentionId
					newexam.ServiceAreaID = val.ServiceAreaID
					newexam.ServiceAreaCode = val.ServiceAreaCode
					newexam.Status = val.Status
					newexam.ExternalSystem = "MEDIWEB"
					newexam.CreatedDate = exam.CreatedDate
					_, errUpt := col.UpdateExam(context.Background(), newexam.ID, &newexam)
					if errUpt != nil {
						panic(errUpt)
					}

					mongo.GetDiagnosticCollection().DeleteDiagnostics(context.Background(), newexam.AttentionID, newexam.ServiceAreaID, newexam.ID)

					diagnostics := functions.GetDiagnosticsFromOphtalmology(newexam.ID, val)
					if len(diagnostics) > 0 {
						_, err2 := mongo.GetDiagnosticCollection().AddManyDiagnostics(context.Background(), diagnostics)
						if err2 != nil {
							panic(err2)
						}
					}
					if test != nil {
						newTest.ID = test.ID
						newTest.AttentionId = val.AttentionId
						newTest.ServiceAreaID = val.ServiceAreaID
						newTest.ServiceAreaCode = val.ServiceAreaCode
						newTest.ExamId = exam.ID
						newTest.DistanceNoCorrector = val.DistanceNoCorrector
						newTest.CloseNoCorrector = val.CloseNoCorrector
						newTest.DistanceWithCorrector = val.DistanceWithCorrector
						newTest.CloseWithCorrector = val.CloseWithCorrector
						newTest.Stereopsis = val.Stereopsis
						newTest.Status = val.Status
						_, errUpt := mongo.GetOphtalmologyTestCollection().UpdateOphtalmologyTest(context.Background(), newTest.ID, &newTest)
						if errUpt != nil {
							panic(errUpt)
						}
					} else {
						newTest.AttentionId = val.AttentionId
						newTest.ServiceAreaID = val.ServiceAreaID
						newTest.ServiceAreaCode = val.ServiceAreaCode
						newTest.ExamId = exam.ID
						newTest.DistanceNoCorrector = val.DistanceNoCorrector
						newTest.CloseNoCorrector = val.CloseNoCorrector
						newTest.DistanceWithCorrector = val.DistanceWithCorrector
						newTest.CloseWithCorrector = val.CloseWithCorrector
						newTest.Stereopsis = val.Stereopsis
						newTest.Status = val.Status
						newTest.ExternalId = val.ID
						newTest.ExternalSystem = "MEDIWEB"
						_, errUpt := mongo.GetOphtalmologyTestCollection().AddOphtalmologyTest(context.Background(), &newTest)
						if errUpt != nil {
							panic(errUpt)
						}
					}
				} else {
					newexam.AttentionID = val.AttentionId
					newexam.ServiceAreaID = val.ServiceAreaID
					newexam.ServiceAreaCode = val.ServiceAreaCode
					newexam.Status = val.Status
					newexam.ExternalId = val.ID
					newexam.ExternalSystem = "MEDIWEB"
					value, err2 := col.AddExam(context.Background(), &newexam)
					if err2 != nil {
						panic(err2)
					}

					mongo.GetDiagnosticCollection().DeleteDiagnostics(context.Background(), value.AttentionID, value.ServiceAreaID, value.ID)

					diagnostics := functions.GetDiagnosticsFromOphtalmology(value.ID, val)
					if len(diagnostics) > 0 {
						_, err2 := mongo.GetDiagnosticCollection().AddManyDiagnostics(context.Background(), diagnostics)
						if err2 != nil {
							panic(err2)
						}
					}
					if test != nil {
						newTest.ID = test.ID
						newTest.AttentionId = val.AttentionId
						newTest.ServiceAreaID = val.ServiceAreaID
						newTest.ServiceAreaCode = val.ServiceAreaCode
						newTest.ExamId = value.ID
						newTest.DistanceNoCorrector = val.DistanceNoCorrector
						newTest.CloseNoCorrector = val.CloseNoCorrector
						newTest.DistanceWithCorrector = val.DistanceWithCorrector
						newTest.CloseWithCorrector = val.CloseWithCorrector
						newTest.Status = val.Status
						_, errUpt := mongo.GetOphtalmologyTestCollection().UpdateOphtalmologyTest(context.Background(), newTest.ID, &newTest)
						if errUpt != nil {
							panic(errUpt)
						}
					} else {
						newTest.AttentionId = val.AttentionId
						newTest.ServiceAreaID = val.ServiceAreaID
						newTest.ServiceAreaCode = val.ServiceAreaCode
						newTest.ExamId = value.ID
						newTest.DistanceNoCorrector = val.DistanceNoCorrector
						newTest.CloseNoCorrector = val.CloseNoCorrector
						newTest.DistanceWithCorrector = val.DistanceWithCorrector
						newTest.CloseWithCorrector = val.CloseWithCorrector
						newTest.Status = val.Status
						newTest.ExternalId = val.ID
						newTest.ExternalSystem = "MEDIWEB"
						_, errUpt := mongo.GetOphtalmologyTestCollection().AddOphtalmologyTest(context.Background(), &newTest)
						if errUpt != nil {
							panic(errUpt)
						}
					}
				}
			}
		}(i, chunkSize)
	}
	wg.Wait()
}
