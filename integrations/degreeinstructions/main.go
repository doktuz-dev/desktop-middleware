package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"desktop-middleware/models"
	"sync"

	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	db := database.InitDatabases()
	defer db.DisconnectDatabases()
	col := mongo.GetDegreeInstructionCollection()
	results := []*models.DegreeInstruction{
		{
			Code: "1",
			Name: "Analfabeto",
		},
		{
			Code: "2",
			Name: "Primaria Completa",
		},
		{
			Code: "3",
			Name: "Primaria Incompleta",
		},
		{
			Code: "4",
			Name: "Secundaria Completa",
		},
		{
			Code: "5",
			Name: "Secundaria Incompleta",
		},
		{
			Code: "6",
			Name: "Tecnico Completo",
		},
		{
			Code: "7",
			Name: "Universitario Completo",
		},
		{
			Code: "8",
			Name: "Tecnico Incompleto",
		},
		{
			Code: "9",
			Name: "Universitario Incompleto",
		},
	}
	numCPU := viper.GetInt("kafka.threadCount")
	length := len(results)
	wg := sync.WaitGroup{}

	chunkSize := (length + numCPU - 1) / numCPU

	for i := 0; i < length; i += chunkSize {

		wg.Add(1)
		go func(i int, chunkSize int) {
			end := i + chunkSize
			defer wg.Done()

			if end > length {
				end = length
			}
			slice := results[i:end]

			for _, val := range slice {
				_, err2 := col.AddDegreeInstruction(context.Background(), val)
				if err2 != nil {
					panic(err2)
				}
			}
		}(i, chunkSize)
	}
	wg.Wait()
}
