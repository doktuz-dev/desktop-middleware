package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"desktop-middleware/database/mysql"
	functions "desktop-middleware/functions"
	"desktop-middleware/models"
	"fmt"
	"strings"
	"sync"

	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	db := database.InitDatabases()
	results, err := mysql.GetPersons(context.TODO())
	if err != nil {
		panic(err)
	}
	defer db.DisconnectDatabases()

	maritalStatus, errGet2 := mongo.GetMaritalStatusCollection().GetMaritalStatuss(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}

	degreeInstruction, errGet2 := mongo.GetDegreeInstructionCollection().GetDegreeInstructions(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}

	patients, errGet2 := mongo.GetPatientCollection().GetPatients(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}

	districts, errGet2 := mongo.GetDistrictCollection().GetDistricts(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}

	numCPU := viper.GetInt("kafka.threadCount")
	length := len(results)
	wg := sync.WaitGroup{}

	chunkSize := (length + numCPU - 1) / numCPU

	for i := 0; i < length; i += chunkSize {

		wg.Add(1)
		go func(i int, chunkSize int) {
			end := i + chunkSize
			defer wg.Done()

			if end > length {
				end = length
			}
			slice := results[i:end]

			for _, val := range slice {
				var newpatient models.Patient
				degree := functions.GetDegreeInstructions(val.DegreeInstructionId, degreeInstruction)
				mar := functions.GetMaritalStatus(val.MaritalStatusId, maritalStatus)
				district := functions.GetDistrict(val.DistrictId, districts)
				if mar != nil {
					newpatient.MaritalStatusId = mar.ID
					newpatient.MaritalStatusName = mar.Name
				}
				if degree != nil {
					newpatient.DegreeInstructionId = degree.ID
					newpatient.DegreeInstructionName = degree.Name
				}
				if district != nil {
					newpatient.DistrictId = district.ID
					newpatient.DistrictName = district.Name
				}

				newpatient.DocumentNumber = val.DocumentNumber
				newpatient.DocumentTypeID = val.DocumentTypeID
				newpatient.FirstName = val.FirstName
				newpatient.LastName = val.LastName
				newpatient.BirthDate = val.BirthDate
				newpatient.Sex = val.Sex
				newpatient.Email = strings.ToLower(val.Email)
				newpatient.PhoneNumber = val.PhoneNumber
				newpatient.ExternalSystem = "MEDIWEB"
				newpatient.ExternalId = val.ID
				newpatient.PersonID = ""
				patient := functions.GetPatient(val.DocumentTypeID, val.DocumentNumber, patients)
				if patient != nil {
					newpatient.ID = patient.ID
					newpatient.CreatedDate = patient.CreatedDate
					_, err2 := mongo.GetPatientCollection().UpdatePatient(context.Background(), newpatient.ID, &newpatient)
					if err2 != nil {
						panic(err2)
					}
					fmt.Println("updated")
				} else {
					_, err2 := mongo.GetPatientCollection().AddPatient(context.Background(), &newpatient)
					if err2 != nil {
						panic(err2)
					}
					fmt.Println("created")
				}
			}
		}(i, chunkSize)
	}
	wg.Wait()
}
