package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"desktop-middleware/database/mysql"
	functions "desktop-middleware/functions"
	"sync"

	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	db := database.InitDatabases()
	results, err := mysql.GetSpecialities(context.TODO())
	if err != nil {
		panic(err)
	}
	defer db.DisconnectDatabases()
	col := mongo.GetSpecialityCollection()
	specialitys, errGet := col.GetSpecialities(context.Background())
	if errGet != nil {
		panic(errGet)
	}

	numCPU := viper.GetInt("kafka.threadCount")
	length := len(results)
	wg := sync.WaitGroup{}

	chunkSize := (length + numCPU - 1) / numCPU

	for i := 0; i < length; i += chunkSize {

		wg.Add(1)
		go func(i int, chunkSize int) {
			end := i + chunkSize
			defer wg.Done()

			if end > length {
				end = length
			}
			slice := results[i:end]

			for _, val := range slice {
				cust := functions.GetSpeciality(val.ExternalId, specialitys)
				if cust != nil {
					val.ID = cust.ID
					val.Code = val.ExternalId
					val.ExternalSystem = "MEDIWEB"
					val.CreatedDate = cust.CreatedDate
					_, errUpt := col.UpdateSpeciality(context.Background(), val.ID, val)
					if errUpt != nil {
						panic(errUpt)
					}
				} else {
					val.ExternalSystem = "MEDIWEB"
					val.Code = val.ExternalId
					_, err2 := col.AddSpeciality(context.Background(), val)
					if err2 != nil {
						panic(err2)
					}
				}
			}
		}(i, chunkSize)
	}
	wg.Wait()
}
