package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/consumer_models"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	mongo_v2 "desktop-middleware/database/mongo_v2"
	"desktop-middleware/models"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	fmt.Println("Consumer started!")
	topic := viper.GetString("kafka.database") + ".dbmediweb.procedimiento_particular"
	fmt.Println(topic)
	// Create Consumer instance
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": viper.GetString("kafka.bootstrap_servers"),
		"sasl.mechanisms":   viper.GetString("kafka.sasl_mechanisms"),
		"security.protocol": viper.GetString("kafka.security_protocol"),
		"sasl.username":     viper.GetString("kafka.sasl_username"),
		"sasl.password":     viper.GetString("kafka.sasl_password"),
		"group.id":          viper.GetString("kafka.groupId"),
		"auto.offset.reset": "earliest",
	})
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	err = c.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	// Set up a channel for handling Ctrl-C, etc
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	// Process messages
	run := true
	flag := true
	var services []*consumer_models.NoEmoServiceConsumer
	db := database.InitDatabases()
	defer db.DisconnectDatabases()
	for run {
		var logModel models.Log
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			if flag {
				msg, err := c.ReadMessage(100 * time.Millisecond)
				if err != nil {
					// Errors are informational and automatically handled by the consumer
					flag = false
					continue
				}
				if msg.Value == nil || len(msg.Value) == 0 {
					continue
				}
				var service consumer_models.NoEmoServiceConsumer
				logModel.Collection = "middleware.registers.services"
				logModel.Status = "In Progress"
				logModel.Body = msg.Value
				log, _ := mongo.GetLogsCollection().AddLogs(context.TODO(), &logModel)
				err = json.Unmarshal(msg.Value, &service)
				if err != nil {
					logModel.Operation = "Decode"
					logModel.Status = "Error"
					logModel.Collection = "middleware.registers.services"
					logModel.Error = fmt.Sprintf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					json.Unmarshal(msg.Value, &logModel.Body)
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), log.ID, &logModel)
					fmt.Printf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					flag = false
					continue
				}
				service.IdLog = log.ID
				services = append(services, &service)
				if len(services) > viper.GetInt("kafka.fetchCount") {
					flag = false
				}
			} else {
				if len(services) > 0 {
					numCPU := viper.GetInt("kafka.threadCount")
					length := len(services)
					wg := sync.WaitGroup{}
					chunkSize := (length + numCPU - 1) / numCPU

					for i := 0; i < length; i += chunkSize {

						wg.Add(1)
						go func(i int, chunkSize int) {
							end := i + chunkSize
							defer wg.Done()

							if end > length {
								end = length
							}
							slice := services[i:end]

							for _, service := range slice {

								flag, err := strconv.ParseBool(service.IsDeleted)
								if err != nil {
									logModel.Operation = "Decode"
									logModel.Status = "Error"
									logModel.Collection = "middleware.registers.services"
									logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
									logModel.Body = service
									mongo.GetLogsCollection().UpdateLogs(context.TODO(), service.IdLog, &logModel)
									fmt.Printf("Failed to decode JSON: %v", err)
									continue
								}

								if flag {
									_, err := mongo.GetServiceCollection().DeleteServiceByExternalId(context.TODO(), strconv.Itoa(service.ID), "2")
									if err != nil {
										logModel.Operation = "Delete"
										logModel.Status = "Error"
										logModel.Collection = "middleware.registers.services"
										logModel.Error = fmt.Sprintf("Failed to delete service: %v", err)
										logModel.Body = service
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), service.IdLog, &logModel)
										fmt.Printf("Failed to delete service: %v", err)
									} else {
										logModel.Operation = "Delete"
										logModel.Status = "Done"
										logModel.Collection = "middleware.registers.services"
										logModel.Error = ""
										logModel.Body = service
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), service.IdLog, &logModel)
									}
								} else {
									oldservice, err := mongo.GetServiceCollection().GetServiceById(context.TODO(), strconv.Itoa(service.ID), "2")
									if err != nil {
										logModel.Operation = "Get"
										logModel.Status = "Error"
										logModel.Collection = "middleware.registers.services"
										logModel.Error = fmt.Sprintf("Failed to get JSON: %v", err)
										logModel.Body = service
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), service.IdLog, &logModel)
										fmt.Printf("Failed to get service: %v", err)
									}

									if oldservice != nil {
										oldservice.Name = service.Name
										oldservice.Code = service.Code
										oldservice.Status = service.Status
										oldservice.Blocked, _ = strconv.Atoi(strings.TrimSpace(service.Blocked))
										oldservice.AttentionTypeId = "2"
										_, err := mongo.GetServiceCollection().UpdateService(context.TODO(), oldservice.ID, oldservice)
										if err != nil {
											logModel.Operation = "Update"
											logModel.Status = "Error"
											logModel.Collection = "middleware.registers.services"
											logModel.Error = fmt.Sprintf("Failed to update JSON: %v", err)
											logModel.Body = service
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), service.IdLog, &logModel)
											fmt.Printf("Failed to update service: %v", err)
											continue
										} else {
											logModel.Operation = "Update"
											logModel.Status = "Done"
											logModel.Collection = "middleware.registers.services"
											logModel.Error = ""
											logModel.Body = service
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), service.IdLog, &logModel)
										}
										go mongo_v2.GetAttentionCollection().UpdateServicesInAttentionLines(context.Background(), oldservice.ID, oldservice)
										fmt.Println("updated: ", oldservice.ID)
									} else {
										temp, _ := strconv.Atoi(strings.TrimSpace(service.Blocked))
										newService := models.Service{
											Name:            service.Name,
											Code:            service.Code,
											AttentionTypeId: "2",
											Status:          service.Status,
											Blocked:         temp,
											ExternalId:      strconv.Itoa(service.ID),
										}
										val, err := mongo.GetServiceCollection().AddService(context.TODO(), &newService)
										if err != nil {
											logModel.Operation = "Insert"
											logModel.Status = "Error"
											logModel.Collection = "middleware.registers.services"
											logModel.Error = fmt.Sprintf("Failed to add JSON: %v", err)
											logModel.Body = service
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), service.IdLog, &logModel)
											fmt.Printf("Failed to add service: %v", err)
											continue
										} else {
											logModel.Operation = "Insert"
											logModel.Status = "Done"
											logModel.Collection = "middleware.registers.services"
											logModel.Error = ""
											logModel.Body = service
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), service.IdLog, &logModel)
										}
										fmt.Println("created: ", val.ID)
									}
								}
							}
						}(i, chunkSize)
					}
					wg.Wait()
					services = []*consumer_models.NoEmoServiceConsumer{}
				}
				flag = true
			}
		}
	}

	fmt.Printf("Closing consumer\n")
	c.Close()
}
