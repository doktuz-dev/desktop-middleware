package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/consumer_models"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	mongo_v2 "desktop-middleware/database/mongo_v2"
	"desktop-middleware/functions"
	"desktop-middleware/models"
	"desktop-middleware/models_v2"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	"github.com/araddon/dateparse"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	fmt.Println("Consumer started!")
	topic := viper.GetString("kafka.database") + ".dbmediweb.paciente"
	fmt.Println(topic)
	// Create Consumer instance
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": viper.GetString("kafka.bootstrap_servers"),
		"sasl.mechanisms":   viper.GetString("kafka.sasl_mechanisms"),
		"security.protocol": viper.GetString("kafka.security_protocol"),
		"sasl.username":     viper.GetString("kafka.sasl_username"),
		"sasl.password":     viper.GetString("kafka.sasl_password"),
		"group.id":          viper.GetString("kafka.groupId"),
		"auto.offset.reset": "earliest",
	})
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	err = c.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	// Set up a channel for handling Ctrl-C, etc
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	// Process messages
	run := true
	flag := true
	var persons []*consumer_models.PersonConsumer
	db := database.InitDatabases()
	defer db.DisconnectDatabases()

	maritalStatus, errGet2 := mongo.GetMaritalStatusCollection().GetMaritalStatuss(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}

	degreeInstruction, errGet2 := mongo.GetDegreeInstructionCollection().GetDegreeInstructions(context.Background())
	if errGet2 != nil {
		panic(errGet2)
	}
	for run {
		var logModel models.Log
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			if flag {
				msg, err := c.ReadMessage(100 * time.Millisecond)
				if err != nil {
					// Errors are informational and automatically handled by the consumer
					flag = false
					continue
				}
				if msg.Value == nil || len(msg.Value) == 0 {
					continue
				}
				var person consumer_models.PersonConsumer
				logModel.Collection = "middleware.registers.patients2"
				logModel.Status = "In Progress"
				logModel.Body = msg.Value
				log, _ := mongo.GetLogsCollection().AddLogs(context.TODO(), &logModel)
				err = json.Unmarshal(msg.Value, &person)
				if err != nil {
					logModel.Operation = "Decode"
					logModel.Status = "Error"
					logModel.Collection = "middleware.registers.patients2"
					logModel.Error = fmt.Sprintf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					json.Unmarshal(msg.Value, &logModel.Body)
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), log.ID, &logModel)
					fmt.Printf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					flag = false
					continue
				}
				person.IdLog = log.ID
				persons = append(persons, &person)
				if len(persons) > viper.GetInt("kafka.fetchCount") {
					flag = false
				}
			} else {
				if len(persons) > 0 {
					numCPU := viper.GetInt("kafka.threadCount")
					length := len(persons)
					wg := sync.WaitGroup{}
					chunkSize := (length + numCPU - 1) / numCPU

					for i := 0; i < length; i += chunkSize {

						wg.Add(1)
						go func(i int, chunkSize int) {
							end := i + chunkSize
							defer wg.Done()

							if end > length {
								end = length
							}
							slice := persons[i:end]

							for _, person := range slice {
								AddPersonConsumerV2(person, logModel, maritalStatus, degreeInstruction)
							}
						}(i, chunkSize)
					}
					wg.Wait()
					persons = []*consumer_models.PersonConsumer{}
				}
				flag = true
			}
		}
	}

	fmt.Printf("Closing consumer\n")
	c.Close()
}

func AddPersonConsumerV2(person *consumer_models.PersonConsumer, logModel models.Log, maritalStatus []*models.MaritalStatus, degreeInstruction []*models.DegreeInstruction) {

	flag, err := strconv.ParseBool(person.IsDeleted)
	if err != nil {
		logModel.Operation = "Decode"
		logModel.Status = "Error"
		logModel.Collection = "middleware.registers.patients2"
		logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
		logModel.Body = person
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), person.IdLog, &logModel)
		fmt.Printf("Failed to decode JSON: %v", err)
		return
	}
	district, _ := mongo_v2.GetDistrictCollection().GetDistrictById(context.TODO(), person.District)
	if flag {
		_, err := mongo_v2.GetPatientCollection().DeletePatientByExternalId(context.TODO(), strconv.Itoa(person.ID))
		if err != nil {
			logModel.Operation = "Delete"
			logModel.Status = "Error"
			logModel.Collection = "middleware.registers.patients2"
			logModel.Error = fmt.Sprintf("Failed to delete person: %v", err)
			logModel.Body = person
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), person.IdLog, &logModel)
			fmt.Printf("Failed to delete person: %v", err)
		} else {
			logModel.Operation = "Delete"
			logModel.Status = "Done"
			logModel.Collection = "middleware.registers.patients2"
			logModel.Error = ""
			logModel.Body = person
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), person.IdLog, &logModel)
		}
	} else {
		oldpatient, _ := mongo_v2.GetPatientCollection().GetPatientById(context.TODO(), strconv.Itoa(person.ID))
		if oldpatient == nil {
			oldpatient, _ = mongo_v2.GetPatientCollection().GetPatientByDocument(context.TODO(), person.DocumentNumber, person.DocumentTypeID)
		}
		if oldpatient != nil {
			oldpatient.FirstName = person.FirstName
			oldpatient.LastName = person.LastName
			oldpatient.DocumentNumber = person.DocumentNumber
			oldpatient.DocumentTypeID = person.DocumentTypeID
			birthDate, _ := dateparse.ParseLocal(person.BirthDate)
			oldpatient.BirthDate = birthDate
			oldpatient.Sex = person.Sex
			oldpatient.Email = person.Email
			oldpatient.PhoneNumber = person.PhoneNumber
			oldpatient.ExternalId = strconv.Itoa(person.ID)
			oldpatient.ExternalSystem = "MEDIWEB"
			mar := functions.GetMaritalStatus(person.MaritalStatus, maritalStatus)
			if mar != nil {
				oldpatient.MaritalStatusId = mar.ID
				oldpatient.MaritalStatusName = mar.Name
			}
			degree := functions.GetDegreeInstructions(person.DegreeInstructions, degreeInstruction)
			if degree != nil {
				oldpatient.DegreeInstructionId = degree.ID
				oldpatient.DegreeInstructionName = degree.Name
			}
			if district != nil {
				oldpatient.DistrictId = district.ID
				oldpatient.DistrictName = district.Name
			}
			mongo_v2.GetPatientCollection().UpdatePatient(context.TODO(), oldpatient.ID, oldpatient)
			UpdatePerson(person, oldpatient, maritalStatus, degreeInstruction, district)
			mongo_v2.GetAttentionCollection().CustomSetFieldById(context.Background(), "patientId", oldpatient.ID, "patient", oldpatient)
			fmt.Println("updated: ", oldpatient.ID)
		} else {
			var newPatient models_v2.Patient
			newPatient.FirstName = person.FirstName
			newPatient.LastName = person.LastName
			newPatient.DocumentNumber = person.DocumentNumber
			newPatient.DocumentTypeID = person.DocumentTypeID
			birthDate, _ := dateparse.ParseLocal(person.BirthDate)
			newPatient.BirthDate = birthDate
			newPatient.Sex = person.Sex
			newPatient.Email = person.Email
			newPatient.PhoneNumber = person.PhoneNumber
			mar := functions.GetMaritalStatus(person.MaritalStatus, maritalStatus)
			if mar != nil {
				newPatient.MaritalStatusId = mar.ID
				newPatient.MaritalStatusName = mar.Name
			}
			degree := functions.GetDegreeInstructions(person.DegreeInstructions, degreeInstruction)
			if degree != nil {
				newPatient.DegreeInstructionId = degree.ID
				newPatient.DegreeInstructionName = degree.Name
			}
			if district != nil {
				newPatient.DistrictId = district.ID
				newPatient.DistrictName = district.Name
			}
			newPatient.ExternalId = strconv.Itoa(person.ID)
			newPatient.ExternalSystem = "MEDIWEB"
			val, err := mongo_v2.GetPatientCollection().AddPatient(context.TODO(), &newPatient)
			if err != nil {
				logModel.Operation = "Insert"
				logModel.Status = "Error"
				logModel.Collection = "middleware.registers.patients2"
				logModel.Error = fmt.Sprintf("Failed to insert person: %v", err)
				logModel.Body = person
				mongo.GetLogsCollection().UpdateLogs(context.TODO(), person.IdLog, &logModel)
				fmt.Printf("Failed to add person: %v", err)
				return
			} else {
				logModel.Operation = "Insert"
				logModel.Status = "Done"
				logModel.Collection = "middleware.registers.patients2"
				logModel.Error = ""
				logModel.Body = person
				mongo.GetLogsCollection().UpdateLogs(context.TODO(), person.IdLog, &logModel)
			}
			UpdatePerson(person, val, maritalStatus, degreeInstruction, district)
			fmt.Println("created: ", val.ID)
		}
	}
}

func UpdatePerson(consumer *consumer_models.PersonConsumer, patient *models_v2.Patient, maritalStatus []*models.MaritalStatus, degreeInstruction []*models.DegreeInstruction, district *models.District) {
	var logModel models.Log
	oldperson, _ := mongo_v2.GetPersonCollection().GetPersonByDocumentNumber(context.TODO(), patient.DocumentNumber, patient.DocumentTypeID)
	if oldperson != nil {
		mar := functions.GetMaritalStatus(consumer.MaritalStatus, maritalStatus)
		if mar != nil {
			oldperson.MaritalStatusId = &mar.ID
			oldperson.MaritalStatusName = &mar.Name
		}
		degree := functions.GetDegreeInstructions(consumer.DegreeInstructions, degreeInstruction)
		if degree != nil {
			oldperson.DegreeInstructionId = &degree.ID
			oldperson.DegreeInstructionName = &degree.Name
		}
		if district != nil {
			oldperson.DistrictId = &district.ID
			oldperson.DistrictName = &district.Name
		}
		oldperson.PatientId = &patient.ID
		oldperson.Patient = patient
		_, err := mongo_v2.GetPersonCollection().UpdatePerson(context.TODO(), oldperson.ID, oldperson)
		if err != nil {
			logModel.Operation = "Update"
			logModel.Status = "Error"
			logModel.Collection = "middleware.registers.patients2"
			logModel.Error = fmt.Sprintf("Failed to update JSON: %v", err)
			logModel.Body = consumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), consumer.IdLog, &logModel)
			fmt.Printf("Failed to update person: %v", err)
		} else {
			logModel.Operation = "Update"
			logModel.Status = "Done"
			logModel.Collection = "middleware.registers.patients2"
			logModel.Error = ""
			logModel.Body = consumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), consumer.IdLog, &logModel)
		}

		_, err = mongo_v2.GetAttentionCollection().UpdatePersonIdToAttentions(context.TODO(), *oldperson.PatientId, oldperson.ID)
		if err != nil {
			logModel.Operation = "Update"
			logModel.Status = "Error"
			logModel.Collection = "middleware.registers.patients2"
			logModel.Error = fmt.Sprintf("Failed to update JSON: %v", err)
			logModel.Body = consumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), consumer.IdLog, &logModel)
			fmt.Printf("Failed to update person: %v", err)
		} else {
			logModel.Operation = "Update"
			logModel.Status = "Done"
			logModel.Collection = "middleware.registers.patients2"
			logModel.Error = ""
			logModel.Body = consumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), consumer.IdLog, &logModel)
		}
		oldperson.LastAnyEmoAttention = nil
		oldperson.LastEmoAttention = nil
		oldperson.LastNoEmoAttention = nil
		oldperson.LastEmoScheduling = nil
		oldperson.LastNoEmoScheduling = nil
		mongo_v2.GetAttentionCollection().CustomSetFieldById(context.Background(), "personId", oldperson.ID, "person", oldperson)
	}
}
