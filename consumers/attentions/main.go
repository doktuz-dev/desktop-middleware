package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/consumer_models"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	mongo_v2 "desktop-middleware/database/mongo_v2"
	"desktop-middleware/models"
	"desktop-middleware/models_v2"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/araddon/dateparse"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func main() {
	config.SetConfiguration()
	fmt.Println("Consumer started!")
	topic := viper.GetString("kafka.database") + ".dbmediweb.comprobante"
	fmt.Println(topic)
	// Create Consumer instance
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": viper.GetString("kafka.bootstrap_servers"),
		"sasl.mechanisms":   viper.GetString("kafka.sasl_mechanisms"),
		"security.protocol": viper.GetString("kafka.security_protocol"),
		"sasl.username":     viper.GetString("kafka.sasl_username"),
		"sasl.password":     viper.GetString("kafka.sasl_password"),
		"group.id":          viper.GetString("kafka.groupId"),
		"auto.offset.reset": "earliest",
	})
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	err = c.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	// Set up a channel for handling Ctrl-C, etc
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	// Process messages
	run := true
	flag := true
	var attentions []*consumer_models.AttentionConsumer
	db := database.InitDatabases()
	defer db.DisconnectDatabases()
	for run {
		var logModel models.Log
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			if flag {
				msg, err := c.ReadMessage(100 * time.Millisecond)
				if err != nil {
					// Errors are informational and automatically handled by the consumer
					flag = false
					continue
				}
				if msg.Value == nil || len(msg.Value) == 0 {
					continue
				}
				var attention consumer_models.AttentionConsumer
				logModel.Collection = "middleware.transactions.attentions"
				logModel.Status = "In Progress"
				logModel.Body = msg.Value
				log, _ := mongo.GetLogsCollection().AddLogs(context.TODO(), &logModel)
				err = json.Unmarshal(msg.Value, &attention)
				if err != nil {
					logModel.Operation = "Decode"
					logModel.Status = "Error"
					logModel.Error = fmt.Sprintf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					json.Unmarshal(msg.Value, &logModel.Body)
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), log.ID, &logModel)
					fmt.Printf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					flag = false
					continue
				}
				attention.IdLog = log.ID
				attentions = append(attentions, &attention)
				if len(attentions) > viper.GetInt("kafka.fetchCount") {
					flag = false
				}
			} else {
				if len(attentions) > 0 {
					numCPU := viper.GetInt("kafka.threadCount")
					length := len(attentions)
					wg := sync.WaitGroup{}
					chunkSize := (length + numCPU - 1) / numCPU

					for i := 0; i < length; i += chunkSize {

						wg.Add(1)
						go func(i int, chunkSize int) {
							end := i + chunkSize
							defer wg.Done()

							if end > length {
								end = length
							}
							slice := attentions[i:end]

							for _, attention := range slice {
								attentionId := primitive.NewObjectID().Hex()
								AddAttentionV2(attention, logModel, attentionId)
							}
						}(i, chunkSize)
					}
					wg.Wait()
					attentions = []*consumer_models.AttentionConsumer{}
				}
				flag = true
			}
		}
	}

	fmt.Printf("Closing consumer\n")
	c.Close()
}

func AddAttentionV2(attention *consumer_models.AttentionConsumer, logModel models.Log, attentionId string) {
	oldattention, err := mongo_v2.GetAttentionCollection().GetAttentionByExternalId(context.TODO(), strconv.Itoa(attention.ID))
	if err != nil {
		logModel.Operation = "Get"
		logModel.Status = "Error"
		logModel.Error = fmt.Sprintf("Failed to get attention: %v", err)
		logModel.Body = attention
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), attention.IdLog, &logModel)
		fmt.Printf("Failed to get attention: %v", err)
	}

	if oldattention != nil {
		if attention.CustomerId != "" {
			cust, _ := mongo_v2.GetCustomerCollection().GetCustomerById(context.Background(), attention.CustomerId)
			if cust != nil {
				oldattention.CustomerId = &cust.ID
				oldattention.CustomerCode = &cust.Code
				oldattention.CustomerName = &cust.Name
				oldattention.Customer = cust
			}
		} else if attention.CustomerId2 != 0 {
			cust, _ := mongo_v2.GetCustomerCollection().GetCustomerById(context.Background(), strconv.Itoa(attention.CustomerId2))
			if cust != nil {
				oldattention.CustomerId = &cust.ID
				oldattention.CustomerCode = &cust.Code
				oldattention.Customer = cust
			}
		}
		pat, _ := mongo_v2.GetPatientCollection().GetPatientById(context.Background(), attention.PatientId)
		if pat != nil {
			oldattention.PatientId = &pat.ID
			oldattention.Patient = pat
			if oldattention.CustomerId != nil {
				per, _ := mongo_v2.GetPersonCollection().GetPersonByDocumentNumberAndCustomer(context.Background(), pat.DocumentNumber, pat.DocumentTypeID, *oldattention.CustomerId)
				if per != nil {
					oldattention.PersonId = &per.ID
					oldattention.Person = per
				} else {
					mongo.GetAttentionCollection().UnsetPersonIdAttention(context.TODO(), oldattention.ID)
				}
			}
		} else {
			mongo.GetAttentionCollection().UnsetPersonIdAttention(context.TODO(), oldattention.ID)
		}
		depa, _ := mongo.GetDepartmentCollection().GetDepartmentById(context.Background(), strconv.Itoa(attention.DepartmentId))
		if depa != nil {
			oldattention.DepartmentId = &depa.ID
			oldattention.DepartmentName = &depa.Name
		}
		district, _ := mongo.GetDistrictCollection().GetDistrictById(context.Background(), strconv.Itoa(attention.DistrictId))
		if district != nil {
			oldattention.DistrictId = &district.ID
			oldattention.DistrictName = &district.Name
		}
		attType, _ := mongo.GetAttentionTypeCollection().GetAttentionTypesById(context.TODO(), attention.AttentionTypeId)
		if attType != nil {
			oldattention.AttentionTypeId = &attType.ID
			oldattention.AttentionTypeName = &attType.Name
		}
		if attention.AttentionTypeId == "1" {
			delType, _ := mongo.GetDeliveryTypeCollection().GetDeliveryTypeTypesById(context.TODO(), strconv.Itoa(attention.NoemoDeliveryTypeId))
			if delType != nil {
				oldattention.DeliveryTypeId = &delType.ID
				oldattention.DeliveryTypeName = &delType.Name
			}
		} else if attention.AttentionTypeId == "2" {
			delType, _ := mongo.GetDeliveryTypeCollection().GetDeliveryTypeTypesById(context.TODO(), attention.EmoDeliveryTypeId)
			if delType != nil {
				oldattention.DeliveryTypeId = &delType.ID
				oldattention.DeliveryTypeName = &delType.Name
			}
		}
		healthCentr, _ := mongo_v2.GetHealthCentreCollection().GetHealthCentreById(context.TODO(), strconv.Itoa(attention.HealthcentreId))
		if healthCentr != nil {
			oldattention.HealthCentreId = &healthCentr.ID
			oldattention.HealthCentreName = &healthCentr.Name
			oldattention.Healthcentre = healthCentr
		}
		workSta, _ := mongo_v2.GetWorkStationCollection().GetWorkStationById(context.TODO(), strconv.Itoa(attention.WorkStationId))
		if workSta != nil {
			oldattention.WorkStationId = &workSta.ID
			oldattention.WorkStationName = &workSta.Name
			oldattention.WorkStation = workSta
		}
		workPro, _ := mongo_v2.GetWorkProfileCollection().GetWorkProfileById(context.TODO(), strconv.Itoa(attention.WorkProfileId))
		if workPro != nil {
			oldattention.WorkProfileId = &workPro.ID
			oldattention.WorkProfileName = &workPro.Name
			oldattention.WorkProfile = workPro
		}
		examType, _ := mongo.GetExamTypeCollection().GetExamTypeById(context.TODO(), attention.ExamTypeId)
		if examType != nil {
			oldattention.ExamTypeId = &examType.ID
			oldattention.ExamTypeName = &examType.Name
		}
		if attention.ExternalClinicID != 0 {
			externalClinic, _ := mongo_v2.GetExternalClinicCollection().GetExternalClinicById(context.TODO(), attention.ExternalClinicID)
			if externalClinic != nil {
				oldattention.ExternalClinicID = &externalClinic.ID
				oldattention.ExternalClinicName = &externalClinic.Name
				oldattention.ExternalClinic = externalClinic
			}
		}
		attentionDate, _ := dateparse.ParseLocal(attention.AttentionDate)
		oldattention.AttentionDate = &attentionDate
		oldattention.MailSentDateTime = attention.SetSentDate()
		oldattention.MailOpenedDateTime = attention.SetOpenedDate()
		oldattention.Status = &attention.Status
		oldattention.WorkingArea = &attention.WorkingArea
		oldattention.JobTitle = &attention.JobTitle
		if strings.TrimSpace(attention.ExternalClinicFlag) == "on" {
			externalClinicFlag := true
			oldattention.ExternalClinicFlag = &externalClinicFlag
		} else {
			externalClinicFlag := false
			oldattention.ExternalClinicFlag = &externalClinicFlag
		}
		if attention.MailStatus != nil {
			switch *attention.MailStatus {
			case 0:
				mailStatus := "Sin resultado"
				oldattention.MailStatus = &mailStatus
			case 1:
				mailStatus := "Por enviar"
				oldattention.MailStatus = &mailStatus
			case 2:
				mailStatus := "Enviado"
				oldattention.MailStatus = &mailStatus
			case 3:
				mailStatus := "Abierto"
				oldattention.MailStatus = &mailStatus
			case 4:
				mailStatus := "Correo Invalido"
				oldattention.MailStatus = &mailStatus
			case 5:
				mailStatus := "Mismo Doc y Correo Diferente"
				oldattention.MailStatus = &mailStatus
			default:
				mailStatus := "-"
				oldattention.MailStatus = &mailStatus
			}
		} else {
			mailStatus := "-"
			oldattention.MailStatus = &mailStatus
		}
		if attention.WasAttended != nil {
			if *attention.WasAttended == "on" {
				wasAttended := true
				oldattention.WasAttended = &wasAttended
			} else {
				wasAttended := false
				oldattention.WasAttended = &wasAttended
			}
		} else {
			wasAttended := false
			oldattention.WasAttended = &wasAttended
		}
		_, err := mongo_v2.GetAttentionCollection().UpdateAttention(context.TODO(), oldattention.ID, oldattention)
		if err != nil {
			logModel.Operation = "Update"
			logModel.Status = "Error"
			logModel.Error = fmt.Sprintf("Failed to update attention: %v", err)
			logModel.Body = attention
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), attention.IdLog, &logModel)
			fmt.Printf("Failed to update attention: %v", err)
			return
		} else {
			logModel.Operation = "Update"
			logModel.Status = "Done"
			logModel.Body = attention
			logModel.Error = ""
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), attention.IdLog, &logModel)
		}
		if oldattention.PersonId != nil && oldattention.CustomerId != nil {
			if *oldattention.AttentionTypeName == "Ocupacional" {
				lastEmo, err := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *oldattention.PersonId, *oldattention.CustomerId, "emo")
				if err != nil {
					logModel.Operation = "Update"
					logModel.Status = "Error"
					logModel.Error = fmt.Sprintf("Failed to update lastEmo attention: %v", err)
					logModel.Body = attention
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), attention.IdLog, &logModel)
					fmt.Printf("Failed to update attention: %v", err)
					return
				}
				if lastEmo != nil {
					mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *oldattention.PersonId, "lastEmoAttention", lastEmo)
				}
				lastAnyEmo, err := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *oldattention.PersonId, *oldattention.CustomerId, "any")
				if err != nil {
					logModel.Operation = "Update"
					logModel.Status = "Error"
					logModel.Error = fmt.Sprintf("Failed to update lastAnyEmo attention: %v", err)
					logModel.Body = attention
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), attention.IdLog, &logModel)
					fmt.Printf("Failed to update attention: %v", err)
					return
				}
				if lastAnyEmo != nil {
					lastAnyEmo.Customer = nil
					lastAnyEmo.Patient = nil
					lastAnyEmo.Person = nil
					lastAnyEmo.Consultations = nil
					lastAnyEmo.AttentionLines = nil
					lastAnyEmo.Exams = nil
					lastAnyEmo.AudiometryTests = nil
					lastAnyEmo.BiochemistryTests = nil
					lastAnyEmo.CovidTests = nil
					lastAnyEmo.Diagnostics = nil
					lastAnyEmo.DrugTests = nil
					lastAnyEmo.HematologyTests = nil
					lastAnyEmo.ImmunologyTests = nil
					lastAnyEmo.MicrobiologyTests = nil
					lastAnyEmo.OphtalmologyTests = nil
					lastAnyEmo.PsychologyTests = nil
					lastAnyEmo.Recommendations = nil
					lastAnyEmo.ExamResults = nil
					lastAnyEmo.SkeletalMuscleTests = nil
					lastAnyEmo.UrineTests = nil
					lastAnyEmo.MedicalHistories = nil
					lastAnyEmo.PathologicalHistories = nil
					lastAnyEmo.Triages = nil
					lastAnyEmo.WorkHistories = nil
					lastAnyEmo.SpirometryTests = nil
					mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *oldattention.PersonId, "lastAnyEmoAttention", lastAnyEmo)
				}
			}
			if *oldattention.AttentionTypeName == "Asistencial" {
				lastNoEmo, err := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *oldattention.PersonId, *oldattention.CustomerId, "no_emo")
				if err != nil {
					logModel.Operation = "Update"
					logModel.Status = "Error"
					logModel.Error = fmt.Sprintf("Failed to update lastNoEmo attention: %v", err)
					logModel.Body = attention
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), attention.IdLog, &logModel)
					fmt.Printf("Failed to update attention: %v", err)
					return
				}
				if lastNoEmo != nil {
					mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *oldattention.PersonId, "lastNoEmoAttention", lastNoEmo)
				}
			}
		}
		fmt.Println("updated: ", oldattention.ID)
	} else {
		var newAttention models_v2.Attention
		if attention.CustomerId != "" {
			cust, _ := mongo_v2.GetCustomerCollection().GetCustomerById(context.Background(), attention.CustomerId)
			if cust != nil {
				newAttention.CustomerId = &cust.ID
				newAttention.CustomerCode = &cust.Code
				newAttention.CustomerName = &cust.Name
				newAttention.Customer = cust
			}
		} else if attention.CustomerId2 != 0 {
			cust, _ := mongo_v2.GetCustomerCollection().GetCustomerById(context.Background(), strconv.Itoa(attention.CustomerId2))
			if cust != nil {
				newAttention.CustomerId = &cust.ID
				newAttention.CustomerCode = &cust.Code
				newAttention.Customer = cust
			}
		}
		depa, _ := mongo.GetDepartmentCollection().GetDepartmentById(context.Background(), strconv.Itoa(attention.DepartmentId))
		if depa != nil {
			newAttention.DepartmentId = &depa.ID
			newAttention.DepartmentName = &depa.Name
		}
		district, _ := mongo.GetDistrictCollection().GetDistrictById(context.Background(), strconv.Itoa(attention.DistrictId))
		if district != nil {
			newAttention.DistrictId = &district.ID
			newAttention.DistrictName = &district.Name
		}
		attType, _ := mongo.GetAttentionTypeCollection().GetAttentionTypesById(context.TODO(), attention.AttentionTypeId)
		if attType != nil {
			newAttention.AttentionTypeId = &attType.ID
			newAttention.AttentionTypeName = &attType.Name
		}
		if attention.AttentionTypeId == "1" {
			delType, _ := mongo.GetDeliveryTypeCollection().GetDeliveryTypeTypesById(context.TODO(), strconv.Itoa(attention.NoemoDeliveryTypeId))
			if delType != nil {
				newAttention.DeliveryTypeId = &delType.ID
				newAttention.DeliveryTypeName = &delType.Name
			}
		} else if attention.AttentionTypeId == "2" {
			delType, _ := mongo.GetDeliveryTypeCollection().GetDeliveryTypeTypesById(context.TODO(), attention.EmoDeliveryTypeId)
			if delType != nil {
				newAttention.DeliveryTypeId = &delType.ID
				newAttention.DeliveryTypeName = &delType.Name
			}
		}
		healthCentr, _ := mongo_v2.GetHealthCentreCollection().GetHealthCentreById(context.TODO(), strconv.Itoa(attention.HealthcentreId))
		if healthCentr != nil {
			newAttention.HealthCentreId = &healthCentr.ID
			newAttention.HealthCentreName = &healthCentr.Name
			newAttention.Healthcentre = healthCentr
		}
		workSta, _ := mongo_v2.GetWorkStationCollection().GetWorkStationById(context.TODO(), strconv.Itoa(attention.WorkStationId))
		if workSta != nil {
			newAttention.WorkStationId = &workSta.ID
			newAttention.WorkStationName = &workSta.Name
			newAttention.WorkStation = workSta
		}
		workPro, _ := mongo_v2.GetWorkProfileCollection().GetWorkProfileById(context.TODO(), strconv.Itoa(attention.WorkProfileId))
		if workPro != nil {
			newAttention.WorkProfileId = &workPro.ID
			newAttention.WorkProfileName = &workPro.Name
			newAttention.WorkProfile = workPro
		}
		examType, _ := mongo_v2.GetExamTypeCollection().GetExamTypeById(context.TODO(), attention.ExamTypeId)
		if examType != nil {
			newAttention.ExamTypeId = &examType.ID
			newAttention.ExamTypeName = &examType.Name
		}
		if attention.ExternalClinicID != 0 {
			externalClinic, _ := mongo_v2.GetExternalClinicCollection().GetExternalClinicById(context.TODO(), attention.ExternalClinicID)
			if externalClinic != nil {
				newAttention.ExternalClinicID = &externalClinic.ID
				newAttention.ExternalClinicName = &externalClinic.Name
				newAttention.ExternalClinic = externalClinic
			}
		}
		attentionDate, _ := dateparse.ParseLocal(attention.AttentionDate)
		newAttention.AttentionDate = &attentionDate
		newAttention.MailSentDateTime = attention.SetSentDate()
		newAttention.MailOpenedDateTime = attention.SetOpenedDate()
		newAttention.Status = &attention.Status
		newAttention.WorkingArea = &attention.WorkingArea
		newAttention.JobTitle = &attention.JobTitle
		externalId := strconv.Itoa(attention.ID)
		newAttention.ExternalId = &externalId
		if strings.TrimSpace(attention.ExternalClinicFlag) == "on" {
			externalClinicFlag := true
			newAttention.ExternalClinicFlag = &externalClinicFlag
		} else {
			externalClinicFlag := false
			newAttention.ExternalClinicFlag = &externalClinicFlag
		}
		if attention.MailStatus != nil {
			switch *attention.MailStatus {
			case 0:
				mailStatus := "Sin resultado"
				newAttention.MailStatus = &mailStatus
			case 1:
				mailStatus := "Por enviar"
				newAttention.MailStatus = &mailStatus
			case 2:
				mailStatus := "Enviado"
				newAttention.MailStatus = &mailStatus
			case 3:
				mailStatus := "Abierto"
				newAttention.MailStatus = &mailStatus
			case 4:
				mailStatus := "Correo Invalido"
				newAttention.MailStatus = &mailStatus
			case 5:
				mailStatus := "Mismo Doc y Correo Diferente"
				newAttention.MailStatus = &mailStatus
			default:
				mailStatus := "-"
				newAttention.MailStatus = &mailStatus
			}
		} else {
			mailStatus := "-"
			newAttention.MailStatus = &mailStatus
		}
		if attention.WasAttended != nil {
			if *attention.WasAttended == "on" {
				wasAttended := true
				newAttention.WasAttended = &wasAttended
			} else {
				wasAttended := false
				newAttention.WasAttended = &wasAttended
			}
		} else {
			wasAttended := false
			newAttention.WasAttended = &wasAttended
		}
		pat, _ := mongo_v2.GetPatientCollection().GetPatientById(context.Background(), attention.PatientId)
		if pat != nil {
			newAttention.PatientId = &pat.ID
			newAttention.Patient = pat
			if newAttention.CustomerId != nil {
				per, _ := mongo_v2.GetPersonCollection().GetPersonByDocumentNumberAndCustomer(context.Background(), pat.DocumentNumber, pat.DocumentTypeID, *newAttention.CustomerId)
				if per != nil {
					newAttention.PersonId = &per.ID
					newAttention.Person = per
				}
			}
		}
		newAttention.ID = attentionId
		val, err := mongo_v2.GetAttentionCollection().AddAttention(context.TODO(), &newAttention)
		if err != nil {
			logModel.Operation = "Insert"
			logModel.Status = "Error"
			logModel.Error = fmt.Sprintf("Failed to insert attention: %v", err)
			logModel.Body = attention
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), attention.IdLog, &logModel)
			fmt.Printf("Failed to add attention: %v", err)
			return
		} else {
			logModel.Operation = "Insert"
			logModel.Status = "Done"
			logModel.Body = attention
			logModel.Error = ""
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), attention.IdLog, &logModel)
		}
		if newAttention.PersonId != nil && newAttention.AttentionTypeName != nil && newAttention.CustomerId != nil {
			if *newAttention.AttentionTypeName == "Ocupacional" {
				lastEmo, err := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *newAttention.PersonId, *newAttention.CustomerId, "emo")
				if err != nil {
					logModel.Operation = "Update"
					logModel.Status = "Error"
					logModel.Error = fmt.Sprintf("Failed to update lastEmo attention: %v", err)
					logModel.Body = attention
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), attention.IdLog, &logModel)
					fmt.Printf("Failed to update attention: %v", err)
					return
				}
				if lastEmo != nil {
					mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *newAttention.PersonId, "lastEmoAttention", lastEmo)
				}
				lastAnyEmo, err := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *newAttention.PersonId, *newAttention.CustomerId, "any")
				if err != nil {
					logModel.Operation = "Update"
					logModel.Status = "Error"
					logModel.Error = fmt.Sprintf("Failed to update lastAnyEmo attention: %v", err)
					logModel.Body = attention
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), attention.IdLog, &logModel)
					fmt.Printf("Failed to update attention: %v", err)
					return
				}
				if lastAnyEmo != nil {
					lastAnyEmo.Customer = nil
					lastAnyEmo.Patient = nil
					lastAnyEmo.Person = nil
					lastAnyEmo.Consultations = nil
					lastAnyEmo.AttentionLines = nil
					lastAnyEmo.Exams = nil
					lastAnyEmo.AudiometryTests = nil
					lastAnyEmo.BiochemistryTests = nil
					lastAnyEmo.CovidTests = nil
					lastAnyEmo.Diagnostics = nil
					lastAnyEmo.DrugTests = nil
					lastAnyEmo.HematologyTests = nil
					lastAnyEmo.ImmunologyTests = nil
					lastAnyEmo.MicrobiologyTests = nil
					lastAnyEmo.OphtalmologyTests = nil
					lastAnyEmo.PsychologyTests = nil
					lastAnyEmo.Recommendations = nil
					lastAnyEmo.ExamResults = nil
					lastAnyEmo.SkeletalMuscleTests = nil
					lastAnyEmo.UrineTests = nil
					lastAnyEmo.MedicalHistories = nil
					lastAnyEmo.PathologicalHistories = nil
					lastAnyEmo.Triages = nil
					lastAnyEmo.WorkHistories = nil
					lastAnyEmo.SpirometryTests = nil
					mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *newAttention.PersonId, "lastAnyEmoAttention", lastAnyEmo)
				}
			}
			if *newAttention.AttentionTypeName == "Asistencial" {
				lastNoEmo, err := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *newAttention.PersonId, *newAttention.CustomerId, "no_emo")
				if err != nil {
					logModel.Operation = "Update"
					logModel.Status = "Error"
					logModel.Error = fmt.Sprintf("Failed to update lastNoEmo attention: %v", err)
					logModel.Body = attention
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), attention.IdLog, &logModel)
					fmt.Printf("Failed to update attention: %v", err)
					return
				}
				if lastNoEmo != nil {
					mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *newAttention.PersonId, "lastNoEmoAttention", lastNoEmo)
				}
			}
		}
		fmt.Println("created: ", val.ID)
	}
}
