package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/consumer_models"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"desktop-middleware/models"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	mongo_v2 "desktop-middleware/database/mongo_v2"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func main() {
	config.SetConfiguration()
	fmt.Println("Consumer started!")
	topic := viper.GetString("kafka.database") + ".dbmediweb.triaje"
	fmt.Println(topic)
	// Create Consumer instance
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": viper.GetString("kafka.bootstrap_servers"),
		"sasl.mechanisms":   viper.GetString("kafka.sasl_mechanisms"),
		"security.protocol": viper.GetString("kafka.security_protocol"),
		"sasl.username":     viper.GetString("kafka.sasl_username"),
		"sasl.password":     viper.GetString("kafka.sasl_password"),
		"group.id":          viper.GetString("kafka.groupId"),
		"auto.offset.reset": "earliest",
	})
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	err = c.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	// Set up a channel for handling Ctrl-C, etc
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	// Process messages
	run := true
	flag := true
	var triages []*consumer_models.TriageConsumer
	db := database.InitDatabases()
	defer db.DisconnectDatabases()
	for run {
		var logModel models.Log
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			if flag {
				msg, err := c.ReadMessage(100 * time.Millisecond)
				if err != nil {
					// Errors are informational and automatically handled by the consumer
					flag = false
					continue
				}
				if msg.Value == nil || len(msg.Value) == 0 {
					continue
				}
				var triage consumer_models.TriageConsumer
				logModel.Collection = "middleware.transactions.attentions.triages"
				logModel.Status = "In Progress"
				logModel.Body = msg.Value
				log, _ := mongo.GetLogsCollection().AddLogs(context.TODO(), &logModel)
				err = json.Unmarshal(msg.Value, &triage)
				if err != nil {
					logModel.Operation = "Decode"
					logModel.Status = "Error"
					logModel.Collection = "middleware.transactions.attentions.triages"
					logModel.Error = fmt.Sprintf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					json.Unmarshal(msg.Value, &logModel.Body)
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), log.ID, &logModel)
					fmt.Printf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					flag = false
					continue
				}
				triage.IdLog = log.ID
				triages = append(triages, &triage)
				if len(triages) > viper.GetInt("kafka.fetchCount") {
					flag = false
				}
			} else {
				if len(triages) > 0 {
					numCPU := viper.GetInt("kafka.threadCount")
					length := len(triages)
					wg := sync.WaitGroup{}
					chunkSize := (length + numCPU - 1) / numCPU

					for i := 0; i < length; i += chunkSize {

						wg.Add(1)
						go func(i int, chunkSize int) {
							end := i + chunkSize
							defer wg.Done()

							if end > length {
								end = length
							}
							slice := triages[i:end]

							for _, triage := range slice {
								AddTriageConsumerV2(triage, logModel)
							}
						}(i, chunkSize)
					}
					wg.Wait()
					triages = []*consumer_models.TriageConsumer{}
				}
				flag = true
			}
		}
	}

	fmt.Printf("Closing consumer\n")
	c.Close()
}

func AddTriageConsumerV2(triage *consumer_models.TriageConsumer, logModel models.Log) {

	flag, err := strconv.ParseBool(triage.IsDeleted)
	if err != nil {
		logModel.Operation = "Decode"
		logModel.Status = "Error"
		logModel.Collection = "middleware.transactions.attentions.triages"
		logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
		logModel.Body = triage
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), triage.IdLog, &logModel)
		fmt.Printf("Failed to decode JSON: %v", err)
		return
	}

	if flag || triage.Status == 0 {
		_, err := mongo_v2.GetAttentionCollection().UnsetFieldByExternalId(context.TODO(), strconv.Itoa(triage.AttentionId), "triages")
		if err != nil {
			logModel.Operation = "Delete"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.triages"
			logModel.Error = fmt.Sprintf("Failed to delete triage: %v", err)
			logModel.Body = triage
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), triage.IdLog, &logModel)
			fmt.Printf("Failed to delete triage: %v", err)
		} else {
			logModel.Operation = "Delete"
			logModel.Status = "Done"
			logModel.Collection = "middleware.transactions.attentions.triages"
			logModel.Error = ""
			logModel.Body = triage
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), triage.IdLog, &logModel)
		}
	} else {
		attention, err := mongo_v2.GetAttentionCollection().GetAttentionByExternalId(context.TODO(), strconv.Itoa(triage.AttentionId))
		if err != nil || attention == nil {
			logModel.Operation = "Get"
			logModel.Error = fmt.Sprintf("Failed to get triage: %v", err)
			logModel.Body = triage
			logModel.Status = "Error"
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), triage.IdLog, &logModel)
			fmt.Printf("Failed to get triage: %v", err)
		}

		if attention == nil {
			return
		}

		oldtriage := attention.Triages

		if oldtriage != nil {
			oldtriage.AttentionId = attention.ID
			if attention.PatientId != nil {
				oldtriage.PatientId = *attention.PatientId
			}
			if attention.PersonId != nil {
				oldtriage.PersonId = *attention.PersonId
			}
			oldtriage.LastUpdatedDate = time.Now()
			if systolicPressure, err := strconv.ParseFloat(triage.SystolicPressure, 64); err == nil {
				oldtriage.SystolicPressure = systolicPressure
			}
			if diastolicPressure, err := strconv.ParseFloat(triage.DiastolicPressure, 64); err == nil {
				oldtriage.DiastolicPressure = diastolicPressure
			}
			if heartRate, err := strconv.ParseFloat(triage.HeartRate, 64); err == nil {
				oldtriage.HeartRate = heartRate
			}
			if respiratoryRate, err := strconv.ParseFloat(triage.RespiratoryRate, 64); err == nil {
				oldtriage.RespiratoryRate = respiratoryRate
			}
			if weight, err := strconv.ParseFloat(triage.Weight, 64); err == nil {
				oldtriage.Weight = weight
			}
			if height, err := strconv.ParseFloat(triage.Height, 64); err == nil {
				oldtriage.Height = height
			}
			if bmi, err := strconv.ParseFloat(triage.Bmi, 64); err == nil {
				oldtriage.Bmi = bmi
			}
			if abdominal, err := strconv.ParseFloat(triage.Abdominal, 64); err == nil {
				oldtriage.Abdominal = abdominal
			}
			oldtriage.Status = triage.Status

			_, err := mongo_v2.GetAttentionCollection().SetFieldByID(context.Background(), attention.ID, "triages", oldtriage)
			if err != nil {
				logModel.Operation = "Update"
				logModel.Status = "Error"
				logModel.Collection = "middleware.transactions.attentions.triages - v2"
				logModel.Error = fmt.Sprintf("Failed to update triage: %v", err)
				logModel.Body = triage
				mongo.GetLogsCollection().UpdateLogs(context.TODO(), triage.IdLog, &logModel)
				fmt.Printf("Failed to update triage: %v", err)
				return
			} else {
				logModel.Operation = "Update"
				logModel.Status = "Done"
				logModel.Collection = "middleware.transactions.attentions.triages - v2"
				logModel.Error = ""
				logModel.Body = triage
				mongo.GetLogsCollection().UpdateLogs(context.TODO(), triage.IdLog, &logModel)
			}
			fmt.Println("updated: ", oldtriage.ID)
		} else {
			var newTriage models.Triage
			newTriage.ID = primitive.NewObjectID().Hex()
			newTriage.ExternalId = strconv.Itoa(triage.ID)
			newTriage.ExternalSystem = "MEDIWEB"
			newTriage.CreatedDate = time.Now()
			newTriage.LastUpdatedDate = time.Now()
			newTriage.AttentionId = attention.ID
			if attention.PatientId != nil {
				newTriage.PatientId = *attention.PatientId
			}
			if attention.PersonId != nil {
				newTriage.PersonId = *attention.PersonId
			}
			if systolicPressure, err := strconv.ParseFloat(triage.SystolicPressure, 64); err == nil {
				newTriage.SystolicPressure = systolicPressure
			}
			if diastolicPressure, err := strconv.ParseFloat(triage.DiastolicPressure, 64); err == nil {
				newTriage.DiastolicPressure = diastolicPressure
			}
			if heartRate, err := strconv.ParseFloat(triage.HeartRate, 64); err == nil {
				newTriage.HeartRate = heartRate
			}
			if respiratoryRate, err := strconv.ParseFloat(triage.RespiratoryRate, 64); err == nil {
				newTriage.RespiratoryRate = respiratoryRate
			}
			if weight, err := strconv.ParseFloat(triage.Weight, 64); err == nil {
				newTriage.Weight = weight
			}
			if height, err := strconv.ParseFloat(triage.Height, 64); err == nil {
				newTriage.Height = height
			}
			if bmi, err := strconv.ParseFloat(triage.Bmi, 64); err == nil {
				newTriage.Bmi = bmi
			}
			if abdominal, err := strconv.ParseFloat(triage.Abdominal, 64); err == nil {
				newTriage.Abdominal = abdominal
			}
			newTriage.Status = triage.Status
			newTriage.ExternalId = strconv.Itoa(triage.ID)
			newTriage.ExternalSystem = "MEDIWEB"
			_, err := mongo_v2.GetAttentionCollection().SetFieldByID(context.Background(), attention.ID, "triages", newTriage)
			if err != nil {
				logModel.Operation = "Insert"
				logModel.Status = "Error"
				logModel.Collection = "middleware.transactions.attentions.triages - v2"
				logModel.Error = fmt.Sprintf("Failed to insert triage: %v", err)
				logModel.Body = triage
				mongo.GetLogsCollection().UpdateLogs(context.TODO(), triage.IdLog, &logModel)
				fmt.Printf("Failed to add triage: %v", err)
				return
			} else {
				logModel.Operation = "Insert"
				logModel.Status = "Done"
				logModel.Collection = "middleware.transactions.attentions.triages - v2"
				logModel.Error = ""
				logModel.Body = triage
				mongo.GetLogsCollection().UpdateLogs(context.TODO(), triage.IdLog, &logModel)
			}
			fmt.Println("created: ", newTriage.ID)
		}
		if attention.PersonId != nil && attention.AttentionTypeName != nil && attention.CustomerId != nil {
			if *attention.AttentionTypeName == "Ocupacional" {
				lastEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *attention.PersonId, *attention.CustomerId, "emo")
				if lastEmo != nil {
					mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *attention.PersonId, "lastEmoAttention", lastEmo)
				}
				lastAnyEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *attention.PersonId, *attention.CustomerId, "any")
				if lastAnyEmo != nil {
					lastAnyEmo.Customer = nil
					lastAnyEmo.Patient = nil
					lastAnyEmo.Person = nil
					lastAnyEmo.Consultations = nil
					lastAnyEmo.AttentionLines = nil
					lastAnyEmo.Exams = nil
					lastAnyEmo.AudiometryTests = nil
					lastAnyEmo.BiochemistryTests = nil
					lastAnyEmo.CovidTests = nil
					lastAnyEmo.Diagnostics = nil
					lastAnyEmo.DrugTests = nil
					lastAnyEmo.HematologyTests = nil
					lastAnyEmo.ImmunologyTests = nil
					lastAnyEmo.MicrobiologyTests = nil
					lastAnyEmo.OphtalmologyTests = nil
					lastAnyEmo.PsychologyTests = nil
					lastAnyEmo.Recommendations = nil
					lastAnyEmo.ExamResults = nil
					lastAnyEmo.SkeletalMuscleTests = nil
					lastAnyEmo.UrineTests = nil
					lastAnyEmo.MedicalHistories = nil
					lastAnyEmo.PathologicalHistories = nil
					lastAnyEmo.Triages = nil
					lastAnyEmo.WorkHistories = nil
					lastAnyEmo.SpirometryTests = nil
					mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *attention.PersonId, "lastAnyEmoAttention", lastAnyEmo)
				}
			}
			if *attention.AttentionTypeName == "Asistencial" {
				lastNoEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *attention.PersonId, *attention.CustomerId, "no_emo")
				if lastNoEmo != nil {
					mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *attention.PersonId, "lastNoEmoAttention", lastNoEmo)
				}
			}
		}
	}
}
