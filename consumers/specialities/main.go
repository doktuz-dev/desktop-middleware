package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/consumer_models"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	mongo_v2 "desktop-middleware/database/mongo_v2"
	"desktop-middleware/models"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	fmt.Println("Consumer started!")
	topic := viper.GetString("kafka.database") + ".dbmediweb.especialidad_inter"
	fmt.Println(topic)
	// Create Consumer instance
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": viper.GetString("kafka.bootstrap_servers"),
		"sasl.mechanisms":   viper.GetString("kafka.sasl_mechanisms"),
		"security.protocol": viper.GetString("kafka.security_protocol"),
		"sasl.username":     viper.GetString("kafka.sasl_username"),
		"sasl.password":     viper.GetString("kafka.sasl_password"),
		"group.id":          viper.GetString("kafka.groupId"),
		"auto.offset.reset": "earliest",
	})
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	err = c.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	// Set up a channel for handling Ctrl-C, etc
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	// Process messages
	run := true
	flag := true
	var specialitys []*consumer_models.SpecialityConsumer
	db := database.InitDatabases()
	defer db.DisconnectDatabases()
	for run {
		var logModel models.Log
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			if flag {
				msg, err := c.ReadMessage(100 * time.Millisecond)
				if err != nil {
					// Errors are informational and automatically handled by the consumer
					flag = false
					continue
				}
				if msg.Value == nil || len(msg.Value) == 0 {
					continue
				}
				var speciality consumer_models.SpecialityConsumer
				logModel.Collection = "middleware.registers.specialities"
				logModel.Status = "In Progress"
				logModel.Body = msg.Value
				log, _ := mongo.GetLogsCollection().AddLogs(context.TODO(), &logModel)
				err = json.Unmarshal(msg.Value, &speciality)
				if err != nil {
					logModel.Operation = "Decode"
					logModel.Status = "Error"
					logModel.Collection = "middleware.registers.specialities"
					logModel.Error = fmt.Sprintf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					json.Unmarshal(msg.Value, &logModel.Body)
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), log.ID, &logModel)
					fmt.Printf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					flag = false
					continue
				}
				speciality.IdLog = log.ID
				specialitys = append(specialitys, &speciality)

				if len(specialitys) > viper.GetInt("kafka.fetchCount") {
					flag = false
				}
			} else {
				if len(specialitys) > 0 {
					numCPU := viper.GetInt("kafka.threadCount")
					length := len(specialitys)
					wg := sync.WaitGroup{}
					chunkSize := (length + numCPU - 1) / numCPU

					for i := 0; i < length; i += chunkSize {

						wg.Add(1)
						go func(i int, chunkSize int) {
							end := i + chunkSize
							defer wg.Done()

							if end > length {
								end = length
							}
							slice := specialitys[i:end]

							for _, speciality := range slice {

								flag, err := strconv.ParseBool(speciality.IsDeleted)
								if err != nil {
									logModel.Operation = "Decode"
									logModel.Status = "Error"
									logModel.Collection = "middleware.registers.specialities"
									logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
									logModel.Body = speciality
									mongo.GetLogsCollection().UpdateLogs(context.TODO(), speciality.IdLog, &logModel)
									fmt.Printf("Failed to decode JSON: %v", err)
									continue
								}

								if flag {
									_, err := mongo.GetSpecialityCollection().DeleteSpecialityByExternalId(context.TODO(), strconv.Itoa(speciality.ID))
									if err != nil {
										logModel.Operation = "Delete"
										logModel.Status = "Error"
										logModel.Collection = "middleware.registers.specialities"
										logModel.Error = fmt.Sprintf("Failed to delete speciality: %v", err)
										logModel.Body = speciality
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), speciality.IdLog, &logModel)
										fmt.Printf("Failed to delete speciality: %v", err)
									} else {
										logModel.Operation = "Delete"
										logModel.Status = "Done"
										logModel.Collection = "middleware.registers.specialities"
										logModel.Error = ""
										logModel.Body = speciality
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), speciality.IdLog, &logModel)
									}
								} else {
									oldspeciality, err := mongo.GetSpecialityCollection().GetSpecialityByExternalId(context.TODO(), strconv.Itoa(speciality.ID))
									if err != nil {
										logModel.Operation = "Get"
										logModel.Status = "Error"
										logModel.Collection = "middleware.registers.specialities"
										logModel.Error = fmt.Sprintf("Failed to get speciality: %v", err)
										logModel.Body = speciality
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), speciality.IdLog, &logModel)
										fmt.Printf("Failed to get speciality: %v", err)
									}

									if oldspeciality != nil {
										oldspeciality.Name = speciality.Name
										oldspeciality.Code = oldspeciality.ExternalId
										status, _ := strconv.Atoi(speciality.Status)
										oldspeciality.Status = status
										_, err := mongo.GetSpecialityCollection().UpdateSpeciality(context.TODO(), oldspeciality.ID, oldspeciality)
										if err != nil {
											logModel.Operation = "Update"
											logModel.Status = "Error"
											logModel.Collection = "middleware.registers.specialities"
											logModel.Error = fmt.Sprintf("Failed to update speciality: %v", err)
											logModel.Body = speciality
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), speciality.IdLog, &logModel)
											fmt.Printf("Failed to update speciality: %v", err)
											continue
										} else {
											logModel.Operation = "Update"
											logModel.Status = "Done"
											logModel.Collection = "middleware.registers.specialities"
											logModel.Error = ""
											logModel.Body = speciality
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), speciality.IdLog, &logModel)
										}
										go mongo_v2.GetAttentionCollection().UpdateSpecialityInConsultations(context.Background(), oldspeciality.ID, oldspeciality)
										fmt.Println("updated: ", oldspeciality.ID)
									} else {
										status, _ := strconv.Atoi(speciality.Status)
										newService := models.Speciality{
											Name:       speciality.Name,
											Status:     status,
											ExternalId: strconv.Itoa(speciality.ID),
										}
										newService.Code = newService.ExternalId
										val, err := mongo.GetSpecialityCollection().AddSpeciality(context.TODO(), &newService)
										if err != nil {
											logModel.Operation = "Insert"
											logModel.Status = "Error"
											logModel.Collection = "middleware.registers.specialities"
											logModel.Error = fmt.Sprintf("Failed to add speciality: %v", err)
											logModel.Body = speciality
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), speciality.IdLog, &logModel)
											fmt.Printf("Failed to add speciality: %v", err)
											continue
										} else {
											logModel.Operation = "Insert"
											logModel.Status = "Done"
											logModel.Collection = "middleware.registers.specialities"
											logModel.Error = ""
											logModel.Body = speciality
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), speciality.IdLog, &logModel)
										}
										fmt.Println("created: ", val.ID)
									}
								}
							}
						}(i, chunkSize)
					}
					wg.Wait()
					specialitys = []*consumer_models.SpecialityConsumer{}
				}
				flag = true
			}
		}
	}

	fmt.Printf("Closing consumer\n")
	c.Close()
}
