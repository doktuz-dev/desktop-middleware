package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/consumer_models"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	mongo_v2 "desktop-middleware/database/mongo_v2"
	"desktop-middleware/models"
	"desktop-middleware/models_v2"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func main() {
	config.SetConfiguration()
	fmt.Println("Consumer started!")
	topic := viper.GetString("kafka.database") + ".dbmediweb.formato_x_orden"
	fmt.Println(topic)
	// Create Consumer instance
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": viper.GetString("kafka.bootstrap_servers"),
		"sasl.mechanisms":   viper.GetString("kafka.sasl_mechanisms"),
		"security.protocol": viper.GetString("kafka.security_protocol"),
		"sasl.username":     viper.GetString("kafka.sasl_username"),
		"sasl.password":     viper.GetString("kafka.sasl_password"),
		"group.id":          viper.GetString("kafka.groupId"),
		"auto.offset.reset": "earliest",
	})
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	err = c.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	// Set up a channel for handling Ctrl-C, etc
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	// Process messages
	run := true
	flag := true
	var examResults []*consumer_models.ExamResultConsumer
	db := database.InitDatabases()
	defer db.DisconnectDatabases()
	for run {
		var logModel models.Log
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			if flag {
				msg, err := c.ReadMessage(100 * time.Millisecond)
				if err != nil {
					// Errors are informational and automatically handled by the consumer
					flag = false
					continue
				}
				if msg.Value == nil || len(msg.Value) == 0 {
					continue
				}
				var examResult consumer_models.ExamResultConsumer
				logModel.Collection = "middleware.transactions.attentions.exams.results"
				logModel.Status = "In Progress"
				logModel.Body = msg.Value
				log, _ := mongo.GetLogsCollection().AddLogs(context.TODO(), &logModel)
				err = json.Unmarshal(msg.Value, &examResult)
				if err != nil {
					logModel.Operation = "Decode"
					logModel.Status = "Error"
					logModel.Collection = "middleware.transactions.attentions.exams.results"
					logModel.Error = fmt.Sprintf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					json.Unmarshal(msg.Value, &logModel.Body)
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), log.ID, &logModel)
					fmt.Printf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					flag = false
					continue
				}
				examResult.IdLog = log.ID
				examResults = append(examResults, &examResult)
				if len(examResults) > viper.GetInt("kafka.fetchCount") {
					flag = false
				}
			} else {
				if len(examResults) > 0 {
					numCPU := viper.GetInt("kafka.threadCount")
					length := len(examResults)
					wg := sync.WaitGroup{}
					chunkSize := (length + numCPU - 1) / numCPU

					for i := 0; i < length; i += chunkSize {

						wg.Add(1)
						go func(i int, chunkSize int) {
							end := i + chunkSize
							defer wg.Done()

							if end > length {
								end = length
							}
							slice := examResults[i:end]

							for _, examResult := range slice {
								AddExamResultV2(examResult, logModel)
							}
						}(i, chunkSize)
					}
					wg.Wait()
					examResults = []*consumer_models.ExamResultConsumer{}
				}
				flag = true
			}
		}
	}

	fmt.Printf("Closing consumer\n")
	c.Close()
}

func AddExamResultV2(examResult *consumer_models.ExamResultConsumer, logModel models.Log) {

	flag, err := strconv.ParseBool(examResult.IsDeleted)
	if err != nil {
		logModel.Operation = "Decode"
		logModel.Status = "Error"
		logModel.Collection = "middleware.transactions.attentions.exams.results"
		logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
		logModel.Body = examResult
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), examResult.IdLog, &logModel)
		fmt.Printf("Failed to decode JSON: %v", err)
		return
	}

	if flag || examResult.Status == 0 {
		_, err = mongo_v2.GetAttentionCollection().PullFieldByExternalId(context.TODO(), strconv.Itoa(examResult.AttentionId), "examResults", strconv.Itoa(examResult.ID))
		if err != nil {
			logModel.Operation = "Delete"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.exams.results"
			logModel.Error = fmt.Sprintf("Failed to delete examResult: %v", err)
			logModel.Body = examResult
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), examResult.IdLog, &logModel)
			fmt.Printf("Failed to delete examResult: %v", err)
		} else {
			logModel.Operation = "Delete"
			logModel.Status = "Done"
			logModel.Collection = "middleware.transactions.attentions.exams.results"
			logModel.Error = ""
			logModel.Body = examResult
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), examResult.IdLog, &logModel)
		}
	} else {
		attention, err := mongo_v2.GetAttentionCollection().GetAttentionByExternalId(context.TODO(), strconv.Itoa(examResult.AttentionId))
		if err != nil {
			logModel.Operation = "Get"
			logModel.Status = "Error"
			logModel.Error = fmt.Sprintf("Failed to get examResult: %v", err)
			logModel.Body = examResult
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), examResult.IdLog, &logModel)
			fmt.Printf("Failed to get examResult: %v", err)
		}

		if attention == nil {
			return
		}
		attentionId := attention.ID
		serv, _ := mongo_v2.GetServiceAreaCollection().GetServiceAreaByExternalId(context.Background(), strconv.Itoa(examResult.ServiceAreaId))

		var oldexamResult *models_v2.ExamResult

		for _, body := range attention.ExamResults {
			if body.ExternalId == strconv.Itoa(examResult.ID) {
				oldexamResult = body
			}
		}

		if oldexamResult != nil {
			CreateExamResult(serv, attentionId, examResult, oldexamResult, logModel)
			fmt.Println("updated: ", oldexamResult.ID)
		} else {
			var newExamResult models_v2.ExamResult
			CreateExamResult(serv, attentionId, examResult, &newExamResult, logModel)
			fmt.Println("created: ", examResult.ID)
		}
		if attention.PersonId != nil && attention.AttentionTypeName != nil && attention.CustomerId != nil {
			if *attention.AttentionTypeName == "Ocupacional" {
				lastEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *attention.PersonId, *attention.CustomerId, "emo")
				if lastEmo != nil {
					mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *attention.PersonId, "lastEmoAttention", lastEmo)
				}
				lastAnyEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *attention.PersonId, *attention.CustomerId, "any")
				if lastAnyEmo != nil {
					lastAnyEmo.Customer = nil
					lastAnyEmo.Patient = nil
					lastAnyEmo.Person = nil
					lastAnyEmo.Consultations = nil
					lastAnyEmo.AttentionLines = nil
					lastAnyEmo.Exams = nil
					lastAnyEmo.AudiometryTests = nil
					lastAnyEmo.BiochemistryTests = nil
					lastAnyEmo.CovidTests = nil
					lastAnyEmo.Diagnostics = nil
					lastAnyEmo.DrugTests = nil
					lastAnyEmo.HematologyTests = nil
					lastAnyEmo.ImmunologyTests = nil
					lastAnyEmo.MicrobiologyTests = nil
					lastAnyEmo.OphtalmologyTests = nil
					lastAnyEmo.PsychologyTests = nil
					lastAnyEmo.Recommendations = nil
					lastAnyEmo.ExamResults = nil
					lastAnyEmo.SkeletalMuscleTests = nil
					lastAnyEmo.UrineTests = nil
					lastAnyEmo.MedicalHistories = nil
					lastAnyEmo.PathologicalHistories = nil
					lastAnyEmo.Triages = nil
					lastAnyEmo.WorkHistories = nil
					lastAnyEmo.SpirometryTests = nil
					mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *attention.PersonId, "lastAnyEmoAttention", lastAnyEmo)
				}
			}
			if *attention.AttentionTypeName == "Asistencial" {
				lastNoEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *attention.PersonId, *attention.CustomerId, "no_emo")
				if lastNoEmo != nil {
					mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *attention.PersonId, "lastNoEmoAttention", lastNoEmo)
				}
			}
		}
	}
}

func CreateExamResult(serv *models_v2.ServiceArea, attentionId string, consumer *consumer_models.ExamResultConsumer, examResult *models_v2.ExamResult, logModel models.Log) {
	if examResult.ID == "" {
		examResult.ID = primitive.NewObjectID().Hex()
		examResult.ExternalId = strconv.Itoa(consumer.ID)
		examResult.ExternalSystem = "MEDIWEB"
		examResult.CreatedDate = time.Now()
		examResult.LastUpdatedDate = time.Now()
	}
	examResult.AttentionId = attentionId
	if serv != nil {
		examResult.ServiceAreaID = serv.ID
		examResult.ServiceAreaCode = serv.Code
		examResult.ServiceArea = serv
	}
	examResult.PrintId = strconv.Itoa(consumer.PrintId)
	examResult.Filename = consumer.Filename
	examResult.Status = consumer.Status

	if consumer.Reviewed == "ON" {
		examResult.Reviewed = true
	} else {
		examResult.Reviewed = false
	}

	if serv != nil && serv.Code == "DOCUMENT" {
		printId := strconv.Itoa(consumer.PrintId)
		if printId != "" && printId != "0" {
			examResult.Reviewed = true
		}
	}
	mongo_v2.GetAttentionCollection().PullFieldByExternalId(context.TODO(), strconv.Itoa(consumer.AttentionId), "examResults", strconv.Itoa(consumer.ID))
	_, err := mongo_v2.GetAttentionCollection().PushFieldByID(context.TODO(), attentionId, "examResults", examResult)
	if err != nil {
		logModel.Operation = "Insert"
		logModel.Status = "Error"
		logModel.Error = fmt.Sprintf("Failed to insert examResults: %v", err)
		logModel.Body = consumer
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), consumer.IdLog, &logModel)
		fmt.Printf("Failed to add examResults: %v", err)
	} else {
		logModel.Operation = "Insert"
		logModel.Status = "Done"
		logModel.Body = consumer
		logModel.Error = ""
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), consumer.IdLog, &logModel)

	}
}
