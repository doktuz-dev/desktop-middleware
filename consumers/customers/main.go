package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/consumer_models"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	mongo_v2 "desktop-middleware/database/mongo_v2"
	"desktop-middleware/models"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	fmt.Println("Consumer started!")
	topic := viper.GetString("kafka.database") + ".dbmediweb.empresa"
	fmt.Println(topic)
	// Create Consumer instance
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": viper.GetString("kafka.bootstrap_servers"),
		"sasl.mechanisms":   viper.GetString("kafka.sasl_mechanisms"),
		"security.protocol": viper.GetString("kafka.security_protocol"),
		"sasl.username":     viper.GetString("kafka.sasl_username"),
		"sasl.password":     viper.GetString("kafka.sasl_password"),
		"group.id":          viper.GetString("kafka.groupId"),
		"auto.offset.reset": "earliest",
	})
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	err = c.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	// Set up a channel for handling Ctrl-C, etc
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	// Process messages
	run := true
	flag := true
	var customers []*consumer_models.CustomerConsumer
	db := database.InitDatabases()
	defer db.DisconnectDatabases()
	for run {
		var logModel models.Log
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			ctx := context.Background()
			if flag {
				msg, err := c.ReadMessage(100 * time.Millisecond)
				if err != nil {
					// Errors are informational and automatically handled by the consumer
					flag = false
					continue
				}
				if msg.Value == nil || len(msg.Value) == 0 {
					continue
				}
				var customer consumer_models.CustomerConsumer
				logModel.Collection = "middleware.registers.customers"
				logModel.Status = "In Progress"
				logModel.Body = msg.Value
				log, _ := mongo.GetLogsCollection().AddLogs(context.TODO(), &logModel)
				err = json.Unmarshal(msg.Value, &customer)
				if err != nil {
					logModel.Operation = "Decode"
					logModel.Status = "Error"
					logModel.Collection = "middleware.registers.customers"
					logModel.Error = fmt.Sprintf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					json.Unmarshal(msg.Value, &logModel.Body)
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), log.ID, &logModel)
					flag = false
					continue
				}
				customer.IdLog = log.ID
				customers = append(customers, &customer)
				if len(customers) > viper.GetInt("kafka.fetchCount") {
					flag = false
				}
			} else {
				if len(customers) > 0 {
					numCPU := viper.GetInt("kafka.threadCount")
					length := len(customers)
					wg := sync.WaitGroup{}
					chunkSize := (length + numCPU - 1) / numCPU

					for i := 0; i < length; i += chunkSize {

						wg.Add(1)
						go func(i int, chunkSize int) {
							end := i + chunkSize
							defer wg.Done()

							if end > length {
								end = length
							}
							slice := customers[i:end]

							for _, customer := range slice {

								flag, err := strconv.ParseBool(customer.IsDeleted)
								if err != nil {
									logModel.Operation = "Decode"
									logModel.Status = "Error"
									logModel.Collection = "middleware.registers.customers"
									logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
									logModel.Body = customer
									mongo.GetLogsCollection().UpdateLogs(context.TODO(), customer.IdLog, &logModel)
									fmt.Printf("Failed to decode JSON: %v", err)
									continue
								}

								if flag {
									_, err := mongo.GetCustomerCollection().DeleteCustomerByExternalId(ctx, strconv.Itoa(customer.ID))
									if err != nil {
										logModel.Operation = "Delete"
										logModel.Status = "Error"
										logModel.Collection = "middleware.registers.customers"
										logModel.Error = fmt.Sprintf("Failed to delete customer: %v", err)
										logModel.Body = customer
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), customer.IdLog, &logModel)
										fmt.Printf("Failed to delete customer: %v", err)
									} else {
										logModel.Operation = "Delete"
										logModel.Status = "Done"
										logModel.Collection = "middleware.registers.customers"
										logModel.Error = ""
										logModel.Body = customer
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), customer.IdLog, &logModel)

									}
								} else {
									oldcustomer, err := mongo.GetCustomerCollection().GetCustomerByCode(ctx, strings.TrimSpace(customer.Code))
									if err != nil {
										logModel.Operation = "Decode"
										logModel.Status = "Error"
										logModel.Collection = "middleware.registers.customers"
										logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
										logModel.Body = customer
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), customer.IdLog, &logModel)
										fmt.Printf("Failed to get customer: %v", err)
									}

									if oldcustomer != nil {
										if oldcustomer.ExternalSystem == "MEDIWEB" {
											oldcustomer.Name = customer.Name
											oldcustomer.Code = customer.Code
											oldcustomer.Status = customer.Status
											oldcustomer.Address = customer.Address
											oldcustomer.PhoneNumber = customer.PhoneNumber
										} else {
											oldcustomer.ExternalId = strconv.Itoa(customer.ID)
										}
										_, err := mongo.GetCustomerCollection().UpdateCustomer(ctx, oldcustomer.ID, oldcustomer)
										if err != nil {
											logModel.Operation = "Update"
											logModel.Status = "Error"
											logModel.Collection = "middleware.registers.customers"
											logModel.Error = fmt.Sprintf("Failed to update customer: %v", err)
											logModel.Body = customer
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), customer.IdLog, &logModel)
											fmt.Printf("Failed to update customer: %v", err)
											continue
										} else {
											logModel.Operation = "Update"
											logModel.Status = "Done"
											logModel.Collection = "middleware.registers.customers"
											logModel.Error = ""
											logModel.Body = customer
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), customer.IdLog, &logModel)
										}
										go mongo_v2.GetAttentionCollection().CustomSetFieldById(context.TODO(), "customerId", oldcustomer.ID, "customer", oldcustomer)
										go mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "customerId", oldcustomer.ID, "customer", oldcustomer)
										fmt.Println("updated: ", oldcustomer.ID)
									} else {
										newCustomer := models.Customer{
											Name:        customer.Name,
											Code:        customer.Code,
											Address:     customer.Address,
											PhoneNumber: customer.PhoneNumber,
											Status:      customer.Status,
											ExternalId:  strconv.Itoa(customer.ID),
										}
										val, err := mongo.GetCustomerCollection().AddCustomer(ctx, &newCustomer)
										if err != nil {
											logModel.Operation = "Insert"
											logModel.Status = "Error"
											logModel.Collection = "middleware.registers.customers"
											logModel.Error = fmt.Sprintf("Failed to add customer: %v", err)
											logModel.Body = customer
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), customer.IdLog, &logModel)
											fmt.Printf("Failed to insert customer: %v", err)
											continue
										} else {
											logModel.Operation = "Insert"
											logModel.Status = "Done"
											logModel.Collection = "middleware.registers.customers"
											logModel.Error = ""
											logModel.Body = customer
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), customer.IdLog, &logModel)
										}
										mongo.GetNotificationsCollection().DeleteNotificationsByItemId(context.Background(), val.ID)
										list := val.SetNotifications()
										if len(list) > 0 {
											mongo.GetNotificationsCollection().AddManyNotifications(context.Background(), list)
										}
										fmt.Println("created: ", val.ID)
									}
								}
							}
						}(i, chunkSize)
					}
					wg.Wait()
					customers = []*consumer_models.CustomerConsumer{}
				}
				flag = true
			}
		}
	}

	fmt.Printf("Closing consumer\n")
	c.Close()
}
