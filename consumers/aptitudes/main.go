package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/consumer_models"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	mongo_v2 "desktop-middleware/database/mongo_v2"
	"desktop-middleware/models"
	"desktop-middleware/models_v2"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func main() {
	config.SetConfiguration()
	fmt.Println("Consumer started!")
	topic := viper.GetString("kafka.database") + ".dbmediweb.formato_diagnostico"
	fmt.Println(topic)
	// Create Consumer instance
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": viper.GetString("kafka.bootstrap_servers"),
		"sasl.mechanisms":   viper.GetString("kafka.sasl_mechanisms"),
		"security.protocol": viper.GetString("kafka.security_protocol"),
		"sasl.username":     viper.GetString("kafka.sasl_username"),
		"sasl.password":     viper.GetString("kafka.sasl_password"),
		"group.id":          viper.GetString("kafka.groupId"),
		"auto.offset.reset": "earliest",
	})
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	err = c.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	// Set up a channel for handling Ctrl-C, etc
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	// Process messages
	run := true
	flag := true
	var aptitudes []*consumer_models.AptitudeConsumer
	db := database.InitDatabases()
	defer db.DisconnectDatabases()
	for run {
		var logModel models.Log
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			if flag {
				msg, err := c.ReadMessage(100 * time.Millisecond)
				if err != nil {
					// Errors are informational and automatically handled by the consumer
					flag = false
					continue
				}
				if msg.Value == nil || len(msg.Value) == 0 {
					continue
				}
				var aptitude consumer_models.AptitudeConsumer
				logModel.Collection = "middleware.transactions.attentions.aptitudes"
				logModel.Status = "In Progress"
				logModel.Body = msg.Value
				log, _ := mongo.GetLogsCollection().AddLogs(context.TODO(), &logModel)
				err = json.Unmarshal(msg.Value, &aptitude)
				if err != nil {
					logModel.Operation = "Decode"
					logModel.Status = "Error"
					logModel.Error = fmt.Sprintf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					json.Unmarshal(msg.Value, &logModel.Body)
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), log.ID, &logModel)
					fmt.Printf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					flag = false
					continue
				}
				aptitude.IdLog = log.ID
				aptitudes = append(aptitudes, &aptitude)
				if len(aptitudes) > viper.GetInt("kafka.fetchCount") {
					flag = false
				}
			} else {
				if len(aptitudes) > 0 {
					numCPU := viper.GetInt("kafka.threadCount")
					length := len(aptitudes)
					wg := sync.WaitGroup{}
					chunkSize := (length + numCPU - 1) / numCPU

					for i := 0; i < length; i += chunkSize {

						wg.Add(1)
						go func(i int, chunkSize int) {
							end := i + chunkSize
							defer wg.Done()

							if end > length {
								end = length
							}
							slice := aptitudes[i:end]

							for _, aptitude := range slice {
								AddAptitudeV2(aptitude, logModel)
							}
						}(i, chunkSize)
					}
					wg.Wait()
					aptitudes = []*consumer_models.AptitudeConsumer{}
				}
				flag = true
			}
		}
	}

	fmt.Printf("Closing consumer\n")
	c.Close()
}

func AddAptitudeV2(aptitude *consumer_models.AptitudeConsumer, logModel models.Log) {

	flag, err := strconv.ParseBool(aptitude.IsDeleted)
	if err != nil {
		logModel.Operation = "Decode"
		logModel.Status = "Error"
		logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
		logModel.Body = aptitude
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), aptitude.IdLog, &logModel)
		fmt.Printf("Failed to decode JSON: %v", err)
		return
	}

	if flag || aptitude.Status == "0" {
		_, err := mongo_v2.GetAttentionCollection().UnsetFieldByExternalId(context.TODO(), strconv.Itoa(aptitude.AttentionId), "aptitude")
		if err != nil {
			logModel.Operation = "Delete"
			logModel.Status = "Error"
			logModel.Error = fmt.Sprintf("Failed to delete aptitude: %v", err)
			logModel.Body = aptitude
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), aptitude.IdLog, &logModel)
			fmt.Printf("Failed to delete aptitude: %v", err)
		} else {
			logModel.Operation = "Delete"
			logModel.Status = "Done"
			logModel.Body = aptitude
			logModel.Error = ""
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), aptitude.IdLog, &logModel)

		}
	} else {
		restrictions := []string{}

		if aptitude.Restriction1 != nil {
			if *aptitude.Restriction1 == "si" {
				restrictions = append(restrictions, "USO PERMANENTE DE LENTES CORRECTORES CON MEDIDA ADECUADA")
			}
		}

		if aptitude.Restriction2 != nil {
			if *aptitude.Restriction2 == "si" {
				restrictions = append(restrictions, "RESTRICCIÓN PARA REALIZAR LABORES QUE REQUIERAN DISCRIMINAR COLORES")
			}
		}

		if aptitude.Restriction3 != nil {
			if *aptitude.Restriction3 == "si" {
				restrictions = append(restrictions, "SE RESTRINGEN LABORES SIN EL USO PERMANENTE DE LENTES CORRECTORES.")
			}
		}

		if aptitude.Restriction4 != nil {
			if *aptitude.Restriction4 == "si" {
				restrictions = append(restrictions, "RESTRICCION A MANEJO DE VEHICULOS MOVILES")
			}
		}

		if aptitude.Restriction5 != nil {
			if *aptitude.Restriction5 == "si" {
				restrictions = append(restrictions, "RESTRICCIÓN A REALIZAR TRABAJOS EN ALTURA MAYOR A 1.8 METROS")
			}
		}

		if aptitude.Restriction6 != nil {
			if *aptitude.Restriction6 == "si" {
				restrictions = append(restrictions, "SE RESTRINGE LA EXPOSICIÓN A RUIDO MAYOR A 85 DECIBELES SIN PROTECCIÓN AUDITIVA.")
			}
		}

		if aptitude.Restriction7 != nil {
			if *aptitude.Restriction7 == "si" {
				restrictions = append(restrictions, "RESTRICCIÓN A REALIZAR TRABAJOS EN ALTURA MAYOR A 1.8 METROS SIN LENTES CORRECTORES")
			}
		}

		if aptitude.Restriction9 != nil {
			if *aptitude.Restriction9 == "si" {
				restrictions = append(restrictions, "RESTRICCIÓN A POSTURA ESTÁTICA PROLONGADA DE COLUMNA")
			}
		}

		if aptitude.Restriction10 != nil {
			if *aptitude.Restriction10 == "si" {
				restrictions = append(restrictions, "RESTRICCIÓN A ELEVAR LOS MIEMBROS SUPERIORES POR ENCIMA DEL NIVEL DEL HOMBRO")
			}
		}

		if aptitude.Restriction11 != nil {
			if *aptitude.Restriction11 == "si" {
				restrictions = append(restrictions, "RESTRICCIÓN A POSTURAS FORZADAS DE COLUMNA")
			}
		}

		if aptitude.Restriction12 != nil {
			if *aptitude.Restriction12 == "si" {
				restrictions = append(restrictions, "RESTRICCIÓN A MOVIMIENTOS DE FLEXO EXTENSIÓN REPETITIVOS DE RODILLAS")
			}
		}

		if aptitude.Restriction13 != nil {
			if *aptitude.Restriction13 == "si" {
				restrictions = append(restrictions, "RESTRICCIÓN A MOVIMIENTOS DE FLEXO EXTENSIÓN REPETITIVOS DE MUÑECAS")
			}
		}

		if aptitude.Restriction14 != nil {
			if *aptitude.Restriction14 == "si" {
				restrictions = append(restrictions, "RESTRICCIÓN A MANIPULACIÓN MANUAL DE CARGA")
			}
		}

		if aptitude.Restriction15 != nil {
			if *aptitude.Restriction15 == "si" {
				restrictions = append(restrictions, "RESTRICCIÓN A BIPEDESTACIÓN PROLONGADA")
			}
		}

		if aptitude.Restriction16 != nil {
			if *aptitude.Restriction16 == "si" {
				restrictions = append(restrictions, "RESTRICCIÓN A ACTIVIDADES QUE INVOLUCREN TORSIÓN DE LA COLUMNA ASOCIADO A MANIPULACIÓN MANUAL DE CARGAS")
			}
		}

		if aptitude.Restriction17 != nil {
			if *aptitude.Restriction17 == "si" {
				restrictions = append(restrictions, "RESTRICCIÓN A EXPOSICIÓN DE POLVO SIN USO DE EPP")
			}
		}

		if aptitude.Restriction18 != nil {
			if *aptitude.Restriction18 == "si" {
				restrictions = append(restrictions, "PERDIDA DEL 10% DEL PESO CORPORAL EN LOS 6 MESES SIGUIENTES")
			}
		}

		if aptitude.Restriction19 != nil {
			if *aptitude.Restriction19 == "si" {
				restrictions = append(restrictions, "RESTRICCIÓN A SITUACIONES QUE GENEREN TENSIÓN Y EXPOSICIÓN A RIESGO PSICOSOCIAL")
			}
		}

		if aptitude.Restriction20 != nil {
			if *aptitude.Restriction20 == "si" {
				restrictions = append(restrictions, "RESTRICCIÓN A LA EXPOSICIÓN DE SUSTANCIAS HEPATOTÓXICAS SIN USO DE EPP")
			}
		}

		if aptitude.Restriction21 != nil {
			if *aptitude.Restriction21 == "si" {
				restrictions = append(restrictions, "RESTRICCIÓN A LABORES EN TURNO NOCTURNO QUE REQUIERAN ALTO NIVEL DE CONCENTRACIÓN")
			}
		}

		if aptitude.Restriction22 != nil {
			if *aptitude.Restriction22 == "si" {
				restrictions = append(restrictions, "RESTRICCIÓN A LABORES EN ESPACIOS CONFINADOS")
			}
		}

		if aptitude.Restriction23 != nil {
			if *aptitude.Restriction23 == "si" {
				restrictions = append(restrictions, "RESTRICCIÓN A LABORES CON EXPOSICION DIRECTA A RADIACIÓN SOLAR SIN USO DE EPP")
			}
		}
		attention, err := mongo_v2.GetAttentionCollection().GetAttentionByExternalId(context.TODO(), strconv.Itoa(aptitude.AttentionId))
		if err != nil {
			logModel.Operation = "Get"
			logModel.Error = fmt.Sprintf("Failed to get aptitude: %v", err)
			logModel.Body = aptitude
			logModel.Status = "Error"
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), aptitude.IdLog, &logModel)
			fmt.Printf("Failed to get aptitude: %v", err)
		}

		if attention == nil {
			return
		}

		oldaptitude := attention.Aptitude

		if oldaptitude != nil {
			oldaptitude.AttentionId = attention.ID
			oldaptitude.StartDate = aptitude.AddDateStarDate()
			oldaptitude.EndDate = aptitude.AddDateEndDate()
			status, _ := strconv.Atoi(aptitude.Status)
			oldaptitude.Status = status
			if aptitude.ObservedReason != nil {
				oldaptitude.ObservedReason = *aptitude.ObservedReason
			} else {
				oldaptitude.ObservedReason = ""
			}
			if aptitude.AptitudeDetail != nil {
				oldaptitude.AptitudeDetail = *aptitude.AptitudeDetail
			} else {
				oldaptitude.AptitudeDetail = ""
			}
			oldaptitude.Restriction = restrictions
			oldaptitude.Recommendations = aptitude.SetRecommendations()

			oldaptitude.ResultCode = aptitude.Aptitude
			switch aptitude.Aptitude {
			case "1":
				oldaptitude.ResultDescription = "Medicamente Apto"
			case "2":
				oldaptitude.ResultDescription = "Apto con Restricción"
			case "3":
				oldaptitude.ResultDescription = "Observado"
			case "4":
				oldaptitude.ResultDescription = "Medicamente no Apto"
			case "5":
				oldaptitude.ResultDescription = "Pendiente por falta de resultados"
			case "6":
				oldaptitude.ResultDescription = "Pendiente por falta de examen"
			case "8":
				oldaptitude.ResultDescription = "Retiro"
			default:
				oldaptitude.ResultDescription = "Sin Aptitud"
			}

			if aptitude.Reviewed == "ON" {
				oldaptitude.Reviewed = true
			} else {
				oldaptitude.Reviewed = false
			}

			if aptitude.Audited == "ON" {
				oldaptitude.Audited = true
			} else {
				oldaptitude.Audited = false
			}
			oldaptitude.LastUpdatedDate = time.Now()

			_, err := mongo_v2.GetAttentionCollection().SetFieldByID(context.TODO(), attention.ID, "aptitude", oldaptitude)
			if err != nil {
				logModel.Operation = "Update"
				logModel.Error = fmt.Sprintf("Failed to update aptitude: %v", err)
				logModel.Body = aptitude
				logModel.Status = "Error"
				mongo.GetLogsCollection().UpdateLogs(context.TODO(), aptitude.IdLog, &logModel)
				fmt.Printf("Failed to update aptitude: %v", err)
				return
			} else {
				logModel.Operation = "Update"
				logModel.Body = aptitude
				logModel.Status = "Done"
				logModel.Error = ""
				mongo.GetLogsCollection().UpdateLogs(context.TODO(), aptitude.IdLog, &logModel)
			}
			fmt.Println("updated: ", oldaptitude.ID)
		} else {
			var newAptitude models_v2.Aptitude
			newAptitude.ID = primitive.NewObjectID().Hex()
			newAptitude.AttentionId = attention.ID
			newAptitude.StartDate = aptitude.AddDateStarDate()
			newAptitude.EndDate = aptitude.AddDateEndDate()
			status, _ := strconv.Atoi(aptitude.Status)
			newAptitude.Status = status
			if aptitude.ObservedReason != nil {
				newAptitude.ObservedReason = *aptitude.ObservedReason
			} else {
				newAptitude.ObservedReason = ""
			}
			if aptitude.AptitudeDetail != nil {
				newAptitude.AptitudeDetail = *aptitude.AptitudeDetail
			} else {
				newAptitude.AptitudeDetail = ""
			}
			newAptitude.Restriction = restrictions
			newAptitude.Recommendations = aptitude.SetRecommendations()
			newAptitude.ResultCode = aptitude.Aptitude
			switch aptitude.Aptitude {
			case "1":
				newAptitude.ResultDescription = "Medicamente Apto"
			case "2":
				newAptitude.ResultDescription = "Apto con Restricción"
			case "3":
				newAptitude.ResultDescription = "Observado"
			case "4":
				newAptitude.ResultDescription = "Medicamente no Apto"
			case "5":
				newAptitude.ResultDescription = "Pendiente por falta de resultados"
			case "6":
				newAptitude.ResultDescription = "Pendiente por falta de examen"
			case "8":
				newAptitude.ResultDescription = "Retiro"
			default:
				newAptitude.ResultDescription = "Sin Aptitud"
			}

			if aptitude.Reviewed == "ON" {
				newAptitude.Reviewed = true
			} else {
				newAptitude.Reviewed = false
			}

			if aptitude.Audited == "ON" {
				newAptitude.Audited = true
			} else {
				newAptitude.Audited = false
			}
			newAptitude.CreatedDate = time.Now()
			newAptitude.LastUpdatedDate = time.Now()
			newAptitude.ExternalId = strconv.Itoa(aptitude.ID)
			newAptitude.ExternalSystem = "MEDIWEB"
			_, err := mongo_v2.GetAttentionCollection().SetFieldByID(context.TODO(), attention.ID, "aptitude", newAptitude)
			if err != nil {
				logModel.Operation = "Insert"
				logModel.Error = fmt.Sprintf("Failed to insert aptitude: %v", err)
				logModel.Body = aptitude
				logModel.Status = "Error"
				mongo.GetLogsCollection().UpdateLogs(context.TODO(), aptitude.IdLog, &logModel)
				fmt.Printf("Failed to add aptitude: %v", err)
				return
			} else {
				logModel.Operation = "Insert"
				logModel.Body = aptitude
				logModel.Status = "Done"
				logModel.Error = ""
				mongo.GetLogsCollection().UpdateLogs(context.TODO(), aptitude.IdLog, &logModel)
			}
			fmt.Println("created: ", newAptitude.ID)
		}
		if attention.PersonId != nil && attention.CustomerId != nil {
			if *attention.AttentionTypeName == "Ocupacional" {
				lastEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *attention.PersonId, *attention.CustomerId, "emo")
				if lastEmo != nil {
					mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *attention.PersonId, "lastEmoAttention", lastEmo)
				}
				lastAnyEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *attention.PersonId, *attention.CustomerId, "any")
				if lastAnyEmo != nil {
					lastAnyEmo.Customer = nil
					lastAnyEmo.Patient = nil
					lastAnyEmo.Person = nil
					lastAnyEmo.Consultations = nil
					lastAnyEmo.AttentionLines = nil
					lastAnyEmo.Exams = nil
					lastAnyEmo.AudiometryTests = nil
					lastAnyEmo.BiochemistryTests = nil
					lastAnyEmo.CovidTests = nil
					lastAnyEmo.Diagnostics = nil
					lastAnyEmo.DrugTests = nil
					lastAnyEmo.HematologyTests = nil
					lastAnyEmo.ImmunologyTests = nil
					lastAnyEmo.MicrobiologyTests = nil
					lastAnyEmo.OphtalmologyTests = nil
					lastAnyEmo.PsychologyTests = nil
					lastAnyEmo.Recommendations = nil
					lastAnyEmo.ExamResults = nil
					lastAnyEmo.SkeletalMuscleTests = nil
					lastAnyEmo.UrineTests = nil
					lastAnyEmo.MedicalHistories = nil
					lastAnyEmo.PathologicalHistories = nil
					lastAnyEmo.Triages = nil
					lastAnyEmo.WorkHistories = nil
					lastAnyEmo.SpirometryTests = nil
					mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *attention.PersonId, "lastAnyEmoAttention", lastAnyEmo)
				}
			}
			if *attention.AttentionTypeName == "Asistencial" {
				lastNoEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *attention.PersonId, *attention.CustomerId, "no_emo")
				if lastNoEmo != nil {
					mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *attention.PersonId, "lastNoEmoAttention", lastNoEmo)
				}
			}
		}
	}
}
