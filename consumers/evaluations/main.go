package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/consumer_models"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"desktop-middleware/models"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"

	mongo_v2 "desktop-middleware/database/mongo_v2"

	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func main() {
	config.SetConfiguration()
	fmt.Println("Consumer started!")
	topic := viper.GetString("kafka.database") + ".doktuz.nps"
	fmt.Println(topic)
	// Create Consumer instance
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": viper.GetString("kafka.bootstrap_servers"),
		"sasl.mechanisms":   viper.GetString("kafka.sasl_mechanisms"),
		"security.protocol": viper.GetString("kafka.security_protocol"),
		"sasl.username":     viper.GetString("kafka.sasl_username"),
		"sasl.password":     viper.GetString("kafka.sasl_password"),
		"group.id":          viper.GetString("kafka.groupId"),
		"auto.offset.reset": "earliest",
	})
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	err = c.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	// Set up a channel for handling Ctrl-C, etc
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	// Process messages
	run := true
	flag := true
	var evaluations []*consumer_models.EvaluationConsumer
	db := database.InitDatabases()
	defer db.DisconnectDatabases()
	for run {
		var logModel models.Log
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			if flag {
				msg, err := c.ReadMessage(100 * time.Millisecond)
				if err != nil {
					// Errors are informational and automatically handled by the consumer
					flag = false
					continue
				}
				if msg.Value == nil || len(msg.Value) == 0 {
					continue
				}
				var evaluation consumer_models.EvaluationConsumer
				logModel.Collection = "middleware.transactions.evaluations"
				logModel.Status = "In Progress"
				logModel.Body = msg.Value
				log, _ := mongo.GetLogsCollection().AddLogs(context.TODO(), &logModel)
				err = json.Unmarshal(msg.Value, &evaluation)
				if err != nil {
					logModel.Operation = "Decode"
					logModel.Status = "Error"
					logModel.Collection = "middleware.transactions.evaluations"
					logModel.Error = fmt.Sprintf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					json.Unmarshal(msg.Value, &logModel.Body)
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), log.ID, &logModel)
					fmt.Printf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					flag = false
					continue
				}
				evaluation.IdLog = log.ID
				evaluations = append(evaluations, &evaluation)
				if len(evaluations) > viper.GetInt("kafka.fetchCount") {
					flag = false
				}
			} else {
				if len(evaluations) > 0 {
					numCPU := viper.GetInt("kafka.threadCount")
					length := len(evaluations)
					wg := sync.WaitGroup{}
					chunkSize := (length + numCPU - 1) / numCPU

					for i := 0; i < length; i += chunkSize {

						wg.Add(1)
						go func(i int, chunkSize int) {
							end := i + chunkSize
							defer wg.Done()

							if end > length {
								end = length
							}
							slice := evaluations[i:end]

							for _, evaluation := range slice {
								AddEvaluationsV2(evaluation, logModel)
							}
						}(i, chunkSize)
					}
					wg.Wait()
					evaluations = []*consumer_models.EvaluationConsumer{}
				}
				flag = true
			}
		}
	}

	fmt.Printf("Closing consumer\n")
	c.Close()
}

func AddEvaluationsV2(evaluation *consumer_models.EvaluationConsumer, logModel models.Log) {

	flag, err := strconv.ParseBool(evaluation.IsDeleted)
	if err != nil {
		logModel.Operation = "Decode"
		logModel.Status = "Error"
		logModel.Collection = "middleware.transactions.attentions.evaluations"
		logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
		logModel.Body = evaluation
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), evaluation.IdLog, &logModel)
		fmt.Printf("Failed to decode JSON: %v", err)
		return
	}

	if flag {
		_, err := mongo_v2.GetAttentionCollection().UnsetFieldByExternalId(context.TODO(), strconv.Itoa(evaluation.AttentionId), "nps")
		if err != nil {
			logModel.Operation = "Delete"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.evaluations"
			logModel.Error = fmt.Sprintf("Failed to delete evaluation: %v", err)
			logModel.Body = evaluation
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), evaluation.IdLog, &logModel)
			fmt.Printf("Failed to delete evaluation: %v", err)
		} else {
			logModel.Operation = "Delete"
			logModel.Status = "Done"
			logModel.Collection = "middleware.transactions.attentions.evaluations"
			logModel.Error = ""
			logModel.Body = evaluation
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), evaluation.IdLog, &logModel)
		}
	} else {
		attention, err := mongo_v2.GetAttentionCollection().GetAttentionByExternalId(context.TODO(), strconv.Itoa(evaluation.AttentionId))
		if err != nil || attention == nil {
			logModel.Operation = "Get"
			logModel.Error = fmt.Sprintf("Failed to get evaluation: %v", err)
			logModel.Body = evaluation
			logModel.Status = "Error"
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), evaluation.IdLog, &logModel)
			fmt.Printf("Failed to get evaluation: %v", err)
		}

		if attention == nil {
			return
		}

		oldevaluation := attention.NPS

		if oldevaluation != nil {
			oldevaluation.EvaluationDate = evaluation.SetEvaluationDate()
			oldevaluation.Result = evaluation.Result
			oldevaluation.Comments = evaluation.Comments
			oldevaluation.EvaluationType = "NPS"

			if evaluation.WasRejected == 1 {
				oldevaluation.WasRejected = true
			} else {
				oldevaluation.WasRejected = false
			}
			oldevaluation.LastUpdatedDate = time.Now()
			oldevaluation.AttentionId = attention.ID
			if attention.PatientId != nil {
				oldevaluation.PatientId = *attention.PatientId
			}
			if attention.PersonId != nil {
				oldevaluation.PersonId = *attention.PersonId
			}

			_, err := mongo_v2.GetAttentionCollection().SetFieldByID(context.Background(), attention.ID, "nps", oldevaluation)
			if err != nil {
				logModel.Operation = "Update"
				logModel.Status = "Error"
				logModel.Collection = "middleware.transactions.attentions.evaluations - v2"
				logModel.Error = fmt.Sprintf("Failed to update evaluation: %v", err)
				logModel.Body = evaluation
				mongo.GetLogsCollection().UpdateLogs(context.TODO(), evaluation.IdLog, &logModel)
				fmt.Printf("Failed to update evaluation: %v", err)
				return
			} else {
				logModel.Operation = "Update"
				logModel.Status = "Done"
				logModel.Collection = "middleware.transactions.attentions.evaluations - v2"
				logModel.Error = ""
				logModel.Body = evaluation
				mongo.GetLogsCollection().UpdateLogs(context.TODO(), evaluation.IdLog, &logModel)
			}
			fmt.Println("updated: ", oldevaluation.ID)
		} else {
			var newEvaluation models.Evaluation
			newEvaluation.ID = primitive.NewObjectID().Hex()
			newEvaluation.EvaluationDate = evaluation.SetEvaluationDate()
			newEvaluation.Result = evaluation.Result
			newEvaluation.Comments = evaluation.Comments
			newEvaluation.EvaluationType = "NPS"

			if evaluation.WasRejected == 1 {
				newEvaluation.WasRejected = true
			} else {
				newEvaluation.WasRejected = false
			}
			newEvaluation.AttentionId = attention.ID
			if attention.PatientId != nil {
				newEvaluation.PatientId = *attention.PatientId
			}
			if attention.PersonId != nil {
				newEvaluation.PersonId = *attention.PersonId
			}
			newEvaluation.CreatedDate = time.Now()
			newEvaluation.LastUpdatedDate = time.Now()
			newEvaluation.ExternalId = strconv.Itoa(evaluation.ID)
			newEvaluation.ExternalSystem = "MEDIWEB"
			_, err := mongo_v2.GetAttentionCollection().SetFieldByID(context.Background(), attention.ID, "nps", newEvaluation)
			if err != nil {
				logModel.Operation = "Insert"
				logModel.Status = "Error"
				logModel.Collection = "middleware.transactions.attentions.evaluations - v2"
				logModel.Error = fmt.Sprintf("Failed to insert evaluation: %v", err)
				logModel.Body = evaluation
				mongo.GetLogsCollection().UpdateLogs(context.TODO(), evaluation.IdLog, &logModel)
				fmt.Printf("Failed to add evaluation: %v", err)
				return
			} else {
				logModel.Operation = "Insert"
				logModel.Status = "Done"
				logModel.Collection = "middleware.transactions.attentions.evaluations - v2"
				logModel.Error = ""
				logModel.Body = evaluation
				mongo.GetLogsCollection().UpdateLogs(context.TODO(), evaluation.IdLog, &logModel)
			}
			fmt.Println("created: ", newEvaluation.ID)
		}
		if attention.PersonId != nil && attention.AttentionTypeName != nil && attention.CustomerId != nil {
			if *attention.AttentionTypeName == "Ocupacional" {
				lastEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *attention.PersonId, *attention.CustomerId, "emo")
				if lastEmo != nil {
					mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *attention.PersonId, "lastEmoAttention", lastEmo)
				}
				lastAnyEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *attention.PersonId, *attention.CustomerId, "any")
				if lastAnyEmo != nil {
					lastAnyEmo.Customer = nil
					lastAnyEmo.Patient = nil
					lastAnyEmo.Person = nil
					lastAnyEmo.Consultations = nil
					lastAnyEmo.AttentionLines = nil
					lastAnyEmo.Exams = nil
					lastAnyEmo.AudiometryTests = nil
					lastAnyEmo.BiochemistryTests = nil
					lastAnyEmo.CovidTests = nil
					lastAnyEmo.Diagnostics = nil
					lastAnyEmo.DrugTests = nil
					lastAnyEmo.HematologyTests = nil
					lastAnyEmo.ImmunologyTests = nil
					lastAnyEmo.MicrobiologyTests = nil
					lastAnyEmo.OphtalmologyTests = nil
					lastAnyEmo.PsychologyTests = nil
					lastAnyEmo.Recommendations = nil
					lastAnyEmo.ExamResults = nil
					lastAnyEmo.SkeletalMuscleTests = nil
					lastAnyEmo.UrineTests = nil
					lastAnyEmo.MedicalHistories = nil
					lastAnyEmo.PathologicalHistories = nil
					lastAnyEmo.Triages = nil
					lastAnyEmo.WorkHistories = nil
					lastAnyEmo.SpirometryTests = nil
					mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *attention.PersonId, "lastAnyEmoAttention", lastAnyEmo)
				}
			}
			if *attention.AttentionTypeName == "Asistencial" {
				lastNoEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *attention.PersonId, *attention.CustomerId, "no_emo")
				if lastNoEmo != nil {
					mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *attention.PersonId, "lastNoEmoAttention", lastNoEmo)
				}
			}
		}
	}
}
