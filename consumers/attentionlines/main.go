package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/consumer_models"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	mongo_v2 "desktop-middleware/database/mongo_v2"
	"desktop-middleware/models"
	"desktop-middleware/models_v2"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func main() {
	config.SetConfiguration()
	fmt.Println("Consumer started!")
	topic := viper.GetString("kafka.database") + ".dbmediweb.det_comprobante"
	fmt.Println(topic)
	// Create Consumer instance
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": viper.GetString("kafka.bootstrap_servers"),
		"sasl.mechanisms":   viper.GetString("kafka.sasl_mechanisms"),
		"security.protocol": viper.GetString("kafka.security_protocol"),
		"sasl.username":     viper.GetString("kafka.sasl_username"),
		"sasl.password":     viper.GetString("kafka.sasl_password"),
		"group.id":          viper.GetString("kafka.groupId"),
		"auto.offset.reset": "earliest",
	})
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	err = c.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	// Set up a channel for handling Ctrl-C, etc
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	// Process messages
	run := true
	flag := true
	var attentionLines []*consumer_models.AttentionLineConsumer
	db := database.InitDatabases()
	defer db.DisconnectDatabases()
	for run {
		var logModel models.Log
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			if flag {
				msg, err := c.ReadMessage(100 * time.Millisecond)
				if err != nil {
					// Errors are informational and automatically handled by the consumer
					flag = false
					continue
				}
				if msg.Value == nil || len(msg.Value) == 0 {
					continue
				}
				var attentionLine consumer_models.AttentionLineConsumer

				logModel.Collection = "middleware.transactions.attentions.lines"
				logModel.Status = "In Progress"
				logModel.Body = msg.Value
				log, _ := mongo.GetLogsCollection().AddLogs(context.TODO(), &logModel)

				err = json.Unmarshal(msg.Value, &attentionLine)
				if err != nil {

					logModel.Operation = "Decode"
					logModel.Status = "Error"
					logModel.Error = fmt.Sprintf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					json.Unmarshal(msg.Value, &logModel.Body)
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), log.ID, &logModel)
					fmt.Printf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					flag = false
					continue
				}
				attentionLine.IdLog = log.ID

				attentionLines = append(attentionLines, &attentionLine)
				if len(attentionLines) > viper.GetInt("kafka.fetchCount") {
					flag = false
				}
			} else {
				if len(attentionLines) > 0 {
					numCPU := viper.GetInt("kafka.threadCount")
					length := len(attentionLines)
					wg := sync.WaitGroup{}
					chunkSize := (length + numCPU - 1) / numCPU

					for i := 0; i < length; i += chunkSize {

						wg.Add(1)
						go func(i int, chunkSize int) {
							end := i + chunkSize
							defer wg.Done()

							if end > length {
								end = length
							}
							slice := attentionLines[i:end]

							for _, attentionLine := range slice {
								AddAttentionLinesV2(attentionLine, logModel)
							}
						}(i, chunkSize)
					}
					wg.Wait()
					attentionLines = []*consumer_models.AttentionLineConsumer{}
				}
				flag = true
			}
		}
	}

	fmt.Printf("Closing consumer\n")
	c.Close()
}

func AddAttentionLinesV2(attentionLine *consumer_models.AttentionLineConsumer, logModel models.Log) {

	flag, err := strconv.ParseBool(attentionLine.IsDeleted)
	if err != nil {
		logModel.Operation = "Decode"
		logModel.Status = "Error"
		logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
		logModel.Body = attentionLine
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), attentionLine.IdLog, &logModel)
		fmt.Printf("Failed to decode JSON: %v", err)
		return
	}

	if flag || attentionLine.Status == 0 {
		_, err = mongo_v2.GetAttentionCollection().PullFieldByExternalId(context.TODO(), strconv.Itoa(attentionLine.AttentionId), "attentionLines", strconv.Itoa(attentionLine.ID))
		if err != nil {
			logModel.Operation = "Delete"
			logModel.Status = "Error"
			logModel.Error = fmt.Sprintf("Failed to delete attentionLine: %v", err)
			logModel.Body = attentionLine
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), attentionLine.IdLog, &logModel)
			fmt.Printf("Failed to delete attentionLine: %v", err)
		} else {
			logModel.Operation = "Delete"
			logModel.Status = "Done"
			logModel.Body = attentionLine
			logModel.Error = ""
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), attentionLine.IdLog, &logModel)
		}

	} else {
		attention, err := mongo_v2.GetAttentionCollection().GetAttentionByExternalId(context.TODO(), strconv.Itoa(attentionLine.AttentionId))
		if err != nil {
			logModel.Operation = "Get"
			logModel.Status = "Error"
			logModel.Error = fmt.Sprintf("Failed to get attentionLine: %v", err)
			logModel.Body = attentionLine
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), attentionLine.IdLog, &logModel)
			fmt.Printf("Failed to get attentionLine: %v", err)
		}

		if attention == nil {
			return
		}

		var oldattentionLine *models_v2.AttentionLine

		for _, body := range attention.AttentionLines {
			if body.ExternalId == strconv.Itoa(attentionLine.ID) {
				oldattentionLine = body
			}
		}

		if oldattentionLine != nil {
			oldattentionLine.AttentionId = attention.ID
			serv, _ := mongo_v2.GetServiceCollection().GetServiceByExternalId(context.Background(), strconv.Itoa(attentionLine.ServiceId))
			if serv != nil {
				oldattentionLine.ServiceID = serv.ID
				oldattentionLine.Service = serv

				serviceArea, _ := mongo_v2.GetServiceAreaCollection().GetServiceAreaById(context.Background(), serv.ServiceAreaId)

				if serviceArea != nil {
					oldattentionLine.ServiceArea = serviceArea
				}
			}
			oldattentionLine.Status = attentionLine.Status
			oldattentionLine.LastUpdatedDate = time.Now()

			mongo_v2.GetAttentionCollection().PullFieldByExternalId(context.TODO(), strconv.Itoa(attentionLine.AttentionId), "attentionLines", strconv.Itoa(attentionLine.ID))
			_, err := mongo_v2.GetAttentionCollection().PushFieldByID(context.TODO(), oldattentionLine.AttentionId, "attentionLines", oldattentionLine)
			if err != nil {
				logModel.Operation = "Update"
				logModel.Status = "Error"
				logModel.Error = fmt.Sprintf("Failed to update attentionLine: %v", err)
				logModel.Body = attentionLine
				mongo.GetLogsCollection().UpdateLogs(context.TODO(), attentionLine.IdLog, &logModel)
				fmt.Printf("Failed to update attentionLine: %v", err)
				return
			} else {
				logModel.Operation = "Update"
				logModel.Status = "Done"
				logModel.Body = attentionLine
				logModel.Error = ""
				mongo.GetLogsCollection().UpdateLogs(context.TODO(), attentionLine.IdLog, &logModel)
			}
			fmt.Println("updated: ", oldattentionLine.ID)
		} else {
			var newAttentionLine models_v2.AttentionLine
			newAttentionLine.ID = primitive.NewObjectID().Hex()
			newAttentionLine.AttentionId = attention.ID
			serv, _ := mongo_v2.GetServiceCollection().GetServiceByExternalId(context.Background(), strconv.Itoa(attentionLine.ServiceId))
			if serv != nil {
				newAttentionLine.ServiceID = serv.ID
				newAttentionLine.Service = serv

				serviceArea, _ := mongo_v2.GetServiceAreaCollection().GetServiceAreaById(context.Background(), serv.ServiceAreaId)

				if serviceArea != nil {
					newAttentionLine.ServiceArea = serviceArea
				}
			}
			newAttentionLine.Status = attentionLine.Status
			newAttentionLine.ExternalId = strconv.Itoa(attentionLine.ID)
			newAttentionLine.ExternalSystem = "MEDIWEB"
			newAttentionLine.CreatedDate = time.Now()
			newAttentionLine.LastUpdatedDate = time.Now()
			_, err := mongo_v2.GetAttentionCollection().PushFieldByID(context.TODO(), newAttentionLine.AttentionId, "attentionLines", newAttentionLine)
			if err != nil {
				logModel.Operation = "Insert"
				logModel.Status = "Error"
				logModel.Error = fmt.Sprintf("Failed to insert attentionLine: %v", err)
				logModel.Body = attentionLine
				mongo.GetLogsCollection().UpdateLogs(context.TODO(), attentionLine.IdLog, &logModel)
				fmt.Printf("Failed to add attentionLine: %v", err)
				return
			} else {
				logModel.Operation = "Insert"
				logModel.Status = "Done"
				logModel.Body = attentionLine
				logModel.Error = ""
				mongo.GetLogsCollection().UpdateLogs(context.TODO(), attentionLine.IdLog, &logModel)
			}

			fmt.Println("created: ", newAttentionLine.ID)
		}
		if attention.PersonId != nil && attention.CustomerId != nil {
			if *attention.AttentionTypeName == "Ocupacional" {
				lastEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *attention.PersonId, *attention.CustomerId, "emo")
				if lastEmo != nil {
					mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *attention.PersonId, "lastEmoAttention", lastEmo)
				}
				lastAnyEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *attention.PersonId, *attention.CustomerId, "any")
				if lastAnyEmo != nil {
					lastAnyEmo.Customer = nil
					lastAnyEmo.Patient = nil
					lastAnyEmo.Person = nil
					lastAnyEmo.Consultations = nil
					lastAnyEmo.AttentionLines = nil
					lastAnyEmo.Exams = nil
					lastAnyEmo.AudiometryTests = nil
					lastAnyEmo.BiochemistryTests = nil
					lastAnyEmo.CovidTests = nil
					lastAnyEmo.Diagnostics = nil
					lastAnyEmo.DrugTests = nil
					lastAnyEmo.HematologyTests = nil
					lastAnyEmo.ImmunologyTests = nil
					lastAnyEmo.MicrobiologyTests = nil
					lastAnyEmo.OphtalmologyTests = nil
					lastAnyEmo.PsychologyTests = nil
					lastAnyEmo.Recommendations = nil
					lastAnyEmo.ExamResults = nil
					lastAnyEmo.SkeletalMuscleTests = nil
					lastAnyEmo.UrineTests = nil
					lastAnyEmo.MedicalHistories = nil
					lastAnyEmo.PathologicalHistories = nil
					lastAnyEmo.Triages = nil
					lastAnyEmo.WorkHistories = nil
					lastAnyEmo.SpirometryTests = nil
					mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *attention.PersonId, "lastAnyEmoAttention", lastAnyEmo)
				}
			}
			if *attention.AttentionTypeName == "Asistencial" {
				lastNoEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *attention.PersonId, *attention.CustomerId, "no_emo")
				if lastNoEmo != nil {
					mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *attention.PersonId, "lastNoEmoAttention", lastNoEmo)
				}
			}
		}
	}
}
