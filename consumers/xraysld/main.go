package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/consumer_models"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"desktop-middleware/functions_v2"
	"desktop-middleware/models"
	"desktop-middleware/models_v2"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	mongo_v2 "desktop-middleware/database/mongo_v2"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func main() {
	config.SetConfiguration()
	fmt.Println("Consumer started!")
	topic := viper.GetString("kafka.database") + ".dbmediweb.rayosxc"
	fmt.Println(topic)
	// Create Consumer instance
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": viper.GetString("kafka.bootstrap_servers"),
		"sasl.mechanisms":   viper.GetString("kafka.sasl_mechanisms"),
		"security.protocol": viper.GetString("kafka.security_protocol"),
		"sasl.username":     viper.GetString("kafka.sasl_username"),
		"sasl.password":     viper.GetString("kafka.sasl_password"),
		"group.id":          viper.GetString("kafka.groupId"),
		"auto.offset.reset": "earliest",
	})
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	err = c.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	// Set up a channel for handling Ctrl-C, etc
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	// Process messages
	run := true
	flag := true
	var xRayLDs []*consumer_models.XRayLDConsumer
	db := database.InitDatabases()
	defer db.DisconnectDatabases()
	for run {
		var logModel models.Log
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			if flag {
				msg, err := c.ReadMessage(100 * time.Millisecond)
				if err != nil {
					// Errors are informational and automatically handled by the consumer
					flag = false
					continue
				}
				if msg.Value == nil || len(msg.Value) == 0 {
					continue
				}
				var xRayLD consumer_models.XRayLDConsumer
				logModel.Collection = "middleware.transactions.attentions.exams"
				logModel.Status = "In Progress"
				logModel.Body = msg.Value
				log, _ := mongo.GetLogsCollection().AddLogs(context.TODO(), &logModel)
				err = json.Unmarshal(msg.Value, &xRayLD)
				if err != nil {
					logModel.Operation = "Decode"
					logModel.Status = "Error"
					logModel.Collection = "middleware.transactions.attentions.exams"
					logModel.Error = fmt.Sprintf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					json.Unmarshal(msg.Value, &logModel.Body)
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), log.ID, &logModel)
					fmt.Printf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					flag = false
					continue
				}
				xRayLD.IdLog = log.ID
				xRayLDs = append(xRayLDs, &xRayLD)
				if len(xRayLDs) > viper.GetInt("kafka.fetchCount") {
					flag = false
				}
			} else {
				if len(xRayLDs) > 0 {
					numCPU := viper.GetInt("kafka.threadCount")
					length := len(xRayLDs)
					wg := sync.WaitGroup{}
					chunkSize := (length + numCPU - 1) / numCPU

					for i := 0; i < length; i += chunkSize {

						wg.Add(1)
						go func(i int, chunkSize int) {
							end := i + chunkSize
							defer wg.Done()

							if end > length {
								end = length
							}
							slice := xRayLDs[i:end]

							for _, xRayLD := range slice {
								AddXRayLDConsumerV2(xRayLD, logModel)
							}
						}(i, chunkSize)
					}
					wg.Wait()
					xRayLDs = []*consumer_models.XRayLDConsumer{}
				}
				flag = true
			}
		}
	}

	fmt.Printf("Closing consumer\n")
	c.Close()
}

func AddXRayLDConsumerV2(xRayLD *consumer_models.XRayLDConsumer, logModel models.Log) {

	flag, err := strconv.ParseBool(xRayLD.IsDeleted)
	if err != nil {
		logModel.Operation = "Decode"
		logModel.Status = "Error"
		logModel.Collection = "middleware.transactions.attentions.exams"
		logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
		logModel.Body = xRayLD
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), xRayLD.IdLog, &logModel)
		fmt.Printf("Failed to decode JSON: %v", err)
		return
	}
	serv, err := mongo_v2.GetServiceAreaCollection().GetServiceAreaByExternalId(context.Background(), "14")
	if err != nil {
		logModel.Operation = "Get"
		logModel.Status = "Error"
		logModel.Collection = "middleware.registers.serviceareas"
		logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
		logModel.Body = xRayLD
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), xRayLD.IdLog, &logModel)
		fmt.Printf("Failed to decode JSON: %v", err)
		return
	}
	atte, err := mongo_v2.GetAttentionCollection().GetAttentionByExternalId(context.Background(), xRayLD.AttentionId)
	if err != nil {
		logModel.Operation = "Get"
		logModel.Status = "Error"
		logModel.Collection = "middleware.transactions.attentions"
		logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
		logModel.Body = xRayLD
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), xRayLD.IdLog, &logModel)
		fmt.Printf("Failed to decode JSON: %v", err)
		return
	}
	if atte == nil {
		return
	}
	attentionId := atte.ID

	if flag || xRayLD.Status == 0 {
		_, err = mongo_v2.GetAttentionCollection().PullBodyFromExamsByExternalId(context.TODO(), attentionId, strconv.Itoa(xRayLD.ID), serv.ID)
		if err != nil {
			logModel.Operation = "Delete"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.exams"
			logModel.Error = fmt.Sprintf("Failed to delete xRayLD: %v", err)
			logModel.Body = xRayLD
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), xRayLD.IdLog, &logModel)
			fmt.Printf("Failed to delete xRayLD: %v", err)
		} else {
			logModel.Operation = "Delete"
			logModel.Status = "Done"
			logModel.Collection = "middleware.transactions.attentions.exams"
			logModel.Error = ""
			logModel.Body = xRayLD
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), xRayLD.IdLog, &logModel)
		}

		mongo_v2.GetAttentionCollection().PullDiagnosticsByExternalId(context.Background(), attentionId, serv.ID)
	} else {

		var oldxRayLD *models_v2.Exam

		for _, body := range atte.Exams {
			if body.ExternalId == strconv.Itoa(xRayLD.ID) && body.ServiceAreaID == serv.ID {
				oldxRayLD = body
			}
		}

		if oldxRayLD != nil {
			CreateXRay(atte, serv, attentionId, xRayLD, oldxRayLD, logModel)
			fmt.Println("updated: ", oldxRayLD.ID)
		} else {
			var newXRayLD models_v2.Exam
			CreateXRay(atte, serv, attentionId, xRayLD, &newXRayLD, logModel)
			fmt.Println("created: ", xRayLD.ID)
		}
	}
	if atte.PersonId != nil && atte.AttentionTypeName != nil && atte.CustomerId != nil {
		if *atte.AttentionTypeName == "Ocupacional" {
			lastEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *atte.PersonId, *atte.CustomerId, "emo")
			if lastEmo != nil {
				mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *atte.PersonId, "lastEmoAttention", lastEmo)
			}
			lastAnyEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *atte.PersonId, *atte.CustomerId, "any")
			if lastAnyEmo != nil {
				lastAnyEmo.Customer = nil
				lastAnyEmo.Patient = nil
				lastAnyEmo.Person = nil
				lastAnyEmo.Consultations = nil
				lastAnyEmo.AttentionLines = nil
				lastAnyEmo.Exams = nil
				lastAnyEmo.AudiometryTests = nil
				lastAnyEmo.BiochemistryTests = nil
				lastAnyEmo.CovidTests = nil
				lastAnyEmo.Diagnostics = nil
				lastAnyEmo.DrugTests = nil
				lastAnyEmo.HematologyTests = nil
				lastAnyEmo.ImmunologyTests = nil
				lastAnyEmo.MicrobiologyTests = nil
				lastAnyEmo.OphtalmologyTests = nil
				lastAnyEmo.PsychologyTests = nil
				lastAnyEmo.Recommendations = nil
				lastAnyEmo.ExamResults = nil
				lastAnyEmo.SkeletalMuscleTests = nil
				lastAnyEmo.UrineTests = nil
				lastAnyEmo.MedicalHistories = nil
				lastAnyEmo.PathologicalHistories = nil
				lastAnyEmo.Triages = nil
				lastAnyEmo.WorkHistories = nil
				lastAnyEmo.SpirometryTests = nil
				mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *atte.PersonId, "lastAnyEmoAttention", lastAnyEmo)
			}
		}
		if *atte.AttentionTypeName == "Asistencial" {
			lastNoEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *atte.PersonId, *atte.CustomerId, "no_emo")
			if lastNoEmo != nil {
				mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *atte.PersonId, "lastNoEmoAttention", lastNoEmo)
			}
		}
	}
}

func CreateXRay(atte *models_v2.Attention, serv *models_v2.ServiceArea, attentionId string, xRayLD *consumer_models.XRayLDConsumer, exam *models_v2.Exam, logModel models.Log) {
	if exam.ID == "" {
		exam.ID = primitive.NewObjectID().Hex()
		exam.ExternalId = strconv.Itoa(xRayLD.ID)
		exam.ExternalSystem = "MEDIWEB"
		exam.CreatedDate = time.Now()
		exam.LastUpdatedDate = time.Now()
	}
	exam.AttentionID = attentionId
	exam.ServiceAreaID = serv.ID
	exam.ServiceAreaCode = serv.Code
	exam.ServiceArea = serv
	exam.Result = xRayLD.Result

	var filenames []string
	if xRayLD.FileName1 != "" {
		img := strings.Split(xRayLD.FileName1, ".")
		filenames = append(filenames, fmt.Sprintf("1.%s", img[1]))
	}
	if xRayLD.FileName2 != "" {
		img := strings.Split(xRayLD.FileName2, ".")
		filenames = append(filenames, fmt.Sprintf("2.%s", img[1]))
	}
	if xRayLD.FileName3 != "" {
		img := strings.Split(xRayLD.FileName3, ".")
		filenames = append(filenames, fmt.Sprintf("3.%s", img[1]))
	}
	if xRayLD.FileName4 != "" {
		img := strings.Split(xRayLD.FileName4, ".")
		filenames = append(filenames, fmt.Sprintf("4.%s", img[1]))
	}
	if xRayLD.FileName5 != "" {
		img := strings.Split(xRayLD.FileName5, ".")
		filenames = append(filenames, fmt.Sprintf("5.%s", img[1]))
	}
	if xRayLD.FileName6 != "" {
		img := strings.Split(xRayLD.FileName6, ".")
		filenames = append(filenames, fmt.Sprintf("6.%s", img[1]))
	}
	if xRayLD.FileName7 != "" {
		img := strings.Split(xRayLD.FileName7, ".")
		filenames = append(filenames, fmt.Sprintf("7.%s", img[1]))
	}
	if xRayLD.FileName8 != "" {
		img := strings.Split(xRayLD.FileName8, ".")
		filenames = append(filenames, fmt.Sprintf("8.%s", img[1]))
	}
	if xRayLD.FileName9 != "" {
		img := strings.Split(xRayLD.FileName9, ".")
		filenames = append(filenames, fmt.Sprintf("9.%s", img[1]))
	}
	if xRayLD.FileName10 != "" {
		img := strings.Split(xRayLD.FileName10, ".")
		filenames = append(filenames, fmt.Sprintf("10.%s", img[1]))
	}
	if xRayLD.FileName11 != "" {
		img := strings.Split(xRayLD.FileName11, ".")
		filenames = append(filenames, fmt.Sprintf("11.%s", img[1]))
	}
	if xRayLD.FileName12 != "" {
		img := strings.Split(xRayLD.FileName12, ".")
		filenames = append(filenames, fmt.Sprintf("12.%s", img[1]))
	}
	if xRayLD.FileName13 != "" {
		img := strings.Split(xRayLD.FileName13, ".")
		filenames = append(filenames, fmt.Sprintf("13.%s", img[1]))
	}
	if xRayLD.FileName14 != "" {
		img := strings.Split(xRayLD.FileName14, ".")
		filenames = append(filenames, fmt.Sprintf("14.%s", img[1]))
	}
	if xRayLD.FileName15 != "" {
		img := strings.Split(xRayLD.FileName15, ".")
		if len(img) > 1 {
			filenames = append(filenames, fmt.Sprintf("15.%s", img[1]))
		}
	}

	exam.Status = xRayLD.Status
	exam.Filenames = filenames
	exam.LastUpdatedDate = time.Now()

	mongo_v2.GetAttentionCollection().PullBodyFromExamsByExternalId(context.TODO(), attentionId, strconv.Itoa(xRayLD.ID), serv.ID)
	_, err := mongo_v2.GetAttentionCollection().PushBodyToExamsByExternalId(context.TODO(), attentionId, exam)
	if err != nil {
		logModel.Operation = "Update"
		logModel.Status = "Error"
		logModel.Collection = "middleware.transactions.attentions.exams"
		logModel.Error = fmt.Sprintf("Failed to update xRay: %v", err)
		logModel.Body = xRayLD
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), xRayLD.IdLog, &logModel)
		fmt.Printf("Failed to update xRay: %v", err)
	} else {
		logModel.Operation = "Update"
		logModel.Status = "Done"
		logModel.Collection = "middleware.transactions.attentions.exams"
		logModel.Error = ""
		logModel.Body = xRayLD
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), xRayLD.IdLog, &logModel)
	}

	mongo_v2.GetAttentionCollection().PullDiagnosticsByExternalId(context.Background(), attentionId, serv.ID)

	diagnostics := functions_v2.GetDiagnosticsFromXRayLDConsumer(exam.ID, exam.AttentionID, exam.ServiceAreaID, exam.ServiceAreaCode, xRayLD, serv)
	if len(diagnostics) > 0 {
		_, err := mongo_v2.GetAttentionCollection().PushDiagnosticsByExternalId(context.Background(), attentionId, diagnostics)
		if err != nil {
			logModel.Operation = "Insert"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.exams.diagnostics"
			logModel.Error = fmt.Sprintf("Failed to insert xRay: %v", err)
			logModel.Body = xRayLD
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), xRayLD.IdLog, &logModel)
			fmt.Printf("Failed to add xRay: %v", err)
		} else {
			logModel.Operation = "Insert"
			logModel.Status = "Done"
			logModel.Body = xRayLD
			logModel.Collection = "middleware.transactions.attentions.exams.diagnostics"
			logModel.Error = ""
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), xRayLD.IdLog, &logModel)

		}
	}
}
