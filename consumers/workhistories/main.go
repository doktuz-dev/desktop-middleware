package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/consumer_models"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"desktop-middleware/models"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	mongo_v2 "desktop-middleware/database/mongo_v2"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	fmt.Println("Consumer started!")
	topic := viper.GetString("kafka.database") + ".dbmediweb.historia_ocupacional"
	fmt.Println(topic)
	// Create Consumer instance
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": viper.GetString("kafka.bootstrap_servers"),
		"sasl.mechanisms":   viper.GetString("kafka.sasl_mechanisms"),
		"security.protocol": viper.GetString("kafka.security_protocol"),
		"sasl.username":     viper.GetString("kafka.sasl_username"),
		"sasl.password":     viper.GetString("kafka.sasl_password"),
		"group.id":          viper.GetString("kafka.groupId"),
		"auto.offset.reset": "earliest",
	})
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	err = c.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	// Set up a channel for handling Ctrl-C, etc
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	// Process messages
	run := true
	flag := true
	var workHistorys []*consumer_models.WorkHistoryConsumer
	db := database.InitDatabases()
	defer db.DisconnectDatabases()
	for run {
		var logModel models.Log
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			if flag {
				msg, err := c.ReadMessage(100 * time.Millisecond)
				if err != nil {
					// Errors are informational and automatically handled by the consumer
					flag = false
					continue
				}
				if msg.Value == nil || len(msg.Value) == 0 {
					continue
				}
				var workHistory consumer_models.WorkHistoryConsumer
				logModel.Collection = "middleware.transactions.attentions.medicalhistories"
				logModel.Status = "In Progress"
				logModel.Body = msg.Value
				log, _ := mongo.GetLogsCollection().AddLogs(context.TODO(), &logModel)
				err = json.Unmarshal(msg.Value, &workHistory)
				if err != nil {
					logModel.Operation = "Decode"
					logModel.Status = "Error"
					logModel.Collection = "middleware.transactions.attentions.medicalhistories"
					logModel.Error = fmt.Sprintf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					json.Unmarshal(msg.Value, &logModel.Body)
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), log.ID, &logModel)
					fmt.Printf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					flag = false
					continue
				}
				workHistory.IdLog = log.ID
				workHistorys = append(workHistorys, &workHistory)
				if len(workHistorys) > viper.GetInt("kafka.fetchCount") {
					flag = false
				}
			} else {
				if len(workHistorys) > 0 {
					numCPU := viper.GetInt("kafka.threadCount")
					length := len(workHistorys)
					wg := sync.WaitGroup{}
					chunkSize := (length + numCPU - 1) / numCPU

					for i := 0; i < length; i += chunkSize {

						wg.Add(1)
						go func(i int, chunkSize int) {
							end := i + chunkSize
							defer wg.Done()

							if end > length {
								end = length
							}
							slice := workHistorys[i:end]

							for _, workHistory := range slice {
								AddWorkHistoryConsumerV2(workHistory, logModel)
							}
						}(i, chunkSize)
					}
					wg.Wait()
					workHistorys = []*consumer_models.WorkHistoryConsumer{}
				}
				flag = true
			}
		}
	}

	fmt.Printf("Closing consumer\n")
	c.Close()
}

func AddWorkHistoryConsumerV2(workHistory *consumer_models.WorkHistoryConsumer, logModel models.Log) {

	flag, err := strconv.ParseBool(workHistory.IsDeleted)
	if err != nil {
		logModel.Operation = "Decode"
		logModel.Status = "Error"
		logModel.Collection = "middleware.transactions.attentions.medicalhistories"
		logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
		logModel.Body = workHistory
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), workHistory.IdLog, &logModel)
		fmt.Printf("Failed to decode JSON: %v", err)
		return
	}

	if flag || workHistory.Status == "0" {
		_, err := mongo_v2.GetAttentionCollection().UnsetFieldByExternalId(context.TODO(), strconv.Itoa(workHistory.AttentionId), "workHistories")
		if err != nil {
			logModel.Operation = "Delete"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.medicalhistories"
			logModel.Error = fmt.Sprintf("Failed to delete workHistory: %v", err)
			logModel.Body = workHistory
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), workHistory.IdLog, &logModel)
			fmt.Printf("Failed to delete workHistory: %v", err)
		} else {
			logModel.Operation = "Delete"
			logModel.Status = "Done"
			logModel.Collection = "middleware.transactions.attentions.medicalhistories"
			logModel.Error = ""
			logModel.Body = workHistory
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), workHistory.IdLog, &logModel)
		}
	} else {
		mongo_v2.GetAttentionCollection().UnsetFieldByExternalId(context.TODO(), strconv.Itoa(workHistory.AttentionId), "workHistories")
		atte, err := mongo_v2.GetAttentionCollection().GetAttentionByExternalId(context.Background(), strconv.Itoa(workHistory.AttentionId))
		if err != nil {
			logModel.Operation = "Get"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions"
			logModel.Error = fmt.Sprintf("Failed to get JSON: %v", err)
			logModel.Body = workHistory
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), workHistory.IdLog, &logModel)
			fmt.Printf("Failed to get workHistory: %v", err)
		}
		if atte != nil {
			personId := ""
			if atte.PersonId != nil {
				personId = *atte.PersonId
			}
			workHistories := workHistory.SetWorkHistories(atte.ID, personId, *atte.PatientId)
			if len(workHistories) > 0 {
				_, err := mongo_v2.GetAttentionCollection().SetArrayFieldByExternalId(context.Background(), strconv.Itoa(workHistory.AttentionId), "workHistories", workHistories)
				if err != nil {
					logModel.Operation = "Insert"
					logModel.Status = "Error"
					logModel.Collection = "middleware.transactions.attentions.medicalhistories"
					logModel.Error = fmt.Sprintf("Failed to add WorkHistoryInjuries: %v", err)
					logModel.Body = workHistory
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), workHistory.IdLog, &logModel)
					fmt.Printf("Failed to add WorkHistoryInjuries: %v", err)
					return
				} else {
					logModel.Operation = "Insert"
					logModel.Status = "Done"
					logModel.Collection = "middleware.transactions.attentions.medicalhistories"
					logModel.Error = ""
					logModel.Body = workHistory
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), workHistory.IdLog, &logModel)
				}
				fmt.Println("created: ", workHistory.ID)
			}
			if atte.PersonId != nil && atte.AttentionTypeName != nil && atte.CustomerId != nil {
				if *atte.AttentionTypeName == "Ocupacional" {
					lastEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *atte.PersonId, *atte.CustomerId, "emo")
					if lastEmo != nil {
						mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *atte.PersonId, "lastEmoAttention", lastEmo)
					}
					lastAnyEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *atte.PersonId, *atte.CustomerId, "any")
					if lastAnyEmo != nil {
						lastAnyEmo.Customer = nil
						lastAnyEmo.Patient = nil
						lastAnyEmo.Person = nil
						lastAnyEmo.Consultations = nil
						lastAnyEmo.AttentionLines = nil
						lastAnyEmo.Exams = nil
						lastAnyEmo.AudiometryTests = nil
						lastAnyEmo.BiochemistryTests = nil
						lastAnyEmo.CovidTests = nil
						lastAnyEmo.Diagnostics = nil
						lastAnyEmo.DrugTests = nil
						lastAnyEmo.HematologyTests = nil
						lastAnyEmo.ImmunologyTests = nil
						lastAnyEmo.MicrobiologyTests = nil
						lastAnyEmo.OphtalmologyTests = nil
						lastAnyEmo.PsychologyTests = nil
						lastAnyEmo.Recommendations = nil
						lastAnyEmo.ExamResults = nil
						lastAnyEmo.SkeletalMuscleTests = nil
						lastAnyEmo.UrineTests = nil
						lastAnyEmo.MedicalHistories = nil
						lastAnyEmo.PathologicalHistories = nil
						lastAnyEmo.Triages = nil
						lastAnyEmo.WorkHistories = nil
						lastAnyEmo.SpirometryTests = nil
						mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *atte.PersonId, "lastAnyEmoAttention", lastAnyEmo)
					}
				}
				if *atte.AttentionTypeName == "Asistencial" {
					lastNoEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *atte.PersonId, *atte.CustomerId, "no_emo")
					if lastNoEmo != nil {
						mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *atte.PersonId, "lastNoEmoAttention", lastNoEmo)
					}
				}
			}

		}
	}
}
