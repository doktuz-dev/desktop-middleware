package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/consumer_models"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	mongo_v2 "desktop-middleware/database/mongo_v2"
	"desktop-middleware/functions_v2"
	"desktop-middleware/models"
	"desktop-middleware/models_v2"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func main() {
	config.SetConfiguration()
	fmt.Println("Consumer started!")
	topic := viper.GetString("kafka.database") + ".dbmediweb.mesqueletica"
	fmt.Println(topic)
	// Create Consumer instance
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": viper.GetString("kafka.bootstrap_servers"),
		"sasl.mechanisms":   viper.GetString("kafka.sasl_mechanisms"),
		"security.protocol": viper.GetString("kafka.security_protocol"),
		"sasl.username":     viper.GetString("kafka.sasl_username"),
		"sasl.password":     viper.GetString("kafka.sasl_password"),
		"group.id":          viper.GetString("kafka.groupId"),
		"auto.offset.reset": "earliest",
	})
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	err = c.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	// Set up a channel for handling Ctrl-C, etc
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	// Process messages
	run := true
	flag := true
	var skeletalMuscles []*consumer_models.SkeletalMuscleConsumer
	db := database.InitDatabases()
	defer db.DisconnectDatabases()
	for run {
		var logModel models.Log
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			if flag {
				msg, err := c.ReadMessage(100 * time.Millisecond)
				if err != nil {
					// Errors are informational and automatically handled by the consumer
					flag = false
					continue
				}
				if msg.Value == nil || len(msg.Value) == 0 {
					continue
				}
				var skeletalMuscle consumer_models.SkeletalMuscleConsumer
				logModel.Collection = "middleware.transactions.attentions.exams"
				logModel.Status = "In Progress"
				logModel.Body = msg.Value
				log, _ := mongo.GetLogsCollection().AddLogs(context.TODO(), &logModel)
				err = json.Unmarshal(msg.Value, &skeletalMuscle)
				if err != nil {
					logModel.Operation = "Decode"
					logModel.Status = "Error"
					logModel.Collection = "middleware.transactions.attentions.exams"
					logModel.Error = fmt.Sprintf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					json.Unmarshal(msg.Value, &logModel.Body)
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), log.ID, &logModel)
					fmt.Printf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					flag = false
					continue
				}
				skeletalMuscle.IdLog = log.ID
				skeletalMuscles = append(skeletalMuscles, &skeletalMuscle)
				if len(skeletalMuscles) > viper.GetInt("kafka.fetchCount") {
					flag = false
				}
			} else {
				if len(skeletalMuscles) > 0 {
					numCPU := viper.GetInt("kafka.threadCount")
					length := len(skeletalMuscles)
					wg := sync.WaitGroup{}
					chunkSize := (length + numCPU - 1) / numCPU

					for i := 0; i < length; i += chunkSize {

						wg.Add(1)
						go func(i int, chunkSize int) {
							end := i + chunkSize
							defer wg.Done()

							if end > length {
								end = length
							}
							slice := skeletalMuscles[i:end]

							for _, skeletalMuscle := range slice {
								AddSkeletalMuscleConsumerV2(skeletalMuscle, logModel)
							}
						}(i, chunkSize)
					}
					wg.Wait()
					skeletalMuscles = []*consumer_models.SkeletalMuscleConsumer{}
				}
				flag = true
			}
		}
	}

	fmt.Printf("Closing consumer\n")
	c.Close()
}

func AddSkeletalMuscleConsumerV2(skeletalMuscle *consumer_models.SkeletalMuscleConsumer, logModel models.Log) {

	flag, err := strconv.ParseBool(skeletalMuscle.IsDeleted)
	if err != nil {
		logModel.Operation = "Decode"
		logModel.Status = "Error"
		logModel.Collection = "middleware.transactions.attentions.exams"
		logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
		logModel.Body = skeletalMuscle
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), skeletalMuscle.IdLog, &logModel)
		fmt.Printf("Failed to decode JSON: %v", err)
		return
	}
	serv, err := mongo_v2.GetServiceAreaCollection().GetServiceAreaByExternalId(context.Background(), "18")
	if err != nil {
		logModel.Operation = "Get"
		logModel.Status = "Error"
		logModel.Collection = "middleware.registers.serviceareas"
		logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
		logModel.Body = skeletalMuscle
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), skeletalMuscle.IdLog, &logModel)
		fmt.Printf("Failed to decode JSON: %v", err)
		return
	}
	atte, err := mongo_v2.GetAttentionCollection().GetAttentionByExternalId(context.Background(), strconv.Itoa(skeletalMuscle.AttentionId))
	if err != nil {
		logModel.Operation = "Get"
		logModel.Status = "Error"
		logModel.Collection = "middleware.transactions.attentions"
		logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
		logModel.Body = skeletalMuscle
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), skeletalMuscle.IdLog, &logModel)
		fmt.Printf("Failed to decode JSON: %v", err)
		return
	}
	if atte == nil {
		return
	}
	attentionId := atte.ID

	if flag || skeletalMuscle.Status == 0 {
		_, err := mongo_v2.GetAttentionCollection().PullBodyFromExamsByExternalId(context.TODO(), attentionId, strconv.Itoa(skeletalMuscle.ID), serv.ID)
		if err != nil {
			logModel.Operation = "Delete"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.exams"
			logModel.Error = fmt.Sprintf("Failed to delete skeletalMuscle: %v", err)
			logModel.Body = skeletalMuscle
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), skeletalMuscle.IdLog, &logModel)
			fmt.Printf("Failed to delete skeletalMuscle: %v", err)
		} else {
			logModel.Operation = "Delete"
			logModel.Status = "Done"
			logModel.Collection = "middleware.transactions.attentions.exams"
			logModel.Error = ""
			logModel.Body = skeletalMuscle
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), skeletalMuscle.IdLog, &logModel)
		}
		_, err = mongo_v2.GetAttentionCollection().UnsetFieldByExternalId(context.TODO(), strconv.Itoa(skeletalMuscle.AttentionId), "skeletalmuscleTests")
		if err != nil {
			logModel.Operation = "Delete"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.exams.skeletalmuscletests"
			logModel.Error = fmt.Sprintf("Failed to delete skeletalMuscle: %v", err)
			logModel.Body = skeletalMuscle
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), skeletalMuscle.IdLog, &logModel)
			fmt.Printf("Failed to delete skeletalMuscle: %v", err)
		} else {
			logModel.Operation = "Delete"
			logModel.Collection = "middleware.transactions.attentions.exams.skeletalmuscletests"
			logModel.Status = "Done"
			logModel.Body = skeletalMuscle
			logModel.Error = ""
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), skeletalMuscle.IdLog, &logModel)

		}

		mongo_v2.GetAttentionCollection().PullDiagnosticsByExternalId(context.Background(), attentionId, serv.ID)
	} else {

		var oldskeletalMuscle *models_v2.Exam

		for _, body := range atte.Exams {
			if body.ExternalId == strconv.Itoa(skeletalMuscle.ID) && body.ServiceAreaID == serv.ID {
				oldskeletalMuscle = body
			}
		}

		if oldskeletalMuscle != nil {
			CreateSkeletalmuscle(atte, serv, attentionId, skeletalMuscle, oldskeletalMuscle, logModel)
			fmt.Println("updated: ", skeletalMuscle.ID)

		} else {
			var newSkeletalMuscle models_v2.Exam
			CreateSkeletalmuscle(atte, serv, attentionId, skeletalMuscle, &newSkeletalMuscle, logModel)
			fmt.Println("created: ", newSkeletalMuscle.ID)
		}
	}
	if atte.PersonId != nil && atte.AttentionTypeName != nil && atte.CustomerId != nil {
		if *atte.AttentionTypeName == "Ocupacional" {
			lastEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *atte.PersonId, *atte.CustomerId, "emo")
			if lastEmo != nil {
				mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *atte.PersonId, "lastEmoAttention", lastEmo)
			}
			lastAnyEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *atte.PersonId, *atte.CustomerId, "any")
			if lastAnyEmo != nil {
				lastAnyEmo.Customer = nil
				lastAnyEmo.Patient = nil
				lastAnyEmo.Person = nil
				lastAnyEmo.Consultations = nil
				lastAnyEmo.AttentionLines = nil
				lastAnyEmo.Exams = nil
				lastAnyEmo.AudiometryTests = nil
				lastAnyEmo.BiochemistryTests = nil
				lastAnyEmo.CovidTests = nil
				lastAnyEmo.Diagnostics = nil
				lastAnyEmo.DrugTests = nil
				lastAnyEmo.HematologyTests = nil
				lastAnyEmo.ImmunologyTests = nil
				lastAnyEmo.MicrobiologyTests = nil
				lastAnyEmo.OphtalmologyTests = nil
				lastAnyEmo.PsychologyTests = nil
				lastAnyEmo.Recommendations = nil
				lastAnyEmo.ExamResults = nil
				lastAnyEmo.SkeletalMuscleTests = nil
				lastAnyEmo.UrineTests = nil
				lastAnyEmo.MedicalHistories = nil
				lastAnyEmo.PathologicalHistories = nil
				lastAnyEmo.Triages = nil
				lastAnyEmo.WorkHistories = nil
				lastAnyEmo.SpirometryTests = nil
				mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *atte.PersonId, "lastAnyEmoAttention", lastAnyEmo)
			}
		}
		if *atte.AttentionTypeName == "Asistencial" {
			lastNoEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *atte.PersonId, *atte.CustomerId, "no_emo")
			if lastNoEmo != nil {
				mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *atte.PersonId, "lastNoEmoAttention", lastNoEmo)
			}
		}
	}
}

func CreateSkeletalmuscle(atte *models_v2.Attention, serv *models_v2.ServiceArea, attentionId string, skeletalMuscle *consumer_models.SkeletalMuscleConsumer, exam *models_v2.Exam, logModel models.Log) {
	if exam.ID == "" {
		exam.ID = primitive.NewObjectID().Hex()
		exam.ExternalId = strconv.Itoa(skeletalMuscle.ID)
		exam.ExternalSystem = "MEDIWEB"
		exam.CreatedDate = time.Now()
		exam.LastUpdatedDate = time.Now()
	}
	exam.AttentionID = attentionId
	exam.ServiceAreaID = serv.ID
	exam.ServiceAreaCode = serv.Code
	exam.ServiceArea = serv
	exam.Status = skeletalMuscle.Status
	exam.LastUpdatedDate = time.Now()

	mongo_v2.GetAttentionCollection().PullBodyFromExamsByExternalId(context.TODO(), attentionId, strconv.Itoa(skeletalMuscle.ID), serv.ID)
	_, err := mongo_v2.GetAttentionCollection().PushBodyToExamsByExternalId(context.TODO(), attentionId, exam)
	if err != nil {
		logModel.Operation = "Update"
		logModel.Status = "Error"
		logModel.Collection = "middleware.transactions.attentions.exams"
		logModel.Error = fmt.Sprintf("Failed to update skeletalMuscle: %v", err)
		logModel.Body = skeletalMuscle
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), skeletalMuscle.IdLog, &logModel)
		fmt.Printf("Failed to update skeletalMuscle: %v", err)
	} else {
		logModel.Operation = "Update"
		logModel.Status = "Done"
		logModel.Collection = "middleware.transactions.attentions.exams"
		logModel.Error = ""
		logModel.Body = skeletalMuscle
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), skeletalMuscle.IdLog, &logModel)
	}

	mongo_v2.GetAttentionCollection().PullDiagnosticsByExternalId(context.Background(), attentionId, serv.ID)

	diagnostics := functions_v2.GetDiagnosticsFromSkeletalMuscleConsumer(exam.ID, exam.AttentionID, exam.ServiceAreaID, exam.ServiceAreaCode, skeletalMuscle, serv)
	if len(diagnostics) > 0 {
		_, err := mongo_v2.GetAttentionCollection().PushDiagnosticsByExternalId(context.Background(), attentionId, diagnostics)
		if err != nil {
			logModel.Operation = "Insert"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.exams.diagnostics"
			logModel.Error = fmt.Sprintf("Failed to insert skeletalMuscle: %v", err)
			logModel.Body = skeletalMuscle
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), skeletalMuscle.IdLog, &logModel)
			fmt.Printf("Failed to add skeletalMuscle: %v", err)
		} else {
			logModel.Operation = "Insert"
			logModel.Status = "Done"
			logModel.Body = skeletalMuscle
			logModel.Collection = "middleware.transactions.attentions.exams.diagnostics"
			logModel.Error = ""
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), skeletalMuscle.IdLog, &logModel)

		}
	}
	newTest := skeletalMuscle.SetSkeletalMuscleTest()
	newTest.AttentionID = attentionId
	newTest.ServiceAreaID = exam.ServiceAreaID
	newTest.ServiceAreaCode = exam.ServiceAreaCode
	newTest.ExamID = exam.ID

	test := atte.SkeletalMuscleTests

	if test != nil {
		newTest.ID = test.ID
		newTest.CreatedDate = test.CreatedDate
		newTest.LastUpdatedDate = time.Now()
	} else {
		newTest.ID = primitive.NewObjectID().Hex()
		newTest.Status = exam.Status
		newTest.ExternalId = strconv.Itoa(skeletalMuscle.ID)
		newTest.ExternalSystem = "MEDIWEB"
		newTest.CreatedDate = time.Now()
		newTest.LastUpdatedDate = time.Now()
	}
	_, err = mongo_v2.GetAttentionCollection().SetFieldByID(context.Background(), attentionId, "skeletalmuscleTests", newTest)
	if err != nil {
		logModel.Operation = "Update"
		logModel.Status = "Error"
		logModel.Collection = "middleware.transactions.attentions.exams.skeletalMuscletests"
		logModel.Error = fmt.Sprintf("Failed to add skeletalMuscle: %v", err)
		logModel.Body = skeletalMuscle
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), skeletalMuscle.IdLog, &logModel)
		fmt.Printf("Failed to add skeletalMuscle: %v", err)
	} else {
		logModel.Operation = "Update"
		logModel.Status = "Done"
		logModel.Collection = "middleware.transactions.attentions.exams.skeletalMuscletests"
		logModel.Error = ""
		logModel.Body = skeletalMuscle
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), skeletalMuscle.IdLog, &logModel)
	}
}
