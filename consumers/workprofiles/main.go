package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/consumer_models"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	mongo_v2 "desktop-middleware/database/mongo_v2"
	"desktop-middleware/models"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	fmt.Println("Consumer started!")
	topic := viper.GetString("kafka.database") + ".dbmediweb.obra"
	fmt.Println(topic)
	// Create Consumer instance
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": viper.GetString("kafka.bootstrap_servers"),
		"sasl.mechanisms":   viper.GetString("kafka.sasl_mechanisms"),
		"security.protocol": viper.GetString("kafka.security_protocol"),
		"sasl.username":     viper.GetString("kafka.sasl_username"),
		"sasl.password":     viper.GetString("kafka.sasl_password"),
		"group.id":          viper.GetString("kafka.groupId"),
		"auto.offset.reset": "earliest",
	})
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	err = c.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	// Set up a channel for handling Ctrl-C, etc
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	// Process messages
	run := true
	flag := true
	var workProfiles []*consumer_models.WorkProfileConsumer
	db := database.InitDatabases()
	defer db.DisconnectDatabases()
	for run {
		var logModel models.Log
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			if flag {
				msg, err := c.ReadMessage(100 * time.Millisecond)
				if err != nil {
					// Errors are informational and automatically handled by the consumer
					flag = false
					continue
				}
				if msg.Value == nil || len(msg.Value) == 0 {
					continue
				}
				var workProfile consumer_models.WorkProfileConsumer
				logModel.Collection = "middleware.registers.workprofiles"
				logModel.Status = "In Progress"
				logModel.Body = msg.Value
				log, _ := mongo.GetLogsCollection().AddLogs(context.TODO(), &logModel)
				err = json.Unmarshal(msg.Value, &workProfile)
				if err != nil {
					logModel.Operation = "Decode"
					logModel.Status = "Error"
					logModel.Collection = "middleware.registers.workprofiles"
					logModel.Error = fmt.Sprintf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					json.Unmarshal(msg.Value, &logModel.Body)
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), log.ID, &logModel)
					fmt.Printf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					flag = false
					continue
				}
				workProfile.IdLog = log.ID
				workProfiles = append(workProfiles, &workProfile)
				if len(workProfiles) > viper.GetInt("kafka.fetchCount") {
					flag = false
				}
			} else {
				if len(workProfiles) > 0 {
					numCPU := viper.GetInt("kafka.threadCount")
					length := len(workProfiles)
					wg := sync.WaitGroup{}
					chunkSize := (length + numCPU - 1) / numCPU

					for i := 0; i < length; i += chunkSize {

						wg.Add(1)
						go func(i int, chunkSize int) {
							end := i + chunkSize
							defer wg.Done()

							if end > length {
								end = length
							}
							slice := workProfiles[i:end]

							for _, workProfile := range slice {

								flag, err := strconv.ParseBool(workProfile.IsDeleted)
								if err != nil {
									logModel.Operation = "Decode"
									logModel.Status = "Error"
									logModel.Collection = "middleware.registers.workprofiles"
									logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
									logModel.Body = workProfile
									mongo.GetLogsCollection().UpdateLogs(context.TODO(), workProfile.IdLog, &logModel)
									fmt.Printf("Failed to decode JSON: %v", err)
									continue
								}

								if flag {
									_, err := mongo.GetWorkProfileCollection().DeleteWorkProfileByExternalId(context.TODO(), strconv.Itoa(workProfile.ID))
									if err != nil {
										logModel.Operation = "Delete"
										logModel.Status = "Error"
										logModel.Collection = "middleware.registers.workprofiles"
										logModel.Error = fmt.Sprintf("Failed to delete workProfile: %v", err)
										logModel.Body = workProfile
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), workProfile.IdLog, &logModel)
										fmt.Printf("Failed to delete workProfile: %v", err)
									} else {
										logModel.Operation = "Delete"
										logModel.Status = "Done"
										logModel.Collection = "middleware.registers.workprofiles"
										logModel.Error = ""
										logModel.Body = workProfile
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), workProfile.IdLog, &logModel)
									}
								} else {
									oldworkProfile, _ := mongo.GetWorkProfileCollection().GetWorkProfileById(context.TODO(), strconv.Itoa(workProfile.ID))

									if oldworkProfile != nil {
										oldworkProfile.Name = workProfile.Name
										if workProfile.Blocked == "si" {
											oldworkProfile.Blocked = 1
										} else {
											oldworkProfile.Blocked = 0
										}
										_, err := mongo.GetWorkProfileCollection().UpdateObra(context.TODO(), oldworkProfile.ID, oldworkProfile)
										if err != nil {
											logModel.Operation = "Update"
											logModel.Status = "Error"
											logModel.Collection = "middleware.registers.workprofiles"
											logModel.Error = fmt.Sprintf("Failed to update JSON: %v", err)
											logModel.Body = workProfile
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), workProfile.IdLog, &logModel)
											fmt.Printf("Failed to update workProfile: %v", err)
											continue
										} else {
											logModel.Operation = "Update"
											logModel.Status = "Done"
											logModel.Collection = "middleware.registers.workprofiles"
											logModel.Error = ""
											logModel.Body = workProfile
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), workProfile.IdLog, &logModel)
										}
										go mongo_v2.GetAttentionCollection().CustomSetFieldById(context.Background(), "workProfileId", oldworkProfile.ID, "workProfile", oldworkProfile)
										fmt.Println("updated: ", oldworkProfile.ID)
									} else {
										newService := models.WorkProfile{
											Name:       workProfile.Name,
											ExternalId: strconv.Itoa(workProfile.ID),
										}
										if workProfile.Blocked == "si" {
											newService.Blocked = 1
										} else {
											newService.Blocked = 0
										}
										val, err := mongo.GetWorkProfileCollection().AddWorkProfile(context.TODO(), &newService)
										if err != nil {
											logModel.Operation = "Insert"
											logModel.Status = "Error"
											logModel.Collection = "middleware.registers.workprofiles"
											logModel.Error = fmt.Sprintf("Failed to add JSON: %v", err)
											logModel.Body = workProfile
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), workProfile.IdLog, &logModel)
											fmt.Printf("Failed to add workProfile: %v", err)
											continue
										} else {
											logModel.Operation = "Insert"
											logModel.Status = "Done"
											logModel.Collection = "middleware.registers.workprofiles"
											logModel.Error = ""
											logModel.Body = workProfile
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), workProfile.IdLog, &logModel)
										}
										fmt.Println("created: ", val.ID)
									}
								}
							}
						}(i, chunkSize)
					}
					wg.Wait()
					workProfiles = []*consumer_models.WorkProfileConsumer{}
				}
				flag = true
			}
		}
	}

	fmt.Printf("Closing consumer\n")
	c.Close()
}
