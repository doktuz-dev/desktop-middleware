package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/consumer_models"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"desktop-middleware/models"
	"desktop-middleware/models_v2"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	mongo_v2 "desktop-middleware/database/mongo_v2"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func main() {
	config.SetConfiguration()
	fmt.Println("Consumer started!")
	topic := viper.GetString("kafka.database") + ".dbmediweb.det_interconsulta"
	fmt.Println(topic)
	// Create Consumer instance
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": viper.GetString("kafka.bootstrap_servers"),
		"sasl.mechanisms":   viper.GetString("kafka.sasl_mechanisms"),
		"security.protocol": viper.GetString("kafka.security_protocol"),
		"sasl.username":     viper.GetString("kafka.sasl_username"),
		"sasl.password":     viper.GetString("kafka.sasl_password"),
		"group.id":          viper.GetString("kafka.groupId"),
		"auto.offset.reset": "earliest",
	})
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	err = c.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	// Set up a channel for handling Ctrl-C, etc
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	// Process messages
	run := true
	flag := true
	var consultations []*consumer_models.ConsultationConsumer
	db := database.InitDatabases()
	defer db.DisconnectDatabases()
	for run {
		var logModel models.Log
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			if flag {
				msg, err := c.ReadMessage(100 * time.Millisecond)
				if err != nil {
					// Errors are informational and automatically handled by the consumer
					flag = false
					continue
				}
				if msg.Value == nil || len(msg.Value) == 0 {
					continue
				}
				var consultation consumer_models.ConsultationConsumer
				logModel.Collection = "transactions.attentions.consultations"
				logModel.Status = "In Progress"
				logModel.Body = msg.Value
				log, _ := mongo.GetLogsCollection().AddLogs(context.TODO(), &logModel)
				err = json.Unmarshal(msg.Value, &consultation)
				if err != nil {
					logModel.Operation = "Decode"
					logModel.Status = "Error"
					logModel.Collection = "transactions.attentions.consultations"
					logModel.Error = fmt.Sprintf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					json.Unmarshal(msg.Value, &logModel.Body)
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), log.ID, &logModel)
					fmt.Printf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					flag = false
					continue
				}
				consultation.IdLog = log.ID
				consultations = append(consultations, &consultation)
				if len(consultations) > viper.GetInt("kafka.fetchCount") {
					flag = false
				}
			} else {
				if len(consultations) > 0 {
					numCPU := viper.GetInt("kafka.threadCount")
					length := len(consultations)
					wg := sync.WaitGroup{}
					chunkSize := (length + numCPU - 1) / numCPU

					for i := 0; i < length; i += chunkSize {

						wg.Add(1)
						go func(i int, chunkSize int) {
							end := i + chunkSize
							defer wg.Done()

							if end > length {
								end = length
							}
							slice := consultations[i:end]

							for _, consultation := range slice {
								AddConsultationConsumerV2(consultation, logModel)
							}
						}(i, chunkSize)
					}
					wg.Wait()
					consultations = []*consumer_models.ConsultationConsumer{}
				}
				flag = true
			}
		}
	}

	fmt.Printf("Closing consumer\n")
	c.Close()
}

func AddConsultationConsumerV2(consultation *consumer_models.ConsultationConsumer, logModel models.Log) {

	flag, err := strconv.ParseBool(consultation.IsDeleted)
	if err != nil {
		logModel.Operation = "Decode"
		logModel.Status = "Error"
		logModel.Collection = "transactions.attentions.consultations"
		logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
		logModel.Body = consultation
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), consultation.IdLog, &logModel)
		fmt.Printf("Failed to decode JSON: %v", err)
		return
	}

	if flag || consultation.Status == "0" {
		_, err = mongo_v2.GetAttentionCollection().PullFieldByExternalId(context.TODO(), strconv.Itoa(consultation.AttentionId), "consultation", strconv.Itoa(consultation.ID))
		if err != nil {
			logModel.Operation = "Delete"
			logModel.Status = "Error"
			logModel.Collection = "transactions.attentions.consultations"
			logModel.Error = fmt.Sprintf("Failed to delete consultation: %v", err)
			logModel.Body = consultation
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), consultation.IdLog, &logModel)
			fmt.Printf("Failed to delete consultation: %v", err)
		} else {
			logModel.Operation = "Delete"
			logModel.Status = "Done"
			logModel.Collection = "transactions.attentions.consultations"
			logModel.Error = ""
			logModel.Body = consultation
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), consultation.IdLog, &logModel)

		}
	} else {
		attention, err := mongo_v2.GetAttentionCollection().GetAttentionByExternalId(context.TODO(), strconv.Itoa(consultation.AttentionId))
		if err != nil {
			logModel.Operation = "Get"
			logModel.Status = "Error"
			logModel.Error = fmt.Sprintf("Failed to get consultation: %v", err)
			logModel.Body = consultation
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), consultation.IdLog, &logModel)
			fmt.Printf("Failed to get consultation: %v", err)
		}

		if attention == nil {
			return
		}
		attentionId := attention.ID
		spe, _ := mongo_v2.GetSpecialityCollection().GetSpecialityByExternalId(context.Background(), strconv.Itoa(consultation.SpecialityID))

		var oldconsultation *models_v2.Consultation

		for _, body := range attention.Consultations {
			if body.ExternalId == strconv.Itoa(consultation.ID) {
				oldconsultation = body
			}
		}

		if oldconsultation != nil {
			CreateConsultation(attention, spe, attentionId, consultation, oldconsultation, logModel)
			fmt.Println("updated: ", consultation.ID)
		} else {
			var newConsultation models_v2.Consultation
			CreateConsultation(attention, spe, attentionId, consultation, &newConsultation, logModel)
			fmt.Println("created: ", consultation.ID)
		}
		if attention.PersonId != nil && attention.CustomerId != nil {
			if *attention.AttentionTypeName == "Ocupacional" {
				lastEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *attention.PersonId, *attention.CustomerId, "emo")
				if lastEmo != nil {
					mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *attention.PersonId, "lastEmoAttention", lastEmo)
				}
				lastAnyEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *attention.PersonId, *attention.CustomerId, "any")
				if lastAnyEmo != nil {
					lastAnyEmo.Customer = nil
					lastAnyEmo.Patient = nil
					lastAnyEmo.Person = nil
					lastAnyEmo.Consultations = nil
					lastAnyEmo.AttentionLines = nil
					lastAnyEmo.Exams = nil
					lastAnyEmo.AudiometryTests = nil
					lastAnyEmo.BiochemistryTests = nil
					lastAnyEmo.CovidTests = nil
					lastAnyEmo.Diagnostics = nil
					lastAnyEmo.DrugTests = nil
					lastAnyEmo.HematologyTests = nil
					lastAnyEmo.ImmunologyTests = nil
					lastAnyEmo.MicrobiologyTests = nil
					lastAnyEmo.OphtalmologyTests = nil
					lastAnyEmo.PsychologyTests = nil
					lastAnyEmo.Recommendations = nil
					lastAnyEmo.ExamResults = nil
					lastAnyEmo.SkeletalMuscleTests = nil
					lastAnyEmo.UrineTests = nil
					lastAnyEmo.MedicalHistories = nil
					lastAnyEmo.PathologicalHistories = nil
					lastAnyEmo.Triages = nil
					lastAnyEmo.WorkHistories = nil
					lastAnyEmo.SpirometryTests = nil
					mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *attention.PersonId, "lastAnyEmoAttention", lastAnyEmo)
				}
			}
			if *attention.AttentionTypeName == "Asistencial" {
				lastNoEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *attention.PersonId, *attention.CustomerId, "no_emo")
				if lastNoEmo != nil {
					mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *attention.PersonId, "lastNoEmoAttention", lastNoEmo)
				}
			}
		}
	}
}

func CreateConsultation(atte *models_v2.Attention, spe *models_v2.Speciality, attentionId string, consumer *consumer_models.ConsultationConsumer, consultation *models_v2.Consultation, logModel models.Log) {
	if consultation.ID == "" {
		consultation.ID = primitive.NewObjectID().Hex()
		consultation.ExternalId = strconv.Itoa(consumer.ID)
		consultation.ExternalSystem = "MEDIWEB"
		consultation.CreatedDate = time.Now()
		consultation.LastUpdatedDate = time.Now()
	}
	consultation.AttentionId = atte.ID
	if atte.PersonId != nil {
		consultation.PersonID = *atte.PersonId
	}
	if spe != nil {
		consultation.SpecialityID = spe.ID
	}
	consultation.PrintId = strconv.Itoa(consumer.ID)
	if consumer.Replied == "1" {
		consultation.Replied = true
	} else {
		consultation.Replied = false
	}
	status, _ := strconv.Atoi(consumer.Status)
	consultation.Status = status
	mongo_v2.GetAttentionCollection().PullFieldByExternalId(context.TODO(), strconv.Itoa(consumer.AttentionId), "consultation", strconv.Itoa(consumer.ID))
	_, err := mongo_v2.GetAttentionCollection().PushFieldByID(context.TODO(), attentionId, "consultation", consultation)
	if err != nil {
		logModel.Operation = "Insert"
		logModel.Status = "Error"
		logModel.Error = fmt.Sprintf("Failed to insert consultations: %v", err)
		logModel.Body = consumer
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), consumer.IdLog, &logModel)
		fmt.Printf("Failed to add consultations: %v", err)
	} else {
		logModel.Operation = "Insert"
		logModel.Status = "Done"
		logModel.Body = consumer
		logModel.Error = ""
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), consumer.IdLog, &logModel)

	}
}
