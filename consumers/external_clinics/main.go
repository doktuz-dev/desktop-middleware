package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/consumer_models"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"desktop-middleware/models"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	mongo_v2 "desktop-middleware/database/mongo_v2"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	fmt.Println("Consumer started!")
	topic := viper.GetString("kafka.database") + ".dbmediweb.clinica"
	fmt.Println(topic)
	// Create Consumer instance
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": viper.GetString("kafka.bootstrap_servers"),
		"sasl.mechanisms":   viper.GetString("kafka.sasl_mechanisms"),
		"security.protocol": viper.GetString("kafka.security_protocol"),
		"sasl.username":     viper.GetString("kafka.sasl_username"),
		"sasl.password":     viper.GetString("kafka.sasl_password"),
		"group.id":          viper.GetString("kafka.groupId"),
		"auto.offset.reset": "earliest",
	})
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	err = c.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	// Set up a channel for handling Ctrl-C, etc
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	// Process messages
	run := true
	flag := true
	var externalClinics []*consumer_models.ExternalClinicConsumer
	db := database.InitDatabases()
	defer db.DisconnectDatabases()
	for run {
		var logModel models.Log
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			if flag {
				msg, err := c.ReadMessage(100 * time.Millisecond)
				if err != nil {
					// Errors are informational and automatically handled by the consumer
					flag = false
					continue
				}
				if msg.Value == nil || len(msg.Value) == 0 {
					continue
				}
				var externalClinic consumer_models.ExternalClinicConsumer
				logModel.Collection = "middleware.registers.externalclinics"
				logModel.Status = "In Progress"
				logModel.Body = msg.Value
				log, _ := mongo.GetLogsCollection().AddLogs(context.TODO(), &logModel)
				err = json.Unmarshal(msg.Value, &externalClinic)
				if err != nil {
					logModel.Operation = "Decode"
					logModel.Status = "Error"
					logModel.Collection = "middleware.registers.externalclinics"
					logModel.Error = fmt.Sprintf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					json.Unmarshal(msg.Value, &logModel.Body)
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), log.ID, &logModel)
					fmt.Printf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					flag = false
					continue
				}
				externalClinic.IdLog = log.ID
				externalClinics = append(externalClinics, &externalClinic)

				if len(externalClinics) > viper.GetInt("kafka.fetchCount") {
					flag = false
				}
			} else {
				if len(externalClinics) > 0 {
					numCPU := viper.GetInt("kafka.threadCount")
					length := len(externalClinics)
					wg := sync.WaitGroup{}
					chunkSize := (length + numCPU - 1) / numCPU

					for i := 0; i < length; i += chunkSize {

						wg.Add(1)
						go func(i int, chunkSize int) {
							end := i + chunkSize
							defer wg.Done()

							if end > length {
								end = length
							}
							slice := externalClinics[i:end]

							for _, externalClinic := range slice {

								flag, err := strconv.ParseBool(externalClinic.IsDeleted)
								if err != nil {
									logModel.Operation = "Decode"
									logModel.Status = "Error"
									logModel.Collection = "middleware.registers.externalclinics"
									logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
									logModel.Body = externalClinic
									mongo.GetLogsCollection().UpdateLogs(context.TODO(), externalClinic.IdLog, &logModel)
									fmt.Printf("Failed to decode JSON: %v", err)
									continue
								}

								if flag {
									_, err := mongo.GetExternalClinicCollection().DeleteExternalClinicByExternalId(context.TODO(), externalClinic.ID)
									if err != nil {
										logModel.Operation = "Delete"
										logModel.Status = "Error"
										logModel.Collection = "middleware.registers.externalclinics"
										logModel.Error = fmt.Sprintf("Failed to delete externalClinic: %v", err)
										logModel.Body = externalClinic
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), externalClinic.IdLog, &logModel)
										fmt.Printf("Failed to delete externalClinic: %v", err)
									} else {
										logModel.Operation = "Delete"
										logModel.Status = "Done"
										logModel.Collection = "middleware.registers.externalclinics"
										logModel.Error = ""
										logModel.Body = externalClinic
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), externalClinic.IdLog, &logModel)
									}
								} else {
									oldexternalClinic, err := mongo.GetExternalClinicCollection().GetExternalClinicById(context.TODO(), externalClinic.ID)
									if err != nil {
										logModel.Operation = "Get"
										logModel.Status = "Error"
										logModel.Collection = "middleware.registers.externalclinics"
										logModel.Error = fmt.Sprintf("Failed to get externalClinic: %v", err)
										logModel.Body = externalClinic
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), externalClinic.IdLog, &logModel)
										fmt.Printf("Failed to get externalClinic: %v", err)
									}

									if oldexternalClinic != nil {
										oldexternalClinic.Code = externalClinic.ID
										oldexternalClinic.Name = externalClinic.Name
										oldexternalClinic.Status = externalClinic.Status
										_, err := mongo.GetExternalClinicCollection().UpdateExternalClinic(context.TODO(), oldexternalClinic.ID, oldexternalClinic)
										if err != nil {
											logModel.Operation = "Update"
											logModel.Status = "Error"
											logModel.Collection = "middleware.registers.externalclinics"
											logModel.Error = fmt.Sprintf("Failed to update externalClinic: %v", err)
											logModel.Body = externalClinic
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), externalClinic.IdLog, &logModel)
											fmt.Printf("Failed to update externalClinic: %v", err)
											continue
										} else {
											logModel.Operation = "Update"
											logModel.Status = "Done"
											logModel.Collection = "middleware.registers.externalclinics"
											logModel.Error = ""
											logModel.Body = externalClinic
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), externalClinic.IdLog, &logModel)
										}
										go mongo_v2.GetAttentionCollection().CustomSetFieldById(context.Background(), "externalClinicId", oldexternalClinic.ID, "externalClinic", oldexternalClinic)

										healthcentre, _ := mongo.GetHealthCentreCollection().GetHealthCentreByName(context.TODO(), oldexternalClinic.Name)
										if healthcentre != nil {
											healthcentre.ClinicId = oldexternalClinic.ExternalId
											go mongo.GetHealthCentreCollection().UpdateHealthCentre(context.Background(), healthcentre.ID, healthcentre)
										}
										fmt.Println("updated: ", oldexternalClinic.ID)
									} else {
										newExternalClinic := models.ExternalClinic{
											Code:       externalClinic.ID,
											Name:       externalClinic.Name,
											Status:     externalClinic.Status,
											ExternalId: fmt.Sprintf("%d", externalClinic.ID),
										}
										newHealthCentre := models.HealthCentre{
											Code:       "0",
											Name:       externalClinic.Name,
											ClinicName: externalClinic.Name,
											Status:     externalClinic.Status,
											ExternalId: fmt.Sprintf("%d", externalClinic.ID),
											ClinicId:   fmt.Sprintf("%d", externalClinic.ID),
										}
										val, err := mongo.GetExternalClinicCollection().AddExternalClinic(context.TODO(), &newExternalClinic)
										if err != nil {
											logModel.Operation = "Insert"
											logModel.Status = "Error"
											logModel.Collection = "middleware.registers.externalclinics"
											logModel.Error = fmt.Sprintf("Failed to add externalClinic: %v", err)
											logModel.Body = externalClinic
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), externalClinic.IdLog, &logModel)
											fmt.Printf("Failed to add externalClinic: %v", err)
											continue
										} else {
											logModel.Operation = "Insert"
											logModel.Status = "Done"
											logModel.Collection = "middleware.registers.externalclinics"
											logModel.Error = ""
											logModel.Body = externalClinic
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), externalClinic.IdLog, &logModel)
										}

										_, err = mongo.GetHealthCentreCollection().AddHealthCentre(context.TODO(), &newHealthCentre)
										if err != nil {
											logModel.Operation = "Insert"
											logModel.Status = "Error"
											logModel.Collection = "middleware.registers.externalclinics"
											logModel.Error = fmt.Sprintf("Failed to add externalClinic: %v", err)
											logModel.Body = externalClinic
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), externalClinic.IdLog, &logModel)
											fmt.Printf("Failed to add externalClinic: %v", err)
											continue
										} else {
											logModel.Operation = "Insert"
											logModel.Status = "Done"
											logModel.Collection = "middleware.registers.externalclinics"
											logModel.Error = ""
											logModel.Body = externalClinic
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), externalClinic.IdLog, &logModel)
										}
										fmt.Println("created: ", val.ID)
									}
								}
							}
						}(i, chunkSize)
					}
					wg.Wait()
					externalClinics = []*consumer_models.ExternalClinicConsumer{}
				}
				flag = true
			}
		}
	}

	fmt.Printf("Closing consumer\n")
	c.Close()
}
