package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/consumer_models"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"desktop-middleware/models"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	fmt.Println("Consumer started!")
	topic := viper.GetString("kafka.database") + ".dbmediweb.graldistritos"
	fmt.Println(topic)
	// Create Consumer instance
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": viper.GetString("kafka.bootstrap_servers"),
		"sasl.mechanisms":   viper.GetString("kafka.sasl_mechanisms"),
		"security.protocol": viper.GetString("kafka.security_protocol"),
		"sasl.username":     viper.GetString("kafka.sasl_username"),
		"sasl.password":     viper.GetString("kafka.sasl_password"),
		"group.id":          viper.GetString("kafka.groupId"),
		"auto.offset.reset": "earliest",
	})
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	err = c.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	// Set up a channel for handling Ctrl-C, etc
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	// Process messages
	run := true
	flag := true
	var districts []*consumer_models.DistrictConsumer
	db := database.InitDatabases()
	defer db.DisconnectDatabases()
	for run {
		var logModel models.Log
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			if flag {
				msg, err := c.ReadMessage(100 * time.Millisecond)
				if err != nil {
					// Errors are informational and automatically handled by the consumer
					flag = false
					continue
				}
				if msg.Value == nil || len(msg.Value) == 0 {
					continue
				}
				var district consumer_models.DistrictConsumer
				logModel.Collection = "middleware.registers.districts"
				logModel.Status = "In Progress"
				logModel.Body = msg.Value
				log, _ := mongo.GetLogsCollection().AddLogs(context.TODO(), &logModel)
				err = json.Unmarshal(msg.Value, &district)
				if err != nil {
					logModel.Operation = "Decode"
					logModel.Status = "Error"
					logModel.Collection = "middleware.registers.districts"
					logModel.Error = fmt.Sprintf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					json.Unmarshal(msg.Value, &logModel.Body)
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), log.ID, &logModel)
					fmt.Printf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					flag = false
					continue
				}
				district.IdLog = log.ID
				districts = append(districts, &district)
				if len(districts) > viper.GetInt("kafka.fetchCount") {
					flag = false
				}
			} else {
				if len(districts) > 0 {
					numCPU := viper.GetInt("kafka.threadCount")
					length := len(districts)
					wg := sync.WaitGroup{}
					chunkSize := (length + numCPU - 1) / numCPU

					for i := 0; i < length; i += chunkSize {

						wg.Add(1)
						go func(i int, chunkSize int) {
							end := i + chunkSize
							defer wg.Done()

							if end > length {
								end = length
							}
							slice := districts[i:end]

							for _, district := range slice {

								flag, err := strconv.ParseBool(district.IsDeleted)
								if err != nil {
									logModel.Operation = "Decode"
									logModel.Status = "Error"
									logModel.Collection = "middleware.registers.districts"
									logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
									logModel.Body = district
									mongo.GetLogsCollection().UpdateLogs(context.TODO(), district.IdLog, &logModel)
									fmt.Printf("Failed to decode JSON: %v", err)
									continue
								}

								if flag {
									_, err := mongo.GetDistrictCollection().DeleteDistrictByExternalId(context.TODO(), strconv.Itoa(district.ID))
									if err != nil {
										logModel.Operation = "Delete"
										logModel.Status = "Error"
										logModel.Collection = "middleware.registers.districts"
										logModel.Error = fmt.Sprintf("Failed to delete district: %v", err)
										logModel.Body = district
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), district.IdLog, &logModel)
										fmt.Printf("Failed to delete district: %v", err)
									} else {
										logModel.Operation = "Delete"
										logModel.Status = "Done"
										logModel.Collection = "middleware.registers.districts"
										logModel.Error = ""
										logModel.Body = district
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), district.IdLog, &logModel)
									}
								} else {
									olddistrict, err := mongo.GetDistrictCollection().GetDistrictById(context.TODO(), strconv.Itoa(district.ID))
									if err != nil {
										logModel.Operation = "Get"
										logModel.Status = "Error"
										logModel.Collection = "middleware.registers.districts"
										logModel.Error = fmt.Sprintf("Failed to get attention: %v", err)
										logModel.Body = district
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), district.IdLog, &logModel)
										fmt.Printf("Failed to get district: %v", err)
									}

									if olddistrict != nil {
										department, _ := mongo.GetDepartmentCollection().GetDepartmentById(context.Background(), strconv.Itoa(district.DepartmentID))
										if department != nil {
											olddistrict.DepartmentID = department.ID
										}
										province, _ := mongo.GetProvinceCollection().GetProvinceById(context.Background(), strconv.Itoa(district.ProvinceID))
										if province != nil {
											olddistrict.ProvinceID = province.ID
										}
										olddistrict.Name = district.Name
										_, err := mongo.GetDistrictCollection().UpdateDistrict(context.TODO(), olddistrict.ID, olddistrict)
										if err != nil {
											logModel.Operation = "Update"
											logModel.Status = "Error"
											logModel.Collection = "middleware.registers.districts"
											logModel.Error = fmt.Sprintf("Failed to update attention: %v", err)
											logModel.Body = district
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), district.IdLog, &logModel)
											fmt.Printf("Failed to update district: %v", err)
											continue
										} else {
											logModel.Operation = "Update"
											logModel.Status = "Done"
											logModel.Collection = "middleware.registers.districts"
											logModel.Error = ""
											logModel.Body = district
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), district.IdLog, &logModel)
										}
										fmt.Println("updated: ", olddistrict.ID)
									} else {
										newDistrict := models.District{
											Name:       district.Name,
											ExternalId: strconv.Itoa(district.ID),
										}
										department, _ := mongo.GetDepartmentCollection().GetDepartmentById(context.Background(), strconv.Itoa(district.DepartmentID))
										if department != nil {
											newDistrict.DepartmentID = department.ID
										}
										province, _ := mongo.GetProvinceCollection().GetProvinceById(context.Background(), strconv.Itoa(district.ProvinceID))
										if province != nil {
											newDistrict.ProvinceID = province.ID
										}
										val, err := mongo.GetDistrictCollection().AddDistrict(context.TODO(), &newDistrict)
										if err != nil {
											logModel.Operation = "Insert"
											logModel.Status = "Error"
											logModel.Collection = "middleware.registers.districts"
											logModel.Error = fmt.Sprintf("Failed to add attention: %v", err)
											logModel.Body = district
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), district.IdLog, &logModel)
											fmt.Printf("Failed to add district: %v", err)
											continue
										} else {
											logModel.Operation = "Insert"
											logModel.Status = "Done"
											logModel.Collection = "middleware.registers.districts"
											logModel.Error = ""
											logModel.Body = district
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), district.IdLog, &logModel)

										}
										fmt.Println("created: ", val.ID)
									}
								}
							}
						}(i, chunkSize)
					}
					wg.Wait()
					districts = []*consumer_models.DistrictConsumer{}
				}
				flag = true
			}
		}
	}

	fmt.Printf("Closing consumer\n")
	c.Close()
}
