package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/consumer_models"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"desktop-middleware/models"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	fmt.Println("Consumer started!")
	topic := viper.GetString("kafka.database") + ".dbmediweb.tipo_examen"
	fmt.Println(topic)
	// Create Consumer instance
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": viper.GetString("kafka.bootstrap_servers"),
		"sasl.mechanisms":   viper.GetString("kafka.sasl_mechanisms"),
		"security.protocol": viper.GetString("kafka.security_protocol"),
		"sasl.username":     viper.GetString("kafka.sasl_username"),
		"sasl.password":     viper.GetString("kafka.sasl_password"),
		"group.id":          viper.GetString("kafka.groupId"),
		"auto.offset.reset": "earliest",
	})
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	err = c.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	// Set up a channel for handling Ctrl-C, etc
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	// Process messages
	run := true
	flag := true
	var examTypes []*consumer_models.ExamTypeConsumer
	db := database.InitDatabases()
	defer db.DisconnectDatabases()
	for run {
		var logModel models.Log
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			if flag {
				msg, err := c.ReadMessage(100 * time.Millisecond)
				if err != nil {
					// Errors are informational and automatically handled by the consumer
					flag = false
					continue
				}
				if msg.Value == nil || len(msg.Value) == 0 {
					continue
				}
				var examType consumer_models.ExamTypeConsumer
				logModel.Collection = "middleware.registers.examtypes"
				logModel.Status = "In Progress"
				logModel.Body = msg.Value
				log, _ := mongo.GetLogsCollection().AddLogs(context.TODO(), &logModel)
				err = json.Unmarshal(msg.Value, &examType)
				if err != nil {
					logModel.Operation = "Decode"
					logModel.Status = "Error"
					logModel.Collection = "middleware.registers.examtypes"
					logModel.Error = fmt.Sprintf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					json.Unmarshal(msg.Value, &logModel.Body)
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), log.ID, &logModel)
					fmt.Printf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					flag = false
					continue
				}
				examType.IdLog = log.ID
				examTypes = append(examTypes, &examType)

				if len(examTypes) > viper.GetInt("kafka.fetchCount") {
					flag = false
				}
			} else {
				if len(examTypes) > 0 {
					numCPU := viper.GetInt("kafka.threadCount")
					length := len(examTypes)
					wg := sync.WaitGroup{}
					chunkSize := (length + numCPU - 1) / numCPU

					for i := 0; i < length; i += chunkSize {

						wg.Add(1)
						go func(i int, chunkSize int) {
							end := i + chunkSize
							defer wg.Done()

							if end > length {
								end = length
							}
							slice := examTypes[i:end]

							for _, examType := range slice {

								flag, err := strconv.ParseBool(examType.IsDeleted)
								if err != nil {
									logModel.Operation = "Decode"
									logModel.Status = "Error"
									logModel.Collection = "middleware.registers.examtypes"
									logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
									logModel.Body = examType
									mongo.GetLogsCollection().UpdateLogs(context.TODO(), examType.IdLog, &logModel)
									fmt.Printf("Failed to decode JSON: %v", err)
									continue
								}

								if flag {
									_, err := mongo.GetExamTypeCollection().DeleteExamTypeByExternalId(context.TODO(), examType.ID)
									if err != nil {
										logModel.Operation = "Delete"
										logModel.Status = "Error"
										logModel.Collection = "middleware.registers.examtypes"
										logModel.Error = fmt.Sprintf("Failed to delete examType: %v", err)
										logModel.Body = examType
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), examType.IdLog, &logModel)
										fmt.Printf("Failed to delete examType: %v", err)
									} else {
										logModel.Operation = "Delete"
										logModel.Status = "Done"
										logModel.Collection = "middleware.registers.examtypes"
										logModel.Error = ""
										logModel.Body = examType
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), examType.IdLog, &logModel)
									}
								} else {
									oldexamType, err := mongo.GetExamTypeCollection().GetExamTypeById(context.TODO(), examType.ID)
									if err != nil {
										logModel.Operation = "Get"
										logModel.Status = "Error"
										logModel.Collection = "middleware.registers.examtypes"
										logModel.Error = fmt.Sprintf("Failed to get examType: %v", err)
										logModel.Body = examType
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), examType.IdLog, &logModel)
										fmt.Printf("Failed to get examType: %v", err)
									}

									if oldexamType != nil {
										oldexamType.Code = examType.ID
										oldexamType.Name = examType.Name
										oldexamType.Status = examType.Status
										_, err := mongo.GetExamTypeCollection().UpdateExamType(context.TODO(), oldexamType.ID, oldexamType)
										if err != nil {
											logModel.Operation = "Update"
											logModel.Status = "Error"
											logModel.Collection = "middleware.registers.examtypes"
											logModel.Error = fmt.Sprintf("Failed to update examType: %v", err)
											logModel.Body = examType
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), examType.IdLog, &logModel)
											fmt.Printf("Failed to update examType: %v", err)
											continue
										} else {
											logModel.Operation = "Update"
											logModel.Status = "Done"
											logModel.Collection = "middleware.registers.examtypes"
											logModel.Error = ""
											logModel.Body = examType
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), examType.IdLog, &logModel)
										}
										fmt.Println("updated: ", oldexamType.ID)
									} else {
										newExamType := models.ExamType{
											Code:       examType.ID,
											Name:       examType.Name,
											Status:     examType.Status,
											ExternalId: examType.ID,
										}
										val, err := mongo.GetExamTypeCollection().AddExamType(context.TODO(), &newExamType)
										if err != nil {
											logModel.Operation = "Insert"
											logModel.Status = "Error"
											logModel.Collection = "middleware.registers.examtypes"
											logModel.Error = fmt.Sprintf("Failed to add examType: %v", err)
											logModel.Body = examType
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), examType.IdLog, &logModel)
											fmt.Printf("Failed to add examType: %v", err)
											continue
										} else {
											logModel.Operation = "Insert"
											logModel.Status = "Done"
											logModel.Collection = "middleware.registers.examtypes"
											logModel.Error = ""
											logModel.Body = examType
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), examType.IdLog, &logModel)
										}
										fmt.Println("created: ", val.ID)
									}
								}
							}
						}(i, chunkSize)
					}
					wg.Wait()
					examTypes = []*consumer_models.ExamTypeConsumer{}
				}
				flag = true
			}
		}
	}

	fmt.Printf("Closing consumer\n")
	c.Close()
}
