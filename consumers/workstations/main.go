package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/consumer_models"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"desktop-middleware/models"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	mongo_v2 "desktop-middleware/database/mongo_v2"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	fmt.Println("Consumer started!")
	topic := viper.GetString("kafka.database") + ".dbmediweb.proyecto"
	fmt.Println(topic)
	// Create Consumer instance
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": viper.GetString("kafka.bootstrap_servers"),
		"sasl.mechanisms":   viper.GetString("kafka.sasl_mechanisms"),
		"security.protocol": viper.GetString("kafka.security_protocol"),
		"sasl.username":     viper.GetString("kafka.sasl_username"),
		"sasl.password":     viper.GetString("kafka.sasl_password"),
		"group.id":          viper.GetString("kafka.groupId"),
		"auto.offset.reset": "earliest",
	})
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	err = c.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	// Set up a channel for handling Ctrl-C, etc
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	// Process messages
	run := true
	flag := true
	var workStations []*consumer_models.WorkStationConsumer
	db := database.InitDatabases()
	defer db.DisconnectDatabases()
	for run {
		var logModel models.Log
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			if flag {
				msg, err := c.ReadMessage(100 * time.Millisecond)
				if err != nil {
					// Errors are informational and automatically handled by the consumer
					flag = false
					continue
				}
				if msg.Value == nil || len(msg.Value) == 0 {
					continue
				}
				var workStation consumer_models.WorkStationConsumer
				logModel.Collection = "middleware.registers.customers.workstations"
				logModel.Status = "In Progress"
				logModel.Body = msg.Value
				log, _ := mongo.GetLogsCollection().AddLogs(context.TODO(), &logModel)
				err = json.Unmarshal(msg.Value, &workStation)
				if err != nil {
					logModel.Operation = "Decode"
					logModel.Status = "Error"
					logModel.Collection = "middleware.registers.customers.workstations"
					logModel.Error = fmt.Sprintf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					json.Unmarshal(msg.Value, &logModel.Body)
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), log.ID, &logModel)
					fmt.Printf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					flag = false
					continue
				}
				workStation.IdLog = log.ID
				workStations = append(workStations, &workStation)
				if len(workStations) > viper.GetInt("kafka.fetchCount") {
					flag = false
				}
			} else {
				if len(workStations) > 0 {
					numCPU := viper.GetInt("kafka.threadCount")
					length := len(workStations)
					wg := sync.WaitGroup{}
					chunkSize := (length + numCPU - 1) / numCPU

					for i := 0; i < length; i += chunkSize {

						wg.Add(1)
						go func(i int, chunkSize int) {
							end := i + chunkSize
							defer wg.Done()

							if end > length {
								end = length
							}
							slice := workStations[i:end]

							for _, workStation := range slice {

								flag, err := strconv.ParseBool(workStation.IsDeleted)
								if err != nil {
									logModel.Operation = "Decode"
									logModel.Status = "Error"
									logModel.Collection = "middleware.registers.customers.workstations"
									logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
									logModel.Body = workStation
									mongo.GetLogsCollection().UpdateLogs(context.TODO(), workStation.IdLog, &logModel)
									fmt.Printf("Failed to decode JSON: %v", err)
									continue
								}

								if flag {
									_, err := mongo.GetWorkStationCollection().DeleteWorkStationByExternalId(context.TODO(), strconv.Itoa(workStation.ID))
									if err != nil {
										logModel.Operation = "Delete"
										logModel.Status = "Error"
										logModel.Collection = "middleware.registers.customers.workstations"
										logModel.Error = fmt.Sprintf("Failed to delete workStation: %v", err)
										logModel.Body = workStation
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), workStation.IdLog, &logModel)
										fmt.Printf("Failed to delete workStation: %v", err)
									} else {
										logModel.Operation = "Delete"
										logModel.Status = "Done"
										logModel.Collection = "middleware.registers.customers.workstations"
										logModel.Error = ""
										logModel.Body = workStation
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), workStation.IdLog, &logModel)
									}
								} else {
									oldworkStation, err := mongo.GetWorkStationCollection().GetWorkStationById(context.TODO(), strconv.Itoa(workStation.ID))
									if err != nil {
										logModel.Operation = "Get"
										logModel.Status = "Error"
										logModel.Collection = "middleware.registers.customers.workstations"
										logModel.Error = fmt.Sprintf("Failed to get JSON: %v", err)
										logModel.Body = workStation
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), workStation.IdLog, &logModel)
										fmt.Printf("Failed to get workStation: %v", err)
									}

									if oldworkStation != nil {
										if workStation.CustomerId != "" {
											customer, err := mongo.GetCustomerCollection().GetCustomerById(context.TODO(), workStation.CustomerId)
											if err != nil {
												logModel.Operation = "Get"
												logModel.Status = "Error"
												logModel.Collection = "middleware.registers.customers"
												logModel.Error = fmt.Sprintf("Failed to get JSON: %v", err)
												logModel.Body = workStation
												mongo.GetLogsCollection().UpdateLogs(context.TODO(), workStation.IdLog, &logModel)
												fmt.Printf("Failed to get workStation: %v", err)
											}
											oldworkStation.CustomerId = customer.ID
										}
										oldworkStation.Name = workStation.Name
										oldworkStation.Code = strconv.Itoa(workStation.ID)
										status, _ := strconv.Atoi(workStation.Status)
										oldworkStation.Status = status
										_, err := mongo.GetWorkStationCollection().UpdateWorkStation(context.TODO(), oldworkStation.ID, oldworkStation)
										if err != nil {
											logModel.Operation = "Update"
											logModel.Status = "Error"
											logModel.Collection = "middleware.registers.customers.workstations"
											logModel.Error = fmt.Sprintf("Failed to update JSON: %v", err)
											logModel.Body = workStation
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), workStation.IdLog, &logModel)
											fmt.Printf("Failed to update workStation: %v", err)
											continue
										} else {
											logModel.Operation = "Update"
											logModel.Status = "Done"
											logModel.Collection = "middleware.registers.customers.workstations"
											logModel.Error = ""
											logModel.Body = workStation
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), workStation.IdLog, &logModel)
										}
										go mongo_v2.GetAttentionCollection().CustomSetFieldById(context.Background(), "workStationId", oldworkStation.ID, "workStation", oldworkStation)
										go mongo_v2.GetPersonCollection().CustomSetFieldById(context.Background(), "workStationId", oldworkStation.ID, "workStation", oldworkStation)
										fmt.Println("updated: ", oldworkStation.ID)
									} else {
										status, _ := strconv.Atoi(workStation.Status)
										newWorkStation := models.WorkStation{
											Code:       strconv.Itoa(workStation.ID),
											Name:       workStation.Name,
											Status:     status,
											ExternalId: strconv.Itoa(workStation.ID),
										}
										if workStation.CustomerId != "" {
											customer, err := mongo.GetCustomerCollection().GetCustomerById(context.TODO(), workStation.CustomerId)
											if err != nil {
												logModel.Operation = "Get"
												logModel.Status = "Error"
												logModel.Collection = "middleware.registers.customers"
												logModel.Error = fmt.Sprintf("Failed to get JSON: %v", err)
												logModel.Body = workStation
												mongo.GetLogsCollection().UpdateLogs(context.TODO(), workStation.IdLog, &logModel)
												fmt.Printf("Failed to get workStation: %v", err)
												continue
											}
											newWorkStation.CustomerId = customer.ID
										}
										val, err := mongo.GetWorkStationCollection().AddWorkStation(context.TODO(), &newWorkStation)
										if err != nil {
											logModel.Operation = "Insert"
											logModel.Status = "Error"
											logModel.Collection = "middleware.registers.customers.workstations"
											logModel.Error = fmt.Sprintf("Failed to add JSON: %v", err)
											logModel.Body = workStation
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), workStation.IdLog, &logModel)
											fmt.Printf("Failed to add workStation: %v", err)
											continue
										} else {
											logModel.Operation = "Insert"
											logModel.Status = "Done"
											logModel.Collection = "middleware.registers.customers.workstations"
											logModel.Error = ""
											logModel.Body = workStation
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), workStation.IdLog, &logModel)
										}
										fmt.Println("created: ", val.ID)
									}
								}
							}
						}(i, chunkSize)
					}
					wg.Wait()
					workStations = []*consumer_models.WorkStationConsumer{}
				}
				flag = true
			}
		}
	}

	fmt.Printf("Closing consumer\n")
	c.Close()
}
