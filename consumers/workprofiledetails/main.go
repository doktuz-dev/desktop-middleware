package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/consumer_models"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	mongo_v2 "desktop-middleware/database/mongo_v2"
	"desktop-middleware/models"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/spf13/viper"
	"github.com/thoas/go-funk"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func main() {
	config.SetConfiguration()
	fmt.Println("Consumer started!")
	topic := viper.GetString("kafka.database") + ".dbmediweb.protocolo"
	fmt.Println(topic)
	// Create Consumer instance
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": viper.GetString("kafka.bootstrap_servers"),
		"sasl.mechanisms":   viper.GetString("kafka.sasl_mechanisms"),
		"security.protocol": viper.GetString("kafka.security_protocol"),
		"sasl.username":     viper.GetString("kafka.sasl_username"),
		"sasl.password":     viper.GetString("kafka.sasl_password"),
		"group.id":          viper.GetString("kafka.groupId"),
		"auto.offset.reset": "earliest",
	})
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	err = c.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	// Set up a channel for handling Ctrl-C, etc
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	// Process messages
	run := true
	flag := true
	var workProfiles []*consumer_models.WorkProfileDetailConsumer
	db := database.InitDatabases()
	defer db.DisconnectDatabases()
	for run {
		var logModel models.Log
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			if flag {
				msg, err := c.ReadMessage(100 * time.Millisecond)
				if err != nil {
					// Errors are informational and automatically handled by the consumer
					flag = false
					continue
				}
				if msg.Value == nil || len(msg.Value) == 0 {
					continue
				}
				var workProfile consumer_models.WorkProfileDetailConsumer
				logModel.Collection = "middleware.registers.workprofiles"
				logModel.Status = "In Progress"
				logModel.Body = msg.Value
				log, _ := mongo.GetLogsCollection().AddLogs(context.TODO(), &logModel)
				err = json.Unmarshal(msg.Value, &workProfile)
				if err != nil {
					logModel.Operation = "Decode"
					logModel.Status = "Error"
					logModel.Collection = "middleware.registers.workprofiles"
					logModel.Error = fmt.Sprintf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					json.Unmarshal(msg.Value, &logModel.Body)
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), log.ID, &logModel)
					fmt.Printf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					flag = false
					continue
				}
				workProfile.IdLog = log.ID
				workProfiles = append(workProfiles, &workProfile)
				if len(workProfiles) > viper.GetInt("kafka.fetchCount") {
					flag = false
				}
			} else {
				if len(workProfiles) > 0 {
					for _, workProfile := range workProfiles {
						go AddWorkprofileV2(workProfile, logModel)
					}
					workProfiles = []*consumer_models.WorkProfileDetailConsumer{}
				}
				flag = true
			}
		}
	}

	fmt.Printf("Closing consumer\n")
	c.Close()
}

func AddWorkprofileV2(workProfile *consumer_models.WorkProfileDetailConsumer, logModel models.Log) {
	oldworkProfile, err := mongo.GetWorkProfileCollection().GetWorkProfileById(context.Background(), strconv.Itoa(workProfile.ID))
	if err != nil {
		logModel.Operation = "Get"
		logModel.Status = "Error"
		logModel.Collection = "middleware.registers.workprofiles"
		logModel.Error = fmt.Sprintf("Failed to get JSON: %v", err)
		logModel.Body = workProfile
		mongo.GetLogsCollection().UpdateLogs(context.Background(), workProfile.IdLog, &logModel)
		fmt.Printf("Failed to get workProfile: %v", err)
	}

	if oldworkProfile != nil {
		if oldworkProfile.Version <= workProfile.Version {
			oldworkProfile.EndDate = workProfile.AddDateEndDate()
			if workProfile.Status == "on" && workProfile.Status2 == 1 {
				oldworkProfile.Status = 1
			} else {
				oldworkProfile.Status = 0
			}
			oldworkProfile.Version = workProfile.Version
			customer, _ := mongo.GetCustomerCollection().GetCustomerById(context.Background(), strconv.Itoa(workProfile.CustomerID))
			if customer != nil {
				oldworkProfile.CustomerId = customer.ID
			}
			examtypes := []*string{}
			if workProfile.CheckOcu == "on" {
				examType, _ := mongo.GetExamTypeCollection().GetExamTypeById(context.Background(), "2")
				examtypes = append(examtypes, &examType.ID)
			}
			if workProfile.CheckPre == "on" {
				examType, _ := mongo.GetExamTypeCollection().GetExamTypeById(context.Background(), "1")
				examtypes = append(examtypes, &examType.ID)
			}
			if workProfile.CheckRet == "on" {
				examType, _ := mongo.GetExamTypeCollection().GetExamTypeById(context.Background(), "3")
				examtypes = append(examtypes, &examType.ID)
			}
			if workProfile.CheckVis == "on" {
				examType, _ := mongo.GetExamTypeCollection().GetExamTypeById(context.Background(), "4")
				examtypes = append(examtypes, &examType.ID)
			}

			var healthcentres []*models.HealthCentre
			clinicId := strconv.Itoa(workProfile.ClinicId)
			healthcentreList, _ := mongo.GetHealthCentreCollection().GetHealthCentreByClinicId(context.Background(), clinicId)

			if len(healthcentreList) > 0 {
				healthcentres = funk.Map(healthcentreList, func(p *models.HealthCentre) *models.HealthCentre {
					p.ExamTypesIds = examtypes
					return p
				}).([]*models.HealthCentre)
				oldworkProfile.Healthcentres = healthcentres
			}

			externalProtocolId := strconv.Itoa(workProfile.ProtocolID)

			flagWorkProfile, _ := mongo.GetWorkProfileCollection().GetWorkProfileByProtocolId(context.Background(), externalProtocolId)

			if flagWorkProfile == nil && oldworkProfile.ExternalProtocolId != "" {
				newWorkProfile := oldworkProfile
				newWorkProfile.ID = primitive.NewObjectID().Hex()
				newWorkProfile.ExternalProtocolId = externalProtocolId
				_, err := mongo.GetWorkProfileCollection().AddWorkProfile(context.Background(), newWorkProfile)
				if err != nil {
					logModel.Operation = "Add"
					logModel.Status = "Error"
					logModel.Collection = "middleware.registers.workprofiles"
					logModel.Error = fmt.Sprintf("Failed to update JSON: %v", err)
					logModel.Body = workProfile
					mongo.GetLogsCollection().UpdateLogs(context.Background(), workProfile.IdLog, &logModel)
					fmt.Printf("Failed to update workProfile: %v", err)
					return
				} else {
					logModel.Operation = "Add"
					logModel.Status = "Done"
					logModel.Collection = "middleware.registers.workprofiles"
					logModel.Error = ""
					logModel.Body = workProfile
					mongo.GetLogsCollection().UpdateLogs(context.Background(), workProfile.IdLog, &logModel)
				}
				fmt.Println("Added: ", oldworkProfile.ID)
			} else {
				if flagWorkProfile != nil {
					oldworkProfile.ID = flagWorkProfile.ID
					oldworkProfile.LastUpdatedDate = flagWorkProfile.LastUpdatedDate
					oldworkProfile.CreatedDate = flagWorkProfile.CreatedDate
				}
				oldworkProfile.ExternalProtocolId = externalProtocolId
				_, err := mongo.GetWorkProfileCollection().UpdateProtocolo(context.Background(), oldworkProfile.ID, oldworkProfile)
				if err != nil {
					logModel.Operation = "Update"
					logModel.Status = "Error"
					logModel.Collection = "middleware.registers.workprofiles"
					logModel.Error = fmt.Sprintf("Failed to update JSON: %v", err)
					logModel.Body = workProfile
					mongo.GetLogsCollection().UpdateLogs(context.Background(), workProfile.IdLog, &logModel)
					fmt.Printf("Failed to update workProfile: %v", err)
					return
				} else {
					logModel.Operation = "Update"
					logModel.Status = "Done"
					logModel.Collection = "middleware.registers.workprofiles"
					logModel.Error = ""
					logModel.Body = workProfile
					mongo.GetLogsCollection().UpdateLogs(context.Background(), workProfile.IdLog, &logModel)
				}
				fmt.Println("updated: ", oldworkProfile.ID)
			}

			go mongo_v2.GetAttentionCollection().CustomSetFieldById(context.Background(), "workProfileId", oldworkProfile.ID, "workProfile", oldworkProfile)
		}
	}
}
