package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/consumer_models"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	mongo_v2 "desktop-middleware/database/mongo_v2"
	"desktop-middleware/models"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func main() {
	config.SetConfiguration()
	fmt.Println("Consumer started!")
	topic := viper.GetString("kafka.database") + ".dbmediweb.medicina"
	fmt.Println(topic)
	// Create Consumer instance
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": viper.GetString("kafka.bootstrap_servers"),
		"sasl.mechanisms":   viper.GetString("kafka.sasl_mechanisms"),
		"security.protocol": viper.GetString("kafka.security_protocol"),
		"sasl.username":     viper.GetString("kafka.sasl_username"),
		"sasl.password":     viper.GetString("kafka.sasl_password"),
		"group.id":          viper.GetString("kafka.groupId"),
		"auto.offset.reset": "earliest",
	})
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	err = c.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	// Set up a channel for handling Ctrl-C, etc
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	// Process messages
	run := true
	flag := true
	var medicalHistorys []*consumer_models.MedicalHistoryConsumer
	db := database.InitDatabases()
	defer db.DisconnectDatabases()
	for run {
		var logModel models.Log
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			if flag {
				msg, err := c.ReadMessage(100 * time.Millisecond)
				if err != nil {
					// Errors are informational and automatically handled by the consumer
					flag = false
					continue
				}
				if msg.Value == nil || len(msg.Value) == 0 {
					continue
				}
				var medicalHistory consumer_models.MedicalHistoryConsumer
				logModel.Collection = "middleware.transactions.attentions.medicalhistories"
				logModel.Status = "In Progress"
				logModel.Body = msg.Value
				log, _ := mongo.GetLogsCollection().AddLogs(context.TODO(), &logModel)
				err = json.Unmarshal(msg.Value, &medicalHistory)
				if err != nil {
					logModel.Operation = "Decode"
					logModel.Status = "Error"
					logModel.Collection = "middleware.transactions.attentions.medicalhistories"
					logModel.Error = fmt.Sprintf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					json.Unmarshal(msg.Value, &logModel.Body)
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), log.ID, &logModel)
					fmt.Printf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					flag = false
					continue
				}
				medicalHistory.IdLog = log.ID
				medicalHistorys = append(medicalHistorys, &medicalHistory)
				if len(medicalHistorys) > viper.GetInt("kafka.fetchCount") {
					flag = false
				}
			} else {
				if len(medicalHistorys) > 0 {
					numCPU := viper.GetInt("kafka.threadCount")
					length := len(medicalHistorys)
					wg := sync.WaitGroup{}
					chunkSize := (length + numCPU - 1) / numCPU

					for i := 0; i < length; i += chunkSize {

						wg.Add(1)
						go func(i int, chunkSize int) {
							end := i + chunkSize
							defer wg.Done()

							if end > length {
								end = length
							}
							slice := medicalHistorys[i:end]

							for _, medicalHistory := range slice {
								AddMedicalHistoryConsumerV2(medicalHistory, logModel)
							}
						}(i, chunkSize)
					}
					wg.Wait()
					medicalHistorys = []*consumer_models.MedicalHistoryConsumer{}
				}
				flag = true
			}
		}
	}

	fmt.Printf("Closing consumer\n")
	c.Close()
}

func AddMedicalHistoryConsumerV2(medicalHistory *consumer_models.MedicalHistoryConsumer, logModel models.Log) {

	flag, err := strconv.ParseBool(medicalHistory.IsDeleted)
	if err != nil {
		logModel.Operation = "Decode"
		logModel.Status = "Error"
		logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
		logModel.Body = medicalHistory
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), medicalHistory.IdLog, &logModel)
		fmt.Printf("Failed to decode JSON: %v", err)
		return
	}

	if flag || medicalHistory.Status == "0" {
		_, err := mongo_v2.GetAttentionCollection().UnsetFieldByExternalId(context.TODO(), strconv.Itoa(medicalHistory.AttentionId), "medicalHistories")
		if err != nil {
			logModel.Operation = "Delete"
			logModel.Status = "Error"
			logModel.Error = fmt.Sprintf("Failed to delete medicalHistory: %v", err)
			logModel.Body = medicalHistory
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), medicalHistory.IdLog, &logModel)
			fmt.Printf("Failed to delete medicalHistory: %v", err)
		} else {
			logModel.Operation = "Delete"
			logModel.Status = "Done"
			logModel.Body = medicalHistory
			logModel.Error = ""
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), medicalHistory.IdLog, &logModel)

		}
	} else {
		attention, err := mongo_v2.GetAttentionCollection().GetAttentionByExternalId(context.TODO(), strconv.Itoa(medicalHistory.AttentionId))
		if err != nil {
			logModel.Operation = "Get"
			logModel.Error = fmt.Sprintf("Failed to get medicalHistory: %v", err)
			logModel.Body = medicalHistory
			logModel.Status = "Error"
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), medicalHistory.IdLog, &logModel)
			fmt.Printf("Failed to get medicalHistory: %v", err)
		}

		if attention == nil {
			return
		}

		oldmedicalHistory := attention.MedicalHistories

		if oldmedicalHistory != nil {
			oldmedicalHistory.LastUpdatedDate = time.Now()

			_, err := mongo_v2.GetAttentionCollection().SetFieldByID(context.TODO(), attention.ID, "medicalHistories", oldmedicalHistory)
			if err != nil {
				logModel.Operation = "Update"
				logModel.Error = fmt.Sprintf("Failed to update medicalHistory: %v", err)
				logModel.Body = medicalHistory
				logModel.Status = "Error"
				mongo.GetLogsCollection().UpdateLogs(context.TODO(), medicalHistory.IdLog, &logModel)
				fmt.Printf("Failed to update medicalHistory: %v", err)
				return
			} else {
				logModel.Operation = "Update"
				logModel.Body = medicalHistory
				logModel.Status = "Done"
				logModel.Error = ""
				mongo.GetLogsCollection().UpdateLogs(context.TODO(), medicalHistory.IdLog, &logModel)
			}
			fmt.Println("updated: ", oldmedicalHistory.ID)
		} else {
			newMedicalHistory := medicalHistory.SetMedicalHistory()
			newMedicalHistory.ID = primitive.NewObjectID().Hex()
			newMedicalHistory.CreatedDate = time.Now()
			newMedicalHistory.LastUpdatedDate = time.Now()
			newMedicalHistory.ExternalId = strconv.Itoa(medicalHistory.ID)
			newMedicalHistory.ExternalSystem = "MEDIWEB"
			_, err := mongo_v2.GetAttentionCollection().SetFieldByID(context.TODO(), attention.ID, "medicalHistories", newMedicalHistory)
			if err != nil {
				logModel.Operation = "Insert"
				logModel.Error = fmt.Sprintf("Failed to insert medicalHistory: %v", err)
				logModel.Body = medicalHistory
				logModel.Status = "Error"
				mongo.GetLogsCollection().UpdateLogs(context.TODO(), medicalHistory.IdLog, &logModel)
				fmt.Printf("Failed to add medicalHistory: %v", err)
				return
			} else {
				logModel.Operation = "Insert"
				logModel.Body = medicalHistory
				logModel.Status = "Done"
				logModel.Error = ""
				mongo.GetLogsCollection().UpdateLogs(context.TODO(), medicalHistory.IdLog, &logModel)
			}
			fmt.Println("created: ", newMedicalHistory.ID)
		}
		if attention.PersonId != nil && attention.AttentionTypeName != nil && attention.CustomerId != nil {
			if *attention.AttentionTypeName == "Ocupacional" {
				lastEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *attention.PersonId, *attention.CustomerId, "emo")
				if lastEmo != nil {
					mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *attention.PersonId, "lastEmoAttention", lastEmo)
				}
				lastAnyEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *attention.PersonId, *attention.CustomerId, "any")
				if lastAnyEmo != nil {
					lastAnyEmo.Customer = nil
					lastAnyEmo.Patient = nil
					lastAnyEmo.Person = nil
					lastAnyEmo.Consultations = nil
					lastAnyEmo.AttentionLines = nil
					lastAnyEmo.Exams = nil
					lastAnyEmo.AudiometryTests = nil
					lastAnyEmo.BiochemistryTests = nil
					lastAnyEmo.CovidTests = nil
					lastAnyEmo.Diagnostics = nil
					lastAnyEmo.DrugTests = nil
					lastAnyEmo.HematologyTests = nil
					lastAnyEmo.ImmunologyTests = nil
					lastAnyEmo.MicrobiologyTests = nil
					lastAnyEmo.OphtalmologyTests = nil
					lastAnyEmo.PsychologyTests = nil
					lastAnyEmo.Recommendations = nil
					lastAnyEmo.ExamResults = nil
					lastAnyEmo.SkeletalMuscleTests = nil
					lastAnyEmo.UrineTests = nil
					lastAnyEmo.MedicalHistories = nil
					lastAnyEmo.PathologicalHistories = nil
					lastAnyEmo.Triages = nil
					lastAnyEmo.WorkHistories = nil
					lastAnyEmo.SpirometryTests = nil
					mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *attention.PersonId, "lastAnyEmoAttention", lastAnyEmo)
				}
			}
			if *attention.AttentionTypeName == "Asistencial" {
				lastNoEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *attention.PersonId, *attention.CustomerId, "no_emo")
				if lastNoEmo != nil {
					mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *attention.PersonId, "lastNoEmoAttention", lastNoEmo)
				}
			}
		}
	}
}
