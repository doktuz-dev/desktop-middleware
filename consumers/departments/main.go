package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/consumer_models"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"desktop-middleware/models"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	fmt.Println("Consumer started!")
	topic := viper.GetString("kafka.database") + ".dbmediweb.graldepartamentos"
	fmt.Println(topic)
	// Create Consumer instance
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": viper.GetString("kafka.bootstrap_servers"),
		"sasl.mechanisms":   viper.GetString("kafka.sasl_mechanisms"),
		"security.protocol": viper.GetString("kafka.security_protocol"),
		"sasl.username":     viper.GetString("kafka.sasl_username"),
		"sasl.password":     viper.GetString("kafka.sasl_password"),
		"group.id":          viper.GetString("kafka.groupId"),
		"auto.offset.reset": "earliest",
	})
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	err = c.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	// Set up a channel for handling Ctrl-C, etc
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	// Process messages
	run := true
	flag := true
	var departments []*consumer_models.DepartmentConsumer
	db := database.InitDatabases()
	defer db.DisconnectDatabases()
	for run {
		var logModel models.Log
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			if flag {
				msg, err := c.ReadMessage(100 * time.Millisecond)
				if err != nil {
					// Errors are informational and automatically handled by the consumer
					flag = false
					continue
				}
				if msg.Value == nil || len(msg.Value) == 0 {
					continue
				}
				var department consumer_models.DepartmentConsumer
				logModel.Collection = "middleware.registers.departments"
				logModel.Status = "In Progress"
				logModel.Body = msg.Value
				log, _ := mongo.GetLogsCollection().AddLogs(context.TODO(), &logModel)
				err = json.Unmarshal(msg.Value, &department)
				if err != nil {
					logModel.Operation = "Decode"
					logModel.Status = "Error"
					logModel.Collection = "middleware.registers.departments"
					logModel.Error = fmt.Sprintf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					json.Unmarshal(msg.Value, &logModel.Body)
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), log.ID, &logModel)
					fmt.Printf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					flag = false
					continue
				}
				department.IdLog = log.ID
				departments = append(departments, &department)
				if len(departments) > viper.GetInt("kafka.fetchCount") {
					flag = false
				}
			} else {
				if len(departments) > 0 {
					numCPU := viper.GetInt("kafka.threadCount")
					length := len(departments)
					wg := sync.WaitGroup{}
					chunkSize := (length + numCPU - 1) / numCPU

					for i := 0; i < length; i += chunkSize {

						wg.Add(1)
						go func(i int, chunkSize int) {
							end := i + chunkSize
							defer wg.Done()

							if end > length {
								end = length
							}
							slice := departments[i:end]

							for _, department := range slice {

								flag, err := strconv.ParseBool(department.IsDeleted)
								if err != nil {
									logModel.Operation = "Decode"
									logModel.Status = "Error"
									logModel.Collection = "middleware.registers.departments"
									logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
									logModel.Body = department
									mongo.GetLogsCollection().UpdateLogs(context.TODO(), department.IdLog, &logModel)
									fmt.Printf("Failed to decode JSON: %v", err)
									continue
								}

								if flag {
									_, err := mongo.GetDepartmentCollection().DeleteDepartmentByExternalId(context.TODO(), strconv.Itoa(department.ID))
									if err != nil {
										logModel.Operation = "Delete"
										logModel.Status = "Error"
										logModel.Collection = "middleware.registers.departments"
										logModel.Error = fmt.Sprintf("Failed to delete department: %v", err)
										logModel.Body = department
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), department.IdLog, &logModel)
										fmt.Printf("Failed to delete department: %v", err)
									} else {
										logModel.Operation = "Delete"
										logModel.Status = "Done"
										logModel.Collection = "middleware.registers.departments"
										logModel.Error = ""
										logModel.Body = department
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), department.IdLog, &logModel)

									}
								} else {
									olddepartment, err := mongo.GetDepartmentCollection().GetDepartmentById(context.TODO(), strconv.Itoa(department.ID))
									if err != nil {
										logModel.Operation = "Get"
										logModel.Status = "Error"
										logModel.Collection = "middleware.registers.departments"
										logModel.Error = fmt.Sprintf("Failed to get department: %v", err)
										logModel.Body = department
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), department.IdLog, &logModel)
										fmt.Printf("Failed to get department: %v", err)
									}

									if olddepartment != nil {
										olddepartment.Name = department.Name
										_, err := mongo.GetDepartmentCollection().UpdateDepartment(context.TODO(), olddepartment.ID, olddepartment)
										if err != nil {
											logModel.Operation = "Update"
											logModel.Status = "Error"
											logModel.Collection = "middleware.registers.departments"
											logModel.Error = fmt.Sprintf("Failed to update department: %v", err)
											logModel.Body = department
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), department.IdLog, &logModel)
											fmt.Printf("Failed to update department: %v", err)
											continue
										} else {
											logModel.Operation = "Update"
											logModel.Status = "Done"
											logModel.Collection = "middleware.registers.departments"
											logModel.Error = ""
											logModel.Body = department
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), department.IdLog, &logModel)
										}
										fmt.Println("updated: ", olddepartment.ID)
									} else {
										newDepartment := models.Department{
											Name:       department.Name,
											ExternalId: strconv.Itoa(department.ID),
										}
										val, err := mongo.GetDepartmentCollection().AddDepartment(context.TODO(), &newDepartment)
										if err != nil {
											logModel.Operation = "Insert"
											logModel.Status = "Error"
											logModel.Collection = "middleware.registers.departments"
											logModel.Error = fmt.Sprintf("Failed to add department: %v", err)
											logModel.Body = department
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), department.IdLog, &logModel)
											fmt.Printf("Failed to add department: %v", err)
											continue
										} else {
											logModel.Operation = "Insert"
											logModel.Status = "Done"
											logModel.Collection = "middleware.registers.departments"
											logModel.Error = ""
											logModel.Body = department
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), department.IdLog, &logModel)
										}
										fmt.Println("created: ", val.ID)
									}
								}
							}
						}(i, chunkSize)
					}
					wg.Wait()
					departments = []*consumer_models.DepartmentConsumer{}
				}
				flag = true
			}
		}
	}

	fmt.Printf("Closing consumer\n")
	c.Close()
}
