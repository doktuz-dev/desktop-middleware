package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/consumer_models"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"desktop-middleware/models"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	mongo_v2 "desktop-middleware/database/mongo_v2"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	fmt.Println("Consumer started!")
	topic := viper.GetString("kafka.database") + ".dbmediweb.seccion"
	fmt.Println(topic)
	// Create Consumer instance
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": viper.GetString("kafka.bootstrap_servers"),
		"sasl.mechanisms":   viper.GetString("kafka.sasl_mechanisms"),
		"security.protocol": viper.GetString("kafka.security_protocol"),
		"sasl.username":     viper.GetString("kafka.sasl_username"),
		"sasl.password":     viper.GetString("kafka.sasl_password"),
		"group.id":          viper.GetString("kafka.groupId"),
		"auto.offset.reset": "earliest",
	})
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	err = c.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	// Set up a channel for handling Ctrl-C, etc
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	// Process messages
	run := true
	flag := true
	var serviceAreas []*consumer_models.ServiceAreaConsumer
	db := database.InitDatabases()
	defer db.DisconnectDatabases()
	for run {
		var logModel models.Log
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			if flag {
				msg, err := c.ReadMessage(100 * time.Millisecond)
				if err != nil {
					// Errors are informational and automatically handled by the consumer
					flag = false
					continue
				}
				if msg.Value == nil || len(msg.Value) == 0 {
					continue
				}
				var serviceArea consumer_models.ServiceAreaConsumer
				logModel.Collection = "middleware.registers.serviceareas"
				logModel.Status = "In Progress"
				logModel.Body = msg.Value
				log, _ := mongo.GetLogsCollection().AddLogs(context.TODO(), &logModel)
				err = json.Unmarshal(msg.Value, &serviceArea)
				if err != nil {
					logModel.Operation = "Decode"
					logModel.Status = "Error"
					logModel.Collection = "middleware.registers.serviceareas"
					logModel.Error = fmt.Sprintf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					json.Unmarshal(msg.Value, &logModel.Body)
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), log.ID, &logModel)
					fmt.Printf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					flag = false
					continue
				}
				serviceArea.IdLog = log.ID
				serviceAreas = append(serviceAreas, &serviceArea)

				if len(serviceAreas) > viper.GetInt("kafka.fetchCount") {
					flag = false
				}
			} else {
				if len(serviceAreas) > 0 {
					numCPU := viper.GetInt("kafka.threadCount")
					length := len(serviceAreas)
					wg := sync.WaitGroup{}
					chunkSize := (length + numCPU - 1) / numCPU

					for i := 0; i < length; i += chunkSize {

						wg.Add(1)
						go func(i int, chunkSize int) {
							end := i + chunkSize
							defer wg.Done()

							if end > length {
								end = length
							}
							slice := serviceAreas[i:end]

							for _, serviceArea := range slice {

								flag, err := strconv.ParseBool(serviceArea.IsDeleted)
								if err != nil {
									logModel.Operation = "Decode"
									logModel.Status = "Error"
									logModel.Collection = "middleware.registers.serviceareas"
									logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
									logModel.Body = serviceArea
									mongo.GetLogsCollection().UpdateLogs(context.TODO(), serviceArea.IdLog, &logModel)
									fmt.Printf("Failed to decode JSON: %v", err)
									continue
								}

								if flag {
									_, err := mongo.GetServiceAreaCollection().DeleteServiceAreaByExternalId(context.TODO(), strconv.Itoa(serviceArea.ID))
									if err != nil {
										logModel.Operation = "Delete"
										logModel.Status = "Error"
										logModel.Collection = "middleware.registers.serviceareas"
										logModel.Error = fmt.Sprintf("Failed to delete serviceArea: %v", err)
										logModel.Body = serviceArea
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), serviceArea.IdLog, &logModel)
										fmt.Printf("Failed to delete serviceArea: %v", err)
										continue
									} else {
										logModel.Operation = "Delete"
										logModel.Status = "Done"
										logModel.Collection = "middleware.registers.serviceareas"
										logModel.Error = ""
										logModel.Body = serviceArea
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), serviceArea.IdLog, &logModel)
									}
								} else {
									oldserviceArea, err := mongo.GetServiceAreaCollection().GetServiceAreaByExternalId(context.TODO(), strconv.Itoa(serviceArea.ID))
									if err != nil {
										logModel.Operation = "Get"
										logModel.Status = "Error"
										logModel.Collection = "middleware.registers.serviceareas"
										logModel.Error = fmt.Sprintf("Failed to get serviceArea: %v", err)
										logModel.Body = serviceArea
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), serviceArea.IdLog, &logModel)
										fmt.Printf("Failed to get serviceArea: %v", err)
									}

									if oldserviceArea != nil {
										flagChanged := false
										if strings.TrimSpace(serviceArea.Name) != strings.TrimSpace(oldserviceArea.Name) {
											flagChanged = true
										}
										oldserviceArea.Name = serviceArea.Name
										switch serviceArea.ID {
										case 1:
											oldserviceArea.Code = "OPHTALMOLOGY"
										case 2:
											oldserviceArea.Code = "GEOGRAPHICALHEIGHT"
										case 3:
											oldserviceArea.Code = "AUDIOMETRY"
										case 4:
											oldserviceArea.Code = "HEIGHT"
										case 5:
											oldserviceArea.Code = "XRAY"
										case 6:
											oldserviceArea.Code = "PSYCHOLOGY"
										case 7:
											oldserviceArea.Code = "SPIROMETRY"
										case 8:
											oldserviceArea.Code = "EKG"
										case 9:
											oldserviceArea.Code = "DENTISTRY"
										case 10:
											oldserviceArea.Code = "PATHOLOGICALHISTORY"
										case 11:
											oldserviceArea.Code = "DRIVINGCERTIFICATION"
										case 12:
											oldserviceArea.Code = "TRIAGE"
										case 13:
											oldserviceArea.Code = "MEDICINE"
										case 15:
											oldserviceArea.Code = "XRAYLD"
										case 18:
											oldserviceArea.Code = "SKELETALMUSCLE"
										case 19:
											oldserviceArea.Code = "NEUROLOGY"
										case 21:
											oldserviceArea.Code = "DIAGNOSTIC"
										case 22:
											oldserviceArea.Code = "WORKHISTORY"
										case 23:
											oldserviceArea.Code = "DERMATOLOGY"
										case 24:
											oldserviceArea.Code = "STRESSTEST"
										case 26:
											oldserviceArea.Code = "LABORATORY"
										case 27:
											oldserviceArea.Code = "DOCUMENT"
										case 28:
											oldserviceArea.Code = "PSYCHOSENSOMETRIC"
										case 29:
											oldserviceArea.Code = "CONSULTATION"
										case 30:
											oldserviceArea.Code = "APTITUDE"
										case 31:
											oldserviceArea.Code = "MEDICALREPORT"
										case 32:
											oldserviceArea.Code = "NUTRITION"
										case 33:
											oldserviceArea.Code = "CONFINEDSPACE"
										case 34:
											oldserviceArea.Code = "PSP"
										case 38:
											oldserviceArea.Code = "AUXILIARYEXAM"
										case 39:
											oldserviceArea.Code = "GYNECOLOGY"
										case 40:
											oldserviceArea.Code = "ULTRASOUND"
										case 41:
											oldserviceArea.Code = "UROLOGY"
										}
										status, _ := strconv.Atoi(serviceArea.Status)
										oldserviceArea.Status = status
										_, err := mongo.GetServiceAreaCollection().UpdateServiceArea(context.TODO(), oldserviceArea.ID, oldserviceArea)
										if err != nil {
											logModel.Operation = "Update"
											logModel.Status = "Error"
											logModel.Collection = "middleware.registers.serviceareas"
											logModel.Error = fmt.Sprintf("Failed to update serviceArea: %v", err)
											logModel.Body = serviceArea
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), serviceArea.IdLog, &logModel)
											fmt.Printf("Failed to update serviceArea: %v", err)
											continue
										} else {
											logModel.Operation = "Update"
											logModel.Status = "Done"
											logModel.Collection = "middleware.registers.serviceareas"
											logModel.Error = ""
											logModel.Body = serviceArea
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), serviceArea.IdLog, &logModel)
										}
										if flagChanged {
											go mongo_v2.GetAttentionCollection().UpdateServiceAreasInAttentionLines(context.Background(), oldserviceArea.ID, oldserviceArea)
											go mongo_v2.GetAttentionCollection().UpdateServiceAreasInDiagnostics(context.Background(), oldserviceArea.ID, oldserviceArea)
											go mongo_v2.GetAttentionCollection().UpdateServiceAreasInExamResults(context.Background(), oldserviceArea.ID, oldserviceArea)
											go mongo_v2.GetAttentionCollection().UpdateServiceAreasInExams(context.Background(), oldserviceArea.ID, oldserviceArea)
										}
										fmt.Println("updated: ", oldserviceArea.ID)
									} else {
										status, _ := strconv.Atoi(serviceArea.Status)
										newService := models.ServiceArea{
											Name:       serviceArea.Name,
											Status:     status,
											ExternalId: strconv.Itoa(serviceArea.ID),
										}
										switch serviceArea.ID {
										case 1:
											newService.Code = "OPHTALMOLOGY"
										case 2:
											newService.Code = "GEOGRAPHICALHEIGHT"
										case 3:
											newService.Code = "AUDIOMETRY"
										case 4:
											newService.Code = "HEIGHT"
										case 5:
											newService.Code = "XRAY"
										case 6:
											newService.Code = "PSYCHOLOGY"
										case 7:
											newService.Code = "SPIROMETRY"
										case 8:
											newService.Code = "EKG"
										case 9:
											newService.Code = "DENTISTRY"
										case 10:
											newService.Code = "PATHOLOGICALHISTORY"
										case 11:
											newService.Code = "DRIVINGCERTIFICATION"
										case 12:
											newService.Code = "TRIAGE"
										case 13:
											newService.Code = "MEDICINE"
										case 15:
											newService.Code = "XRAYLD"
										case 18:
											newService.Code = "SKELETALMUSCLE"
										case 19:
											newService.Code = "NEUROLOGY"
										case 21:
											newService.Code = "DIAGNOSTIC"
										case 22:
											newService.Code = "WORKHISTORY"
										case 23:
											newService.Code = "DERMATOLOGY"
										case 24:
											newService.Code = "STRESSTEST"
										case 26:
											newService.Code = "LABORATORY"
										case 27:
											newService.Code = "DOCUMENT"
										case 28:
											newService.Code = "PSYCHOSENSOMETRIC"
										case 29:
											newService.Code = "CONSULTATION"
										case 30:
											newService.Code = "APTITUDE"
										case 31:
											newService.Code = "MEDICALREPORT"
										case 32:
											newService.Code = "NUTRITION"
										case 33:
											newService.Code = "CONFINEDSPACE"
										case 34:
											newService.Code = "PSP"
										case 38:
											newService.Code = "AUXILIARYEXAM"
										case 39:
											newService.Code = "GYNECOLOGY"
										case 40:
											newService.Code = "ULTRASOUND"
										case 41:
											newService.Code = "UROLOGY"
										}
										val, err := mongo.GetServiceAreaCollection().AddServiceArea(context.TODO(), &newService)
										if err != nil {
											logModel.Operation = "Insert"
											logModel.Status = "Error"
											logModel.Collection = "middleware.registers.serviceareas"
											logModel.Error = fmt.Sprintf("Failed to add serviceArea: %v", err)
											logModel.Body = serviceArea
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), serviceArea.IdLog, &logModel)
											fmt.Printf("Failed to add serviceArea: %v", err)
											continue
										} else {
											logModel.Operation = "Insert"
											logModel.Status = "Done"
											logModel.Collection = "middleware.registers.serviceareas"
											logModel.Error = ""
											logModel.Body = serviceArea
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), serviceArea.IdLog, &logModel)
										}
										fmt.Println("created: ", val.ID)
									}
								}
							}
						}(i, chunkSize)
					}
					wg.Wait()
					serviceAreas = []*consumer_models.ServiceAreaConsumer{}
				}
				flag = true
			}
		}
	}

	fmt.Printf("Closing consumer\n")
	c.Close()
}
