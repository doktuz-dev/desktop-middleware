package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/consumer_models"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	"desktop-middleware/models"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	mongo_v2 "desktop-middleware/database/mongo_v2"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/spf13/viper"
)

func main() {
	config.SetConfiguration()
	fmt.Println("Consumer started!")
	topic := viper.GetString("kafka.database") + ".dbmediweb.local"
	fmt.Println(topic)
	// Create Consumer instance
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": viper.GetString("kafka.bootstrap_servers"),
		"sasl.mechanisms":   viper.GetString("kafka.sasl_mechanisms"),
		"security.protocol": viper.GetString("kafka.security_protocol"),
		"sasl.username":     viper.GetString("kafka.sasl_username"),
		"sasl.password":     viper.GetString("kafka.sasl_password"),
		"group.id":          viper.GetString("kafka.groupId"),
		"auto.offset.reset": "earliest",
	})
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	err = c.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	// Set up a channel for handling Ctrl-C, etc
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	// Process messages
	run := true
	flag := true
	var healthCentres []*consumer_models.HealthCentreConsumer
	db := database.InitDatabases()
	defer db.DisconnectDatabases()
	for run {
		var logModel models.Log
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			if flag {
				msg, err := c.ReadMessage(100 * time.Millisecond)
				if err != nil {
					// Errors are informational and automatically handled by the consumer
					flag = false
					continue
				}
				if msg.Value == nil || len(msg.Value) == 0 {
					continue
				}
				var healthCentre consumer_models.HealthCentreConsumer
				logModel.Collection = "middleware.registers.healthcentres"
				logModel.Status = "In Progress"
				logModel.Body = msg.Value
				log, _ := mongo.GetLogsCollection().AddLogs(context.TODO(), &logModel)
				err = json.Unmarshal(msg.Value, &healthCentre)
				if err != nil {
					logModel.Operation = "Decode"
					logModel.Status = "Error"
					logModel.Collection = "middleware.registers.healthcentres"
					logModel.Error = fmt.Sprintf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					json.Unmarshal(msg.Value, &logModel.Body)
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), log.ID, &logModel)
					fmt.Printf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					flag = false
					continue
				}
				healthCentre.IdLog = log.ID
				healthCentres = append(healthCentres, &healthCentre)
				if len(healthCentres) > viper.GetInt("kafka.fetchCount") {
					flag = false
				}
			} else {
				if len(healthCentres) > 0 {
					numCPU := viper.GetInt("kafka.threadCount")
					length := len(healthCentres)
					wg := sync.WaitGroup{}
					chunkSize := (length + numCPU - 1) / numCPU

					for i := 0; i < length; i += chunkSize {

						wg.Add(1)
						go func(i int, chunkSize int) {
							end := i + chunkSize
							defer wg.Done()

							if end > length {
								end = length
							}
							slice := healthCentres[i:end]

							for _, healthCentre := range slice {

								flag, err := strconv.ParseBool(healthCentre.IsDeleted)
								if err != nil {
									logModel.Operation = "Decode"
									logModel.Status = "Error"
									logModel.Collection = "middleware.registers.healthcentres"
									logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
									logModel.Body = healthCentre
									mongo.GetLogsCollection().UpdateLogs(context.TODO(), healthCentre.IdLog, &logModel)
									fmt.Printf("Failed to decode JSON: %v", err)
									continue
								}

								if flag {
									_, err := mongo.GetHealthCentreCollection().DeleteHealthCentreByExternalId(context.TODO(), strconv.Itoa(healthCentre.ID))
									if err != nil {
										logModel.Operation = "Delete"
										logModel.Status = "Error"
										logModel.Collection = "middleware.registers.healthcentres"
										logModel.Error = fmt.Sprintf("Failed to delete healthCentre: %v", err)
										logModel.Body = healthCentre
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), healthCentre.IdLog, &logModel)
										fmt.Printf("Failed to delete healthCentre: %v", err)
									} else {
										logModel.Operation = "Delete"
										logModel.Status = "Done"
										logModel.Collection = "middleware.registers.healthcentres"
										logModel.Error = ""
										logModel.Body = healthCentre
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), healthCentre.IdLog, &logModel)
									}
								} else {
									oldhealthCentre, err := mongo.GetHealthCentreCollection().GetHealthCentreById(context.TODO(), strconv.Itoa(healthCentre.ID))
									if err != nil {
										logModel.Operation = "Get"
										logModel.Status = "Error"
										logModel.Collection = "middleware.registers.healthcentres"
										logModel.Error = fmt.Sprintf("Failed to get JSON: %v", err)
										logModel.Body = healthCentre
										mongo.GetLogsCollection().UpdateLogs(context.TODO(), healthCentre.IdLog, &logModel)
										fmt.Printf("Failed to get healthCentre : %v", err)
										continue
									}

									if oldhealthCentre != nil {
										oldhealthCentre.Name = healthCentre.Name
										oldhealthCentre.Code = oldhealthCentre.ExternalId
										if healthCentre.ID == 2 || healthCentre.ID == 8 {
											oldhealthCentre.Status = 1
										} else {
											oldhealthCentre.Status = 0
										}
										_, err := mongo.GetHealthCentreCollection().UpdateHealthCentre(context.TODO(), oldhealthCentre.ID, oldhealthCentre)
										if err != nil {
											logModel.Operation = "Update"
											logModel.Status = "Error"
											logModel.Collection = "middleware.registers.healthcentres"
											logModel.Error = fmt.Sprintf("Failed to update JSON: %v", err)
											logModel.Body = healthCentre
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), healthCentre.IdLog, &logModel)
											fmt.Printf("Failed to update healthCentre: %v", err)
											continue
										} else {
											logModel.Operation = "Update"
											logModel.Status = "Done"
											logModel.Collection = "middleware.registers.healthcentres"
											logModel.Error = ""
											logModel.Body = healthCentre
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), healthCentre.IdLog, &logModel)
										}
										go mongo_v2.GetAttentionCollection().CustomSetFieldById(context.Background(), "healthCentreId", oldhealthCentre.ID, "healthcentre", oldhealthCentre)
										fmt.Println("updated: ", oldhealthCentre.ID)
									} else {
										newHealthCentre := models.HealthCentre{
											Code:       strconv.Itoa(healthCentre.ID),
											Name:       healthCentre.Name,
											ExternalId: strconv.Itoa(healthCentre.ID),
										}
										if healthCentre.ID == 2 || healthCentre.ID == 8 {
											newHealthCentre.Status = 1
										} else {
											newHealthCentre.Status = 0
										}
										val, err := mongo.GetHealthCentreCollection().AddHealthCentre(context.TODO(), &newHealthCentre)
										if err != nil {
											logModel.Operation = "Insert"
											logModel.Status = "Error"
											logModel.Collection = "middleware.registers.healthcentres"
											logModel.Error = fmt.Sprintf("Failed to add JSON: %v", err)
											logModel.Body = healthCentre
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), healthCentre.IdLog, &logModel)
											fmt.Printf("Failed to add healthCentre: %v", err)
											continue
										} else {
											logModel.Operation = "Insert"
											logModel.Status = "Done"
											logModel.Collection = "middleware.registers.healthcentres"
											logModel.Error = ""
											logModel.Body = healthCentre
											mongo.GetLogsCollection().UpdateLogs(context.TODO(), healthCentre.IdLog, &logModel)
										}
										fmt.Println("created: ", val.ID)
									}
								}
							}
						}(i, chunkSize)
					}
					wg.Wait()
					healthCentres = []*consumer_models.HealthCentreConsumer{}
				}
				flag = true
			}
		}
	}

	fmt.Printf("Closing consumer\n")
	c.Close()
}
