package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/consumer_models"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	mongo_v2 "desktop-middleware/database/mongo_v2"
	"desktop-middleware/functions_v2"
	"desktop-middleware/models"
	"desktop-middleware/models_v2"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func main() {
	config.SetConfiguration()
	fmt.Println("Consumer started!")
	topic := viper.GetString("kafka.database") + ".dbmediweb.analisis_cli_arca"
	fmt.Println(topic)
	// Create Consumer instance
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": viper.GetString("kafka.bootstrap_servers"),
		"sasl.mechanisms":   viper.GetString("kafka.sasl_mechanisms"),
		"security.protocol": viper.GetString("kafka.security_protocol"),
		"sasl.username":     viper.GetString("kafka.sasl_username"),
		"sasl.password":     viper.GetString("kafka.sasl_password"),
		"group.id":          viper.GetString("kafka.groupId"),
		"auto.offset.reset": "latest",
	})
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	err = c.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	// Set up a channel for handling Ctrl-C, etc
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	// Process messages
	run := true
	flag := true
	var laboratorys []*consumer_models.LaboratoryConsumer
	db := database.InitDatabases()
	defer db.DisconnectDatabases()
	for run {
		var logModel models.Log
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			if flag {
				msg, err := c.ReadMessage(100 * time.Millisecond)
				if err != nil {
					// Errors are informational and automatically handled by the consumer
					flag = false
					continue
				}
				if msg.Value == nil || len(msg.Value) == 0 {
					continue
				}
				var laboratory consumer_models.LaboratoryConsumer
				logModel.Collection = "middleware.transactions.attentions.exams"
				logModel.Status = "In Progress"
				logModel.Body = msg.Value
				log, _ := mongo.GetLogsCollection().AddLogs(context.TODO(), &logModel)
				err = json.Unmarshal(msg.Value, &laboratory)
				if err != nil {
					logModel.Operation = "Decode"
					logModel.Status = "Error"
					logModel.Collection = "middleware.transactions.attentions.exams"
					logModel.Error = fmt.Sprintf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					json.Unmarshal(msg.Value, &logModel.Body)
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), log.ID, &logModel)
					fmt.Printf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					flag = false
					continue
				}
				laboratory.IdLog = log.ID
				laboratorys = append(laboratorys, &laboratory)
				if len(laboratorys) > viper.GetInt("kafka.fetchCount") {
					flag = false
				}
			} else {
				if len(laboratorys) > 0 {
					numCPU := viper.GetInt("kafka.threadCount")
					length := len(laboratorys)
					wg := sync.WaitGroup{}
					chunkSize := (length + numCPU - 1) / numCPU

					for i := 0; i < length; i += chunkSize {

						wg.Add(1)
						go func(i int, chunkSize int) {
							end := i + chunkSize
							defer wg.Done()

							if end > length {
								end = length
							}
							slice := laboratorys[i:end]

							for _, laboratory := range slice {
								AddLaboratoriesV2(laboratory, logModel)
							}
						}(i, chunkSize)
					}
					wg.Wait()
					laboratorys = []*consumer_models.LaboratoryConsumer{}
				}
				flag = true
			}
		}
	}

	fmt.Printf("Closing consumer\n")
	c.Close()
}

func HematologyTestBatch(laboratoryConsumer *consumer_models.LaboratoryConsumer, laboratory *models.Exam) *models.Log {
	var logModel models.Log
	test, _ := mongo.GetHematologyTestCollection().GetHematologyTestByExternalId(context.Background(), strconv.Itoa(laboratoryConsumer.ID))
	newTest := laboratoryConsumer.SetHematologyTest(laboratory.AttentionID, laboratory.ServiceAreaID, laboratory.ServiceAreaCode, laboratory.ID)
	if test != nil {
		newTest.ID = test.ID
		newTest.CreatedDate = test.CreatedDate
		_, err := mongo.GetHematologyTestCollection().UpdateHematologyTest(context.Background(), newTest.ID, &newTest)
		if err != nil {
			logModel.Operation = "Update"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.exams.hematologytests"
			logModel.Error = fmt.Sprintf("Failed to add laboratory: %v", err)
			logModel.Body = laboratoryConsumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
			return &logModel
		} else {
			logModel.Operation = "Update"
			logModel.Status = "Done"
			logModel.Collection = "middleware.transactions.attentions.exams.hematologytests"
			logModel.Error = ""
			logModel.Body = laboratoryConsumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
		}
	} else {
		_, err := mongo.GetHematologyTestCollection().AddHematologyTest(context.Background(), &newTest)
		if err != nil {
			logModel.Operation = "Insert"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.exams.hematologytests"
			logModel.Error = fmt.Sprintf("Failed to add laboratory: %v", err)
			logModel.Body = laboratoryConsumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
			return &logModel
		} else {
			logModel.Operation = "Insert"
			logModel.Status = "Done"
			logModel.Collection = "middleware.transactions.attentions.exams.hematologytests"
			logModel.Error = fmt.Sprintf("Failed to add laboratory: %v", err)
			logModel.Body = laboratoryConsumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
		}
	}
	return nil
}

func UrineTestBatch(laboratoryConsumer *consumer_models.LaboratoryConsumer, laboratory *models.Exam) *models.Log {
	var logModel models.Log
	test, _ := mongo.GetUrineTestCollection().GetUrineTestByExternalId(context.Background(), strconv.Itoa(laboratoryConsumer.ID))
	newTest := laboratoryConsumer.SetUrineTest(laboratory.AttentionID, laboratory.ServiceAreaID, laboratory.ServiceAreaCode, laboratory.ID)
	if test != nil {
		newTest.ID = test.ID
		newTest.CreatedDate = test.CreatedDate
		_, err := mongo.GetUrineTestCollection().UpdateUrineTest(context.Background(), newTest.ID, &newTest)
		if err != nil {
			logModel.Operation = "Update"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.exams.urinetests"
			logModel.Error = fmt.Sprintf("Failed to add laboratory: %v", err)
			logModel.Body = laboratoryConsumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
			return &logModel
		} else {
			logModel.Operation = "Update"
			logModel.Status = "Done"
			logModel.Collection = "middleware.transactions.attentions.exams.urinetests"
			logModel.Error = ""
			logModel.Body = laboratoryConsumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
		}
	} else {
		_, err := mongo.GetUrineTestCollection().AddUrineTest(context.Background(), &newTest)
		if err != nil {
			logModel.Operation = "Insert"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.exams.urinetests"
			logModel.Error = fmt.Sprintf("Failed to add laboratory: %v", err)
			logModel.Body = laboratoryConsumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
			return &logModel
		} else {
			logModel.Operation = "Insert"
			logModel.Status = "Done"
			logModel.Collection = "middleware.transactions.attentions.exams.urinetests"
			logModel.Error = ""
			logModel.Body = laboratoryConsumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
		}
	}
	return nil
}

func DrugTestBatch(laboratoryConsumer *consumer_models.LaboratoryConsumer, laboratory *models.Exam) *models.Log {
	var logModel models.Log
	test, _ := mongo.GetDrugTestCollection().GetDrugTestByExternalId(context.Background(), strconv.Itoa(laboratoryConsumer.ID))
	newTest := laboratoryConsumer.SetDrugTest(laboratory.AttentionID, laboratory.ServiceAreaID, laboratory.ServiceAreaCode, laboratory.ID)
	if test != nil {
		newTest.ID = test.ID
		newTest.CreatedDate = test.CreatedDate
		_, err := mongo.GetDrugTestCollection().UpdateDrugTest(context.Background(), newTest.ID, &newTest)
		if err != nil {
			logModel.Operation = "Update"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.exams.drugstests"
			logModel.Error = fmt.Sprintf("Failed to add laboratory: %v", err)
			logModel.Body = laboratoryConsumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
			return &logModel
		} else {
			logModel.Operation = "Update"
			logModel.Status = "Done"
			logModel.Collection = "middleware.transactions.attentions.exams.drugstests"
			logModel.Error = ""
			logModel.Body = laboratoryConsumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
		}
	} else {
		_, err := mongo.GetDrugTestCollection().AddDrugTest(context.Background(), &newTest)
		if err != nil {
			logModel.Operation = "Insert"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.exams.drugstests"
			logModel.Error = fmt.Sprintf("Failed to add laboratory: %v", err)
			logModel.Body = laboratoryConsumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
			return &logModel
		} else {
			logModel.Operation = "Insert"
			logModel.Status = "Done"
			logModel.Collection = "middleware.transactions.attentions.exams.drugstests"
			logModel.Error = ""
			logModel.Body = laboratoryConsumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
		}
	}
	return nil
}

func MicrobiologyTestBatch(laboratoryConsumer *consumer_models.LaboratoryConsumer, laboratory *models.Exam) *models.Log {
	var logModel models.Log
	test, _ := mongo.GetMicrobiologyTestCollection().GetMicrobiologyTestByExternalId(context.Background(), strconv.Itoa(laboratoryConsumer.ID))
	newTest := laboratoryConsumer.SetMicrobiologyTest(laboratory.AttentionID, laboratory.ServiceAreaID, laboratory.ServiceAreaCode, laboratory.ID)
	if test != nil {
		newTest.ID = test.ID
		newTest.CreatedDate = test.CreatedDate
		_, err := mongo.GetMicrobiologyTestCollection().UpdateMicrobiologyTest(context.Background(), newTest.ID, &newTest)
		if err != nil {
			logModel.Operation = "Update"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.exams.microbiologytests"
			logModel.Error = fmt.Sprintf("Failed to add laboratory: %v", err)
			logModel.Body = laboratoryConsumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
			return &logModel
		} else {
			logModel.Operation = "Update"
			logModel.Status = "Done"
			logModel.Collection = "middleware.transactions.attentions.exams.microbiologytests"
			logModel.Error = ""
			logModel.Body = laboratoryConsumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
		}
	} else {
		_, err := mongo.GetMicrobiologyTestCollection().AddMicrobiologyTest(context.Background(), &newTest)
		if err != nil {
			logModel.Operation = "Insert"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.exams.microbiologytests"
			logModel.Error = fmt.Sprintf("Failed to add laboratory: %v", err)
			logModel.Body = laboratoryConsumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
			return &logModel
		} else {
			logModel.Operation = "Insert"
			logModel.Status = "Done"
			logModel.Collection = "middleware.transactions.attentions.exams.microbiologytests"
			logModel.Error = ""
			logModel.Body = laboratoryConsumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
		}
	}
	return nil
}

func BiochemistryTestBatch(laboratoryConsumer *consumer_models.LaboratoryConsumer, laboratory *models.Exam) *models.Log {
	var logModel models.Log
	test, _ := mongo.GetBiochemistryTestCollection().GetBiochemistryTestByExternalId(context.Background(), strconv.Itoa(laboratoryConsumer.ID))
	newTest := laboratoryConsumer.SetBiochemistryTest(laboratory.AttentionID, laboratory.ServiceAreaID, laboratory.ServiceAreaCode, laboratory.ID)
	if test != nil {
		newTest.ID = test.ID
		newTest.CreatedDate = test.CreatedDate
		_, err := mongo.GetBiochemistryTestCollection().UpdateBiochemistryTest(context.Background(), newTest.ID, &newTest)
		if err != nil {
			logModel.Operation = "Update"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.exams.biochemistrytests"
			logModel.Error = fmt.Sprintf("Failed to add laboratory: %v", err)
			logModel.Body = laboratoryConsumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
			return &logModel
		} else {
			logModel.Operation = "Update"
			logModel.Status = "Done"
			logModel.Collection = "middleware.transactions.attentions.exams.biochemistrytests"
			logModel.Error = ""
			logModel.Body = laboratoryConsumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
		}
	} else {
		_, err := mongo.GetBiochemistryTestCollection().AddBiochemistryTest(context.Background(), &newTest)
		if err != nil {
			logModel.Operation = "Insert"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.exams.biochemistrytests"
			logModel.Error = fmt.Sprintf("Failed to add laboratory: %v", err)
			logModel.Body = laboratoryConsumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
			return &logModel
		} else {
			logModel.Operation = "Insert"
			logModel.Status = "Done"
			logModel.Collection = "middleware.transactions.attentions.exams.biochemistrytests"
			logModel.Error = ""
			logModel.Body = laboratoryConsumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
		}
	}
	return nil
}

func ImmunologyTestBatch(laboratoryConsumer *consumer_models.LaboratoryConsumer, laboratory *models.Exam) *models.Log {
	var logModel models.Log
	test, _ := mongo.GetImmunologyTestCollection().GetImmunologyTestByExternalId(context.Background(), strconv.Itoa(laboratoryConsumer.ID))
	newTest := laboratoryConsumer.SetImmunologyTest(laboratory.AttentionID, laboratory.ServiceAreaID, laboratory.ServiceAreaCode, laboratory.ID)
	if test != nil {
		newTest.ID = test.ID
		newTest.CreatedDate = test.CreatedDate
		_, err := mongo.GetImmunologyTestCollection().UpdateImmunologyTest(context.Background(), newTest.ID, &newTest)
		if err != nil {
			logModel.Operation = "Update"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.exams.immunologytests"
			logModel.Error = fmt.Sprintf("Failed to add laboratory: %v", err)
			logModel.Body = laboratoryConsumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
			return &logModel
		} else {
			logModel.Operation = "Update"
			logModel.Status = "Doner"
			logModel.Collection = "middleware.transactions.attentions.exams.immunologytests"
			logModel.Error = ""
			logModel.Body = laboratoryConsumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
		}
	} else {
		_, err := mongo.GetImmunologyTestCollection().AddImmunologyTest(context.Background(), &newTest)
		if err != nil {
			logModel.Operation = "Insert"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.exams.immunologytests"
			logModel.Error = fmt.Sprintf("Failed to add laboratory: %v", err)
			logModel.Body = laboratoryConsumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
			return &logModel
		} else {
			logModel.Operation = "Insert"
			logModel.Status = "Done"
			logModel.Collection = "middleware.transactions.attentions.exams.immunologytests"
			logModel.Error = ""
			logModel.Body = laboratoryConsumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
		}
	}
	return nil
}

func CovidTestBatch(laboratoryConsumer *consumer_models.LaboratoryConsumer, laboratory *models.Exam) *models.Log {
	var logModel models.Log
	test, _ := mongo.GetCovidTestCollection().GetCovidTestByExternalId(context.Background(), strconv.Itoa(laboratoryConsumer.ID))
	newTest := laboratoryConsumer.SetCovidTest(laboratory.AttentionID, laboratory.ServiceAreaID, laboratory.ServiceAreaCode, laboratory.ID)
	if test != nil {
		newTest.ID = test.ID
		newTest.CreatedDate = test.CreatedDate
		_, err := mongo.GetCovidTestCollection().UpdateCovidTest(context.Background(), newTest.ID, &newTest)
		if err != nil {
			logModel.Operation = "Update"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.exams.covidtests"
			logModel.Error = fmt.Sprintf("Failed to add laboratory: %v", err)
			logModel.Body = laboratoryConsumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
			return &logModel
		} else {
			logModel.Operation = "Update"
			logModel.Status = "Done"
			logModel.Collection = "middleware.transactions.attentions.exams.covidtests"
			logModel.Error = ""
			logModel.Body = laboratoryConsumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
		}
	} else {
		_, err := mongo.GetCovidTestCollection().AddCovidTest(context.Background(), &newTest)
		if err != nil {
			logModel.Operation = "Insert"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.exams.covidtests"
			logModel.Error = fmt.Sprintf("Failed to add laboratory: %v", err)
			logModel.Body = laboratoryConsumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
			return &logModel
		} else {
			logModel.Operation = "Insert"
			logModel.Status = "Done"
			logModel.Collection = "middleware.transactions.attentions.exams.covidtests"
			logModel.Error = ""
			logModel.Body = laboratoryConsumer
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
		}
	}
	return nil
}

func AddLaboratoriesV2(laboratory *consumer_models.LaboratoryConsumer, logModel models.Log) {

	flag, err := strconv.ParseBool(laboratory.IsDeleted)
	if err != nil {
		logModel.Operation = "Decode"
		logModel.Status = "Error"
		logModel.Collection = "middleware.transactions.attentions.exams"
		logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
		logModel.Body = laboratory
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratory.IdLog, &logModel)
		fmt.Printf("Failed to decode JSON: %v", err)
		return
	}
	serv, err := mongo_v2.GetServiceAreaCollection().GetServiceAreaByExternalId(context.Background(), "26")
	if err != nil {
		logModel.Operation = "Get"
		logModel.Status = "Error"
		logModel.Collection = "middleware.registers.serviceareas"
		logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
		logModel.Body = laboratory
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratory.IdLog, &logModel)
		fmt.Printf("Failed to decode JSON: %v", err)
		return
	}
	atte, err := mongo_v2.GetAttentionCollection().GetAttentionByExternalId(context.Background(), strconv.Itoa(laboratory.AttentionId))
	if err != nil {
		logModel.Operation = "Get"
		logModel.Status = "Error"
		logModel.Collection = "middleware.transactions.attentions"
		logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
		logModel.Body = laboratory
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratory.IdLog, &logModel)
		fmt.Printf("Failed to decode JSON: %v", err)
		return
	}
	if atte == nil {
		return
	}
	attentionId := atte.ID

	if flag || laboratory.Status == "0" {
		_, err = mongo_v2.GetAttentionCollection().PullBodyFromExamsByExternalId(context.TODO(), attentionId, strconv.Itoa(laboratory.ID), serv.ID)
		if err != nil {
			logModel.Operation = "Delete"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.exams"
			logModel.Error = fmt.Sprintf("Failed to delete laboratory: %v", err)
			logModel.Body = laboratory
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratory.IdLog, &logModel)
			fmt.Printf("Failed to delete laboratory: %v", err)

		} else {
			logModel.Operation = "Delete"
			logModel.Status = "Done"
			logModel.Collection = "middleware.transactions.attentions.exams"
			logModel.Error = ""
			logModel.Body = laboratory
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratory.IdLog, &logModel)
		}
		_, err = mongo_v2.GetAttentionCollection().UnsetFieldByExternalId(context.TODO(), strconv.Itoa(laboratory.AttentionId), "biochemistryTests")
		if err != nil {
			logModel.Operation = "Delete"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.exams.biochemistrytests"
			logModel.Error = fmt.Sprintf("Failed to delete laboratory: %v", err)
			logModel.Body = laboratory
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratory.IdLog, &logModel)
			fmt.Printf("Failed to delete laboratory: %v", err)

		} else {
			logModel.Operation = "Delete"
			logModel.Status = "Done"
			logModel.Collection = "middleware.transactions.attentions.exams.biochemistrytests"
			logModel.Error = ""
			logModel.Body = laboratory
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratory.IdLog, &logModel)
		}
		_, err = mongo_v2.GetAttentionCollection().UnsetFieldByExternalId(context.TODO(), strconv.Itoa(laboratory.AttentionId), "covidTests")
		if err != nil {
			logModel.Operation = "Delete"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.exams.covidtests"
			logModel.Error = fmt.Sprintf("Failed to delete laboratory: %v", err)
			logModel.Body = laboratory
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratory.IdLog, &logModel)
			fmt.Printf("Failed to delete laboratory: %v", err)

		} else {
			logModel.Operation = "Delete"
			logModel.Status = "Done"
			logModel.Collection = "middleware.transactions.attentions.exams.covidtests"
			logModel.Error = ""
			logModel.Body = laboratory
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratory.IdLog, &logModel)
		}
		_, err = mongo_v2.GetAttentionCollection().UnsetFieldByExternalId(context.TODO(), strconv.Itoa(laboratory.AttentionId), "drugTests")
		if err != nil {
			logModel.Operation = "Delete"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.exams.drugtests"
			logModel.Error = fmt.Sprintf("Failed to delete laboratory: %v", err)
			logModel.Body = laboratory
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratory.IdLog, &logModel)
			fmt.Printf("Failed to delete laboratory: %v", err)

		} else {
			logModel.Operation = "Delete"
			logModel.Status = "Done"
			logModel.Collection = "middleware.transactions.attentions.exams.drugtests"
			logModel.Error = ""
			logModel.Body = laboratory
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratory.IdLog, &logModel)
		}
		_, err = mongo_v2.GetAttentionCollection().UnsetFieldByExternalId(context.TODO(), strconv.Itoa(laboratory.AttentionId), "hematologyTests")
		if err != nil {
			logModel.Operation = "Delete"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.exams.hematologytests"
			logModel.Error = fmt.Sprintf("Failed to delete laboratory: %v", err)
			logModel.Body = laboratory
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratory.IdLog, &logModel)
			fmt.Printf("Failed to delete laboratory: %v", err)

		} else {
			logModel.Operation = "Delete"
			logModel.Status = "Done"
			logModel.Collection = "middleware.transactions.attentions.exams.hematologytests"
			logModel.Error = ""
			logModel.Body = laboratory
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratory.IdLog, &logModel)
		}
		_, err = mongo_v2.GetAttentionCollection().UnsetFieldByExternalId(context.TODO(), strconv.Itoa(laboratory.AttentionId), "immunologyTests")
		if err != nil {
			logModel.Operation = "Delete"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.exams.immunologytests"
			logModel.Error = fmt.Sprintf("Failed to delete laboratory: %v", err)
			logModel.Body = laboratory
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratory.IdLog, &logModel)
			fmt.Printf("Failed to delete laboratory: %v", err)

		} else {
			logModel.Operation = "Delete"
			logModel.Status = "Done"
			logModel.Collection = "middleware.transactions.attentions.exams.immunologytests"
			logModel.Error = ""
			logModel.Body = laboratory
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratory.IdLog, &logModel)
		}
		_, err = mongo_v2.GetAttentionCollection().UnsetFieldByExternalId(context.TODO(), strconv.Itoa(laboratory.AttentionId), "microbiologyTests")
		if err != nil {
			logModel.Operation = "Delete"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.exams.microbiologytests"
			logModel.Error = fmt.Sprintf("Failed to delete laboratory: %v", err)
			logModel.Body = laboratory
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratory.IdLog, &logModel)
			fmt.Printf("Failed to delete laboratory: %v", err)

		} else {
			logModel.Operation = "Delete"
			logModel.Status = "Done"
			logModel.Collection = "middleware.transactions.attentions.exams.microbiologytests"
			logModel.Error = ""
			logModel.Body = laboratory
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratory.IdLog, &logModel)
		}
		_, err = mongo_v2.GetAttentionCollection().UnsetFieldByExternalId(context.TODO(), strconv.Itoa(laboratory.AttentionId), "urineTests")
		if err != nil {
			logModel.Operation = "Delete"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.exams.urinetests"
			logModel.Error = fmt.Sprintf("Failed to delete laboratory: %v", err)
			logModel.Body = laboratory
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratory.IdLog, &logModel)
			fmt.Printf("Failed to delete laboratory: %v", err)
		} else {
			logModel.Operation = "Delete"
			logModel.Status = "Done"
			logModel.Collection = "middleware.transactions.attentions.exams.urinetests"
			logModel.Error = ""
			logModel.Body = laboratory
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratory.IdLog, &logModel)
		}

		mongo_v2.GetAttentionCollection().PullDiagnosticsByExternalId(context.Background(), attentionId, serv.ID)
	} else {

		var oldlaboratory *models_v2.Exam

		for _, body := range atte.Exams {
			if body.ExternalId == strconv.Itoa(laboratory.ID) && body.ServiceAreaID == serv.ID {
				oldlaboratory = body
			}
		}

		if oldlaboratory != nil {
			CreateLaboratoryV2(atte, serv, attentionId, laboratory, oldlaboratory, logModel)
			fmt.Println("updated: ", oldlaboratory.ID)
		} else {
			var newLaboratory models_v2.Exam
			CreateLaboratoryV2(atte, serv, attentionId, laboratory, &newLaboratory, logModel)
			fmt.Println("created: ", newLaboratory.ID)
		}
	}
	if atte.PersonId != nil && atte.CustomerId != nil {
		if *atte.AttentionTypeName == "Ocupacional" {
			lastEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *atte.PersonId, *atte.CustomerId, "emo")
			if lastEmo != nil {
				mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *atte.PersonId, "lastEmoAttention", lastEmo)
			}
			lastAnyEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *atte.PersonId, *atte.CustomerId, "any")
			if lastAnyEmo != nil {
				lastAnyEmo.Customer = nil
				lastAnyEmo.Patient = nil
				lastAnyEmo.Person = nil
				lastAnyEmo.Consultations = nil
				lastAnyEmo.AttentionLines = nil
				lastAnyEmo.Exams = nil
				lastAnyEmo.AudiometryTests = nil
				lastAnyEmo.BiochemistryTests = nil
				lastAnyEmo.CovidTests = nil
				lastAnyEmo.Diagnostics = nil
				lastAnyEmo.DrugTests = nil
				lastAnyEmo.HematologyTests = nil
				lastAnyEmo.ImmunologyTests = nil
				lastAnyEmo.MicrobiologyTests = nil
				lastAnyEmo.OphtalmologyTests = nil
				lastAnyEmo.PsychologyTests = nil
				lastAnyEmo.Recommendations = nil
				lastAnyEmo.ExamResults = nil
				lastAnyEmo.SkeletalMuscleTests = nil
				lastAnyEmo.UrineTests = nil
				lastAnyEmo.MedicalHistories = nil
				lastAnyEmo.PathologicalHistories = nil
				lastAnyEmo.Triages = nil
				lastAnyEmo.WorkHistories = nil
				lastAnyEmo.SpirometryTests = nil
				mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *atte.PersonId, "lastAnyEmoAttention", lastAnyEmo)
			}
		}
		if *atte.AttentionTypeName == "Asistencial" {
			lastNoEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *atte.PersonId, *atte.CustomerId, "no_emo")
			if lastNoEmo != nil {
				mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *atte.PersonId, "lastNoEmoAttention", lastNoEmo)
			}
		}
	}
}

func CreateLaboratoryV2(atte *models_v2.Attention, serv *models_v2.ServiceArea, attentionId string, laboratory *consumer_models.LaboratoryConsumer, exam *models_v2.Exam, logModel models.Log) {
	if exam.ID == "" {
		exam.ID = primitive.NewObjectID().Hex()
		exam.ExternalId = strconv.Itoa(laboratory.ID)
		exam.ExternalSystem = "MEDIWEB"
		exam.CreatedDate = time.Now()
		exam.LastUpdatedDate = time.Now()
	}
	exam.AttentionID = attentionId
	exam.ServiceAreaID = serv.ID
	exam.ServiceAreaCode = serv.Code

	if laboratory.Reviewed != nil && *laboratory.Reviewed == "ON" {
		exam.Reviewed = true
	} else {
		exam.Reviewed = false
	}
	exam.ServiceArea = serv
	status, _ := strconv.Atoi(laboratory.Status)
	exam.Status = status
	exam.LastUpdatedDate = time.Now()

	mongo_v2.GetAttentionCollection().PullBodyFromExamsByExternalId(context.TODO(), attentionId, strconv.Itoa(laboratory.ID), serv.ID)
	_, err := mongo_v2.GetAttentionCollection().PushBodyToExamsByExternalId(context.TODO(), attentionId, exam)
	if err != nil {
		logModel.Operation = "Update"
		logModel.Status = "Error"
		logModel.Collection = "middleware.transactions.attentions.exams"
		logModel.Error = fmt.Sprintf("Failed to update laboratory: %v", err)
		logModel.Body = laboratory
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratory.IdLog, &logModel)
		fmt.Printf("Failed to update laboratory: %v", err)
	} else {
		logModel.Operation = "Update"
		logModel.Status = "Done"
		logModel.Collection = "middleware.transactions.attentions.exams"
		logModel.Error = ""
		logModel.Body = laboratory
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratory.IdLog, &logModel)
	}

	mongo_v2.GetAttentionCollection().PullDiagnosticsByExternalId(context.Background(), attentionId, serv.ID)

	diagnostics := functions_v2.GetDiagnosticsFromLaboratoryConsumer(exam.ID, exam.AttentionID, exam.ServiceAreaID, exam.ServiceAreaCode, laboratory, serv)
	if len(diagnostics) > 0 {
		_, err := mongo_v2.GetAttentionCollection().PushDiagnosticsByExternalId(context.Background(), attentionId, diagnostics)
		if err != nil {
			logModel.Operation = "Insert"
			logModel.Status = "Error"
			logModel.Collection = "middleware.transactions.attentions.exams.diagnostics"
			logModel.Error = fmt.Sprintf("Failed to insert laboratory: %v", err)
			logModel.Body = laboratory
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratory.IdLog, &logModel)
			fmt.Printf("Failed to add laboratory: %v", err)
		} else {
			logModel.Operation = "Insert"
			logModel.Status = "Done"
			logModel.Body = laboratory
			logModel.Collection = "middleware.transactions.attentions.exams.diagnostics"
			logModel.Error = ""
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratory.IdLog, &logModel)

		}
	}

	errmodels := HematologyTestBatchV2(laboratory, exam, atte.HematologyTests)
	if errmodels != nil {
		return
	}
	errmodels = UrineTestBatchV2(laboratory, exam, atte.UrineTests)
	if errmodels != nil {
		return
	}
	errmodels = DrugTestBatchV2(laboratory, exam, atte.DrugTests)
	if errmodels != nil {
		return
	}
	errmodels = MicrobiologyTestBatchV2(laboratory, exam, atte.MicrobiologyTests)
	if errmodels != nil {
		return
	}
	errmodels = BiochemistryTestBatchV2(laboratory, exam, atte.BiochemistryTests)
	if errmodels != nil {
		return
	}
	errmodels = ImmunologyTestBatchV2(laboratory, exam, atte.ImmunologyTests)
	if errmodels != nil {
		return
	}
	errmodels = CovidTestBatchV2(laboratory, exam, atte.CovidTests)
	if errmodels != nil {
		return
	}
}

func HematologyTestBatchV2(laboratoryConsumer *consumer_models.LaboratoryConsumer, exam *models_v2.Exam, test *models_v2.HematologyTest) *models.Log {
	var logModel models.Log
	newTest := laboratoryConsumer.SetHematologyTest(exam.AttentionID, exam.ServiceAreaID, exam.ServiceAreaCode, exam.ID)
	newTest.AttentionId = exam.AttentionID
	newTest.ServiceAreaID = exam.ServiceAreaID
	newTest.ServiceAreaCode = exam.ServiceAreaCode
	newTest.ExamID = exam.ID

	if test != nil {
		newTest.ID = test.ID
		newTest.CreatedDate = test.CreatedDate
		newTest.LastUpdatedDate = time.Now()
	} else {
		newTest.ID = primitive.NewObjectID().Hex()
		newTest.Status = exam.Status
		newTest.ExternalId = strconv.Itoa(laboratoryConsumer.ID)
		newTest.ExternalSystem = "MEDIWEB"
		newTest.CreatedDate = time.Now()
		newTest.LastUpdatedDate = time.Now()
	}
	_, err := mongo_v2.GetAttentionCollection().SetFieldByID(context.Background(), exam.AttentionID, "hematologyTests", newTest)
	if err != nil {
		logModel.Operation = "Update"
		logModel.Status = "Error"
		logModel.Collection = "middleware.transactions.attentions.exams.laboratorytests"
		logModel.Error = fmt.Sprintf("Failed to add laboratory: %v", err)
		logModel.Body = laboratoryConsumer
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
		fmt.Printf("Failed to add laboratory: %v", err)
	} else {
		logModel.Operation = "Update"
		logModel.Status = "Done"
		logModel.Collection = "middleware.transactions.attentions.exams.laboratorytests"
		logModel.Error = ""
		logModel.Body = laboratoryConsumer
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
	}
	return nil
}

func UrineTestBatchV2(laboratoryConsumer *consumer_models.LaboratoryConsumer, exam *models_v2.Exam, test *models_v2.UrineTest) *models.Log {
	var logModel models.Log
	newTest := laboratoryConsumer.SetUrineTest(exam.AttentionID, exam.ServiceAreaID, exam.ServiceAreaCode, exam.ID)
	newTest.AttentionId = exam.AttentionID
	newTest.ServiceAreaID = exam.ServiceAreaID
	newTest.ServiceAreaCode = exam.ServiceAreaCode
	newTest.ExamID = exam.ID

	if test != nil {
		newTest.ID = test.ID
		newTest.CreatedDate = test.CreatedDate
		newTest.LastUpdatedDate = time.Now()
	} else {
		newTest.ID = primitive.NewObjectID().Hex()
		newTest.Status = exam.Status
		newTest.ExternalId = strconv.Itoa(laboratoryConsumer.ID)
		newTest.ExternalSystem = "MEDIWEB"
		newTest.CreatedDate = time.Now()
		newTest.LastUpdatedDate = time.Now()
	}
	_, err := mongo_v2.GetAttentionCollection().SetFieldByID(context.Background(), exam.AttentionID, "urineTests", newTest)
	if err != nil {
		logModel.Operation = "Update"
		logModel.Status = "Error"
		logModel.Collection = "middleware.transactions.attentions.exams.laboratorytests"
		logModel.Error = fmt.Sprintf("Failed to add laboratory: %v", err)
		logModel.Body = laboratoryConsumer
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
		fmt.Printf("Failed to add laboratory: %v", err)
	} else {
		logModel.Operation = "Update"
		logModel.Status = "Done"
		logModel.Collection = "middleware.transactions.attentions.exams.laboratorytests"
		logModel.Error = ""
		logModel.Body = laboratoryConsumer
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
	}
	return nil
}

func DrugTestBatchV2(laboratoryConsumer *consumer_models.LaboratoryConsumer, exam *models_v2.Exam, test *models_v2.DrugTest) *models.Log {
	var logModel models.Log
	newTest := laboratoryConsumer.SetDrugTest(exam.AttentionID, exam.ServiceAreaID, exam.ServiceAreaCode, exam.ID)
	newTest.AttentionId = exam.AttentionID
	newTest.ServiceAreaID = exam.ServiceAreaID
	newTest.ServiceAreaCode = exam.ServiceAreaCode
	newTest.ExamID = exam.ID

	if test != nil {
		newTest.ID = test.ID
		newTest.CreatedDate = test.CreatedDate
		newTest.LastUpdatedDate = time.Now()
	} else {
		newTest.ID = primitive.NewObjectID().Hex()
		newTest.Status = exam.Status
		newTest.ExternalId = strconv.Itoa(laboratoryConsumer.ID)
		newTest.ExternalSystem = "MEDIWEB"
		newTest.CreatedDate = time.Now()
		newTest.LastUpdatedDate = time.Now()
	}
	_, err := mongo_v2.GetAttentionCollection().SetFieldByID(context.Background(), exam.AttentionID, "drugTests", newTest)
	if err != nil {
		logModel.Operation = "Update"
		logModel.Status = "Error"
		logModel.Collection = "middleware.transactions.attentions.exams.laboratorytests"
		logModel.Error = fmt.Sprintf("Failed to add laboratory: %v", err)
		logModel.Body = laboratoryConsumer
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
		fmt.Printf("Failed to add laboratory: %v", err)
	} else {
		logModel.Operation = "Update"
		logModel.Status = "Done"
		logModel.Collection = "middleware.transactions.attentions.exams.laboratorytests"
		logModel.Error = ""
		logModel.Body = laboratoryConsumer
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
	}
	return nil
}

func MicrobiologyTestBatchV2(laboratoryConsumer *consumer_models.LaboratoryConsumer, exam *models_v2.Exam, test *models_v2.MicrobiologyTest) *models.Log {
	var logModel models.Log
	newTest := laboratoryConsumer.SetMicrobiologyTest(exam.AttentionID, exam.ServiceAreaID, exam.ServiceAreaCode, exam.ID)
	newTest.AttentionId = exam.AttentionID
	newTest.ServiceAreaID = exam.ServiceAreaID
	newTest.ServiceAreaCode = exam.ServiceAreaCode
	newTest.ExamID = exam.ID

	if test != nil {
		newTest.ID = test.ID
		newTest.CreatedDate = test.CreatedDate
		newTest.LastUpdatedDate = time.Now()
	} else {
		newTest.ID = primitive.NewObjectID().Hex()
		newTest.Status = exam.Status
		newTest.ExternalId = strconv.Itoa(laboratoryConsumer.ID)
		newTest.ExternalSystem = "MEDIWEB"
		newTest.CreatedDate = time.Now()
		newTest.LastUpdatedDate = time.Now()
	}
	_, err := mongo_v2.GetAttentionCollection().SetFieldByID(context.Background(), exam.AttentionID, "microbiologyTests", newTest)
	if err != nil {
		logModel.Operation = "Update"
		logModel.Status = "Error"
		logModel.Collection = "middleware.transactions.attentions.exams.laboratorytests"
		logModel.Error = fmt.Sprintf("Failed to add laboratory: %v", err)
		logModel.Body = laboratoryConsumer
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
		fmt.Printf("Failed to add laboratory: %v", err)
	} else {
		logModel.Operation = "Update"
		logModel.Status = "Done"
		logModel.Collection = "middleware.transactions.attentions.exams.laboratorytests"
		logModel.Error = ""
		logModel.Body = laboratoryConsumer
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
	}
	return nil
}

func BiochemistryTestBatchV2(laboratoryConsumer *consumer_models.LaboratoryConsumer, exam *models_v2.Exam, test *models_v2.BiochemistryTest) *models.Log {
	var logModel models.Log
	newTest := laboratoryConsumer.SetBiochemistryTest(exam.AttentionID, exam.ServiceAreaID, exam.ServiceAreaCode, exam.ID)
	newTest.AttentionId = exam.AttentionID
	newTest.ServiceAreaID = exam.ServiceAreaID
	newTest.ServiceAreaCode = exam.ServiceAreaCode
	newTest.ExamID = exam.ID

	if test != nil {
		newTest.ID = test.ID
		newTest.CreatedDate = test.CreatedDate
		newTest.LastUpdatedDate = time.Now()
	} else {
		newTest.ID = primitive.NewObjectID().Hex()
		newTest.Status = exam.Status
		newTest.ExternalId = strconv.Itoa(laboratoryConsumer.ID)
		newTest.ExternalSystem = "MEDIWEB"
		newTest.CreatedDate = time.Now()
		newTest.LastUpdatedDate = time.Now()
	}
	_, err := mongo_v2.GetAttentionCollection().SetFieldByID(context.Background(), exam.AttentionID, "biochemistryTests", newTest)
	if err != nil {
		logModel.Operation = "Update"
		logModel.Status = "Error"
		logModel.Collection = "middleware.transactions.attentions.exams.laboratorytests"
		logModel.Error = fmt.Sprintf("Failed to add laboratory: %v", err)
		logModel.Body = laboratoryConsumer
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
		fmt.Printf("Failed to add laboratory: %v", err)
	} else {
		logModel.Operation = "Update"
		logModel.Status = "Done"
		logModel.Collection = "middleware.transactions.attentions.exams.laboratorytests"
		logModel.Error = ""
		logModel.Body = laboratoryConsumer
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
	}
	return nil
}

func ImmunologyTestBatchV2(laboratoryConsumer *consumer_models.LaboratoryConsumer, exam *models_v2.Exam, test *models_v2.ImmunologyTest) *models.Log {
	var logModel models.Log
	newTest := laboratoryConsumer.SetImmunologyTest(exam.AttentionID, exam.ServiceAreaID, exam.ServiceAreaCode, exam.ID)
	newTest.AttentionId = exam.AttentionID
	newTest.ServiceAreaID = exam.ServiceAreaID
	newTest.ServiceAreaCode = exam.ServiceAreaCode
	newTest.ExamID = exam.ID

	if test != nil {
		newTest.ID = test.ID
		newTest.CreatedDate = test.CreatedDate
		newTest.LastUpdatedDate = time.Now()
	} else {
		newTest.ID = primitive.NewObjectID().Hex()
		newTest.Status = exam.Status
		newTest.ExternalId = strconv.Itoa(laboratoryConsumer.ID)
		newTest.ExternalSystem = "MEDIWEB"
		newTest.CreatedDate = time.Now()
		newTest.LastUpdatedDate = time.Now()
	}
	_, err := mongo_v2.GetAttentionCollection().SetFieldByID(context.Background(), exam.AttentionID, "immunologyTests", newTest)
	if err != nil {
		logModel.Operation = "Update"
		logModel.Status = "Error"
		logModel.Collection = "middleware.transactions.attentions.exams.laboratorytests"
		logModel.Error = fmt.Sprintf("Failed to add laboratory: %v", err)
		logModel.Body = laboratoryConsumer
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
		fmt.Printf("Failed to add laboratory: %v", err)
	} else {
		logModel.Operation = "Update"
		logModel.Status = "Done"
		logModel.Collection = "middleware.transactions.attentions.exams.laboratorytests"
		logModel.Error = ""
		logModel.Body = laboratoryConsumer
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
	}
	return nil
}

func CovidTestBatchV2(laboratoryConsumer *consumer_models.LaboratoryConsumer, exam *models_v2.Exam, test *models_v2.CovidTest) *models.Log {
	var logModel models.Log
	newTest := laboratoryConsumer.SetCovidTest(exam.AttentionID, exam.ServiceAreaID, exam.ServiceAreaCode, exam.ID)
	newTest.AttentionId = exam.AttentionID
	newTest.ServiceAreaID = exam.ServiceAreaID
	newTest.ServiceAreaCode = exam.ServiceAreaCode
	newTest.ExamID = exam.ID

	if test != nil {
		newTest.ID = test.ID
		newTest.CreatedDate = test.CreatedDate
		newTest.LastUpdatedDate = time.Now()
	} else {
		newTest.ID = primitive.NewObjectID().Hex()
		newTest.Status = exam.Status
		newTest.ExternalId = strconv.Itoa(laboratoryConsumer.ID)
		newTest.ExternalSystem = "MEDIWEB"
		newTest.CreatedDate = time.Now()
		newTest.LastUpdatedDate = time.Now()
	}
	_, err := mongo_v2.GetAttentionCollection().SetFieldByID(context.Background(), exam.AttentionID, "covidTests", newTest)
	if err != nil {
		logModel.Operation = "Update"
		logModel.Status = "Error"
		logModel.Collection = "middleware.transactions.attentions.exams.laboratorytests"
		logModel.Error = fmt.Sprintf("Failed to add laboratory: %v", err)
		logModel.Body = laboratoryConsumer
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
		fmt.Printf("Failed to add laboratory: %v", err)
	} else {
		logModel.Operation = "Update"
		logModel.Status = "Done"
		logModel.Collection = "middleware.transactions.attentions.exams.laboratorytests"
		logModel.Error = ""
		logModel.Body = laboratoryConsumer
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), laboratoryConsumer.IdLog, &logModel)
	}
	return nil
}
