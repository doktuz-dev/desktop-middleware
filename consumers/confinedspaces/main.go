package main

import (
	"context"
	config "desktop-middleware/configuration"
	"desktop-middleware/consumer_models"
	"desktop-middleware/database"
	"desktop-middleware/database/mongo"
	mongo_v2 "desktop-middleware/database/mongo_v2"
	"desktop-middleware/models"
	"desktop-middleware/models_v2"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func main() {
	config.SetConfiguration()
	fmt.Println("Consumer started!")
	topic := viper.GetString("kafka.database") + ".dbmediweb.espacio_confi"
	fmt.Println(topic)
	// Create Consumer instance
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": viper.GetString("kafka.bootstrap_servers"),
		"sasl.mechanisms":   viper.GetString("kafka.sasl_mechanisms"),
		"security.protocol": viper.GetString("kafka.security_protocol"),
		"sasl.username":     viper.GetString("kafka.sasl_username"),
		"sasl.password":     viper.GetString("kafka.sasl_password"),
		"group.id":          viper.GetString("kafka.groupId"),
		"auto.offset.reset": "earliest",
	})
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	err = c.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		fmt.Printf("Failed to create consumer: %s", err)
		os.Exit(1)
	}
	// Set up a channel for handling Ctrl-C, etc
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	// Process messages
	run := true
	flag := true
	var confinedSpaces []*consumer_models.ConfinedSpacesConsumer
	db := database.InitDatabases()
	defer db.DisconnectDatabases()
	for run {
		var logModel models.Log
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			if flag {
				msg, err := c.ReadMessage(100 * time.Millisecond)
				if err != nil {
					// Errors are informational and automatically handled by the consumer
					flag = false
					continue
				}
				if msg.Value == nil || len(msg.Value) == 0 {
					continue
				}
				var confinedSpace consumer_models.ConfinedSpacesConsumer
				logModel.Collection = "middleware.transactions.attentions.exams"

				logModel.Status = "In Progress"
				logModel.Body = msg.Value
				log, _ := mongo.GetLogsCollection().AddLogs(context.TODO(), &logModel)
				err = json.Unmarshal(msg.Value, &confinedSpace)
				if err != nil {
					logModel.Operation = "Decode"
					logModel.Status = "Error"
					logModel.Collection = "middleware.transactions.attentions.exams"
					logModel.Error = fmt.Sprintf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					json.Unmarshal(msg.Value, &logModel.Body)
					mongo.GetLogsCollection().UpdateLogs(context.TODO(), log.ID, &logModel)
					fmt.Printf("Failed to decode JSON at offset %d: %v", msg.TopicPartition.Offset, err)
					flag = false
					continue
				}
				confinedSpace.IdLog = log.ID
				confinedSpaces = append(confinedSpaces, &confinedSpace)
				if len(confinedSpaces) > viper.GetInt("kafka.fetchCount") {
					flag = false
				}
			} else {
				if len(confinedSpaces) > 0 {
					numCPU := viper.GetInt("kafka.threadCount")
					length := len(confinedSpaces)
					wg := sync.WaitGroup{}
					chunkSize := (length + numCPU - 1) / numCPU

					for i := 0; i < length; i += chunkSize {

						wg.Add(1)
						go func(i int, chunkSize int) {
							end := i + chunkSize
							defer wg.Done()

							if end > length {
								end = length
							}
							slice := confinedSpaces[i:end]

							for _, confinedSpace := range slice {
								AddConfinedSpacesConsumerV2(confinedSpace, logModel)
							}
						}(i, chunkSize)
					}
					wg.Wait()
					confinedSpaces = []*consumer_models.ConfinedSpacesConsumer{}
				}
				flag = true
			}
		}
	}

	fmt.Printf("Closing consumer\n")
	c.Close()
}

func AddConfinedSpacesConsumerV2(confinedSpace *consumer_models.ConfinedSpacesConsumer, logModel models.Log) {

	flag, err := strconv.ParseBool(confinedSpace.IsDeleted)
	if err != nil {
		logModel.Operation = "Decode"
		logModel.Status = "Error"
		logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
		logModel.Body = confinedSpace
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), confinedSpace.IdLog, &logModel)
		fmt.Printf("Failed to decode JSON: %v", err)
		return
	}
	serv, err := mongo_v2.GetServiceAreaCollection().GetServiceAreaByExternalId(context.Background(), "33")
	if err != nil {
		logModel.Operation = "Get"
		logModel.Status = "Error"
		logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
		logModel.Body = confinedSpace
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), confinedSpace.IdLog, &logModel)
		fmt.Printf("Failed to decode JSON: %v", err)
		return
	}
	atte, err := mongo_v2.GetAttentionCollection().GetAttentionByExternalId(context.Background(), strconv.Itoa(confinedSpace.AttentionId))
	if err != nil {
		logModel.Operation = "Get"
		logModel.Status = "Error"
		logModel.Error = fmt.Sprintf("Failed to decode JSON: %v", err)
		logModel.Body = confinedSpace
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), confinedSpace.IdLog, &logModel)
		fmt.Printf("Failed to decode JSON: %v", err)
		return
	}
	if atte == nil {
		return
	}
	attentionId := atte.ID

	if flag || confinedSpace.Status == "0" {
		_, err = mongo_v2.GetAttentionCollection().PullBodyFromExamsByExternalId(context.TODO(), attentionId, strconv.Itoa(confinedSpace.ID), serv.ID)
		if err != nil {
			logModel.Operation = "Delete"
			logModel.Status = "Error"
			logModel.Error = fmt.Sprintf("Failed to delete confinedSpace: %v", err)
			logModel.Body = confinedSpace
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), confinedSpace.IdLog, &logModel)
			fmt.Printf("Failed to delete confinedSpace: %v", err)
		} else {
			logModel.Operation = "Delete"
			logModel.Status = "Done"
			logModel.Error = ""
			logModel.Body = confinedSpace
			mongo.GetLogsCollection().UpdateLogs(context.TODO(), confinedSpace.IdLog, &logModel)
		}

		mongo_v2.GetAttentionCollection().PullDiagnosticsByExternalId(context.Background(), attentionId, serv.ID)
	} else {

		var oldconfinedSpace *models_v2.Exam

		for _, body := range atte.Exams {
			if body.ExternalId == strconv.Itoa(confinedSpace.ID) && body.ServiceAreaID == serv.ID {
				oldconfinedSpace = body
			}
		}

		if oldconfinedSpace != nil {
			CreateConfinedSpace(atte, serv, attentionId, confinedSpace, oldconfinedSpace, logModel)
			fmt.Println("updated: ", oldconfinedSpace.ID)
		} else {
			var newConfinedSpaces models_v2.Exam
			CreateConfinedSpace(atte, serv, attentionId, confinedSpace, &newConfinedSpaces, logModel)
			fmt.Println("created: ", newConfinedSpaces.ID)
		}
	}
	if atte.PersonId != nil && atte.CustomerId != nil {
		if *atte.AttentionTypeName == "Ocupacional" {
			lastEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *atte.PersonId, *atte.CustomerId, "emo")
			if lastEmo != nil {
				mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *atte.PersonId, "lastEmoAttention", lastEmo)
			}
			lastAnyEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *atte.PersonId, *atte.CustomerId, "any")
			if lastAnyEmo != nil {
				lastAnyEmo.Customer = nil
				lastAnyEmo.Patient = nil
				lastAnyEmo.Person = nil
				lastAnyEmo.Consultations = nil
				lastAnyEmo.AttentionLines = nil
				lastAnyEmo.Exams = nil
				lastAnyEmo.AudiometryTests = nil
				lastAnyEmo.BiochemistryTests = nil
				lastAnyEmo.CovidTests = nil
				lastAnyEmo.Diagnostics = nil
				lastAnyEmo.DrugTests = nil
				lastAnyEmo.HematologyTests = nil
				lastAnyEmo.ImmunologyTests = nil
				lastAnyEmo.MicrobiologyTests = nil
				lastAnyEmo.OphtalmologyTests = nil
				lastAnyEmo.PsychologyTests = nil
				lastAnyEmo.Recommendations = nil
				lastAnyEmo.ExamResults = nil
				lastAnyEmo.SkeletalMuscleTests = nil
				lastAnyEmo.UrineTests = nil
				lastAnyEmo.MedicalHistories = nil
				lastAnyEmo.PathologicalHistories = nil
				lastAnyEmo.Triages = nil
				lastAnyEmo.WorkHistories = nil
				lastAnyEmo.SpirometryTests = nil
				mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *atte.PersonId, "lastAnyEmoAttention", lastAnyEmo)
			}
		}
		if *atte.AttentionTypeName == "Asistencial" {
			lastNoEmo, _ := mongo_v2.GetAttentionCollection().GetLastEmoAttentionByPatientId(context.Background(), *atte.PersonId, *atte.CustomerId, "no_emo")
			if lastNoEmo != nil {
				mongo_v2.GetPersonCollection().CustomSetFieldById(context.TODO(), "_id", *atte.PersonId, "lastNoEmoAttention", lastNoEmo)
			}
		}
	}
}

func CreateConfinedSpace(atte *models_v2.Attention, serv *models_v2.ServiceArea, attentionId string, confinedSpace *consumer_models.ConfinedSpacesConsumer, exam *models_v2.Exam, logModel models.Log) {
	if exam.ID == "" {
		exam.ID = primitive.NewObjectID().Hex()
		exam.ExternalId = strconv.Itoa(confinedSpace.ID)
		exam.ExternalSystem = "MEDIWEB"
		exam.CreatedDate = time.Now()
		exam.LastUpdatedDate = time.Now()
	}
	exam.AttentionID = attentionId
	exam.ServiceAreaID = serv.ID
	exam.ServiceAreaCode = serv.Code
	exam.ServiceArea = serv
	status, _ := strconv.Atoi(confinedSpace.Status)
	exam.Result = confinedSpace.Result
	exam.Status = status
	exam.LastUpdatedDate = time.Now()

	mongo_v2.GetAttentionCollection().PullBodyFromExamsByExternalId(context.TODO(), attentionId, strconv.Itoa(confinedSpace.ID), serv.ID)
	_, err := mongo_v2.GetAttentionCollection().PushBodyToExamsByExternalId(context.TODO(), attentionId, exam)
	if err != nil {
		logModel.Operation = "Update"
		logModel.Status = "Error"
		logModel.Collection = "middleware.transactions.attentions.exams"
		logModel.Error = fmt.Sprintf("Failed to update confinedSpace: %v", err)
		logModel.Body = confinedSpace
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), confinedSpace.IdLog, &logModel)
		fmt.Printf("Failed to update confinedSpace: %v", err)
	} else {
		logModel.Operation = "Update"
		logModel.Status = "Done"
		logModel.Collection = "middleware.transactions.attentions.exams"
		logModel.Error = ""
		logModel.Body = confinedSpace
		mongo.GetLogsCollection().UpdateLogs(context.TODO(), confinedSpace.IdLog, &logModel)
	}
}
