package functions_v2

import (
	"desktop-middleware/consumer_models"
	"desktop-middleware/models_v2"
	"strconv"
	"strings"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

func GetCustomer(id string, values []*models_v2.Customer) *models_v2.Customer {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetDepartment(id string, values []*models_v2.Department) *models_v2.Department {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}
func GetProvince(id string, values []*models_v2.Province) *models_v2.Province {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetHealthCentre(id string, values []*models_v2.HealthCentre) *models_v2.HealthCentre {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetWorkStation(id string, values []*models_v2.WorkStation) *models_v2.WorkStation {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetServiceArea(id string, values []*models_v2.ServiceArea) *models_v2.ServiceArea {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetSpeciality(id string, values []*models_v2.Speciality) *models_v2.Speciality {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetService(id string, attentionTypeId string, values []*models_v2.Service) *models_v2.Service {
	for _, val := range values {
		if val.ExternalId == id && val.AttentionTypeId == attentionTypeId {
			return val
		}
	}
	return nil
}

func GetWorkProfile(id string, values []*models_v2.WorkProfile) *models_v2.WorkProfile {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetExamType(id string, values []*models_v2.ExamType) *models_v2.ExamType {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetExternalClinic(id int, values []*models_v2.ExternalClinic) *models_v2.ExternalClinic {
	for _, val := range values {
		if val.Code == id {
			return val
		}
	}
	return nil
}

func GetPerson(documentNumber string, documentType int, values []*models_v2.Person) *models_v2.Person {
	for _, val := range values {
		if val.DocumentNumber == &documentNumber && val.DocumentTypeID == &documentType {
			return val
		}
	}
	return nil
}

func GetPatient(documentTypeID int, documentNumber string, values []*models_v2.Patient) *models_v2.Patient {
	for _, val := range values {
		if val.DocumentTypeID == documentTypeID && val.DocumentNumber == documentNumber {
			return val
		}
	}
	return nil
}

func GetAttentionType(id string, values []*models_v2.AttentionType) *models_v2.AttentionType {
	for _, val := range values {
		if val.Code == id {
			return val
		}
	}
	return nil
}

func GetDeliveryType(id string, values []*models_v2.DeliveryType) *models_v2.DeliveryType {
	for _, val := range values {
		if val.Code == id {
			return val
		}
	}
	return nil
}

func GetMaritalStatus(id string, values []*models_v2.MaritalStatus) *models_v2.MaritalStatus {
	for _, val := range values {
		if val.Code == id {
			return val
		}
	}
	return nil
}

func GetDegreeInstructions(id string, values []*models_v2.DegreeInstruction) *models_v2.DegreeInstruction {
	for _, val := range values {
		if val.Code == id {
			return val
		}
	}
	return nil
}

func GetMaritalStatusById(id string, values []*models_v2.MaritalStatus) *models_v2.MaritalStatus {
	for _, val := range values {
		if val.ID == id {
			return val
		}
	}
	return nil
}

func GetDegreeInstructionsById(id string, values []*models_v2.DegreeInstruction) *models_v2.DegreeInstruction {
	for _, val := range values {
		if val.ID == id {
			return val
		}
	}
	return nil
}

func GetAttention(id string, values []*models_v2.Attention) *models_v2.Attention {
	for _, val := range values {
		if val.ExternalId == &id {
			return val
		}
	}
	return nil
}

func GetAptitude(id string, values []*models_v2.Aptitude) *models_v2.Aptitude {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetExamResult(id string, values []*models_v2.ExamResult) *models_v2.ExamResult {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetConsultation(id string, values []*models_v2.Consultation) *models_v2.Consultation {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetAttentionLine(id string, values []*models_v2.AttentionLine) *models_v2.AttentionLine {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetEvaluation(id string, values []*models_v2.Evaluation) *models_v2.Evaluation {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetQuestion(id string, values []*models_v2.Question) *models_v2.Question {
	for _, val := range values {
		if val.EvaluationId == id {
			return val
		}
	}
	return nil
}

func GetExam(id string, attentionId string, serviceAreaId string, values []*models_v2.Exam) *models_v2.Exam {
	for _, val := range values {
		if val.ExternalId == id && val.ServiceAreaID == serviceAreaId && val.AttentionID == attentionId {
			return val
		}
	}
	return nil
}

func GetDiagnosticsFromSkeletalMuscle(id string, value *models_v2.SkeletalMuscle, serv *models_v2.ServiceArea) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic4) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  4,
			Diagnostic:      strings.TrimSpace(value.Diagnostic4),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic5) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  5,
			Diagnostic:      strings.TrimSpace(value.Diagnostic5),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic6) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  6,
			Diagnostic:      strings.TrimSpace(value.Diagnostic6),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic7) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  7,
			Diagnostic:      strings.TrimSpace(value.Diagnostic7),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic8) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  8,
			Diagnostic:      strings.TrimSpace(value.Diagnostic8),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic9) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  9,
			Diagnostic:      strings.TrimSpace(value.Diagnostic9),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic10) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  10,
			Diagnostic:      strings.TrimSpace(value.Diagnostic10),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic11) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  11,
			Diagnostic:      strings.TrimSpace(value.Diagnostic11),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromSpirometry(id string, value *models_v2.Spirometry, serv *models_v2.ServiceArea) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromXray(id string, value *models_v2.Xray, serv *models_v2.ServiceArea) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromXrayLD(id string, value *models_v2.XrayLD, serv *models_v2.ServiceArea) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromDermatology(id string, value *models_v2.Dermatology, serv *models_v2.ServiceArea) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromNeurology(id string, value *models_v2.Neurology, serv *models_v2.ServiceArea) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromDentistry(id string, value *models_v2.Dentistry, serv *models_v2.ServiceArea) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	return results
}

func GetRecommendationsFromPsychology(id string, value *models_v2.Psychology) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Recommendation1) != "" {
		result := models_v2.Recommendation{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Recommendation:  strings.TrimSpace(value.Recommendation1),
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Recommendation2) != "" {
		result := models_v2.Recommendation{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Recommendation:  strings.TrimSpace(value.Recommendation2),
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Recommendation3) != "" {
		result := models_v2.Recommendation{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Recommendation:  strings.TrimSpace(value.Recommendation3),
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	return results
}

func GetPsychologyTest(id string, values []*models_v2.PsychologyTest) *models_v2.PsychologyTest {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetPathologicalHistory(id string, values []*models_v2.PathologicalHistory) *models_v2.PathologicalHistory {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetPathologicalHistoryInjuries(id string, values []*models_v2.PathologicalHistoryInjuries) *models_v2.PathologicalHistoryInjuries {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetWorkHistory(id string, company string, values []*models_v2.WorkHistory) *models_v2.WorkHistory {
	for _, val := range values {
		if val.ExternalId == id && val.Company == company {
			return val
		}
	}
	return nil
}

func GetMedicalHistory(id string, values []*models_v2.MedicalHistory) *models_v2.MedicalHistory {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetTriage(id string, values []*models_v2.Triage) *models_v2.Triage {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetDiagnosticsFromAudiometry(id string, value *models_v2.Audiometry, serv *models_v2.ServiceArea) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionId,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionId,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	return results
}

func GetAudiometryTest(id string, values []*models_v2.AudiometryTest, serv *models_v2.ServiceArea) *models_v2.AudiometryTest {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetDiagnosticsFromOphtalmology(id string, value *models_v2.Ophtalmology, serv *models_v2.ServiceArea) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionId,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionId,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionId,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic4) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionId,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  4,
			Diagnostic:      strings.TrimSpace(value.Diagnostic4),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic5) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionId,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  5,
			Diagnostic:      strings.TrimSpace(value.Diagnostic5),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	return results
}

func GetOphtalmologyTest(id string, values []*models_v2.OphtalmologyTest) *models_v2.OphtalmologyTest {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetDiagnosticsFromCardiovascular(id string, value *models_v2.Cardiovascular, serv *models_v2.ServiceArea) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionID,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromLaboratory(id string, value *models_v2.Laboratory, serv *models_v2.ServiceArea) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionId,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionId,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionId,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic4) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionId,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  4,
			Diagnostic:      strings.TrimSpace(value.Diagnostic4),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic5) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     value.AttentionId,
			ServiceAreaID:   value.ServiceAreaID,
			ServiceAreaCode: value.ServiceAreaCode,
			ExamID:          id,
			SequenceNumber:  5,
			Diagnostic:      strings.TrimSpace(value.Diagnostic5),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	return results
}

func GetHematologyTest(id string, values []*models_v2.HematologyTest) *models_v2.HematologyTest {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetDrugTest(id string, values []*models_v2.DrugTest) *models_v2.DrugTest {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetMicrobiologyTest(id string, values []*models_v2.MicrobiologyTest) *models_v2.MicrobiologyTest {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetBiochemistryTest(id string, values []*models_v2.BiochemistryTest) *models_v2.BiochemistryTest {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetImmunologyTest(id string, values []*models_v2.ImmunologyTest) *models_v2.ImmunologyTest {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetCovidTest(id string, values []*models_v2.CovidTest) *models_v2.CovidTest {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetUrineTest(id string, values []*models_v2.UrineTest) *models_v2.UrineTest {
	for _, val := range values {
		if val.ExternalId == id {
			return val
		}
	}
	return nil
}

func GetDiagnosticsFromCardiovascularConsumer(id string, attencionId string, serviceAreaId string, serviceAreaCode string, value *consumer_models.CardiovascularConsumer, serv *models_v2.ServiceArea) []interface{} {
	var results []interface{}
	status, _ := strconv.Atoi(value.Status)
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			ServiceArea:     serv,
		}
		result.Status = status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			ServiceArea:     serv,
		}
		result.Status = status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			ServiceArea:     serv,
		}
		result.Status = status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromDentistryConsumer(id string, attencionId string, serviceAreaId string, serviceAreaCode string, value *consumer_models.DentistryConsumer, serv *models_v2.ServiceArea) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromDermatologyConsumer(id string, attencionId string, serviceAreaId string, serviceAreaCode string, value *consumer_models.DermatologyConsumer, serv *models_v2.ServiceArea) []interface{} {
	var results []interface{}
	status, _ := strconv.Atoi(value.Status)
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			ServiceArea:     serv,
		}
		result.Status = status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			ServiceArea:     serv,
		}
		result.Status = status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			ServiceArea:     serv,
		}
		result.Status = status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromNeurologyConsumer(id string, attencionId string, serviceAreaId string, serviceAreaCode string, value *consumer_models.NeurologyConsumer, serv *models_v2.ServiceArea) []interface{} {
	var results []interface{}
	status, _ := strconv.Atoi(value.Status)
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			ServiceArea:     serv,
		}
		result.Status = status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			ServiceArea:     serv,
		}
		result.Status = status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			ServiceArea:     serv,
		}
		result.Status = status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromSkeletalMuscleConsumer(id string, attencionId string, serviceAreaId string, serviceAreaCode string, value *consumer_models.SkeletalMuscleConsumer, serv *models_v2.ServiceArea) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic4) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  4,
			Diagnostic:      strings.TrimSpace(value.Diagnostic4),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic5) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  5,
			Diagnostic:      strings.TrimSpace(value.Diagnostic5),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic6) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  6,
			Diagnostic:      strings.TrimSpace(value.Diagnostic6),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic7) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  7,
			Diagnostic:      strings.TrimSpace(value.Diagnostic7),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic8) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  8,
			Diagnostic:      strings.TrimSpace(value.Diagnostic8),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic9) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  9,
			Diagnostic:      strings.TrimSpace(value.Diagnostic9),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic10) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  10,
			Diagnostic:      strings.TrimSpace(value.Diagnostic10),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic11) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  11,
			Diagnostic:      strings.TrimSpace(value.Diagnostic11),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromSpirometryConsumer(id string, attencionId string, serviceAreaId string, serviceAreaCode string, value *consumer_models.SpirometryConsumer, serv *models_v2.ServiceArea) []interface{} {
	var results []interface{}
	status, _ := strconv.Atoi(value.Status)
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			ServiceArea:     serv,
		}
		result.Status = status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			ServiceArea:     serv,
		}
		result.Status = status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			ServiceArea:     serv,
		}
		result.Status = status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromXRayConsumer(id string, attencionId string, serviceAreaId string, serviceAreaCode string, value *consumer_models.XRayConsumer, serv *models_v2.ServiceArea) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromXRayLDConsumer(id string, attencionId string, serviceAreaId string, serviceAreaCode string, value *consumer_models.XRayLDConsumer, serv *models_v2.ServiceArea) []interface{} {
	var results []interface{}
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			ServiceArea:     serv,
		}
		result.Status = value.Status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	return results
}

func GetRecommendationsFromPsychologyConsumer(id string, attentionId string, serviceAreaId string, serviceAreaCode string, value *consumer_models.PsychologyConsumer) []interface{} {
	var results []interface{}
	status, _ := strconv.Atoi(value.Status)
	if strings.TrimSpace(value.Recommendation1) != "" {
		result := models_v2.Recommendation{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attentionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Recommendation:  strings.TrimSpace(value.Recommendation1),
		}
		result.Status = status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Recommendation2) != "" {
		result := models_v2.Recommendation{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attentionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Recommendation:  strings.TrimSpace(value.Recommendation2),
		}
		result.Status = status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Recommendation3) != "" {
		result := models_v2.Recommendation{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attentionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Recommendation:  strings.TrimSpace(value.Recommendation3),
		}
		result.Status = status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromAudiometryConsumer(id string, attentionId string, serviceAreaId string, serviceAreaCode string, value *consumer_models.AudiometryConsumer, serv *models_v2.ServiceArea) []interface{} {
	var results []interface{}
	status, _ := strconv.Atoi(value.Status)
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attentionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			ServiceArea:     serv,
		}
		result.Status = status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attentionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			ServiceArea:     serv,
		}
		result.Status = status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromOphtalmologyConsumer(id string, attentionId string, serviceAreaId string, serviceAreaCode string, value *consumer_models.OphtalmologyConsumer, serv *models_v2.ServiceArea) []interface{} {
	var results []interface{}
	status, _ := strconv.Atoi(value.Status)
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attentionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			ServiceArea:     serv,
		}
		result.Status = status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attentionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			ServiceArea:     serv,
		}
		result.Status = status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attentionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			ServiceArea:     serv,
		}
		result.Status = status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic4) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attentionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  4,
			Diagnostic:      strings.TrimSpace(value.Diagnostic4),
			ServiceArea:     serv,
		}
		result.Status = status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic5) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attentionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  5,
			Diagnostic:      strings.TrimSpace(value.Diagnostic5),
			ServiceArea:     serv,
		}
		result.Status = status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	return results
}

func GetDiagnosticsFromLaboratoryConsumer(id string, attencionId string, serviceAreaId string, serviceAreaCode string, value *consumer_models.LaboratoryConsumer, serv *models_v2.ServiceArea) []interface{} {
	var results []interface{}
	status, _ := strconv.Atoi(value.Status)
	if strings.TrimSpace(value.Diagnostic1) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  1,
			Diagnostic:      strings.TrimSpace(value.Diagnostic1),
			ServiceArea:     serv,
		}
		result.Status = status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic2) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  2,
			Diagnostic:      strings.TrimSpace(value.Diagnostic2),
			ServiceArea:     serv,
		}
		result.Status = status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic3) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  3,
			Diagnostic:      strings.TrimSpace(value.Diagnostic3),
			ServiceArea:     serv,
		}
		result.Status = status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic4) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  4,
			Diagnostic:      strings.TrimSpace(value.Diagnostic4),
			ServiceArea:     serv,
		}
		result.Status = status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	if strings.TrimSpace(value.Diagnostic5) != "" {
		result := models_v2.Diagnostic{
			ID:              primitive.NewObjectID().Hex(),
			AttentionID:     attencionId,
			ServiceAreaID:   serviceAreaId,
			ServiceAreaCode: serviceAreaCode,
			ExamID:          id,
			SequenceNumber:  5,
			Diagnostic:      strings.TrimSpace(value.Diagnostic5),
			ServiceArea:     serv,
		}
		result.Status = status
		result.CreatedDate = time.Now()
		result.LastUpdatedDate = time.Now()
		result.ExternalSystem = "MEDIWEB"
		results = append(results, &result)
	}
	return results
}

func GetDistrict(externalId string, values []*models_v2.District) *models_v2.District {
	for _, val := range values {
		if val.ExternalId == externalId {
			return val
		}
	}
	return nil
}

func GetDistrictById(id string, values []*models_v2.District) *models_v2.District {
	for _, val := range values {
		if val.ID == id {
			return val
		}
	}
	return nil
}

func GetWorkingArea(description string, values []*models_v2.WorkingArea) *models_v2.WorkingArea {
	for _, val := range values {
		if strings.TrimSpace(strings.ToUpper(val.Description)) == strings.TrimSpace(strings.ToUpper(description)) {
			return val
		}
	}
	return nil
}

func GetWorkingAreaByID(externalId string, values []*models_v2.WorkingArea) *models_v2.WorkingArea {
	for _, val := range values {
		if val.ExternalId == externalId {
			return val
		}
	}
	return nil
}

func GetJobTitle(description string, values []*models_v2.JobTitle) *models_v2.JobTitle {
	for _, val := range values {
		if strings.TrimSpace(strings.ToUpper(val.Description)) == strings.TrimSpace(strings.ToUpper(description)) {
			return val
		}
	}
	return nil
}

func GetJobTitleByID(externalId string, values []*models_v2.JobTitle) *models_v2.JobTitle {
	for _, val := range values {
		if val.ExternalId == externalId {
			return val
		}
	}
	return nil
}
