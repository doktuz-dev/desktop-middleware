module desktop-middleware

go 1.16

require (
	github.com/araddon/dateparse v0.0.0-20210429162001-6b43995a97de
	github.com/confluentinc/confluent-kafka-go v1.8.2
	github.com/elastic/go-licenser v0.4.0 // indirect
	github.com/elastic/go-sysinfo v1.7.1 // indirect
	github.com/elastic/go-windows v1.0.1 // indirect
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/jcchavezs/porto v0.4.0 // indirect
	github.com/prometheus/procfs v0.7.3 // indirect
	github.com/spf13/viper v1.10.1
	github.com/thoas/go-funk v0.9.3
	go.elastic.co/apm/module/apmmongo v1.15.0
	go.elastic.co/apm/module/apmsql v1.15.0
	go.mongodb.org/mongo-driver v1.8.4
	golang.org/x/sys v0.0.0-20220319134239-a9b59b0215f8 // indirect
	golang.org/x/text v0.3.7
	golang.org/x/tools v0.1.10 // indirect
	howett.net/plist v1.0.0 // indirect
)
