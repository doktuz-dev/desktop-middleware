package consumer_models

type HeightConsumer struct {
	ID          int    `json:"idcmvisitas,omitempty" bson:"idcmvisitas,omitempty"`
	AttentionId int    `json:"idcomprobante,omitempty" bson:"idcomprobante,omitempty"`
	Result      string `json:"apto,omitempty" bson:"apto,omitempty"`
	Status      string `json:"estado" bson:"estado"`
	IsDeleted   string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog       string
}
