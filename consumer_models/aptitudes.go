package consumer_models

import (
	"strings"
	"time"
)

type AptitudeConsumer struct {
	ID               int     `json:"idformato_diagnostico,omitempty" bson:"idformato_diagnostico,omitempty"`
	AttentionId      int     `json:"idcomprobante,omitempty" bson:"idcomprobante,omitempty"`
	Aptitude         string  `json:"aptitud,omitempty" bson:"aptitud,omitempty"`
	ObservedReason   *string `json:"razon_observ,omitempty" bson:"razon_observ,omitempty"`
	AptitudeDetail   *string `json:"detalle_aptitud,omitempty" bson:"detalle_aptitud,omitempty"`
	Restriction1     *string `json:"restriccion1,omitempty" bson:"restriccion1,omitempty"`
	Restriction2     *string `json:"restriccion2,omitempty" bson:"restriccion2,omitempty"`
	Restriction3     *string `json:"restriccion3,omitempty" bson:"restriccion3,omitempty"`
	Restriction4     *string `json:"restriccion4,omitempty" bson:"restriccion4,omitempty"`
	Restriction5     *string `json:"restriccion5,omitempty" bson:"restriccion5,omitempty"`
	Restriction6     *string `json:"restriccion6,omitempty" bson:"restriccion6,omitempty"`
	Restriction7     *string `json:"restriccion7,omitempty" bson:"restriccion7,omitempty"`
	Restriction9     *string `json:"restriccion9,omitempty" bson:"restriccion9,omitempty"`
	Restriction10    *string `json:"restriccion10,omitempty" bson:"restriccion10,omitempty"`
	Restriction11    *string `json:"restriccion11,omitempty" bson:"restriccion11,omitempty"`
	Restriction12    *string `json:"restriccion12,omitempty" bson:"restriccion12,omitempty"`
	Restriction13    *string `json:"restriccion13,omitempty" bson:"restriccion13,omitempty"`
	Restriction14    *string `json:"restriccion14,omitempty" bson:"restriccion14,omitempty"`
	Restriction15    *string `json:"restriccion15,omitempty" bson:"restriccion15,omitempty"`
	Restriction16    *string `json:"restriccion16,omitempty" bson:"restriccion16,omitempty"`
	Restriction17    *string `json:"restriccion17,omitempty" bson:"restriccion17,omitempty"`
	Restriction18    *string `json:"restriccion18,omitempty" bson:"restriccion18,omitempty"`
	Restriction19    *string `json:"restriccion19,omitempty" bson:"restriccion19,omitempty"`
	Restriction20    *string `json:"restriccion20,omitempty" bson:"restriccion20,omitempty"`
	Restriction21    *string `json:"restriccion21,omitempty" bson:"restriccion21,omitempty"`
	Restriction22    *string `json:"restriccion22,omitempty" bson:"restriccion22,omitempty"`
	Restriction23    *string `json:"restriccion23,omitempty" bson:"restriccion23,omitempty"`
	Recommendation1  *string `json:"oft_reco1,omitempty" bson:"oft_reco1,omitempty"`
	Recommendation2  *string `json:"oft_reco2,omitempty" bson:"oft_reco2,omitempty"`
	Recommendation3  *string `json:"oft_reco3,omitempty" bson:"oft_reco3,omitempty"`
	Recommendation4  *string `json:"aud_reco1,omitempty" bson:"aud_reco1,omitempty"`
	Recommendation5  *string `json:"aud_reco2,omitempty" bson:"aud_reco2,omitempty"`
	Recommendation6  *string `json:"aud_reco3,omitempty" bson:"aud_reco3,omitempty"`
	Recommendation7  *string `json:"aud_reco4,omitempty" bson:"aud_reco4,omitempty"`
	Recommendation8  *string `json:"psi_reco3,omitempty" bson:"psi_reco3,omitempty"`
	Recommendation9  *string `json:"ele_reco1,omitempty" bson:"ele_reco1,omitempty"`
	Recommendation10 *string `json:"ele_reco2,omitempty" bson:"ele_reco2,omitempty"`
	Recommendation11 *string `json:"ele_reco3,omitempty" bson:"ele_reco3,omitempty"`
	Recommendation12 *string `json:"ele_reco4,omitempty" bson:"ele_reco4,omitempty"`
	Recommendation13 *string `json:"ele_reco5,omitempty" bson:"ele_reco5,omitempty"`
	Recommendation14 *string `json:"rayosx_reco1,omitempty" bson:"rayosx_reco1,omitempty"`
	Recommendation15 *string `json:"rayosx_reco2,omitempty" bson:"rayosx_reco2,omitempty"`
	Recommendation16 *string `json:"rayosx_reco3,omitempty" bson:"rayosx_reco3,omitempty"`
	Recommendation17 *string `json:"rayosx_reco4,omitempty" bson:"rayosx_reco4,omitempty"`
	Recommendation18 *string `json:"moc_reco1,omitempty" bson:"moc_reco1,omitempty"`
	Recommendation19 *string `json:"moc_reco2,omitempty" bson:"moc_reco2,omitempty"`
	Recommendation20 *string `json:"moc_reco3,omitempty" bson:"moc_reco3,omitempty"`
	Recommendation21 *string `json:"moc_reco4,omitempty" bson:"moc_reco4,omitempty"`
	Recommendation22 *string `json:"moc_reco5,omitempty" bson:"moc_reco5,omitempty"`
	Recommendation23 *string `json:"moc_reco6,omitempty" bson:"moc_reco6,omitempty"`
	Recommendation24 *string `json:"inter_reco1,omitempty" bson:"inter_reco1,omitempty"`
	Recommendation25 *string `json:"inter_reco2,omitempty" bson:"inter_reco2,omitempty"`
	Recommendation26 *string `json:"inter_reco3,omitempty" bson:"inter_reco3,omitempty"`
	Recommendation27 *string `json:"espi_reco1,omitempty" bson:"espi_reco1,omitempty"`
	Recommendation28 *string `json:"espi_reco2,omitempty" bson:"espi_reco2,omitempty"`
	Recommendation29 *string `json:"espi_reco3,omitempty" bson:"espi_reco3,omitempty"`
	Recommendation30 *string `json:"moc_reco7,omitempty" bson:"moc_reco7,omitempty"`
	Recommendation31 *string `json:"moc_reco8,omitempty" bson:"moc_reco8,omitempty"`
	Recommendation32 *string `json:"moc_reco9,omitempty" bson:"moc_reco9,omitempty"`
	Recommendation33 *string `json:"moc_reco10,omitempty" bson:"moc_reco10,omitempty"`
	Recommendation34 *string `json:"otro_reco1,omitempty" bson:"otro_reco1,omitempty"`
	Recommendation35 *string `json:"otro_reco2,omitempty" bson:"otro_reco2,omitempty"`
	Recommendation36 *string `json:"otro_reco3,omitempty" bson:"otro_reco3,omitempty"`
	Recommendation37 *string `json:"otro_reco4,omitempty" bson:"otro_reco4,omitempty"`
	Recommendation38 *string `json:"otro_reco5,omitempty" bson:"otro_reco5,omitempty"`
	Recommendation39 *string `json:"otro_reco6,omitempty" bson:"otro_reco6,omitempty"`
	Recommendation40 *string `json:"otro_reco7,omitempty" bson:"otro_reco7,omitempty"`
	Recommendation41 *string `json:"otro_reco8,omitempty" bson:"otro_reco8,omitempty"`
	Recommendation42 *string `json:"otro_reco9,omitempty" bson:"otro_reco9,omitempty"`
	Recommendation43 *string `json:"otro_reco10,omitempty" bson:"otro_reco10,omitempty"`
	Recommendation44 *string `json:"labo_reco,omitempty" bson:"labo_reco,omitempty"`
	Recommendation45 *string `json:"labo_reco2,omitempty" bson:"labo_reco2,omitempty"`
	Recommendation46 *string `json:"labo_reco3,omitempty" bson:"labo_reco3,omitempty"`
	Recommendation47 *string `json:"musc_reco1,omitempty" bson:"musc_reco1,omitempty"`
	Recommendation48 *string `json:"musc_reco2,omitempty" bson:"musc_reco2,omitempty"`
	Recommendation49 *string `json:"musc_reco3,omitempty" bson:"musc_reco3,omitempty"`
	Recommendation50 *string `json:"derm_reco1,omitempty" bson:"derm_reco1,omitempty"`
	Recommendation51 *string `json:"derm_reco2,omitempty" bson:"derm_reco2,omitempty"`
	Recommendation52 *string `json:"derm_reco3,omitempty" bson:"derm_reco3,omitempty"`
	Recommendation53 *string `json:"rayosx_reco5,omitempty" bson:"rayosx_reco5,omitempty"`
	Recommendation54 *string `json:"rayosx_reco6,omitempty" bson:"rayosx_reco6,omitempty"`
	Recommendation55 *string `json:"rayosx_reco7,omitempty" bson:"rayosx_reco7,omitempty"`
	Recommendation56 *string `json:"rayosx_reco8,omitempty" bson:"rayosx_reco8,omitempty"`
	Recommendation57 *string `json:"neuro_reco1,omitempty" bson:"neuro_reco1,omitempty"`
	Recommendation58 *string `json:"neuro_reco2,omitempty" bson:"neuro_reco2,omitempty"`
	Recommendation59 *string `json:"neuro_reco3,omitempty" bson:"neuro_reco3,omitempty"`
	Recommendation60 *string `json:"labo_reco6,omitempty" bson:"labo_reco6,omitempty"`
	Recommendation61 *string `json:"labo_reco7,omitempty" bson:"labo_reco7,omitempty"`
	Recommendation62 *string `json:"labo_reco8,omitempty" bson:"labo_reco8,omitempty"`
	Recommendation63 *string `json:"labo_reco9,omitempty" bson:"labo_reco9,omitempty"`
	Recommendation64 *string `json:"labo_reco10,omitempty" bson:"labo_reco10,omitempty"`
	Recommendation65 *string `json:"ante_reco1,omitempty" bson:"ante_reco1,omitempty"`
	Recommendation66 *string `json:"ante_reco2,omitempty" bson:"ante_reco2,omitempty"`
	Recommendation67 *string `json:"ante_reco3,omitempty" bson:"ante_reco3,omitempty"`
	Recommendation68 *string `json:"ante_reco4,omitempty" bson:"ante_reco4,omitempty"`
	Recommendation69 *string `json:"ante_reco5,omitempty" bson:"ante_reco5,omitempty"`
	Recommendation70 *string `json:"senso_reco1,omitempty" bson:"senso_reco1,omitempty"`
	Recommendation71 *string `json:"senso_reco2,omitempty" bson:"senso_reco2,omitempty"`
	Recommendation72 *string `json:"senso_reco3,omitempty" bson:"senso_reco3,omitempty"`
	Recommendation73 *string `json:"musc_reco4,omitempty" bson:"musc_reco4,omitempty"`
	Recommendation74 *string `json:"musc_reco5,omitempty" bson:"musc_reco5,omitempty"`
	Recommendation75 *string `json:"musc_reco6,omitempty" bson:"musc_reco6,omitempty"`
	Recommendation76 *string `json:"musc_reco7,omitempty" bson:"musc_reco7,omitempty"`
	Recommendation77 *string `json:"musc_reco8,omitempty" bson:"musc_reco8,omitempty"`
	Recommendation78 *string `json:"mus_reco1,omitempty" bson:"mus_reco1,omitempty"`
	Recommendation79 *string `json:"mus_reco2,omitempty" bson:"mus_reco2,omitempty"`
	Recommendation80 *string `json:"mus_reco3,omitempty" bson:"mus_reco3,omitempty"`
	Recommendation81 *string `json:"odo_reco4,omitempty" bson:"odo_reco4,omitempty"`
	Recommendation82 *string `json:"odo_reco5,omitempty" bson:"odo_reco5,omitempty"`
	Recommendation83 *string `json:"odo_reco6,omitempty" bson:"odo_reco6,omitempty"`
	Recommendation84 *string `json:"psi_reco1,omitempty" bson:"psi_reco1,omitempty"`
	Recommendation85 *string `json:"psi_reco2,omitempty" bson:"psi_reco2,omitempty"`
	StartDate        int32   `json:"fecha_apto,omitempty" bson:"fecha_apto,omitempty"`
	EndDate          int32   `json:"fecha_vencimiento,omitempty" bson:"fecha_vencimiento,omitempty"`
	Reviewed         string  `json:"revisadofh02,omitempty" bson:"revisadofh02,omitempty"`
	Audited          string  `json:"auditadofh02,omitempty" bson:"auditadofh02,omitempty"`
	Status           string  `json:"estado" bson:"estado"`
	IsDeleted        string  `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog            string  `json:"idlog,omitempty" bson:"idlog,omitempty"`
}

func (w AptitudeConsumer) AddDateStarDate() time.Time {
	start := time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC)
	return start.AddDate(0, 0, int(w.StartDate))
}

func (w AptitudeConsumer) AddDateEndDate() time.Time {
	start := time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC)
	return start.AddDate(0, 0, int(w.EndDate))
}

func (w AptitudeConsumer) SetRecommendations() []string {
	var list []string

	if w.Recommendation1 != nil {
		if strings.TrimSpace(*w.Recommendation1) != "" {
			list = append(list, *w.Recommendation1)
		}
	}

	if w.Recommendation2 != nil {
		if strings.TrimSpace(*w.Recommendation2) != "" {
			list = append(list, *w.Recommendation2)
		}
	}

	if w.Recommendation3 != nil {
		if strings.TrimSpace(*w.Recommendation3) != "" {
			list = append(list, *w.Recommendation3)
		}
	}

	if w.Recommendation4 != nil {
		if strings.TrimSpace(*w.Recommendation4) != "" {
			list = append(list, *w.Recommendation4)
		}
	}

	if w.Recommendation5 != nil {
		if strings.TrimSpace(*w.Recommendation5) != "" {
			list = append(list, *w.Recommendation5)
		}
	}

	if w.Recommendation6 != nil {
		if strings.TrimSpace(*w.Recommendation6) != "" {
			list = append(list, *w.Recommendation6)
		}
	}

	if w.Recommendation7 != nil {
		if strings.TrimSpace(*w.Recommendation7) != "" {
			list = append(list, *w.Recommendation7)
		}
	}

	if w.Recommendation8 != nil {
		if strings.TrimSpace(*w.Recommendation8) != "" {
			list = append(list, *w.Recommendation8)
		}
	}

	if w.Recommendation9 != nil {
		if strings.TrimSpace(*w.Recommendation9) != "" {
			list = append(list, *w.Recommendation9)
		}
	}

	if w.Recommendation10 != nil {
		if strings.TrimSpace(*w.Recommendation10) != "" {
			list = append(list, *w.Recommendation10)
		}
	}

	if w.Recommendation11 != nil {
		if strings.TrimSpace(*w.Recommendation11) != "" {
			list = append(list, *w.Recommendation11)
		}
	}

	if w.Recommendation12 != nil {
		if strings.TrimSpace(*w.Recommendation12) != "" {
			list = append(list, *w.Recommendation12)
		}
	}

	if w.Recommendation13 != nil {
		if strings.TrimSpace(*w.Recommendation13) != "" {
			list = append(list, *w.Recommendation13)
		}
	}

	if w.Recommendation14 != nil {
		if strings.TrimSpace(*w.Recommendation14) != "" {
			list = append(list, *w.Recommendation14)
		}
	}

	if w.Recommendation15 != nil {
		if strings.TrimSpace(*w.Recommendation15) != "" {
			list = append(list, *w.Recommendation15)
		}
	}

	if w.Recommendation16 != nil {
		if strings.TrimSpace(*w.Recommendation16) != "" {
			list = append(list, *w.Recommendation16)
		}
	}

	if w.Recommendation17 != nil {
		if strings.TrimSpace(*w.Recommendation17) != "" {
			list = append(list, *w.Recommendation17)
		}
	}

	if w.Recommendation18 != nil {
		if strings.TrimSpace(*w.Recommendation18) != "" {
			list = append(list, *w.Recommendation18)
		}
	}

	if w.Recommendation19 != nil {
		if strings.TrimSpace(*w.Recommendation19) != "" {
			list = append(list, *w.Recommendation19)
		}
	}

	if w.Recommendation20 != nil {
		if strings.TrimSpace(*w.Recommendation20) != "" {
			list = append(list, *w.Recommendation20)
		}
	}

	if w.Recommendation21 != nil {
		if strings.TrimSpace(*w.Recommendation21) != "" {
			list = append(list, *w.Recommendation21)
		}
	}

	if w.Recommendation22 != nil {
		if strings.TrimSpace(*w.Recommendation22) != "" {
			list = append(list, *w.Recommendation22)
		}
	}

	if w.Recommendation23 != nil {
		if strings.TrimSpace(*w.Recommendation23) != "" {
			list = append(list, *w.Recommendation23)
		}
	}

	if w.Recommendation24 != nil {
		if strings.TrimSpace(*w.Recommendation24) != "" {
			list = append(list, *w.Recommendation24)
		}
	}

	if w.Recommendation25 != nil {
		if strings.TrimSpace(*w.Recommendation25) != "" {
			list = append(list, *w.Recommendation25)
		}
	}

	if w.Recommendation26 != nil {
		if strings.TrimSpace(*w.Recommendation26) != "" {
			list = append(list, *w.Recommendation26)
		}
	}

	if w.Recommendation27 != nil {
		if strings.TrimSpace(*w.Recommendation27) != "" {
			list = append(list, *w.Recommendation27)
		}
	}

	if w.Recommendation28 != nil {
		if strings.TrimSpace(*w.Recommendation28) != "" {
			list = append(list, *w.Recommendation28)
		}
	}

	if w.Recommendation29 != nil {
		if strings.TrimSpace(*w.Recommendation29) != "" {
			list = append(list, *w.Recommendation29)
		}
	}

	if w.Recommendation30 != nil {
		if strings.TrimSpace(*w.Recommendation30) != "" {
			list = append(list, *w.Recommendation30)
		}
	}

	if w.Recommendation31 != nil {
		if strings.TrimSpace(*w.Recommendation31) != "" {
			list = append(list, *w.Recommendation31)
		}
	}

	if w.Recommendation32 != nil {
		if strings.TrimSpace(*w.Recommendation32) != "" {
			list = append(list, *w.Recommendation32)
		}
	}

	if w.Recommendation33 != nil {
		if strings.TrimSpace(*w.Recommendation33) != "" {
			list = append(list, *w.Recommendation33)
		}
	}

	if w.Recommendation34 != nil {
		if strings.TrimSpace(*w.Recommendation34) != "" {
			list = append(list, *w.Recommendation34)
		}
	}

	if w.Recommendation35 != nil {
		if strings.TrimSpace(*w.Recommendation35) != "" {
			list = append(list, *w.Recommendation35)
		}
	}

	if w.Recommendation36 != nil {
		if strings.TrimSpace(*w.Recommendation36) != "" {
			list = append(list, *w.Recommendation36)
		}
	}

	if w.Recommendation37 != nil {
		if strings.TrimSpace(*w.Recommendation37) != "" {
			list = append(list, *w.Recommendation37)
		}
	}

	if w.Recommendation38 != nil {
		if strings.TrimSpace(*w.Recommendation38) != "" {
			list = append(list, *w.Recommendation38)
		}
	}

	if w.Recommendation39 != nil {
		if strings.TrimSpace(*w.Recommendation39) != "" {
			list = append(list, *w.Recommendation39)
		}
	}

	if w.Recommendation40 != nil {
		if strings.TrimSpace(*w.Recommendation40) != "" {
			list = append(list, *w.Recommendation40)
		}
	}

	if w.Recommendation41 != nil {
		if strings.TrimSpace(*w.Recommendation41) != "" {
			list = append(list, *w.Recommendation41)
		}
	}

	if w.Recommendation42 != nil {
		if strings.TrimSpace(*w.Recommendation42) != "" {
			list = append(list, *w.Recommendation42)
		}
	}

	if w.Recommendation43 != nil {
		if strings.TrimSpace(*w.Recommendation43) != "" {
			list = append(list, *w.Recommendation43)
		}
	}

	if w.Recommendation44 != nil {
		if strings.TrimSpace(*w.Recommendation44) != "" {
			list = append(list, *w.Recommendation44)
		}
	}

	if w.Recommendation45 != nil {
		if strings.TrimSpace(*w.Recommendation45) != "" {
			list = append(list, *w.Recommendation45)
		}
	}

	if w.Recommendation46 != nil {
		if strings.TrimSpace(*w.Recommendation46) != "" {
			list = append(list, *w.Recommendation46)
		}
	}

	if w.Recommendation47 != nil {
		if strings.TrimSpace(*w.Recommendation47) != "" {
			list = append(list, *w.Recommendation47)
		}
	}

	if w.Recommendation48 != nil {
		if strings.TrimSpace(*w.Recommendation48) != "" {
			list = append(list, *w.Recommendation48)
		}
	}

	if w.Recommendation49 != nil {
		if strings.TrimSpace(*w.Recommendation49) != "" {
			list = append(list, *w.Recommendation49)
		}
	}

	if w.Recommendation50 != nil {
		if strings.TrimSpace(*w.Recommendation50) != "" {
			list = append(list, *w.Recommendation50)
		}
	}

	if w.Recommendation51 != nil {
		if strings.TrimSpace(*w.Recommendation51) != "" {
			list = append(list, *w.Recommendation51)
		}
	}

	if w.Recommendation52 != nil {
		if strings.TrimSpace(*w.Recommendation52) != "" {
			list = append(list, *w.Recommendation52)
		}
	}

	if w.Recommendation53 != nil {
		if strings.TrimSpace(*w.Recommendation53) != "" {
			list = append(list, *w.Recommendation53)
		}
	}

	if w.Recommendation54 != nil {
		if strings.TrimSpace(*w.Recommendation54) != "" {
			list = append(list, *w.Recommendation54)
		}
	}

	if w.Recommendation55 != nil {
		if strings.TrimSpace(*w.Recommendation55) != "" {
			list = append(list, *w.Recommendation55)
		}
	}

	if w.Recommendation56 != nil {
		if strings.TrimSpace(*w.Recommendation56) != "" {
			list = append(list, *w.Recommendation56)
		}
	}

	if w.Recommendation57 != nil {
		if strings.TrimSpace(*w.Recommendation57) != "" {
			list = append(list, *w.Recommendation57)
		}
	}

	if w.Recommendation58 != nil {
		if strings.TrimSpace(*w.Recommendation58) != "" {
			list = append(list, *w.Recommendation58)
		}
	}

	if w.Recommendation59 != nil {
		if strings.TrimSpace(*w.Recommendation59) != "" {
			list = append(list, *w.Recommendation59)
		}
	}

	if w.Recommendation60 != nil {
		if strings.TrimSpace(*w.Recommendation60) != "" {
			list = append(list, *w.Recommendation60)
		}
	}

	if w.Recommendation61 != nil {
		if strings.TrimSpace(*w.Recommendation61) != "" {
			list = append(list, *w.Recommendation61)
		}
	}

	if w.Recommendation62 != nil {
		if strings.TrimSpace(*w.Recommendation62) != "" {
			list = append(list, *w.Recommendation62)
		}
	}

	if w.Recommendation63 != nil {
		if strings.TrimSpace(*w.Recommendation63) != "" {
			list = append(list, *w.Recommendation63)
		}
	}

	if w.Recommendation64 != nil {
		if strings.TrimSpace(*w.Recommendation64) != "" {
			list = append(list, *w.Recommendation64)
		}
	}

	if w.Recommendation65 != nil {
		if strings.TrimSpace(*w.Recommendation65) != "" {
			list = append(list, *w.Recommendation65)
		}
	}

	if w.Recommendation66 != nil {
		if strings.TrimSpace(*w.Recommendation66) != "" {
			list = append(list, *w.Recommendation66)
		}
	}

	if w.Recommendation67 != nil {
		if strings.TrimSpace(*w.Recommendation67) != "" {
			list = append(list, *w.Recommendation67)
		}
	}

	if w.Recommendation68 != nil {
		if strings.TrimSpace(*w.Recommendation68) != "" {
			list = append(list, *w.Recommendation68)
		}
	}

	if w.Recommendation69 != nil {
		if strings.TrimSpace(*w.Recommendation69) != "" {
			list = append(list, *w.Recommendation69)
		}
	}

	if w.Recommendation70 != nil {
		if strings.TrimSpace(*w.Recommendation70) != "" {
			list = append(list, *w.Recommendation70)
		}
	}

	if w.Recommendation71 != nil {
		if strings.TrimSpace(*w.Recommendation71) != "" {
			list = append(list, *w.Recommendation71)
		}
	}

	if w.Recommendation72 != nil {
		if strings.TrimSpace(*w.Recommendation72) != "" {
			list = append(list, *w.Recommendation72)
		}
	}

	if w.Recommendation73 != nil {
		if strings.TrimSpace(*w.Recommendation73) != "" {
			list = append(list, *w.Recommendation73)
		}
	}

	if w.Recommendation74 != nil {
		if strings.TrimSpace(*w.Recommendation74) != "" {
			list = append(list, *w.Recommendation74)
		}
	}

	if w.Recommendation75 != nil {
		if strings.TrimSpace(*w.Recommendation75) != "" {
			list = append(list, *w.Recommendation75)
		}
	}

	if w.Recommendation76 != nil {
		if strings.TrimSpace(*w.Recommendation76) != "" {
			list = append(list, *w.Recommendation76)
		}
	}

	if w.Recommendation77 != nil {
		if strings.TrimSpace(*w.Recommendation77) != "" {
			list = append(list, *w.Recommendation77)
		}
	}

	if w.Recommendation78 != nil {
		if strings.TrimSpace(*w.Recommendation78) != "" {
			list = append(list, *w.Recommendation78)
		}
	}

	if w.Recommendation79 != nil {
		if strings.TrimSpace(*w.Recommendation79) != "" {
			list = append(list, *w.Recommendation79)
		}
	}

	if w.Recommendation80 != nil {
		if strings.TrimSpace(*w.Recommendation80) != "" {
			list = append(list, *w.Recommendation80)
		}
	}

	if w.Recommendation81 != nil {
		if strings.TrimSpace(*w.Recommendation81) != "" {
			list = append(list, *w.Recommendation81)
		}
	}

	if w.Recommendation82 != nil {
		if strings.TrimSpace(*w.Recommendation82) != "" {
			list = append(list, *w.Recommendation82)
		}
	}

	if w.Recommendation83 != nil {
		if strings.TrimSpace(*w.Recommendation83) != "" {
			list = append(list, *w.Recommendation83)
		}
	}

	if w.Recommendation84 != nil {
		if strings.TrimSpace(*w.Recommendation84) != "" {
			list = append(list, *w.Recommendation84)
		}
	}

	if w.Recommendation85 != nil {
		if strings.TrimSpace(*w.Recommendation85) != "" {
			list = append(list, *w.Recommendation85)
		}
	}

	return list
}
