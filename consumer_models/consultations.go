package consumer_models

type ConsultationConsumer struct {
	ID           int    `json:"iddet_interconsulta,omitempty" bson:"iddet_interconsulta,omitempty"`
	AttentionId  int    `json:"idcomprobante,omitempty" bson:"idcomprobante,omitempty"`
	SpecialityID int    `json:"idespecialidad,omitempty" bson:"idespecialidad,omitempty"`
	Replied      string `json:"respondido,omitempty" bson:"respondido,omitempty"`
	Status       string `json:"estado" bson:"estado"`
	IsDeleted    string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog        string
}
