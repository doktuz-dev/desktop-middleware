package consumer_models

type XRayConsumer struct {
	ID          int    `json:"idrayosx,omitempty" bson:"idrayosx,omitempty"`
	AttentionId string `json:"idcomprobante,omitempty" bson:"idcomprobante,omitempty"`
	FileName1   string `json:"descripcion,omitempty" bson:"descripcion,omitempty"`
	FileName2   string `json:"descripcion2,omitempty" bson:"descripcion2,omitempty"`
	FileName3   string `json:"descripcion3,omitempty" bson:"descripcion3,omitempty"`
	Result      string `json:"rxdd,omitempty" bson:"rxdd,omitempty"`
	Diagnostic1 string `json:"diag,omitempty" bson:"diag,omitempty"`
	Diagnostic2 string `json:"diag2,omitempty" bson:"diag2,omitempty"`
	Diagnostic3 string `json:"diag3,omitempty" bson:"diag3,omitempty"`
	Status      int    `json:"estado" bson:"estado"`
	IsDeleted   string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog       string
}
