package consumer_models

import (
	"desktop-middleware/models"
	"math"
	"strconv"
)

type AudiometryConsumer struct {
	ID                       int    `json:"idaudiometrica,omitempty" bson:"idaudiometrica,omitempty"`
	AttentionId              int    `json:"idcomprobante,omitempty" bson:"idcomprobante,omitempty"`
	RightAirConductionR250   string `json:"rcero,omitempty" bson:"rcero,omitempty"`
	RightAirConductionR500   string `json:"runo,omitempty" bson:"runo,omitempty"`
	RightAirConductionR1000  string `json:"rdos,omitempty" bson:"rdos,omitempty"`
	RightAirConductionR2000  string `json:"rtres,omitempty" bson:"rtres,omitempty"`
	RightAirConductionR3000  string `json:"rcuatro,omitempty" bson:"rcuatro,omitempty"`
	RightAirConductionR4000  string `json:"rcinco,omitempty" bson:"rcinco,omitempty"`
	RightAirConductionR6000  string `json:"rseis,omitempty" bson:"rseis,omitempty"`
	RightAirConductionR8000  string `json:"rsiete,omitempty" bson:"rsiete,omitempty"`
	LeftAirConductionR250    string `json:"acero,omitempty" bson:"acero,omitempty"`
	LeftAirConductionR500    string `json:"auno,omitempty" bson:"auno,omitempty"`
	LeftAirConductionR1000   string `json:"ados,omitempty" bson:"ados,omitempty"`
	LeftAirConductionR2000   string `json:"atres,omitempty" bson:"atres,omitempty"`
	LeftAirConductionR3000   string `json:"acuatro,omitempty" bson:"acuatro,omitempty"`
	LeftAirConductionR4000   string `json:"acinco,omitempty" bson:"acinco,omitempty"`
	LeftAirConductionR6000   string `json:"aseis,omitempty" bson:"aseis,omitempty"`
	LeftAirConductionR8000   string `json:"asiete,omitempty" bson:"asiete,omitempty"`
	RightBoneConductionR250  string `json:"raodcero,omitempty" bson:"raodcero,omitempty"`
	RightBoneConductionR500  string `json:"raoduno,omitempty" bson:"raoduno,omitempty"`
	RightBoneConductionR1000 string `json:"raoddos,omitempty" bson:"raoddos,omitempty"`
	RightBoneConductionR2000 string `json:"raodtres,omitempty" bson:"raodtres,omitempty"`
	RightBoneConductionR3000 string `json:"raodcuatro,omitempty" bson:"raodcuatro,omitempty"`
	RightBoneConductionR4000 string `json:"raodcinco,omitempty" bson:"raodcinco,omitempty"`
	RightBoneConductionR6000 string `json:"raodseis,omitempty" bson:"raodseis,omitempty"`
	RightBoneConductionR8000 string `json:"raodsiete,omitempty" bson:"raodsiete,omitempty"`
	LeftBoneConductionR250   string `json:"raoicero,omitempty" bson:"raoicero,omitempty"`
	LeftBoneConductionR500   string `json:"raoiuno,omitempty" bson:"raoiuno,omitempty"`
	LeftBoneConductionR1000  string `json:"raoidos,omitempty" bson:"raoidos,omitempty"`
	LeftBoneConductionR2000  string `json:"raoitres,omitempty" bson:"raoitres,omitempty"`
	LeftBoneConductionR3000  string `json:"raoicuatro,omitempty" bson:"raoicuatro,omitempty"`
	LeftBoneConductionR4000  string `json:"raoicinco,omitempty" bson:"raoicinco,omitempty"`
	LeftBoneConductionR6000  string `json:"raoiseis,omitempty" bson:"raoiseis,omitempty"`
	LeftBoneConductionR8000  string `json:"raoisiete,omitempty" bson:"raoisiete,omitempty"`
	Result                   string `json:"pun_biap,omitempty" bson:"pun_biap,omitempty"`
	OtoscopyRight            string `json:"oi,omitempty" bson:"oi,omitempty"`
	OtoscopyLeft             string `json:"od,omitempty" bson:"od,omitempty"`
	ExposureHour             string `json:"horas_exposicion,omitempty" bson:"horas_exposicion,omitempty"`
	WorkingYears             string `json:"anios_trabajo,omitempty" bson:"anios_trabajo,omitempty"`
	Earplug                  string `json:"upa_tapones" bson:"upa_tapones,omitempty"`
	Earmuffs                 string `json:"upa_orejeras" bson:"upa_orejeras,omitempty"`
	Diagnostic1              string `json:"diag,omitempty" bson:"diag,omitempty"`
	Diagnostic2              string `json:"diag3,omitempty" bson:"diag3,omitempty"`
	Status                   string `json:"estado" bson:"estado"`
	IsDeleted                string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog                    string
}

func (m *AudiometryConsumer) SetAudiometryTest() models.AudiometryTest {
	var newTest models.AudiometryTest
	newTest.RightAirConduction = m.SetRightAirConduction()
	newTest.LeftAirConduction = m.SetLeftAirConduction()
	newTest.RightBoneConduction = m.SetRightBoneConduction()
	newTest.LeftBoneConduction = m.SetLeftBoneConduction()
	if val, err := strconv.ParseFloat(m.Result, 64); err == nil {
		newTest.Result = val
	}
	newTest.OtoscopyRight = m.OtoscopyRight
	newTest.OtoscopyLeft = m.OtoscopyLeft
	newTest.ExposureHour = m.ExposureHour
	newTest.WorkingYears = m.WorkingYears
	if m.Earplug == "on" {
		newTest.Earplug = true
	} else {
		newTest.Earplug = false
	}
	if m.Earmuffs == "on" {
		newTest.Earmuffs = true
	} else {
		newTest.Earmuffs = false
	}
	right := 0.0
	if newTest.RightAirConduction.R250 != nil && newTest.RightAirConduction.R1000 != nil && newTest.RightAirConduction.R2000 != nil && newTest.RightAirConduction.R4000 != nil {
		right = math.Ceil((*newTest.RightAirConduction.R250 + *newTest.RightAirConduction.R1000 + *newTest.RightAirConduction.R2000 + *newTest.RightAirConduction.R4000) / 4)
	}
	left := 0.0
	if newTest.LeftAirConduction.R250 != nil && newTest.LeftAirConduction.R1000 != nil && newTest.LeftAirConduction.R2000 != nil && newTest.LeftAirConduction.R4000 != nil {
		left = math.Ceil((*newTest.LeftAirConduction.R250 + *newTest.LeftAirConduction.R1000 + *newTest.LeftAirConduction.R2000 + *newTest.LeftAirConduction.R4000) / 4)
	}

	if right > 0.0 && left > 0.0 {
		if right < left {
			newTest.Result = (right*7 + left*3) / 10
		} else {
			newTest.Result = (left*7 + right*3) / 10
		}
	}
	status, _ := strconv.Atoi(m.Status)
	newTest.Status = status
	newTest.ExternalId = strconv.Itoa(m.ID)
	newTest.ExternalSystem = "MEDIWEB"
	return newTest
}

func (m *AudiometryConsumer) SetRightAirConduction() *models.Conduction {
	var cond models.Conduction
	if val, err := strconv.ParseFloat(m.RightAirConductionR250, 64); err == nil {
		cond.R250 = &val
	}
	if val, err := strconv.ParseFloat(m.RightAirConductionR500, 64); err == nil {
		cond.R500 = &val
	}
	if val, err := strconv.ParseFloat(m.RightAirConductionR1000, 64); err == nil {
		cond.R1000 = &val
	}
	if val, err := strconv.ParseFloat(m.RightAirConductionR2000, 64); err == nil {
		cond.R2000 = &val
	}
	if val, err := strconv.ParseFloat(m.RightAirConductionR3000, 64); err == nil {
		cond.R3000 = &val
	}
	if val, err := strconv.ParseFloat(m.RightAirConductionR4000, 64); err == nil {
		cond.R4000 = &val
	}
	if val, err := strconv.ParseFloat(m.RightAirConductionR6000, 64); err == nil {
		cond.R6000 = &val
	}
	if val, err := strconv.ParseFloat(m.RightAirConductionR8000, 64); err == nil {
		cond.R8000 = &val
	}
	return &cond
}

func (m *AudiometryConsumer) SetLeftAirConduction() *models.Conduction {
	var cond models.Conduction
	if val, err := strconv.ParseFloat(m.LeftAirConductionR250, 64); err == nil {
		cond.R250 = &val
	}
	if val, err := strconv.ParseFloat(m.LeftAirConductionR500, 64); err == nil {
		cond.R500 = &val
	}
	if val, err := strconv.ParseFloat(m.LeftAirConductionR1000, 64); err == nil {
		cond.R1000 = &val
	}
	if val, err := strconv.ParseFloat(m.LeftAirConductionR2000, 64); err == nil {
		cond.R2000 = &val
	}
	if val, err := strconv.ParseFloat(m.LeftAirConductionR3000, 64); err == nil {
		cond.R3000 = &val
	}
	if val, err := strconv.ParseFloat(m.LeftAirConductionR4000, 64); err == nil {
		cond.R4000 = &val
	}
	if val, err := strconv.ParseFloat(m.LeftAirConductionR6000, 64); err == nil {
		cond.R6000 = &val
	}
	if val, err := strconv.ParseFloat(m.LeftAirConductionR8000, 64); err == nil {
		cond.R8000 = &val
	}
	return &cond
}

func (m *AudiometryConsumer) SetRightBoneConduction() *models.Conduction {
	var cond models.Conduction
	if val, err := strconv.ParseFloat(m.RightBoneConductionR250, 64); err == nil {
		cond.R250 = &val
	}
	if val, err := strconv.ParseFloat(m.RightBoneConductionR500, 64); err == nil {
		cond.R500 = &val
	}
	if val, err := strconv.ParseFloat(m.RightBoneConductionR1000, 64); err == nil {
		cond.R1000 = &val
	}
	if val, err := strconv.ParseFloat(m.RightBoneConductionR2000, 64); err == nil {
		cond.R2000 = &val
	}
	if val, err := strconv.ParseFloat(m.RightBoneConductionR3000, 64); err == nil {
		cond.R3000 = &val
	}
	if val, err := strconv.ParseFloat(m.RightBoneConductionR4000, 64); err == nil {
		cond.R4000 = &val
	}
	if val, err := strconv.ParseFloat(m.RightBoneConductionR6000, 64); err == nil {
		cond.R6000 = &val
	}
	if val, err := strconv.ParseFloat(m.RightBoneConductionR8000, 64); err == nil {
		cond.R8000 = &val
	}
	return &cond
}

func (m *AudiometryConsumer) SetLeftBoneConduction() *models.Conduction {
	var cond models.Conduction
	if val, err := strconv.ParseFloat(m.LeftBoneConductionR250, 64); err == nil {
		cond.R250 = &val
	}
	if val, err := strconv.ParseFloat(m.LeftBoneConductionR500, 64); err == nil {
		cond.R500 = &val
	}
	if val, err := strconv.ParseFloat(m.LeftBoneConductionR1000, 64); err == nil {
		cond.R1000 = &val
	}
	if val, err := strconv.ParseFloat(m.LeftBoneConductionR2000, 64); err == nil {
		cond.R2000 = &val
	}
	if val, err := strconv.ParseFloat(m.LeftBoneConductionR3000, 64); err == nil {
		cond.R3000 = &val
	}
	if val, err := strconv.ParseFloat(m.LeftBoneConductionR4000, 64); err == nil {
		cond.R4000 = &val
	}
	if val, err := strconv.ParseFloat(m.LeftBoneConductionR6000, 64); err == nil {
		cond.R6000 = &val
	}
	if val, err := strconv.ParseFloat(m.LeftBoneConductionR8000, 64); err == nil {
		cond.R8000 = &val
	}
	return &cond
}
