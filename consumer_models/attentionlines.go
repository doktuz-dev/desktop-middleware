package consumer_models

type AttentionLineConsumer struct {
	ID          int    `json:"iddet_comprobante,omitempty" bson:"iddet_comprobante,omitempty"`
	AttentionId int    `json:"idcomprobante,omitempty" bson:"idcomprobante,omitempty"`
	ServiceId   int    `json:"idexamenocu,omitempty" bson:"idexamenocu,omitempty"`
	Status      int    `json:"estado" bson:"estado"`
	IsDeleted   string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog       string
}
