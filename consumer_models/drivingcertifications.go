package consumer_models

type DrivingCertificationConsumer struct {
	ID          int    `json:"idcertmed_conduccion,omitempty" bson:"idcertmed_conduccion,omitempty"`
	AttentionId int    `json:"idcomprobante,omitempty" bson:"idcomprobante,omitempty"`
	Result      string `json:"apto1,omitempty" bson:"apto1,omitempty"`
	Status      string `json:"estado" bson:"estado"`
	IsDeleted   string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog       string
}
