package consumer_models

import "time"

type AttentionConsumer struct {
	ID                  int     `json:"idcomprobante,omitempty" bson:"idcomprobante,omitempty"`
	WorkStationId       int     `json:"idproyecto,omitempty" bson:"idproyecto,omitempty"`
	WorkProfileId       int     `json:"idobra,omitempty" bson:"idobra,omitempty"`
	WorkingArea         string  `json:"area,omitempty" bson:"area,omitempty"`
	ExamTypeId          string  `json:"idtipo,omitempty" bson:"idtipo,omitempty"`
	JobTitle            string  `json:"puesto,omitempty" bson:"puesto,omitempty"`
	HealthcentreId      int     `json:"idlocal,omitempty" bson:"idlocal,omitempty"`
	PatientId           string  `json:"idpaciente,omitempty" bson:"idpaciente,omitempty"`
	CustomerId          string  `json:"idempresa,omitempty" bson:"idempresa,omitempty"`
	CustomerId2         int     `json:"idempresa2,omitempty" bson:"idempresa2=1,omitempty"`
	AttentionTypeId     string  `json:"idservicio,omitempty" bson:"idservicio,omitempty"`
	NoemoDeliveryTypeId int     `json:"idlugar_trabajo,omitempty" bson:"idlugar_trabajo,omitempty"`
	EmoDeliveryTypeId   string  `json:"idtiposervicio,omitempty" bson:"idtiposervicio,omitempty"`
	DepartmentId        int     `json:"iddepartamento,omitempty" bson:"iddepartamento,omitempty"`
	DistrictId          int     `json:"iddistrito,omitempty" bson:"iddistrito,omitempty"`
	AttentionDate       string  `json:"fecha,omitempty" bson:"fecha,omitempty"`
	MailStatus          *int    `json:"estado_correo,omitempty" bson:"estado_correo,omitempty"`
	SentDate            int64   `json:"fechahora_envio,omitempty" bson:"fechahora_envio,omitempty"`
	OpenedDate          int64   `json:"fechahora_abierto,omitempty" bson:"fechahora_abierto,omitempty"`
	ExternalClinicFlag  string  `json:"chk_externo,omitempty" bson:"chk_externo,omitempty"`
	ExternalClinicID    int     `json:"idclinica,omitempty" bson:"idclinica,omitempty"`
	Status              int     `json:"estado" bson:"estado"`
	WasAttended         *string `json:"conformidad2" bson:"conformidad2"`
	IsDeleted           string  `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog               string
}

func (a *AttentionConsumer) SetSentDate() *time.Time {
	sentDate := time.Unix(0, a.SentDate*int64(time.Millisecond))
	return &sentDate
}

func (a *AttentionConsumer) SetOpenedDate() *time.Time {
	openedDate := time.Unix(0, a.OpenedDate*int64(time.Millisecond))
	return &openedDate
}
