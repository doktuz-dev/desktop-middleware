package consumer_models

type HealthCentreConsumer struct {
	ID        int    `json:"idlocal,omitempty" bson:"idlocal,omitempty"`
	Name      string `json:"descripcion,omitempty" bson:"descripcion,omitempty"`
	Status    int    `json:"estado" bson:"status"`
	IsDeleted string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog     string
}
