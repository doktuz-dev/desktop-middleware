package consumer_models

import (
	"desktop-middleware/models"
	"fmt"
	"strconv"
	"strings"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type WorkHistoryConsumer struct {
	ID                  int    `json:"idhistoria_ocupacional,omitempty" bson:"idhistoria_ocupacional,omitempty"`
	AttentionId         int    `json:"idcomprobante,omitempty" bson:"idcomprobante,omitempty"`
	Filename            string `json:"descripcion,omitempty" bson:"descripcion,omitempty"`
	Company1            string `json:"empresa1,omitempty" bson:"empresa1,omitempty"`
	JobTitle1           string `json:"ocupacion1,omitempty" bson:"ocupacion1,omitempty"`
	Dust1               string `json:"polvo1,omitempty" bson:"polvo1,omitempty"`
	Noise1              string `json:"ruido1,omitempty" bson:"ruido1,omitempty"`
	RepetitiveMovement1 string `json:"mov_repetitivos1,omitempty" bson:"mov_repetitivos1,omitempty"`
	SegmentalVibration1 string `json:"vib_segmentaria1,omitempty" bson:"vib_segmentaria1,omitempty"`
	HeavyMetals1        string `json:"met_pesado1,omitempty" bson:"met_pesado1,omitempty"`
	Ergonomic1          string `json:"ergonomico1,omitempty" bson:"ergonomico1,omitempty"`
	TotalVibrations1    string `json:"vib_total1,omitempty" bson:"vib_total1,omitempty"`
	Carcinogenic1       string `json:"cancerigenos1,omitempty" bson:"cancerigenos1,omitempty"`
	Mutagenic1          string `json:"mutagenicos1,omitempty" bson:"mutagenicos1,omitempty"`
	Shifts1             string `json:"turnos1,omitempty" bson:"turnos1,omitempty"`
	Disergonomic1       string `json:"disergon1,omitempty" bson:"disergon1,omitempty"`
	Positions1          string `json:"posturas1,omitempty" bson:"posturas1,omitempty"`
	Falls1              string `json:"caidas1,omitempty" bson:"caidas1,omitempty"`
	Solvent1            string `json:"solventes1,omitempty" bson:"solventes1,omitempty"`
	NightShifts1        string `json:"tnocturnos1,omitempty" bson:"tnocturnos1,omitempty"`
	HighTemperature1    string `json:"alta_t1,omitempty" bson:"alta_t1,omitempty"`
	Biological1         string `json:"biologico1,omitempty" bson:"biologico1,omitempty"`
	Loads1              string `json:"cargas1,omitempty" bson:"cargas1,omitempty"`
	Pvd1                string `json:"pvd1,omitempty" bson:"pvd1,omitempty"`
	Chemical1           string `json:"quimico1,omitempty" bson:"quimico1,omitempty"`
	DangerOthers1       string `json:"otro1,omitempty" bson:"otro1,omitempty"`
	Gloves1             string `json:"guantes1" bson:"guantes1,omitempty"`
	Earplug1            string `json:"tauditivos1" bson:"tauditivos1,omitempty"`
	Earmuffs1           string `json:"orejeras1" bson:"orejeras1,omitempty"`
	Helmet1             string `json:"casco1" bson:"casco1,omitempty"`
	Mask1               string `json:"mascarilla1" bson:"mascarilla1,omitempty"`
	Boots1              string `json:"botas1" bson:"botas1,omitempty"`
	Harness1            string `json:"arnes1" bson:"arnes1,omitempty"`
	Glasses1            string `json:"lentes1" bson:"lentes1,omitempty"`
	Clothes1            string `json:"ropa1" bson:"ropa1,omitempty"`
	Respirators1        string `json:"respiradores1" bson:"respiradores1,omitempty"`
	EppTypeOthers1      string `json:"otro_epp1,omitempty" bson:"otro_epp1,omitempty"`
	Company2            string `json:"empresa2,omitempty" bson:"empresa2,omitempty"`
	JobTitle2           string `json:"ocupacion2,omitempty" bson:"ocupacion2,omitempty"`
	Dust2               string `json:"polvo2,omitempty" bson:"polvo2,omitempty"`
	Noise2              string `json:"ruido2,omitempty" bson:"ruido2,omitempty"`
	RepetitiveMovement2 string `json:"mov_repetitivos2,omitempty" bson:"mov_repetitivos2,omitempty"`
	SegmentalVibration2 string `json:"vib_segmentaria2,omitempty" bson:"vib_segmentaria2,omitempty"`
	HeavyMetals2        string `json:"met_pesado2,omitempty" bson:"met_pesado2,omitempty"`
	Ergonomic2          string `json:"ergonomico2,omitempty" bson:"ergonomico2,omitempty"`
	TotalVibrations2    string `json:"vib_total2,omitempty" bson:"vib_total2,omitempty"`
	Carcinogenic2       string `json:"cancerigenos2,omitempty" bson:"cancerigenos2,omitempty"`
	Mutagenic2          string `json:"mutagenicos2,omitempty" bson:"mutagenicos2,omitempty"`
	Shifts2             string `json:"turnos2,omitempty" bson:"turnos2,omitempty"`
	Disergonomic2       string `json:"disergon2,omitempty" bson:"disergon2,omitempty"`
	Positions2          string `json:"posturas2,omitempty" bson:"posturas2,omitempty"`
	Falls2              string `json:"caidas2,omitempty" bson:"caidas2,omitempty"`
	Solvent2            string `json:"solventes2,omitempty" bson:"solventes2,omitempty"`
	NightShifts2        string `json:"tnocturnos2,omitempty" bson:"tnocturnos2,omitempty"`
	HighTemperature2    string `json:"alta_t2,omitempty" bson:"alta_t2,omitempty"`
	Biological2         string `json:"biologico2,omitempty" bson:"biologico2,omitempty"`
	Loads2              string `json:"cargas2,omitempty" bson:"cargas2,omitempty"`
	Pvd2                string `json:"pvd2,omitempty" bson:"pvd2,omitempty"`
	Chemical2           string `json:"quimico2,omitempty" bson:"quimico2,omitempty"`
	DangerOthers2       string `json:"otro2,omitempty" bson:"otro2,omitempty"`
	Gloves2             string `json:"guantes2" bson:"guantes2,omitempty"`
	Earplug2            string `json:"tauditivos2" bson:"tauditivos2,omitempty"`
	Earmuffs2           string `json:"orejeras2" bson:"orejeras2,omitempty"`
	Helmet2             string `json:"casco2" bson:"casco2,omitempty"`
	Mask2               string `json:"mascarilla2" bson:"mascarilla2,omitempty"`
	Boots2              string `json:"botas2" bson:"botas2,omitempty"`
	Harness2            string `json:"arnes2" bson:"arnes2,omitempty"`
	Glasses2            string `json:"lentes2" bson:"lentes2,omitempty"`
	Clothes2            string `json:"ropa2" bson:"ropa2,omitempty"`
	Respirators2        string `json:"respiradores2" bson:"respiradores2,omitempty"`
	EppTypeOthers2      string `json:"otro_epp2,omitempty" bson:"otro_epp2,omitempty"`
	Company3            string `json:"empresa3,omitempty" bson:"empresa3,omitempty"`
	JobTitle3           string `json:"ocupacion3,omitempty" bson:"ocupacion3,omitempty"`
	Dust3               string `json:"polvo3,omitempty" bson:"polvo3,omitempty"`
	Noise3              string `json:"ruido3,omitempty" bson:"ruido3,omitempty"`
	RepetitiveMovement3 string `json:"mov_repetitivos3,omitempty" bson:"mov_repetitivos3,omitempty"`
	SegmentalVibration3 string `json:"vib_segmentaria3,omitempty" bson:"vib_segmentaria3,omitempty"`
	HeavyMetals3        string `json:"met_pesado3,omitempty" bson:"met_pesado3,omitempty"`
	Ergonomic3          string `json:"ergonomico3,omitempty" bson:"ergonomico3,omitempty"`
	TotalVibrations3    string `json:"vib_total3,omitempty" bson:"vib_total3,omitempty"`
	Carcinogenic3       string `json:"cancerigenos3,omitempty" bson:"cancerigenos3,omitempty"`
	Mutagenic3          string `json:"mutagenicos3,omitempty" bson:"mutagenicos3,omitempty"`
	Shifts3             string `json:"turnos3,omitempty" bson:"turnos3,omitempty"`
	Disergonomic3       string `json:"disergon3,omitempty" bson:"disergon3,omitempty"`
	Positions3          string `json:"posturas3,omitempty" bson:"posturas3,omitempty"`
	Falls3              string `json:"caidas3,omitempty" bson:"caidas3,omitempty"`
	Solvent3            string `json:"solventes3,omitempty" bson:"solventes3,omitempty"`
	NightShifts3        string `json:"tnocturnos3,omitempty" bson:"tnocturnos3,omitempty"`
	HighTemperature3    string `json:"alta_t3,omitempty" bson:"alta_t3,omitempty"`
	Biological3         string `json:"biologico3,omitempty" bson:"biologico3,omitempty"`
	Loads3              string `json:"cargas3,omitempty" bson:"cargas3,omitempty"`
	Pvd3                string `json:"pvd3,omitempty" bson:"pvd3,omitempty"`
	Chemical3           string `json:"quimico3,omitempty" bson:"quimico3,omitempty"`
	DangerOthers3       string `json:"otro3,omitempty" bson:"otro3,omitempty"`
	Gloves3             string `json:"guantes3" bson:"guantes3,omitempty"`
	Earplug3            string `json:"tauditivos3" bson:"tauditivos3,omitempty"`
	Earmuffs3           string `json:"orejeras3" bson:"orejeras3,omitempty"`
	Helmet3             string `json:"casco3" bson:"casco3,omitempty"`
	Mask3               string `json:"mascarilla3" bson:"mascarilla3,omitempty"`
	Boots3              string `json:"botas3" bson:"botas3,omitempty"`
	Harness3            string `json:"arnes3" bson:"arnes3,omitempty"`
	Glasses3            string `json:"lentes3" bson:"lentes3,omitempty"`
	Clothes3            string `json:"ropa3" bson:"ropa3,omitempty"`
	Respirators3        string `json:"respiradores3" bson:"respiradores3,omitempty"`
	EppTypeOthers3      string `json:"otro_epp3,omitempty" bson:"otro_epp3,omitempty"`
	Company4            string `json:"empresa4,omitempty" bson:"empresa4,omitempty"`
	JobTitle4           string `json:"ocupacion4,omitempty" bson:"ocupacion4,omitempty"`
	Dust4               string `json:"polvo4,omitempty" bson:"polvo4,omitempty"`
	Noise4              string `json:"ruido4,omitempty" bson:"ruido4,omitempty"`
	RepetitiveMovement4 string `json:"mov_repetitivos4,omitempty" bson:"mov_repetitivos4,omitempty"`
	SegmentalVibration4 string `json:"vib_segmentaria4,omitempty" bson:"vib_segmentaria4,omitempty"`
	HeavyMetals4        string `json:"met_pesado4,omitempty" bson:"met_pesado4,omitempty"`
	Ergonomic4          string `json:"ergonomico4,omitempty" bson:"ergonomico4,omitempty"`
	TotalVibrations4    string `json:"vib_total4,omitempty" bson:"vib_total4,omitempty"`
	Carcinogenic4       string `json:"cancerigenos4,omitempty" bson:"cancerigenos4,omitempty"`
	Mutagenic4          string `json:"mutagenicos4,omitempty" bson:"mutagenicos4,omitempty"`
	Shifts4             string `json:"turnos4,omitempty" bson:"turnos4,omitempty"`
	Disergonomic4       string `json:"disergon4,omitempty" bson:"disergon4,omitempty"`
	Positions4          string `json:"posturas4,omitempty" bson:"posturas4,omitempty"`
	Falls4              string `json:"caidas4,omitempty" bson:"caidas4,omitempty"`
	Solvent4            string `json:"solventes4,omitempty" bson:"solventes4,omitempty"`
	NightShifts4        string `json:"tnocturnos4,omitempty" bson:"tnocturnos4,omitempty"`
	HighTemperature4    string `json:"alta_t4,omitempty" bson:"alta_t4,omitempty"`
	Biological4         string `json:"biologico4,omitempty" bson:"biologico4,omitempty"`
	Loads4              string `json:"cargas4,omitempty" bson:"cargas4,omitempty"`
	Pvd4                string `json:"pvd4,omitempty" bson:"pvd4,omitempty"`
	Chemical4           string `json:"quimico4,omitempty" bson:"quimico4,omitempty"`
	DangerOthers4       string `json:"otro4,omitempty" bson:"otro4,omitempty"`
	Gloves4             string `json:"guantes4" bson:"guantes4,omitempty"`
	Earplug4            string `json:"tauditivos4" bson:"tauditivos4,omitempty"`
	Earmuffs4           string `json:"orejeras4" bson:"orejeras4,omitempty"`
	Helmet4             string `json:"casco4" bson:"casco4,omitempty"`
	Mask4               string `json:"mascarilla4" bson:"mascarilla4,omitempty"`
	Boots4              string `json:"botas4" bson:"botas4,omitempty"`
	Harness4            string `json:"arnes4" bson:"arnes4,omitempty"`
	Glasses4            string `json:"lentes4" bson:"lentes4,omitempty"`
	Clothes4            string `json:"ropa4" bson:"ropa4,omitempty"`
	Respirators4        string `json:"respiradores4" bson:"respiradores4,omitempty"`
	EppTypeOthers4      string `json:"otro_epp4,omitempty" bson:"otro_epp4,omitempty"`
	Company5            string `json:"empresa5,omitempty" bson:"empresa5,omitempty"`
	JobTitle5           string `json:"ocupacion5,omitempty" bson:"ocupacion5,omitempty"`
	Dust5               string `json:"polvo5,omitempty" bson:"polvo5,omitempty"`
	Noise5              string `json:"ruido5,omitempty" bson:"ruido5,omitempty"`
	RepetitiveMovement5 string `json:"mov_repetitivos5,omitempty" bson:"mov_repetitivos5,omitempty"`
	SegmentalVibration5 string `json:"vib_segmentaria5,omitempty" bson:"vib_segmentaria5,omitempty"`
	HeavyMetals5        string `json:"met_pesado5,omitempty" bson:"met_pesado5,omitempty"`
	Ergonomic5          string `json:"ergonomico5,omitempty" bson:"ergonomico5,omitempty"`
	TotalVibrations5    string `json:"vib_total5,omitempty" bson:"vib_total5,omitempty"`
	Carcinogenic5       string `json:"cancerigenos5,omitempty" bson:"cancerigenos5,omitempty"`
	Mutagenic5          string `json:"mutagenicos5,omitempty" bson:"mutagenicos5,omitempty"`
	Shifts5             string `json:"turnos5,omitempty" bson:"turnos5,omitempty"`
	Disergonomic5       string `json:"disergon5,omitempty" bson:"disergon5,omitempty"`
	Positions5          string `json:"posturas5,omitempty" bson:"posturas5,omitempty"`
	Falls5              string `json:"caidas5,omitempty" bson:"caidas5,omitempty"`
	Solvent5            string `json:"solventes5,omitempty" bson:"solventes5,omitempty"`
	NightShifts5        string `json:"tnocturnos5,omitempty" bson:"tnocturnos5,omitempty"`
	HighTemperature5    string `json:"alta_t5,omitempty" bson:"alta_t5,omitempty"`
	Biological5         string `json:"biologico5,omitempty" bson:"biologico5,omitempty"`
	Loads5              string `json:"cargas5,omitempty" bson:"cargas5,omitempty"`
	Pvd5                string `json:"pvd5,omitempty" bson:"pvd5,omitempty"`
	Chemical5           string `json:"quimico5,omitempty" bson:"quimico5,omitempty"`
	DangerOthers5       string `json:"otro5,omitempty" bson:"otro5,omitempty"`
	Gloves5             string `json:"guantes5" bson:"guantes5,omitempty"`
	Earplug5            string `json:"tauditivos5" bson:"tauditivos5,omitempty"`
	Earmuffs5           string `json:"orejeras5" bson:"orejeras5,omitempty"`
	Helmet5             string `json:"casco5" bson:"casco5,omitempty"`
	Mask5               string `json:"mascarilla5" bson:"mascarilla5,omitempty"`
	Boots5              string `json:"botas5" bson:"botas5,omitempty"`
	Harness5            string `json:"arnes5" bson:"arnes5,omitempty"`
	Glasses5            string `json:"lentes5" bson:"lentes5,omitempty"`
	Clothes5            string `json:"ropa5" bson:"ropa5,omitempty"`
	Respirators5        string `json:"respiradores5" bson:"respiradores5,omitempty"`
	EppTypeOthers5      string `json:"otro_epp5,omitempty" bson:"otro_epp5,omitempty"`
	Company6            string `json:"empresa6,omitempty" bson:"empresa6,omitempty"`
	JobTitle6           string `json:"ocupacion6,omitempty" bson:"ocupacion6,omitempty"`
	Dust6               string `json:"polvo6,omitempty" bson:"polvo6,omitempty"`
	Noise6              string `json:"ruido6,omitempty" bson:"ruido6,omitempty"`
	RepetitiveMovement6 string `json:"mov_repetitivos6,omitempty" bson:"mov_repetitivos6,omitempty"`
	SegmentalVibration6 string `json:"vib_segmentaria6,omitempty" bson:"vib_segmentaria6,omitempty"`
	HeavyMetals6        string `json:"met_pesado6,omitempty" bson:"met_pesado6,omitempty"`
	Ergonomic6          string `json:"ergonomico6,omitempty" bson:"ergonomico6,omitempty"`
	TotalVibrations6    string `json:"vib_total6,omitempty" bson:"vib_total6,omitempty"`
	Carcinogenic6       string `json:"cancerigenos6,omitempty" bson:"cancerigenos6,omitempty"`
	Mutagenic6          string `json:"mutagenicos6,omitempty" bson:"mutagenicos6,omitempty"`
	Shifts6             string `json:"turnos6,omitempty" bson:"turnos6,omitempty"`
	Disergonomic6       string `json:"disergon6,omitempty" bson:"disergon6,omitempty"`
	Positions6          string `json:"posturas6,omitempty" bson:"posturas6,omitempty"`
	Falls6              string `json:"caidas6,omitempty" bson:"caidas6,omitempty"`
	Solvent6            string `json:"solventes6,omitempty" bson:"solventes6,omitempty"`
	NightShifts6        string `json:"tnocturnos6,omitempty" bson:"tnocturnos6,omitempty"`
	HighTemperature6    string `json:"alta_t6,omitempty" bson:"alta_t6,omitempty"`
	Biological6         string `json:"biologico6,omitempty" bson:"biologico6,omitempty"`
	Loads6              string `json:"cargas6,omitempty" bson:"cargas6,omitempty"`
	Pvd6                string `json:"pvd6,omitempty" bson:"pvd6,omitempty"`
	Chemical6           string `json:"quimico6,omitempty" bson:"quimico6,omitempty"`
	DangerOthers6       string `json:"otro6,omitempty" bson:"otro6,omitempty"`
	Gloves6             string `json:"guantes6" bson:"guantes6,omitempty"`
	Earplug6            string `json:"tauditivos6" bson:"tauditivos6,omitempty"`
	Earmuffs6           string `json:"orejeras6" bson:"orejeras6,omitempty"`
	Helmet6             string `json:"casco6" bson:"casco6,omitempty"`
	Mask6               string `json:"mascarilla6" bson:"mascarilla6,omitempty"`
	Boots6              string `json:"botas6" bson:"botas6,omitempty"`
	Harness6            string `json:"arnes6" bson:"arnes6,omitempty"`
	Glasses6            string `json:"lentes6" bson:"lentes6,omitempty"`
	Clothes6            string `json:"ropa6" bson:"ropa6,omitempty"`
	Respirators6        string `json:"respiradores6" bson:"respiradores6,omitempty"`
	EppTypeOthers6      string `json:"otro_epp6,omitempty" bson:"otro_epp6,omitempty"`
	Company7            string `json:"empresa7,omitempty" bson:"empresa7,omitempty"`
	JobTitle7           string `json:"ocupacion7,omitempty" bson:"ocupacion7,omitempty"`
	Dust7               string `json:"polvo7,omitempty" bson:"polvo7,omitempty"`
	Noise7              string `json:"ruido7,omitempty" bson:"ruido7,omitempty"`
	RepetitiveMovement7 string `json:"mov_repetitivos7,omitempty" bson:"mov_repetitivos7,omitempty"`
	SegmentalVibration7 string `json:"vib_segmentaria7,omitempty" bson:"vib_segmentaria7,omitempty"`
	HeavyMetals7        string `json:"met_pesado7,omitempty" bson:"met_pesado7,omitempty"`
	Ergonomic7          string `json:"ergonomico7,omitempty" bson:"ergonomico7,omitempty"`
	TotalVibrations7    string `json:"vib_total7,omitempty" bson:"vib_total7,omitempty"`
	Carcinogenic7       string `json:"cancerigenos7,omitempty" bson:"cancerigenos7,omitempty"`
	Mutagenic7          string `json:"mutagenicos7,omitempty" bson:"mutagenicos7,omitempty"`
	Shifts7             string `json:"turnos7,omitempty" bson:"turnos7,omitempty"`
	Disergonomic7       string `json:"disergon7,omitempty" bson:"disergon7,omitempty"`
	Positions7          string `json:"posturas7,omitempty" bson:"posturas7,omitempty"`
	Falls7              string `json:"caidas7,omitempty" bson:"caidas7,omitempty"`
	Solvent7            string `json:"solventes7,omitempty" bson:"solventes7,omitempty"`
	NightShifts7        string `json:"tnocturnos7,omitempty" bson:"tnocturnos7,omitempty"`
	HighTemperature7    string `json:"alta_t7,omitempty" bson:"alta_t7,omitempty"`
	Biological7         string `json:"biologico7,omitempty" bson:"biologico7,omitempty"`
	Loads7              string `json:"cargas7,omitempty" bson:"cargas7,omitempty"`
	Pvd7                string `json:"pvd7,omitempty" bson:"pvd7,omitempty"`
	Chemical7           string `json:"quimico7,omitempty" bson:"quimico7,omitempty"`
	DangerOthers7       string `json:"otro7,omitempty" bson:"otro7,omitempty"`
	Gloves7             string `json:"guantes7" bson:"guantes7,omitempty"`
	Earplug7            string `json:"tauditivos7" bson:"tauditivos7,omitempty"`
	Earmuffs7           string `json:"orejeras7" bson:"orejeras7,omitempty"`
	Helmet7             string `json:"casco7" bson:"casco7,omitempty"`
	Mask7               string `json:"mascarilla7" bson:"mascarilla7,omitempty"`
	Boots7              string `json:"botas7" bson:"botas7,omitempty"`
	Harness7            string `json:"arnes7" bson:"arnes7,omitempty"`
	Glasses7            string `json:"lentes7" bson:"lentes7,omitempty"`
	Clothes7            string `json:"ropa7" bson:"ropa7,omitempty"`
	Respirators7        string `json:"respiradores7" bson:"respiradores7,omitempty"`
	EppTypeOthers7      string `json:"otro_epp7,omitempty" bson:"otro_epp7,omitempty"`
	Status              string `json:"estado" bson:"estado"`
	IsDeleted           string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog               string
}

func (m *WorkHistoryConsumer) SetWorkHistories(attentionId, personId, patientId string) []interface{} {
	var results []interface{}
	status, _ := strconv.Atoi(m.Status)
	var filenames []string
	if m.Filename != "" {
		img := strings.Split(m.Filename, ".")
		filenames = append(filenames, fmt.Sprintf("1.%s", img[1]))
	}
	if m.Company1 != "" {
		result := models.WorkHistory{
			ID:              primitive.NewObjectID().Hex(),
			AttentionId:     attentionId,
			PersonId:        personId,
			PatientId:       patientId,
			Filename:        filenames,
			Company:         m.Company1,
			JobTitle:        m.JobTitle1,
			Dangers:         m.SetDanger1(),
			EppType:         m.SetEppType1(),
			SequenceNumber:  1,
			Status:          status,
			ExternalId:      strconv.Itoa(m.ID),
			ExternalSystem:  "MEDIWEB",
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
		}
		results = append(results, &result)
	}
	if m.Company2 != "" {
		result := models.WorkHistory{
			ID:              primitive.NewObjectID().Hex(),
			AttentionId:     attentionId,
			PersonId:        personId,
			PatientId:       patientId,
			Filename:        filenames,
			Company:         m.Company2,
			JobTitle:        m.JobTitle2,
			Dangers:         m.SetDanger2(),
			EppType:         m.SetEppType2(),
			SequenceNumber:  2,
			Status:          status,
			ExternalId:      strconv.Itoa(m.ID),
			ExternalSystem:  "MEDIWEB",
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
		}
		results = append(results, &result)
	}
	if m.Company3 != "" {
		result := models.WorkHistory{
			ID:              primitive.NewObjectID().Hex(),
			AttentionId:     attentionId,
			PersonId:        personId,
			PatientId:       patientId,
			Filename:        filenames,
			Company:         m.Company3,
			JobTitle:        m.JobTitle3,
			Dangers:         m.SetDanger3(),
			EppType:         m.SetEppType3(),
			SequenceNumber:  3,
			Status:          status,
			ExternalId:      strconv.Itoa(m.ID),
			ExternalSystem:  "MEDIWEB",
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
		}
		results = append(results, &result)
	}
	if m.Company4 != "" {
		result := models.WorkHistory{
			ID:              primitive.NewObjectID().Hex(),
			AttentionId:     attentionId,
			PersonId:        personId,
			PatientId:       patientId,
			Filename:        filenames,
			Company:         m.Company4,
			JobTitle:        m.JobTitle4,
			Dangers:         m.SetDanger4(),
			EppType:         m.SetEppType4(),
			SequenceNumber:  4,
			Status:          status,
			ExternalId:      strconv.Itoa(m.ID),
			ExternalSystem:  "MEDIWEB",
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
		}
		results = append(results, &result)
	}
	if m.Company5 != "" {
		result := models.WorkHistory{
			ID:              primitive.NewObjectID().Hex(),
			AttentionId:     attentionId,
			PersonId:        personId,
			PatientId:       patientId,
			Filename:        filenames,
			Company:         m.Company5,
			JobTitle:        m.JobTitle5,
			Dangers:         m.SetDanger5(),
			EppType:         m.SetEppType5(),
			SequenceNumber:  5,
			Status:          status,
			ExternalId:      strconv.Itoa(m.ID),
			ExternalSystem:  "MEDIWEB",
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
		}
		results = append(results, &result)
	}
	if m.Company6 != "" {
		result := models.WorkHistory{
			ID:              primitive.NewObjectID().Hex(),
			AttentionId:     attentionId,
			PersonId:        personId,
			PatientId:       patientId,
			Filename:        filenames,
			Company:         m.Company6,
			JobTitle:        m.JobTitle6,
			Dangers:         m.SetDanger6(),
			EppType:         m.SetEppType6(),
			SequenceNumber:  6,
			Status:          status,
			ExternalId:      strconv.Itoa(m.ID),
			ExternalSystem:  "MEDIWEB",
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
		}
		results = append(results, &result)
	}
	if m.Company7 != "" {
		result := models.WorkHistory{
			ID:              primitive.NewObjectID().Hex(),
			AttentionId:     attentionId,
			PersonId:        personId,
			PatientId:       patientId,
			Filename:        filenames,
			Company:         m.Company7,
			JobTitle:        m.JobTitle7,
			Dangers:         m.SetDanger7(),
			EppType:         m.SetEppType7(),
			SequenceNumber:  7,
			Status:          status,
			ExternalId:      strconv.Itoa(m.ID),
			ExternalSystem:  "MEDIWEB",
			CreatedDate:     time.Now(),
			LastUpdatedDate: time.Now(),
		}
		results = append(results, &result)
	}
	return results
}

func (m *WorkHistoryConsumer) SetDanger1() *models.Dangers {
	var danger1 models.Dangers
	if m.Dust1 == "on" {
		danger1.Dust = true
	} else {
		danger1.Dust = false
	}
	if m.Noise1 == "on" {
		danger1.Noise = true
	} else {
		danger1.Noise = false
	}
	if m.RepetitiveMovement1 == "on" {
		danger1.RepetitiveMovement = true
	} else {
		danger1.RepetitiveMovement = false
	}
	if m.SegmentalVibration1 == "on" {
		danger1.SegmentalVibration = true
	} else {
		danger1.SegmentalVibration = false
	}
	if m.HeavyMetals1 == "on" {
		danger1.HeavyMetals = true
	} else {
		danger1.HeavyMetals = false
	}
	if m.Ergonomic1 == "on" {
		danger1.Ergonomic = true
	} else {
		danger1.Ergonomic = false
	}
	if m.TotalVibrations1 == "on" {
		danger1.TotalVibrations = true
	} else {
		danger1.TotalVibrations = false
	}
	if m.Carcinogenic1 == "on" {
		danger1.Carcinogenic = true
	} else {
		danger1.Carcinogenic = false
	}
	if m.Mutagenic1 == "on" {
		danger1.Mutagenic = true
	} else {
		danger1.Mutagenic = false
	}
	if m.Shifts1 == "on" {
		danger1.Shifts = true
	} else {
		danger1.Shifts = false
	}
	if m.Disergonomic1 == "on" {
		danger1.Disergonomic = true
	} else {
		danger1.Disergonomic = false
	}
	if m.Positions1 == "on" {
		danger1.Positions = true
	} else {
		danger1.Positions = false
	}
	if m.Falls1 == "on" {
		danger1.Falls = true
	} else {
		danger1.Falls = false
	}
	if m.Solvent1 == "on" {
		danger1.Solvent = true
	} else {
		danger1.Solvent = false
	}
	if m.NightShifts1 == "on" {
		danger1.NightShifts = true
	} else {
		danger1.NightShifts = false
	}
	if m.HighTemperature1 == "on" {
		danger1.HighTemperature = true
	} else {
		danger1.HighTemperature = false
	}
	if m.Biological1 == "on" {
		danger1.Biological = true
	} else {
		danger1.Biological = false
	}
	if m.Loads1 == "on" {
		danger1.Loads = true
	} else {
		danger1.Loads = false
	}
	if m.Pvd1 == "on" {
		danger1.Pvd = true
	} else {
		danger1.Pvd = false
	}
	if m.Chemical1 == "on" {
		danger1.Chemical = true
	} else {
		danger1.Chemical = false
	}
	if m.DangerOthers1 == "on" {
		danger1.Others = true
	} else {
		danger1.Others = false
	}
	if m.Biological1 == "on" {
		danger1.Biological = true
	} else {
		danger1.Biological = false
	}
	if m.Biological1 == "on" {
		danger1.Biological = true
	} else {
		danger1.Biological = false
	}
	return &danger1
}

func (m *WorkHistoryConsumer) SetEppType1() *models.EppType {
	var eppType1 models.EppType
	if m.Gloves1 == "on" {
		eppType1.Gloves = true
	} else {
		eppType1.Gloves = false
	}
	if m.Earplug1 == "on" {
		eppType1.Earplug = true
	} else {
		eppType1.Earplug = false
	}
	if m.Earmuffs1 == "on" {
		eppType1.Earmuffs = true
	} else {
		eppType1.Earmuffs = false
	}
	if m.Helmet1 == "on" {
		eppType1.Helmet = true
	} else {
		eppType1.Helmet = false
	}
	if m.Mask1 == "on" {
		eppType1.Mask = true
	} else {
		eppType1.Mask = false
	}
	if m.Boots1 == "on" {
		eppType1.Boots = true
	} else {
		eppType1.Boots = false
	}
	if m.Harness1 == "on" {
		eppType1.Harness = true
	} else {
		eppType1.Harness = false
	}
	if m.Glasses1 == "on" {
		eppType1.Glasses = true
	} else {
		eppType1.Glasses = false
	}
	if m.Clothes1 == "on" {
		eppType1.Clothes = true
	} else {
		eppType1.Clothes = false
	}
	if m.Respirators1 == "on" {
		eppType1.Respirators = true
	} else {
		eppType1.Respirators = false
	}
	if m.EppTypeOthers1 == "on" {
		eppType1.Others = true
	} else {
		eppType1.Others = false
	}
	return &eppType1
}

func (m *WorkHistoryConsumer) SetDanger2() *models.Dangers {
	var danger2 models.Dangers
	if m.Dust2 == "on" {
		danger2.Dust = true
	} else {
		danger2.Dust = false
	}
	if m.Noise2 == "on" {
		danger2.Noise = true
	} else {
		danger2.Noise = false
	}
	if m.RepetitiveMovement2 == "on" {
		danger2.RepetitiveMovement = true
	} else {
		danger2.RepetitiveMovement = false
	}
	if m.SegmentalVibration2 == "on" {
		danger2.SegmentalVibration = true
	} else {
		danger2.SegmentalVibration = false
	}
	if m.HeavyMetals2 == "on" {
		danger2.HeavyMetals = true
	} else {
		danger2.HeavyMetals = false
	}
	if m.Ergonomic2 == "on" {
		danger2.Ergonomic = true
	} else {
		danger2.Ergonomic = false
	}
	if m.TotalVibrations2 == "on" {
		danger2.TotalVibrations = true
	} else {
		danger2.TotalVibrations = false
	}
	if m.Carcinogenic2 == "on" {
		danger2.Carcinogenic = true
	} else {
		danger2.Carcinogenic = false
	}
	if m.Mutagenic2 == "on" {
		danger2.Mutagenic = true
	} else {
		danger2.Mutagenic = false
	}
	if m.Shifts2 == "on" {
		danger2.Shifts = true
	} else {
		danger2.Shifts = false
	}
	if m.Disergonomic2 == "on" {
		danger2.Disergonomic = true
	} else {
		danger2.Disergonomic = false
	}
	if m.Positions2 == "on" {
		danger2.Positions = true
	} else {
		danger2.Positions = false
	}
	if m.Falls2 == "on" {
		danger2.Falls = true
	} else {
		danger2.Falls = false
	}
	if m.Solvent2 == "on" {
		danger2.Solvent = true
	} else {
		danger2.Solvent = false
	}
	if m.NightShifts2 == "on" {
		danger2.NightShifts = true
	} else {
		danger2.NightShifts = false
	}
	if m.HighTemperature2 == "on" {
		danger2.HighTemperature = true
	} else {
		danger2.HighTemperature = false
	}
	if m.Biological2 == "on" {
		danger2.Biological = true
	} else {
		danger2.Biological = false
	}
	if m.Loads2 == "on" {
		danger2.Loads = true
	} else {
		danger2.Loads = false
	}
	if m.Pvd2 == "on" {
		danger2.Pvd = true
	} else {
		danger2.Pvd = false
	}
	if m.Chemical2 == "on" {
		danger2.Chemical = true
	} else {
		danger2.Chemical = false
	}
	if m.DangerOthers2 == "on" {
		danger2.Others = true
	} else {
		danger2.Others = false
	}
	if m.Biological2 == "on" {
		danger2.Biological = true
	} else {
		danger2.Biological = false
	}
	if m.Biological2 == "on" {
		danger2.Biological = true
	} else {
		danger2.Biological = false
	}
	return &danger2
}

func (m *WorkHistoryConsumer) SetEppType2() *models.EppType {
	var eppType2 models.EppType
	if m.Gloves2 == "on" {
		eppType2.Gloves = true
	} else {
		eppType2.Gloves = false
	}
	if m.Earplug2 == "on" {
		eppType2.Earplug = true
	} else {
		eppType2.Earplug = false
	}
	if m.Earmuffs2 == "on" {
		eppType2.Earmuffs = true
	} else {
		eppType2.Earmuffs = false
	}
	if m.Helmet2 == "on" {
		eppType2.Helmet = true
	} else {
		eppType2.Helmet = false
	}
	if m.Mask2 == "on" {
		eppType2.Mask = true
	} else {
		eppType2.Mask = false
	}
	if m.Boots2 == "on" {
		eppType2.Boots = true
	} else {
		eppType2.Boots = false
	}
	if m.Harness2 == "on" {
		eppType2.Harness = true
	} else {
		eppType2.Harness = false
	}
	if m.Glasses2 == "on" {
		eppType2.Glasses = true
	} else {
		eppType2.Glasses = false
	}
	if m.Clothes2 == "on" {
		eppType2.Clothes = true
	} else {
		eppType2.Clothes = false
	}
	if m.Respirators2 == "on" {
		eppType2.Respirators = true
	} else {
		eppType2.Respirators = false
	}
	if m.EppTypeOthers2 == "on" {
		eppType2.Others = true
	} else {
		eppType2.Others = false
	}
	return &eppType2
}

func (m *WorkHistoryConsumer) SetDanger3() *models.Dangers {
	var danger3 models.Dangers
	if m.Dust3 == "on" {
		danger3.Dust = true
	} else {
		danger3.Dust = false
	}
	if m.Noise3 == "on" {
		danger3.Noise = true
	} else {
		danger3.Noise = false
	}
	if m.RepetitiveMovement3 == "on" {
		danger3.RepetitiveMovement = true
	} else {
		danger3.RepetitiveMovement = false
	}
	if m.SegmentalVibration3 == "on" {
		danger3.SegmentalVibration = true
	} else {
		danger3.SegmentalVibration = false
	}
	if m.HeavyMetals3 == "on" {
		danger3.HeavyMetals = true
	} else {
		danger3.HeavyMetals = false
	}
	if m.Ergonomic3 == "on" {
		danger3.Ergonomic = true
	} else {
		danger3.Ergonomic = false
	}
	if m.TotalVibrations3 == "on" {
		danger3.TotalVibrations = true
	} else {
		danger3.TotalVibrations = false
	}
	if m.Carcinogenic3 == "on" {
		danger3.Carcinogenic = true
	} else {
		danger3.Carcinogenic = false
	}
	if m.Mutagenic3 == "on" {
		danger3.Mutagenic = true
	} else {
		danger3.Mutagenic = false
	}
	if m.Shifts3 == "on" {
		danger3.Shifts = true
	} else {
		danger3.Shifts = false
	}
	if m.Disergonomic3 == "on" {
		danger3.Disergonomic = true
	} else {
		danger3.Disergonomic = false
	}
	if m.Positions3 == "on" {
		danger3.Positions = true
	} else {
		danger3.Positions = false
	}
	if m.Falls3 == "on" {
		danger3.Falls = true
	} else {
		danger3.Falls = false
	}
	if m.Solvent3 == "on" {
		danger3.Solvent = true
	} else {
		danger3.Solvent = false
	}
	if m.NightShifts3 == "on" {
		danger3.NightShifts = true
	} else {
		danger3.NightShifts = false
	}
	if m.HighTemperature3 == "on" {
		danger3.HighTemperature = true
	} else {
		danger3.HighTemperature = false
	}
	if m.Biological3 == "on" {
		danger3.Biological = true
	} else {
		danger3.Biological = false
	}
	if m.Loads3 == "on" {
		danger3.Loads = true
	} else {
		danger3.Loads = false
	}
	if m.Pvd3 == "on" {
		danger3.Pvd = true
	} else {
		danger3.Pvd = false
	}
	if m.Chemical3 == "on" {
		danger3.Chemical = true
	} else {
		danger3.Chemical = false
	}
	if m.DangerOthers3 == "on" {
		danger3.Others = true
	} else {
		danger3.Others = false
	}
	if m.Biological3 == "on" {
		danger3.Biological = true
	} else {
		danger3.Biological = false
	}
	if m.Biological3 == "on" {
		danger3.Biological = true
	} else {
		danger3.Biological = false
	}
	return &danger3
}

func (m *WorkHistoryConsumer) SetEppType3() *models.EppType {
	var eppType3 models.EppType
	if m.Gloves3 == "on" {
		eppType3.Gloves = true
	} else {
		eppType3.Gloves = false
	}
	if m.Earplug3 == "on" {
		eppType3.Earplug = true
	} else {
		eppType3.Earplug = false
	}
	if m.Earmuffs3 == "on" {
		eppType3.Earmuffs = true
	} else {
		eppType3.Earmuffs = false
	}
	if m.Helmet3 == "on" {
		eppType3.Helmet = true
	} else {
		eppType3.Helmet = false
	}
	if m.Mask3 == "on" {
		eppType3.Mask = true
	} else {
		eppType3.Mask = false
	}
	if m.Boots3 == "on" {
		eppType3.Boots = true
	} else {
		eppType3.Boots = false
	}
	if m.Harness3 == "on" {
		eppType3.Harness = true
	} else {
		eppType3.Harness = false
	}
	if m.Glasses3 == "on" {
		eppType3.Glasses = true
	} else {
		eppType3.Glasses = false
	}
	if m.Clothes3 == "on" {
		eppType3.Clothes = true
	} else {
		eppType3.Clothes = false
	}
	if m.Respirators3 == "on" {
		eppType3.Respirators = true
	} else {
		eppType3.Respirators = false
	}
	if m.EppTypeOthers3 == "on" {
		eppType3.Others = true
	} else {
		eppType3.Others = false
	}
	return &eppType3
}

func (m *WorkHistoryConsumer) SetDanger4() *models.Dangers {
	var danger4 models.Dangers
	if m.Dust4 == "on" {
		danger4.Dust = true
	} else {
		danger4.Dust = false
	}
	if m.Noise4 == "on" {
		danger4.Noise = true
	} else {
		danger4.Noise = false
	}
	if m.RepetitiveMovement4 == "on" {
		danger4.RepetitiveMovement = true
	} else {
		danger4.RepetitiveMovement = false
	}
	if m.SegmentalVibration4 == "on" {
		danger4.SegmentalVibration = true
	} else {
		danger4.SegmentalVibration = false
	}
	if m.HeavyMetals4 == "on" {
		danger4.HeavyMetals = true
	} else {
		danger4.HeavyMetals = false
	}
	if m.Ergonomic4 == "on" {
		danger4.Ergonomic = true
	} else {
		danger4.Ergonomic = false
	}
	if m.TotalVibrations4 == "on" {
		danger4.TotalVibrations = true
	} else {
		danger4.TotalVibrations = false
	}
	if m.Carcinogenic4 == "on" {
		danger4.Carcinogenic = true
	} else {
		danger4.Carcinogenic = false
	}
	if m.Mutagenic4 == "on" {
		danger4.Mutagenic = true
	} else {
		danger4.Mutagenic = false
	}
	if m.Shifts4 == "on" {
		danger4.Shifts = true
	} else {
		danger4.Shifts = false
	}
	if m.Disergonomic4 == "on" {
		danger4.Disergonomic = true
	} else {
		danger4.Disergonomic = false
	}
	if m.Positions4 == "on" {
		danger4.Positions = true
	} else {
		danger4.Positions = false
	}
	if m.Falls4 == "on" {
		danger4.Falls = true
	} else {
		danger4.Falls = false
	}
	if m.Solvent4 == "on" {
		danger4.Solvent = true
	} else {
		danger4.Solvent = false
	}
	if m.NightShifts4 == "on" {
		danger4.NightShifts = true
	} else {
		danger4.NightShifts = false
	}
	if m.HighTemperature4 == "on" {
		danger4.HighTemperature = true
	} else {
		danger4.HighTemperature = false
	}
	if m.Biological4 == "on" {
		danger4.Biological = true
	} else {
		danger4.Biological = false
	}
	if m.Loads4 == "on" {
		danger4.Loads = true
	} else {
		danger4.Loads = false
	}
	if m.Pvd4 == "on" {
		danger4.Pvd = true
	} else {
		danger4.Pvd = false
	}
	if m.Chemical4 == "on" {
		danger4.Chemical = true
	} else {
		danger4.Chemical = false
	}
	if m.DangerOthers4 == "on" {
		danger4.Others = true
	} else {
		danger4.Others = false
	}
	if m.Biological4 == "on" {
		danger4.Biological = true
	} else {
		danger4.Biological = false
	}
	if m.Biological4 == "on" {
		danger4.Biological = true
	} else {
		danger4.Biological = false
	}
	return &danger4
}

func (m *WorkHistoryConsumer) SetEppType4() *models.EppType {
	var eppType4 models.EppType
	if m.Gloves4 == "on" {
		eppType4.Gloves = true
	} else {
		eppType4.Gloves = false
	}
	if m.Earplug4 == "on" {
		eppType4.Earplug = true
	} else {
		eppType4.Earplug = false
	}
	if m.Earmuffs4 == "on" {
		eppType4.Earmuffs = true
	} else {
		eppType4.Earmuffs = false
	}
	if m.Helmet4 == "on" {
		eppType4.Helmet = true
	} else {
		eppType4.Helmet = false
	}
	if m.Mask4 == "on" {
		eppType4.Mask = true
	} else {
		eppType4.Mask = false
	}
	if m.Boots4 == "on" {
		eppType4.Boots = true
	} else {
		eppType4.Boots = false
	}
	if m.Harness4 == "on" {
		eppType4.Harness = true
	} else {
		eppType4.Harness = false
	}
	if m.Glasses4 == "on" {
		eppType4.Glasses = true
	} else {
		eppType4.Glasses = false
	}
	if m.Clothes4 == "on" {
		eppType4.Clothes = true
	} else {
		eppType4.Clothes = false
	}
	if m.Respirators4 == "on" {
		eppType4.Respirators = true
	} else {
		eppType4.Respirators = false
	}
	if m.EppTypeOthers4 == "on" {
		eppType4.Others = true
	} else {
		eppType4.Others = false
	}
	return &eppType4
}

func (m *WorkHistoryConsumer) SetDanger5() *models.Dangers {
	var danger5 models.Dangers
	if m.Dust5 == "on" {
		danger5.Dust = true
	} else {
		danger5.Dust = false
	}
	if m.Noise5 == "on" {
		danger5.Noise = true
	} else {
		danger5.Noise = false
	}
	if m.RepetitiveMovement5 == "on" {
		danger5.RepetitiveMovement = true
	} else {
		danger5.RepetitiveMovement = false
	}
	if m.SegmentalVibration5 == "on" {
		danger5.SegmentalVibration = true
	} else {
		danger5.SegmentalVibration = false
	}
	if m.HeavyMetals5 == "on" {
		danger5.HeavyMetals = true
	} else {
		danger5.HeavyMetals = false
	}
	if m.Ergonomic5 == "on" {
		danger5.Ergonomic = true
	} else {
		danger5.Ergonomic = false
	}
	if m.TotalVibrations5 == "on" {
		danger5.TotalVibrations = true
	} else {
		danger5.TotalVibrations = false
	}
	if m.Carcinogenic5 == "on" {
		danger5.Carcinogenic = true
	} else {
		danger5.Carcinogenic = false
	}
	if m.Mutagenic5 == "on" {
		danger5.Mutagenic = true
	} else {
		danger5.Mutagenic = false
	}
	if m.Shifts5 == "on" {
		danger5.Shifts = true
	} else {
		danger5.Shifts = false
	}
	if m.Disergonomic5 == "on" {
		danger5.Disergonomic = true
	} else {
		danger5.Disergonomic = false
	}
	if m.Positions5 == "on" {
		danger5.Positions = true
	} else {
		danger5.Positions = false
	}
	if m.Falls5 == "on" {
		danger5.Falls = true
	} else {
		danger5.Falls = false
	}
	if m.Solvent5 == "on" {
		danger5.Solvent = true
	} else {
		danger5.Solvent = false
	}
	if m.NightShifts5 == "on" {
		danger5.NightShifts = true
	} else {
		danger5.NightShifts = false
	}
	if m.HighTemperature5 == "on" {
		danger5.HighTemperature = true
	} else {
		danger5.HighTemperature = false
	}
	if m.Biological5 == "on" {
		danger5.Biological = true
	} else {
		danger5.Biological = false
	}
	if m.Loads5 == "on" {
		danger5.Loads = true
	} else {
		danger5.Loads = false
	}
	if m.Pvd5 == "on" {
		danger5.Pvd = true
	} else {
		danger5.Pvd = false
	}
	if m.Chemical5 == "on" {
		danger5.Chemical = true
	} else {
		danger5.Chemical = false
	}
	if m.DangerOthers5 == "on" {
		danger5.Others = true
	} else {
		danger5.Others = false
	}
	if m.Biological5 == "on" {
		danger5.Biological = true
	} else {
		danger5.Biological = false
	}
	if m.Biological5 == "on" {
		danger5.Biological = true
	} else {
		danger5.Biological = false
	}
	return &danger5
}

func (m *WorkHistoryConsumer) SetEppType5() *models.EppType {
	var eppType5 models.EppType
	if m.Gloves5 == "on" {
		eppType5.Gloves = true
	} else {
		eppType5.Gloves = false
	}
	if m.Earplug5 == "on" {
		eppType5.Earplug = true
	} else {
		eppType5.Earplug = false
	}
	if m.Earmuffs5 == "on" {
		eppType5.Earmuffs = true
	} else {
		eppType5.Earmuffs = false
	}
	if m.Helmet5 == "on" {
		eppType5.Helmet = true
	} else {
		eppType5.Helmet = false
	}
	if m.Mask5 == "on" {
		eppType5.Mask = true
	} else {
		eppType5.Mask = false
	}
	if m.Boots5 == "on" {
		eppType5.Boots = true
	} else {
		eppType5.Boots = false
	}
	if m.Harness5 == "on" {
		eppType5.Harness = true
	} else {
		eppType5.Harness = false
	}
	if m.Glasses5 == "on" {
		eppType5.Glasses = true
	} else {
		eppType5.Glasses = false
	}
	if m.Clothes5 == "on" {
		eppType5.Clothes = true
	} else {
		eppType5.Clothes = false
	}
	if m.Respirators5 == "on" {
		eppType5.Respirators = true
	} else {
		eppType5.Respirators = false
	}
	if m.EppTypeOthers5 == "on" {
		eppType5.Others = true
	} else {
		eppType5.Others = false
	}
	return &eppType5
}

func (m *WorkHistoryConsumer) SetDanger6() *models.Dangers {
	var danger6 models.Dangers
	if m.Dust6 == "on" {
		danger6.Dust = true
	} else {
		danger6.Dust = false
	}
	if m.Noise6 == "on" {
		danger6.Noise = true
	} else {
		danger6.Noise = false
	}
	if m.RepetitiveMovement6 == "on" {
		danger6.RepetitiveMovement = true
	} else {
		danger6.RepetitiveMovement = false
	}
	if m.SegmentalVibration6 == "on" {
		danger6.SegmentalVibration = true
	} else {
		danger6.SegmentalVibration = false
	}
	if m.HeavyMetals6 == "on" {
		danger6.HeavyMetals = true
	} else {
		danger6.HeavyMetals = false
	}
	if m.Ergonomic6 == "on" {
		danger6.Ergonomic = true
	} else {
		danger6.Ergonomic = false
	}
	if m.TotalVibrations6 == "on" {
		danger6.TotalVibrations = true
	} else {
		danger6.TotalVibrations = false
	}
	if m.Carcinogenic6 == "on" {
		danger6.Carcinogenic = true
	} else {
		danger6.Carcinogenic = false
	}
	if m.Mutagenic6 == "on" {
		danger6.Mutagenic = true
	} else {
		danger6.Mutagenic = false
	}
	if m.Shifts6 == "on" {
		danger6.Shifts = true
	} else {
		danger6.Shifts = false
	}
	if m.Disergonomic6 == "on" {
		danger6.Disergonomic = true
	} else {
		danger6.Disergonomic = false
	}
	if m.Positions6 == "on" {
		danger6.Positions = true
	} else {
		danger6.Positions = false
	}
	if m.Falls6 == "on" {
		danger6.Falls = true
	} else {
		danger6.Falls = false
	}
	if m.Solvent6 == "on" {
		danger6.Solvent = true
	} else {
		danger6.Solvent = false
	}
	if m.NightShifts6 == "on" {
		danger6.NightShifts = true
	} else {
		danger6.NightShifts = false
	}
	if m.HighTemperature6 == "on" {
		danger6.HighTemperature = true
	} else {
		danger6.HighTemperature = false
	}
	if m.Biological6 == "on" {
		danger6.Biological = true
	} else {
		danger6.Biological = false
	}
	if m.Loads6 == "on" {
		danger6.Loads = true
	} else {
		danger6.Loads = false
	}
	if m.Pvd6 == "on" {
		danger6.Pvd = true
	} else {
		danger6.Pvd = false
	}
	if m.Chemical6 == "on" {
		danger6.Chemical = true
	} else {
		danger6.Chemical = false
	}
	if m.DangerOthers6 == "on" {
		danger6.Others = true
	} else {
		danger6.Others = false
	}
	if m.Biological6 == "on" {
		danger6.Biological = true
	} else {
		danger6.Biological = false
	}
	if m.Biological6 == "on" {
		danger6.Biological = true
	} else {
		danger6.Biological = false
	}
	return &danger6
}

func (m *WorkHistoryConsumer) SetEppType6() *models.EppType {
	var eppType6 models.EppType
	if m.Gloves6 == "on" {
		eppType6.Gloves = true
	} else {
		eppType6.Gloves = false
	}
	if m.Earplug6 == "on" {
		eppType6.Earplug = true
	} else {
		eppType6.Earplug = false
	}
	if m.Earmuffs6 == "on" {
		eppType6.Earmuffs = true
	} else {
		eppType6.Earmuffs = false
	}
	if m.Helmet6 == "on" {
		eppType6.Helmet = true
	} else {
		eppType6.Helmet = false
	}
	if m.Mask6 == "on" {
		eppType6.Mask = true
	} else {
		eppType6.Mask = false
	}
	if m.Boots6 == "on" {
		eppType6.Boots = true
	} else {
		eppType6.Boots = false
	}
	if m.Harness6 == "on" {
		eppType6.Harness = true
	} else {
		eppType6.Harness = false
	}
	if m.Glasses6 == "on" {
		eppType6.Glasses = true
	} else {
		eppType6.Glasses = false
	}
	if m.Clothes6 == "on" {
		eppType6.Clothes = true
	} else {
		eppType6.Clothes = false
	}
	if m.Respirators6 == "on" {
		eppType6.Respirators = true
	} else {
		eppType6.Respirators = false
	}
	if m.EppTypeOthers6 == "on" {
		eppType6.Others = true
	} else {
		eppType6.Others = false
	}
	return &eppType6
}

func (m *WorkHistoryConsumer) SetDanger7() *models.Dangers {
	var danger7 models.Dangers
	if m.Dust7 == "on" {
		danger7.Dust = true
	} else {
		danger7.Dust = false
	}
	if m.Noise7 == "on" {
		danger7.Noise = true
	} else {
		danger7.Noise = false
	}
	if m.RepetitiveMovement7 == "on" {
		danger7.RepetitiveMovement = true
	} else {
		danger7.RepetitiveMovement = false
	}
	if m.SegmentalVibration7 == "on" {
		danger7.SegmentalVibration = true
	} else {
		danger7.SegmentalVibration = false
	}
	if m.HeavyMetals7 == "on" {
		danger7.HeavyMetals = true
	} else {
		danger7.HeavyMetals = false
	}
	if m.Ergonomic7 == "on" {
		danger7.Ergonomic = true
	} else {
		danger7.Ergonomic = false
	}
	if m.TotalVibrations7 == "on" {
		danger7.TotalVibrations = true
	} else {
		danger7.TotalVibrations = false
	}
	if m.Carcinogenic7 == "on" {
		danger7.Carcinogenic = true
	} else {
		danger7.Carcinogenic = false
	}
	if m.Mutagenic7 == "on" {
		danger7.Mutagenic = true
	} else {
		danger7.Mutagenic = false
	}
	if m.Shifts7 == "on" {
		danger7.Shifts = true
	} else {
		danger7.Shifts = false
	}
	if m.Disergonomic7 == "on" {
		danger7.Disergonomic = true
	} else {
		danger7.Disergonomic = false
	}
	if m.Positions7 == "on" {
		danger7.Positions = true
	} else {
		danger7.Positions = false
	}
	if m.Falls7 == "on" {
		danger7.Falls = true
	} else {
		danger7.Falls = false
	}
	if m.Solvent7 == "on" {
		danger7.Solvent = true
	} else {
		danger7.Solvent = false
	}
	if m.NightShifts7 == "on" {
		danger7.NightShifts = true
	} else {
		danger7.NightShifts = false
	}
	if m.HighTemperature7 == "on" {
		danger7.HighTemperature = true
	} else {
		danger7.HighTemperature = false
	}
	if m.Biological7 == "on" {
		danger7.Biological = true
	} else {
		danger7.Biological = false
	}
	if m.Loads7 == "on" {
		danger7.Loads = true
	} else {
		danger7.Loads = false
	}
	if m.Pvd7 == "on" {
		danger7.Pvd = true
	} else {
		danger7.Pvd = false
	}
	if m.Chemical7 == "on" {
		danger7.Chemical = true
	} else {
		danger7.Chemical = false
	}
	if m.DangerOthers7 == "on" {
		danger7.Others = true
	} else {
		danger7.Others = false
	}
	if m.Biological7 == "on" {
		danger7.Biological = true
	} else {
		danger7.Biological = false
	}
	if m.Biological7 == "on" {
		danger7.Biological = true
	} else {
		danger7.Biological = false
	}
	return &danger7
}

func (m *WorkHistoryConsumer) SetEppType7() *models.EppType {
	var eppType7 models.EppType
	if m.Gloves7 == "on" {
		eppType7.Gloves = true
	} else {
		eppType7.Gloves = false
	}
	if m.Earplug7 == "on" {
		eppType7.Earplug = true
	} else {
		eppType7.Earplug = false
	}
	if m.Earmuffs7 == "on" {
		eppType7.Earmuffs = true
	} else {
		eppType7.Earmuffs = false
	}
	if m.Helmet7 == "on" {
		eppType7.Helmet = true
	} else {
		eppType7.Helmet = false
	}
	if m.Mask7 == "on" {
		eppType7.Mask = true
	} else {
		eppType7.Mask = false
	}
	if m.Boots7 == "on" {
		eppType7.Boots = true
	} else {
		eppType7.Boots = false
	}
	if m.Harness7 == "on" {
		eppType7.Harness = true
	} else {
		eppType7.Harness = false
	}
	if m.Glasses7 == "on" {
		eppType7.Glasses = true
	} else {
		eppType7.Glasses = false
	}
	if m.Clothes7 == "on" {
		eppType7.Clothes = true
	} else {
		eppType7.Clothes = false
	}
	if m.Respirators7 == "on" {
		eppType7.Respirators = true
	} else {
		eppType7.Respirators = false
	}
	if m.EppTypeOthers7 == "on" {
		eppType7.Others = true
	} else {
		eppType7.Others = false
	}
	return &eppType7
}
