package consumer_models

type PersonConsumer struct {
	ID                 int    `json:"idpaciente,omitempty"`
	DocumentTypeID     int    `json:"idtipodocumento,omitempty" bson:"idtipodocumento,omitempty"`
	DocumentNumber     string `json:"dni2,omitempty" bson:"dni2,omitempty"`
	LastName           string `json:"apellidos,omitempty"`
	FirstName          string `json:"nombres,omitempty"`
	BirthDate          string `json:"fechanacimiento,omitempty"`
	Sex                string `json:"sexo,omitempty"`
	Email              string `json:"correo,omitempty"`
	PhoneNumber        string `json:"telefono,omitempty"`
	District           string `json:"iddistrito,omitempty"`
	MaritalStatus      string `json:"estadocivil,omitempty"`
	DegreeInstructions string `json:"gradoins,omitempty"`
	IsDeleted          string `json:"__deleted,omitempty"`
	IdLog              string
}
