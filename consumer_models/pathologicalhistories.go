package consumer_models

import (
	"desktop-middleware/models"
	"strconv"
	"strings"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type PathologicalHistoryConsumer struct {
	ID                                  int    `json:"idantepatologico01,omitempty" bson:"idantepatologico01,omitempty"`
	AttentionId                         int    `json:"idcomprobante,omitempty" bson:"idcomprobante,omitempty"`
	PersonalMedicalHistoryDescription   string `json:"descripcion_pato,omitempty" bson:"descripcion_pato,omitempty"`
	PersonalMedicalHistoryAntecedent    string `json:"antecedente_patologico,omitempty" bson:"antecedente_patologico,omitempty"`
	FatherDiseaseHTA                    string `json:"familiares,omitempty" bson:"hta,omitempty"`
	FatherDiseaseDM                     string `json:"familiares1,omitempty" bson:"dm,omitempty"`
	FatherDiseaseCancer                 string `json:"familiares2,omitempty" bson:"cancer,omitempty"`
	FatherDiseaseCancerDescription      string `json:"cancer1,omitempty" bson:"cancerDescription,omitempty"`
	FatherDiseaseRheumatoidDisease      string `json:"familiares3,omitempty" bson:"rheumatoidDisease,omitempty"`
	FatherDiseaseTBC                    string `json:"familiares4,omitempty" bson:"tbc,omitempty"`
	FatherDiseaseNR                     string `json:"familiares28,omitempty" bson:"nr,omitempty"`
	FatherDiseaseOther                  string `json:"familiares5,omitempty" bson:"other,omitempty"`
	FatherDiseaseOtherDescription       string `json:"familiares7,omitempty" bson:"otherDescription,omitempty"`
	MotherDiseaseHTA                    string `json:"familiares8,omitempty" bson:"hta,omitempty"`
	MotherDiseaseDM                     string `json:"familiares9,omitempty" bson:"dm,omitempty"`
	MotherDiseaseCancer                 string `json:"familiares10,omitempty" bson:"cancer,omitempty"`
	MotherDiseaseCancerDescription      string `json:"cancer2,omitempty" bson:"cancerDescription,omitempty"`
	MotherDiseaseRheumatoidDisease      string `json:"familiares11,omitempty" bson:"rheumatoidDisease,omitempty"`
	MotherDiseaseTBC                    string `json:"familiares12,omitempty" bson:"tbc,omitempty"`
	MotherDiseaseNR                     string `json:"familiares29,omitempty" bson:"nr,omitempty"`
	MotherDiseaseOther                  string `json:"familiares13,omitempty" bson:"other,omitempty"`
	MotherDiseaseOtherDescription       string `json:"familiares14,omitempty" bson:"otherDescription,omitempty"`
	BrotherDiseaseHTA                   string `json:"familiares15,omitempty" bson:"hta,omitempty"`
	BrotherDiseaseDM                    string `json:"familiares16,omitempty" bson:"dm,omitempty"`
	BrotherDiseaseCancer                string `json:"familiares17,omitempty" bson:"cancer,omitempty"`
	BrotherDiseaseCancerDescription     string `json:"cancer3,omitempty" bson:"cancerDescription,omitempty"`
	BrotherDiseaseRheumatoidDisease     string `json:"familiares18,omitempty" bson:"rheumatoidDisease,omitempty"`
	BrotherDiseaseTBC                   string `json:"familiares19,omitempty" bson:"tbc,omitempty"`
	BrotherDiseaseNR                    string `json:"familiares30,omitempty" bson:"nr,omitempty"`
	BrotherDiseaseOther                 string `json:"familiares20,omitempty" bson:"other,omitempty"`
	BrotherDiseaseOtherDescription      string `json:"familiares21,omitempty" bson:"otherDescription,omitempty"`
	GrandParentDiseaseHTA               string `json:"familiares6,omitempty" bson:"hta,omitempty"`
	GrandParentDiseaseDM                string `json:"familiares22,omitempty" bson:"familiares22,omitempty"`
	GrandParentDiseaseCancer            string `json:"familiares23,omitempty" bson:"familiares23,omitempty"`
	GrandParentDiseaseCancerDescription string `json:"cancer4,omitempty" bson:"cancer4,omitempty"`
	GrandParentDiseaseRheumatoidDisease string `json:"familiares24,omitempty" bson:"familiares24,omitempty"`
	GrandParentDiseaseTBC               string `json:"familiares25,omitempty" bson:"familiares25,omitempty"`
	GrandParentDiseaseNR                string `json:"familiares31,omitempty" bson:"familiares31,omitempty"`
	GrandParentDiseaseOther             string `json:"familiares26,omitempty" bson:"familiares26,omitempty"`
	GrandParentDiseaseOtherDescription  string `json:"familiares27,omitempty" bson:"familiares27,omitempty"`
	SpouseDiseaseHTA                    string `json:"esposoa_hta,omitempty" bson:"esposoa_hta,omitempty"`
	SpouseDiseaseDM                     string `json:"esposoa_dbt,omitempty" bson:"esposoa_dbt,omitempty"`
	SpouseDiseaseCancer                 string `json:"esposoa_ca,omitempty" bson:"esposoa_ca,omitempty"`
	SpouseDiseaseCancerDescription      string `json:"texto_cancer,omitempty" bson:"texto_cancer,omitempty"`
	SpouseDiseaseRheumatoidDisease      string `json:"esposoa_enf_reu,omitempty" bson:"esposoa_enf_reu,omitempty"`
	SpouseDiseaseTBC                    string `json:"esposoa_enf_tbc,omitempty" bson:"esposoa_enf_tbc,omitempty"`
	SpouseDiseaseNR                     string `json:"esposoa_enf_nr,omitempty" bson:"esposoa_enf_nr,omitempty"`
	SpouseDiseaseOther                  string `json:"esposoa_enf_otro,omitempty" bson:"esposoa_enf_otro,omitempty"`
	SpouseDiseaseOtherDescription       string `json:"esposoa_texto,omitempty" bson:"esposoa_texto,omitempty"`
	Alergies                            string `json:"emf63,omitempty" bson:"emf63,omitempty"`
	AlergiesDescription                 string `json:"desram,omitempty" bson:"desram,omitempty"`
	ConsumeAlcohol                      string `json:"idhabito_nocivo2,omitempty" bson:"idhabito_nocivo2,omitempty"`
	AlcoholType                         string `json:"idtipo_licor,omitempty" bson:"idtipo_licor,omitempty"`
	AlcoholAmount                       string `json:"nivel_consumo02,omitempty" bson:"nivel_consumo02,omitempty"`
	AlcoholConsumptionLevel             string `json:"idnivel_consumo2,omitempty" bson:"idnivel_consumo2,omitempty"`
	ConsumeCigarrete                    string `json:"idhabito_nocivo,omitempty" bson:"idhabito_nocivo,omitempty"`
	CigarreteType                       string `json:"ncigarros,omitempty" bson:"ncigarros,omitempty"`
	CigarreteAmount                     string `json:"nivel_consumo,omitempty" bson:"nivel_consumo,omitempty"`
	CigarreteConsumptionLevel           string `json:"idnivel_consumo,omitempty" bson:"idnivel_consumo,omitempty"`
	ConsumeDrug                         string `json:"idhabito_nocivo3,omitempty" bson:"idhabito_nocivo3,omitempty"`
	DrugType                            string `json:"idtipo_droga,omitempty" bson:"idtipo_droga,omitempty"`
	DrugAmount                          string `json:"nivel_consumo03,omitempty" bson:"nivel_consumo03,omitempty"`
	DrugConsumptionLevel                string `json:"idnivel_consumo3,omitempty" bson:"idnivel_consumo3,omitempty"`
	CurrentMedicine                     string `json:"medicacion_actual,omitempty" bson:"medicacion_actual,omitempty"`
	JobTitle                            string `json:"puesto,omitempty" bson:"puesto,omitempty"`
	Tetanus                             string `json:"inmunizacion2" bson:"inmunizacion2,omitempty"`
	YellowFever                         string `json:"inmunizacion5" bson:"inmunizacion5,omitempty"`
	Flu                                 string `json:"inmunizacion3" bson:"inmunizacion3,omitempty"`
	Hepatitisb                          string `json:"inmunizacion7" bson:"inmunizacion7,omitempty"`
	Mmr                                 string `json:"inmunizacion" bson:"inmunizacion,omitempty"`
	Polio                               string `json:"inmunizacion4" bson:"inmunizacion4,omitempty"`
	Rabies                              string `json:"inmunizacion8" bson:"inmunizacion8,omitempty"`
	Influenza                           string `json:"inmunizacion9" bson:"inmunizacion9,omitempty"`
	Hepatitisa                          string `json:"inmunizacion10" bson:"inmunizacion10,omitempty"`
	Meningococcus                       string `json:"inmunizacion12" bson:"inmunizacion12,omitempty"`
	HumanPapilloma                      string `json:"inmunizacion14" bson:"inmunizacion14,omitempty"`
	Bcg                                 string `json:"bcg" bson:"bcg,omitempty"`
	Ppd                                 string `json:"ppd" bson:"ppd,omitempty"`
	Infancy                             string `json:"infan" bson:"infan,omitempty"`
	Others                              string `json:"inmunizacion16" bson:"inmunizacion16,omitempty"`
	Description1                        string `json:"enfermedad_acc_1,omitempty" bson:"enfermedad_acc_1,omitempty"`
	RelatedWithWork1                    string `json:"asociado_trab_1,omitempty" bson:"asociado_trab_1,omitempty"`
	Year1                               string `json:"acc_anio_1,omitempty" bson:"acc_anio_1,omitempty"`
	RestDays1                           string `json:"dia_descanzo_1,omitempty" bson:"dia_descanzo_1,omitempty"`
	Description2                        string `json:"enfermedad_acc_2,omitempty" bson:"enfermedad_acc_2,omitempty"`
	RelatedWithWork2                    string `json:"asociado_trab_2,omitempty" bson:"asociado_trab_2,omitempty"`
	Year2                               string `json:"acc_anio_2,omitempty" bson:"acc_anio_2,omitempty"`
	RestDays2                           string `json:"dia_descanzo_2,omitempty" bson:"dia_descanzo_2,omitempty"`
	Description3                        string `json:"enfermedad_acc_3,omitempty" bson:"enfermedad_acc_3,omitempty"`
	RelatedWithWork3                    string `json:"asociado_trab_3,omitempty" bson:"asociado_trab_3,omitempty"`
	Year3                               string `json:"acc_anio_3,omitempty" bson:"acc_anio_3,omitempty"`
	RestDays3                           string `json:"dia_descanzo_3,omitempty" bson:"dia_descanzo_3,omitempty"`
	Description4                        string `json:"enfermedad_acc_4,omitempty" bson:"enfermedad_acc_4,omitempty"`
	RelatedWithWork4                    string `json:"asociado_trab_4,omitempty" bson:"asociado_trab_4,omitempty"`
	Year4                               string `json:"acc_anio_4,omitempty" bson:"acc_anio_4,omitempty"`
	Asthma                              string `json:"emf5,omitempty" bson:"emf5,omitempty"`
	AsthmaDetail                        string `json:"descrip_5,omitempty" bson:"descrip_5,omitempty"`
	RestDays4                           string `json:"dia_descanzo_4,omitempty" bson:"dia_descanzo_4,omitempty"`
	AptitudeCertificate                 string `json:"descripcion,omitempty" bson:"descripcion,omitempty"`
	RecordCertificate                   string `json:"descripcion2,omitempty" bson:"descripcion2,omitempty"`
	MedicalReport                       string `json:"descripcion3,omitempty" bson:"descripcion3,omitempty"`
	MedicalHistory                      string `json:"descripcion4,omitempty" bson:"descripcion4,omitempty"`
	Consultation                        string `json:"descripcion5,omitempty" bson:"descripcion5,omitempty"`
	Status                              int    `json:"estado" bson:"estado"`
	IsDeleted                           string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog                               string
}

func (m *PathologicalHistoryConsumer) SetPathologicalHistory() models.PathologicalHistory {
	var pathologicalHistory models.PathologicalHistory
	pathologicalHistory.MedicalHistoryAntecedent = m.PersonalMedicalHistoryAntecedent
	pathologicalHistory.MedicalHistoryDescription = m.PersonalMedicalHistoryDescription
	pathologicalHistory.FatherDisease = m.SetFatherDisease()
	pathologicalHistory.MotherDisease = m.SetMotherDisease()
	pathologicalHistory.BrotherDisease = m.SetBrotherDisease()
	pathologicalHistory.GrandParentDisease = m.SetGrandParentDisease()
	pathologicalHistory.SpouseDisease = m.SetSpouseDisease()
	if m.Alergies == "on" {
		pathologicalHistory.Alergies = true
	} else {
		pathologicalHistory.Alergies = false
	}
	pathologicalHistory.AlergiesDescription = m.AlergiesDescription
	pathologicalHistory.Alcohol = m.SetAlcohol()
	pathologicalHistory.Cigarrete = m.SetCigarrete()
	pathologicalHistory.Drug = m.SetDrug()
	pathologicalHistory.CurrentMedicine = m.CurrentMedicine
	pathologicalHistory.JobTitle = m.JobTitle
	pathologicalHistory.Vaccines = m.SetVaccines()
	pathologicalHistory.DiseaseHistory = m.SetDiseaseHistory()
	pathologicalHistory.Status = m.Status
	pathologicalHistory.ExternalId = strconv.Itoa(m.ID)
	pathologicalHistory.ExternalSystem = "MEDIWEB"
	pathologicalHistory.AptitudeCertificate = strings.TrimSpace(m.AptitudeCertificate)
	pathologicalHistory.RecordCertificate = strings.TrimSpace(m.RecordCertificate)
	pathologicalHistory.MedicalReport = strings.TrimSpace(m.MedicalReport)
	pathologicalHistory.MedicalHistory = strings.TrimSpace(m.MedicalHistory)
	pathologicalHistory.Consultation = strings.TrimSpace(m.Consultation)
	return pathologicalHistory
}

func (m *PathologicalHistoryConsumer) SetPathologicalHistoryInjuriesV2(attentionId, personId, patientId, pathologicalHistoryId string) []*models.PathologicalHistoryInjuries {
	var results []*models.PathologicalHistoryInjuries
	if m.Description1 != "" {
		result := models.PathologicalHistoryInjuries{
			ID:                    primitive.NewObjectID().Hex(),
			AttentionId:           attentionId,
			PersonId:              personId,
			PatientId:             patientId,
			PathologicalHistoryId: pathologicalHistoryId,
			Description:           m.Description1,
			Year:                  m.Year1,
			RestDays:              m.RestDays1,
			SequenceNumber:        1,
			Status:                m.Status,
			ExternalId:            strconv.Itoa(m.ID),
			ExternalSystem:        "MEDIWEB",
			CreatedDate:           time.Now(),
			LastUpdatedDate:       time.Now(),
		}
		if m.RelatedWithWork1 == "on" {
			result.RelatedWithWork = true
		} else {
			result.RelatedWithWork = false
		}
		results = append(results, &result)
	}
	if m.Description2 != "" {
		result := models.PathologicalHistoryInjuries{
			ID:                    primitive.NewObjectID().Hex(),
			AttentionId:           attentionId,
			PersonId:              personId,
			PatientId:             patientId,
			PathologicalHistoryId: pathologicalHistoryId,
			Description:           m.Description2,
			Year:                  m.Year2,
			RestDays:              m.RestDays2,
			SequenceNumber:        2,
			Status:                m.Status,
			ExternalId:            strconv.Itoa(m.ID),
			ExternalSystem:        "MEDIWEB",
			CreatedDate:           time.Now(),
			LastUpdatedDate:       time.Now(),
		}
		if m.RelatedWithWork2 == "on" {
			result.RelatedWithWork = true
		} else {
			result.RelatedWithWork = false
		}
		results = append(results, &result)
	}
	if m.Description3 != "" {
		result := models.PathologicalHistoryInjuries{
			ID:                    primitive.NewObjectID().Hex(),
			AttentionId:           attentionId,
			PersonId:              personId,
			PatientId:             patientId,
			PathologicalHistoryId: pathologicalHistoryId,
			Description:           m.Description3,
			Year:                  m.Year3,
			RestDays:              m.RestDays3,
			SequenceNumber:        3,
			Status:                m.Status,
			ExternalId:            strconv.Itoa(m.ID),
			ExternalSystem:        "MEDIWEB",
			CreatedDate:           time.Now(),
			LastUpdatedDate:       time.Now(),
		}
		if m.RelatedWithWork3 == "on" {
			result.RelatedWithWork = true
		} else {
			result.RelatedWithWork = false
		}
		results = append(results, &result)
	}
	if m.Description4 != "" {
		result := models.PathologicalHistoryInjuries{
			ID:                    primitive.NewObjectID().Hex(),
			AttentionId:           attentionId,
			PersonId:              personId,
			PatientId:             patientId,
			PathologicalHistoryId: pathologicalHistoryId,
			Description:           m.Description4,
			Year:                  m.Year4,
			RestDays:              m.RestDays4,
			SequenceNumber:        4,
			Status:                m.Status,
			ExternalId:            strconv.Itoa(m.ID),
			ExternalSystem:        "MEDIWEB",
			CreatedDate:           time.Now(),
			LastUpdatedDate:       time.Now(),
		}
		if m.RelatedWithWork4 == "on" {
			result.RelatedWithWork = true
		} else {
			result.RelatedWithWork = false
		}
		results = append(results, &result)
	}
	return results
}

func (m *PathologicalHistoryConsumer) SetPathologicalHistoryInjuries(attentionId, personId, patientId, pathologicalHistoryId string) []interface{} {
	var results []interface{}
	if m.Description1 != "" {
		result := models.PathologicalHistoryInjuries{
			ID:                    primitive.NewObjectID().Hex(),
			AttentionId:           attentionId,
			PersonId:              personId,
			PatientId:             patientId,
			PathologicalHistoryId: pathologicalHistoryId,
			Description:           m.Description1,
			Year:                  m.Year1,
			RestDays:              m.RestDays1,
			SequenceNumber:        1,
			Status:                m.Status,
			ExternalId:            strconv.Itoa(m.ID),
			ExternalSystem:        "MEDIWEB",
			CreatedDate:           time.Now(),
			LastUpdatedDate:       time.Now(),
		}
		if m.RelatedWithWork1 == "on" {
			result.RelatedWithWork = true
		} else {
			result.RelatedWithWork = false
		}
		results = append(results, &result)
	}
	if m.Description2 != "" {
		result := models.PathologicalHistoryInjuries{
			ID:                    primitive.NewObjectID().Hex(),
			AttentionId:           attentionId,
			PersonId:              personId,
			PatientId:             patientId,
			PathologicalHistoryId: pathologicalHistoryId,
			Description:           m.Description2,
			Year:                  m.Year2,
			RestDays:              m.RestDays2,
			SequenceNumber:        2,
			Status:                m.Status,
			ExternalId:            strconv.Itoa(m.ID),
			ExternalSystem:        "MEDIWEB",
			CreatedDate:           time.Now(),
			LastUpdatedDate:       time.Now(),
		}
		if m.RelatedWithWork2 == "on" {
			result.RelatedWithWork = true
		} else {
			result.RelatedWithWork = false
		}
		results = append(results, &result)
	}
	if m.Description3 != "" {
		result := models.PathologicalHistoryInjuries{
			ID:                    primitive.NewObjectID().Hex(),
			AttentionId:           attentionId,
			PersonId:              personId,
			PatientId:             patientId,
			PathologicalHistoryId: pathologicalHistoryId,
			Description:           m.Description3,
			Year:                  m.Year3,
			RestDays:              m.RestDays3,
			SequenceNumber:        3,
			Status:                m.Status,
			ExternalId:            strconv.Itoa(m.ID),
			ExternalSystem:        "MEDIWEB",
			CreatedDate:           time.Now(),
			LastUpdatedDate:       time.Now(),
		}
		if m.RelatedWithWork3 == "on" {
			result.RelatedWithWork = true
		} else {
			result.RelatedWithWork = false
		}
		results = append(results, &result)
	}
	if m.Description4 != "" {
		result := models.PathologicalHistoryInjuries{
			ID:                    primitive.NewObjectID().Hex(),
			AttentionId:           attentionId,
			PersonId:              personId,
			PatientId:             patientId,
			PathologicalHistoryId: pathologicalHistoryId,
			Description:           m.Description4,
			Year:                  m.Year4,
			RestDays:              m.RestDays4,
			SequenceNumber:        4,
			Status:                m.Status,
			ExternalId:            strconv.Itoa(m.ID),
			ExternalSystem:        "MEDIWEB",
			CreatedDate:           time.Now(),
			LastUpdatedDate:       time.Now(),
		}
		if m.RelatedWithWork4 == "on" {
			result.RelatedWithWork = true
		} else {
			result.RelatedWithWork = false
		}
		results = append(results, &result)
	}
	return results
}

func (m *PathologicalHistoryConsumer) SetFatherDisease() *models.PathologicalDisease {
	var disease models.PathologicalDisease
	if m.FatherDiseaseHTA == "on" {
		disease.HTA = true
	} else {
		disease.HTA = false
	}
	if m.FatherDiseaseDM == "on" {
		disease.DM = true
	} else {
		disease.DM = false
	}
	if m.FatherDiseaseCancer == "on" {
		disease.Cancer = true
	} else {
		disease.Cancer = false
	}
	if m.FatherDiseaseRheumatoidDisease == "on" {
		disease.RheumatoidDisease = true
	} else {
		disease.RheumatoidDisease = false
	}
	if m.FatherDiseaseTBC == "on" {
		disease.TBC = true
	} else {
		disease.TBC = false
	}
	if m.FatherDiseaseNR == "on" {
		disease.NR = true
	} else {
		disease.NR = false
	}
	if m.FatherDiseaseOther == "on" {
		disease.Other = true
	} else {
		disease.Other = false
	}
	disease.CancerDescription = m.FatherDiseaseCancerDescription
	disease.OtherDescription = m.FatherDiseaseOtherDescription

	return &disease
}

func (m *PathologicalHistoryConsumer) SetMotherDisease() *models.PathologicalDisease {
	var disease models.PathologicalDisease
	if m.MotherDiseaseHTA == "on" {
		disease.HTA = true
	} else {
		disease.HTA = false
	}
	if m.MotherDiseaseDM == "on" {
		disease.DM = true
	} else {
		disease.DM = false
	}
	if m.MotherDiseaseCancer == "on" {
		disease.Cancer = true
	} else {
		disease.Cancer = false
	}
	if m.MotherDiseaseRheumatoidDisease == "on" {
		disease.RheumatoidDisease = true
	} else {
		disease.RheumatoidDisease = false
	}
	if m.MotherDiseaseTBC == "on" {
		disease.TBC = true
	} else {
		disease.TBC = false
	}
	if m.MotherDiseaseNR == "on" {
		disease.NR = true
	} else {
		disease.NR = false
	}
	if m.MotherDiseaseOther == "on" {
		disease.Other = true
	} else {
		disease.Other = false
	}
	disease.CancerDescription = m.MotherDiseaseCancerDescription
	disease.OtherDescription = m.MotherDiseaseOtherDescription

	return &disease
}

func (m *PathologicalHistoryConsumer) SetBrotherDisease() *models.PathologicalDisease {
	var disease models.PathologicalDisease
	if m.BrotherDiseaseHTA == "on" {
		disease.HTA = true
	} else {
		disease.HTA = false
	}
	if m.BrotherDiseaseDM == "on" {
		disease.DM = true
	} else {
		disease.DM = false
	}
	if m.BrotherDiseaseCancer == "on" {
		disease.Cancer = true
	} else {
		disease.Cancer = false
	}
	if m.BrotherDiseaseRheumatoidDisease == "on" {
		disease.RheumatoidDisease = true
	} else {
		disease.RheumatoidDisease = false
	}
	if m.BrotherDiseaseTBC == "on" {
		disease.TBC = true
	} else {
		disease.TBC = false
	}
	if m.BrotherDiseaseNR == "on" {
		disease.NR = true
	} else {
		disease.NR = false
	}
	if m.BrotherDiseaseOther == "on" {
		disease.Other = true
	} else {
		disease.Other = false
	}
	disease.CancerDescription = m.BrotherDiseaseCancerDescription
	disease.OtherDescription = m.BrotherDiseaseOtherDescription

	return &disease
}

func (m *PathologicalHistoryConsumer) SetGrandParentDisease() *models.PathologicalDisease {
	var disease models.PathologicalDisease
	if m.GrandParentDiseaseHTA == "on" {
		disease.HTA = true
	} else {
		disease.HTA = false
	}
	if m.GrandParentDiseaseDM == "on" {
		disease.DM = true
	} else {
		disease.DM = false
	}
	if m.GrandParentDiseaseCancer == "on" {
		disease.Cancer = true
	} else {
		disease.Cancer = false
	}
	if m.GrandParentDiseaseRheumatoidDisease == "on" {
		disease.RheumatoidDisease = true
	} else {
		disease.RheumatoidDisease = false
	}
	if m.GrandParentDiseaseTBC == "on" {
		disease.TBC = true
	} else {
		disease.TBC = false
	}
	if m.GrandParentDiseaseNR == "on" {
		disease.NR = true
	} else {
		disease.NR = false
	}
	if m.GrandParentDiseaseOther == "on" {
		disease.Other = true
	} else {
		disease.Other = false
	}
	disease.CancerDescription = m.GrandParentDiseaseCancerDescription
	disease.OtherDescription = m.GrandParentDiseaseOtherDescription

	return &disease
}

func (m *PathologicalHistoryConsumer) SetSpouseDisease() *models.PathologicalDisease {
	var disease models.PathologicalDisease
	if m.SpouseDiseaseHTA == "on" {
		disease.HTA = true
	} else {
		disease.HTA = false
	}
	if m.SpouseDiseaseDM == "on" {
		disease.DM = true
	} else {
		disease.DM = false
	}
	if m.SpouseDiseaseCancer == "on" {
		disease.Cancer = true
	} else {
		disease.Cancer = false
	}
	if m.SpouseDiseaseRheumatoidDisease == "on" {
		disease.RheumatoidDisease = true
	} else {
		disease.RheumatoidDisease = false
	}
	if m.SpouseDiseaseTBC == "on" {
		disease.TBC = true
	} else {
		disease.TBC = false
	}
	if m.SpouseDiseaseNR == "on" {
		disease.NR = true
	} else {
		disease.NR = false
	}
	if m.SpouseDiseaseOther == "on" {
		disease.Other = true
	} else {
		disease.Other = false
	}
	disease.CancerDescription = m.SpouseDiseaseCancerDescription
	disease.OtherDescription = m.SpouseDiseaseOtherDescription

	return &disease
}

func (m *PathologicalHistoryConsumer) SetAlcohol() *models.PathologicalConsume {
	var disease models.PathologicalConsume
	disease.Consume = m.ConsumeAlcohol
	disease.Type = m.AlcoholType
	disease.Amount = m.AlcoholAmount
	disease.ConsumptionLevel = m.AlcoholConsumptionLevel

	return &disease
}

func (m *PathologicalHistoryConsumer) SetCigarrete() *models.PathologicalConsume {
	var disease models.PathologicalConsume
	disease.Consume = m.ConsumeCigarrete
	disease.Type = m.CigarreteType
	disease.Amount = m.CigarreteAmount
	disease.ConsumptionLevel = m.CigarreteConsumptionLevel

	return &disease
}

func (m *PathologicalHistoryConsumer) SetDrug() *models.PathologicalConsume {
	var disease models.PathologicalConsume
	disease.Consume = m.ConsumeDrug
	disease.Type = m.DrugType
	disease.Amount = m.DrugAmount
	disease.ConsumptionLevel = m.DrugConsumptionLevel

	return &disease
}

func (m *PathologicalHistoryConsumer) SetVaccines() *models.Vaccines {
	var disease models.Vaccines
	if m.Tetanus == "on" {
		disease.Tetanus = true
	} else {
		disease.Tetanus = false
	}
	if m.YellowFever == "on" {
		disease.YellowFever = true
	} else {
		disease.YellowFever = false
	}
	if m.Flu == "on" {
		disease.Flu = true
	} else {
		disease.Flu = false
	}
	if m.Hepatitisb == "on" {
		disease.Hepatitisb = true
	} else {
		disease.Hepatitisb = false
	}
	if m.Mmr == "on" {
		disease.Mmr = true
	} else {
		disease.Mmr = false
	}
	if m.Polio == "on" {
		disease.Polio = true
	} else {
		disease.Polio = false
	}
	if m.Rabies == "on" {
		disease.Rabies = true
	} else {
		disease.Rabies = false
	}
	if m.Influenza == "on" {
		disease.Influenza = true
	} else {
		disease.Influenza = false
	}
	if m.Hepatitisa == "on" {
		disease.Hepatitisa = true
	} else {
		disease.Hepatitisa = false
	}
	if m.Meningococcus == "on" {
		disease.Meningococcus = true
	} else {
		disease.Meningococcus = false
	}
	if m.HumanPapilloma == "on" {
		disease.HumanPapilloma = true
	} else {
		disease.HumanPapilloma = false
	}
	if m.Bcg == "on" {
		disease.Bcg = true
	} else {
		disease.Bcg = false
	}
	if m.Ppd == "on" {
		disease.Ppd = true
	} else {
		disease.Ppd = false
	}
	if m.Infancy == "on" {
		disease.Infancy = true
	} else {
		disease.Infancy = false
	}
	if m.Others == "on" {
		disease.Others = true
	} else {
		disease.Others = false
	}

	return &disease
}

func (m *PathologicalHistoryConsumer) SetDiseaseHistory() *models.DiseaseHistory {
	var disease models.DiseaseHistory
	if m.Asthma == "on" {
		disease.Asthma = true
		disease.AsthmaDetail = m.AsthmaDetail
	} else {
		disease.Asthma = false
	}

	return &disease
}
