package consumer_models

import (
	"desktop-middleware/models"
	"strconv"
)

type MedicalHistoryConsumer struct {
	ID                     int    `json:"idmedicina,omitempty" bson:"idmedicina,omitempty"`
	AttentionId            int    `json:"idcomprobante,omitempty" bson:"idcomprobante,omitempty"`
	Anamnesis              string `json:"anamnesis,omitempty" bson:"anamnesis,omitempty"`
	Skin                   string `json:"piel_fan,omitempty" bson:"piel_fan,omitempty"`
	Head                   string `json:"acabeza,omitempty" bson:"acabeza,omitempty"`
	Neck                   string `json:"acuello,omitempty" bson:"acuello,omitempty"`
	Nose                   string `json:"anariz,omitempty" bson:"anariz,omitempty"`
	Mouth                  string `json:"aboca,omitempty" bson:"aboca,omitempty"`
	Pharynx                string `json:"afaringe,omitempty" bson:"afaringe,omitempty"`
	Larynx                 string `json:"alaringe,omitempty" bson:"alaringe,omitempty"`
	Heart                  string `json:"acorazon,omitempty" bson:"acorazon,omitempty"`
	Lungs                  string `json:"apulmones,omitempty" bson:"apulmones,omitempty"`
	Belly                  string `json:"aabdomen,omitempty" bson:"aabdomen,omitempty"`
	Miemsd                 string `json:"amiemsd,omitempty" bson:"amiemsd,omitempty"`
	Miemid                 string `json:"amiemid,omitempty" bson:"amiemid,omitempty"`
	Backbone               string `json:"acolumna,omitempty" bson:"acolumna,omitempty"`
	Breast                 string `json:"amamader,omitempty" bson:"amamader,omitempty"`
	Genitals               string `json:"agenitales,omitempty" bson:"agenitales,omitempty"`
	Glands                 string `json:"aganglios,omitempty" bson:"aganglios,omitempty"`
	March                  string `json:"amtandem,omitempty" bson:"amtandem,omitempty"`
	OsteotendinousReflexes string `json:"areflejosot,omitempty" bson:"areflejosot,omitempty"`
	Language               string `json:"alenguaje,omitempty" bson:"alenguaje,omitempty"`
	Hernia                 string `json:"ahernias,omitempty" bson:"ahernias,omitempty"`
	VaricoseVein           string `json:"avarices,omitempty" bson:"avarices,omitempty"`
	NervousSistem          string `json:"asis_nervioso,omitempty" bson:"asis_nervioso,omitempty"`
	LymphaticSistem        string `json:"asis_linfa,omitempty" bson:"asis_linfa,omitempty"`
	Status                 string `json:"estado" bson:"estado"`
	IsDeleted              string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog                  string
}

func (m *MedicalHistoryConsumer) SetMedicalHistory() models.MedicalHistory {
	var newTest models.MedicalHistory
	newTest.Anamnesis = m.Anamnesis
	newTest.Skin = m.Skin
	newTest.Head = m.Head
	newTest.Neck = m.Neck
	newTest.Nose = m.Nose
	newTest.Mouth = m.Mouth
	newTest.Pharynx = m.Pharynx
	newTest.Larynx = m.Larynx
	newTest.Heart = m.Heart
	newTest.Lungs = m.Lungs
	newTest.Belly = m.Belly
	newTest.Miemsd = m.Miemsd
	newTest.Miemid = m.Miemid
	newTest.Backbone = m.Backbone
	newTest.Breast = m.Breast
	newTest.Genitals = m.Genitals
	newTest.Glands = m.Glands
	newTest.March = m.March
	newTest.OsteotendinousReflexes = m.OsteotendinousReflexes
	newTest.Language = m.Language
	newTest.Hernia = m.Hernia
	newTest.VaricoseVein = m.VaricoseVein
	newTest.NervousSistem = m.NervousSistem
	newTest.LymphaticSistem = m.LymphaticSistem
	status, _ := strconv.Atoi(m.Status)
	newTest.Status = status
	newTest.ExternalId = strconv.Itoa(m.ID)
	newTest.ExternalSystem = "MEDIWEB"
	return newTest
}
