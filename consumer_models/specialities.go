package consumer_models

type SpecialityConsumer struct {
	ID        int    `json:"idespecialidad_inter,omitempty" bson:"idespecialidad_inter,omitempty"`
	Name      string `json:"descripcion,omitempty" bson:"descripcion,omitempty"`
	Status    string `json:"estado" bson:"estado"`
	IsDeleted string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog     string
}
