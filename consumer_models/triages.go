package consumer_models

type TriageConsumer struct {
	ID                int    `json:"idtriaje,omitempty" bson:"idtriaje,omitempty"`
	AttentionId       int    `json:"idcomprobante,omitempty" bson:"idcomprobante,omitempty"`
	SystolicPressure  string `json:"presion_sistolica" bson:"presion_sistolica"`
	DiastolicPressure string `json:"presion_diastolica" bson:"presion_diastolica"`
	HeartRate         string `json:"fcardiaca" bson:"fcardiaca"`
	RespiratoryRate   string `json:"frespiratoria" bson:"frespiratoria"`
	Weight            string `json:"peso" bson:"peso"`
	Height            string `json:"talla" bson:"talla"`
	Bmi               string `json:"imc" bson:"imc"`
	Abdominal         string `json:"abdominal" bson:"abdominal"`
	Status            int    `json:"estado" bson:"estado"`
	IsDeleted         string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog             string
}
