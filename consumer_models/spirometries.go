package consumer_models

import (
	"desktop-middleware/models"
	"strconv"
)

type SpirometryConsumer struct {
	ID                int    `json:"idespirometria,omitempty" bson:"idespirometria,omitempty"`
	AttentionId       int    `json:"idcomprobante,omitempty" bson:"idcomprobante,omitempty"`
	FileName1         string `json:"descripcion,omitempty" bson:"descripcion,omitempty"`
	Result            string `json:"conclusion,omitempty" bson:"conclusion,omitempty"`
	Quality           string `json:"planta,omitempty" bson:"planta,omitempty"`
	FVC               string `json:"fvc,omitempty" bson:"fvc,omitempty"`
	FEV               string `json:"fev,omitempty" bson:"fev,omitempty"`
	FEV_FVC           string `json:"fev_fvc,omitempty" bson:"fev_fvc,omitempty"`
	FEF               string `json:"fef,omitempty" bson:"fef,omitempty"`
	FVCPercentage     string `json:"fvc2,omitempty" bson:"fvc2,omitempty"`
	FEVPercentage     string `json:"fev2,omitempty" bson:"fev2,omitempty"`
	FEV_FVCPercentage string `json:"fev_fvc2,omitempty" bson:"fev_fvc2,omitempty"`
	FEFPercentage     string `json:"fef2,omitempty" bson:"fef2,omitempty"`
	Diagnostic1       string `json:"diag,omitempty" bson:"diag,omitempty"`
	Diagnostic2       string `json:"diag2,omitempty" bson:"diag2,omitempty"`
	Diagnostic3       string `json:"diag3,omitempty" bson:"diag3,omitempty"`
	Status            string `json:"estado" bson:"estado"`
	IsDeleted         string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog             string
}

func (m *SpirometryConsumer) SetSpirometryTest() models.SpirometryTest {
	var newTest models.SpirometryTest
	if val, err := strconv.ParseFloat(m.FVC, 64); err == nil {
		newTest.FVC = val
	}
	if val, err := strconv.ParseFloat(m.FEV, 64); err == nil {
		newTest.FEV = val
	}
	if val, err := strconv.ParseFloat(m.FEV_FVC, 64); err == nil {
		newTest.FEV_FVC = val
	}
	if val, err := strconv.ParseFloat(m.FEF, 64); err == nil {
		newTest.FEF = val
	}
	if val, err := strconv.ParseFloat(m.FVCPercentage, 64); err == nil {
		newTest.FVCPercentage = val
	}
	if val, err := strconv.ParseFloat(m.FEVPercentage, 64); err == nil {
		newTest.FEVPercentage = val
	}
	if val, err := strconv.ParseFloat(m.FEV_FVCPercentage, 64); err == nil {
		newTest.FEV_FVCPercentage = val
	}
	if val, err := strconv.ParseFloat(m.FEFPercentage, 64); err == nil {
		newTest.FEFPercentage = val
	}
	if m.Result == "1" {
		newTest.Result = "Normal"
	} else if m.Result == "2" {
		newTest.Result = "Anormal"
	}
	status, _ := strconv.Atoi(m.Status)
	newTest.Status = status
	newTest.ExternalId = strconv.Itoa(m.ID)
	newTest.ExternalSystem = "MEDIWEB"
	return newTest
}
