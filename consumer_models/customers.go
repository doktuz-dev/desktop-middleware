package consumer_models

type CustomerConsumer struct {
	ID          int    `json:"idempresa,omitempty" bson:"idempresa,omitempty"`
	Name        string `json:"descripcion,omitempty" bson:"descripcion,omitempty"`
	Code        string `json:"ruc,omitempty" bson:"ruc,omitempty"`
	Status      int    `json:"estado" bson:"estado"`
	Address     string `json:"direccion,omitempty" bson:"direccion,omitempty"`
	PhoneNumber string `json:"telefono,omitempty" bson:"telefono,omitempty"`
	IsDeleted   string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog       string
}
