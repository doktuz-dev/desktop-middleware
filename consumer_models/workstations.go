package consumer_models

type WorkStationConsumer struct {
	ID         int    `json:"idproyecto,omitempty" bson:"idproyecto,omitempty"`
	CustomerId string `json:"idempresa,omitempty" bson:"idempresa,omitempty"`
	Name       string `json:"descripcion,omitempty" bson:"descripcion,omitempty"`
	Status     string `json:"estado" bson:"estado"`
	IsDeleted  string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog      string
}
