package consumer_models

import (
	"desktop-middleware/models"
	"strconv"

	"golang.org/x/text/cases"
	"golang.org/x/text/language"
)

type SkeletalMuscleConsumer struct {
	ID                                                            int    `json:"idmesqueletica,omitempty" bson:"idmesqueletica,omitempty"`
	AttentionId                                                   int    `json:"idcomprobante,omitempty" bson:"idcomprobante,omitempty"`
	Diagnostic1                                                   string `json:"observaciones,omitempty" bson:"observaciones,omitempty"`
	Diagnostic2                                                   string `json:"diagnosticos,omitempty" bson:"diagnosticos,omitempty"`
	Diagnostic3                                                   string `json:"diagnosticos2,omitempty" bson:"diagnosticos2,omitempty"`
	Diagnostic4                                                   string `json:"diagnosticos3,omitempty" bson:"diagnosticos3,omitempty"`
	Diagnostic5                                                   string `json:"diag_mus1,omitempty" bson:"diag_mus1,omitempty"`
	Diagnostic6                                                   string `json:"diag_mus2,omitempty" bson:"diag_mus2,omitempty"`
	Diagnostic7                                                   string `json:"diag_mus3,omitempty" bson:"diag_mus3,omitempty"`
	Diagnostic8                                                   string `json:"diag_mus4,omitempty" bson:"diag_mus4,omitempty"`
	Diagnostic9                                                   string `json:"diag_mus5,omitempty" bson:"diag_mus5,omitempty"`
	Diagnostic10                                                  string `json:"diag_mus6,omitempty" bson:"diag_mus6,omitempty"`
	Diagnostic11                                                  string `json:"diag_mus7,omitempty" bson:"diag_mus7,omitempty"`
	Status                                                        int    `json:"estado" bson:"estado"`
	GeneralPain                                                   string `json:"pre_dolor,omitempty" bson:"pre_dolor,omitempty"`
	StaticEvaluation_physiologicalCurvatures_cervical             string `json:"carac_curvafisio1,omitempty" bson:"carac_curvafisio1,omitempty"`
	StaticEvaluation_physiologicalCurvatures_dorsal               string `json:"carac_curvafisio2,omitempty" bson:"carac_curvafisio2,omitempty"`
	StaticEvaluation_physiologicalCurvatures_lumbar               string `json:"carac_curvafisio3,omitempty" bson:"carac_curvafisio3,omitempty"`
	StaticEvaluation_Feet_feetDiagnostic                          string `json:"caract_pie1,omitempty" bson:"caract_pie1,omitempty"`
	StaticEvaluation_Feet_pesCavus                                string `json:"caract_pie2,omitempty" bson:"caract_pie2,omitempty"`
	StaticEvaluation_Feet_flatFoot                                string `json:"caract_pie3,omitempty" bson:"caract_pie3,omitempty"`
	StaticEvaluation_adamsForwardBendTest                         string `json:"carac_pruebaadams1,omitempty" bson:"carac_pruebaadams1,omitempty"`
	PainMobility_cervical_flexion                                 string `json:"cervi_flexion,omitempty" bson:"cervi_flexion,omitempty"`
	PainMobility_cervical_extension                               string `json:"cervi_exten,omitempty" bson:"cervi_exten,omitempty"`
	PainMobility_cervical_rightLateralization                     string `json:"cervi_infle_der,omitempty" bson:"cervi_infle_der,omitempty"`
	PainMobility_cervical_leftLateralization                      string `json:"cervi_infle_izq,omitempty" bson:"cervi_infle_izq,omitempty"`
	PainMobility_cervical_rightRotation                           string `json:"cervi_rotacion,omitempty" bson:"cervi_rotacion,omitempty"`
	PainMobility_cervical_leftRotation                            string `json:"cervi_rotacioni,omitempty" bson:"cervi_rotacioni,omitempty"`
	PainMobility_cervical_functionalLimitation                    string `json:"cervi_dolor,omitempty" bson:"cervi_dolor,omitempty"`
	PainMobility_cervical_radiatingPain                           string `json:"cervi_irra,omitempty" bson:"cervi_irra,omitempty"`
	PainMobility_dorsal_flexion                                   string `json:"dorsal_flexion,omitempty" bson:"dorsal_flexion,omitempty"`
	PainMobility_dorsal_extension                                 string `json:"dorsal_exten,omitempty" bson:"dorsal_exten,omitempty"`
	PainMobility_dorsal_rightLateralization                       string `json:"dorsal_infle_der,omitempty" bson:"dorsal_infle_der,omitempty"`
	PainMobility_dorsal_leftLateralization                        string `json:"dorsal_infle_izq,omitempty" bson:"dorsal_infle_izq,omitempty"`
	PainMobility_dorsal_rightRotation                             string `json:"dorsal_rotacion,omitempty" bson:"dorsal_rotacion,omitempty"`
	PainMobility_dorsal_leftRotation                              string `json:"dorsal_rotacioni,omitempty" bson:"dorsal_rotacioni,omitempty"`
	PainMobility_dorsal_functionalLimitation                      string `json:"dorsal_dolor,omitempty" bson:"dorsal_dolor,omitempty"`
	PainMobility_dorsal_radiatingPain                             string `json:"dorsal_irra,omitempty" bson:"dorsal_irra,omitempty"`
	PainMobility_lumbar_flexion                                   string `json:"lumbar_flexion,omitempty" bson:"lumbar_flexion,omitempty"`
	PainMobility_lumbar_extension                                 string `json:"lumbar_exten,omitempty" bson:"lumbar_exten,omitempty"`
	PainMobility_lumbar_rightLateralization                       string `json:"lumbar_infle_der,omitempty" bson:"lumbar_infle_der,omitempty"`
	PainMobility_lumbar_leftLateralization                        string `json:"lumbar_infle_izq,omitempty" bson:"lumbar_infle_izq,omitempty"`
	PainMobility_lumbar_rightRotation                             string `json:"lumbar_rotacion,omitempty" bson:"lumbar_rotacion,omitempty"`
	PainMobility_lumbar_leftRotation                              string `json:"lumbar_rotacioni,omitempty" bson:"lumbar_rotacioni,omitempty"`
	PainMobility_lumbar_functionalLimitation                      string `json:"lumbar_dolor,omitempty" bson:"lumbar_dolor,omitempty"`
	PainMobility_lumbar_radiatingPain                             string `json:"lumbar_irra,omitempty" bson:"lumbar_irra,omitempty"`
	BackExploration_lassegueTest_right                            string `json:"lasegue_der,omitempty" bson:"lasegue_der,omitempty"`
	BackExploration_lassegueTest_left                             string `json:"lasegue_izq,omitempty" bson:"lasegue_izq,omitempty"`
	BackExploration_schoberTest                                   string `json:"schober_der,omitempty" bson:"schober_der,omitempty"`
	BackPalpation_cervical_painfulSpinousApophysis                string `json:"palpa_cervical1,omitempty" bson:"palpa_cervical1,omitempty"`
	BackPalpation_cervical_muscleContracture                      string `json:"palpa_cervical2,omitempty" bson:"palpa_cervical2,omitempty"`
	BackPalpation_dorsal_painfulSpinousApophysis                  string `json:"palpa_dorsal1,omitempty" bson:"palpa_dorsal1,omitempty"`
	BackPalpation_dorsal_muscleContracture                        string `json:"palpa_dorsal2,omitempty" bson:"palpa_dorsal2,omitempty"`
	BackPalpation_lumbar_painfulSpinousApophysis                  string `json:"palpa_lumbar1,omitempty" bson:"palpa_lumbar1,omitempty"`
	BackPalpation_lumbar_muscleContracture                        string `json:"palpa_lumbar2,omitempty" bson:"palpa_lumbar2,omitempty"`
	JointsExploration_shoulder_right_abduction                    string `json:"abdu1,omitempty" bson:"abdu1,omitempty"`
	JointsExploration_shoulder_right_adduction                    string `json:"adducc1,omitempty" bson:"adducc1,omitempty"`
	JointsExploration_shoulder_right_flexion                      string `json:"flex1,omitempty" bson:"flex1,omitempty"`
	JointsExploration_shoulder_right_extension                    string `json:"ext1,omitempty" bson:"ext1,omitempty"`
	JointsExploration_shoulder_right_externalRotation             string `json:"rot_ext1,omitempty" bson:"rot_ext1,omitempty"`
	JointsExploration_shoulder_right_internalRotation             string `json:"rot_int1,omitempty" bson:"rot_int1,omitempty"`
	JointsExploration_shoulder_right_functionalLimitation         string `json:"dolor1,omitempty" bson:"dolor1,omitempty"`
	JointsExploration_shoulder_right_radiatingPain                string `json:"irrad1,omitempty" bson:"irrad1,omitempty"`
	JointsExploration_shoulder_right_muscleMassAlterations        string `json:"alt_masa1,omitempty" bson:"alt_masa1,omitempty"`
	JointsExploration_shoulder_left_abduction                     string `json:"abdu2,omitempty" bson:"abdu2,omitempty"`
	JointsExploration_shoulder_left_adduction                     string `json:"adducc2,omitempty" bson:"adducc2,omitempty"`
	JointsExploration_shoulder_left_flexion                       string `json:"flex2,omitempty" bson:"flex2,omitempty"`
	JointsExploration_shoulder_left_extension                     string `json:"ext2,omitempty" bson:"ext2,omitempty"`
	JointsExploration_shoulder_left_externalRotation              string `json:"rot_ext2,omitempty" bson:"rot_ext2,omitempty"`
	JointsExploration_shoulder_left_internalRotation              string `json:"rot_int2,omitempty" bson:"rot_int2,omitempty"`
	JointsExploration_shoulder_left_functionalLimitation          string `json:"dolor2,omitempty" bson:"dolor2,omitempty"`
	JointsExploration_shoulder_left_radiatingPain                 string `json:"irrad2,omitempty" bson:"irrad2,omitempty"`
	JointsExploration_shoulder_left_muscleMassAlterations         string `json:"alt_masa2,omitempty" bson:"alt_masa2,omitempty"`
	JointsExploration_elbow_right_abduction                       string `json:"abdu3,omitempty" bson:"abdu3,omitempty"`
	JointsExploration_elbow_right_adduction                       string `json:"adducc3,omitempty" bson:"adducc3,omitempty"`
	JointsExploration_elbow_right_flexion                         string `json:"flex3,omitempty" bson:"flex3,omitempty"`
	JointsExploration_elbow_right_extension                       string `json:"ext3,omitempty" bson:"ext3,omitempty"`
	JointsExploration_elbow_right_externalRotation                string `json:"rot_ext3,omitempty" bson:"rot_ext3,omitempty"`
	JointsExploration_elbow_right_internalRotation                string `json:"rot_int3,omitempty" bson:"rot_int3,omitempty"`
	JointsExploration_elbow_right_functionalLimitation            string `json:"dolor3,omitempty" bson:"dolor3,omitempty"`
	JointsExploration_elbow_right_radiatingPain                   string `json:"irrad3,omitempty" bson:"irrad3,omitempty"`
	JointsExploration_elbow_right_muscleMassAlterations           string `json:"alt_masa3,omitempty" bson:"alt_masa3,omitempty"`
	JointsExploration_elbow_left_abduction                        string `json:"abdu4,omitempty" bson:"abdu4,omitempty"`
	JointsExploration_elbow_left_adduction                        string `json:"adducc4,omitempty" bson:"adducc4,omitempty"`
	JointsExploration_elbow_left_flexion                          string `json:"flex4,omitempty" bson:"flex4,omitempty"`
	JointsExploration_elbow_left_extension                        string `json:"ext4,omitempty" bson:"ext4,omitempty"`
	JointsExploration_elbow_left_externalRotation                 string `json:"rot_ext4,omitempty" bson:"rot_ext4,omitempty"`
	JointsExploration_elbow_left_internalRotation                 string `json:"rot_int4,omitempty" bson:"rot_int4,omitempty"`
	JointsExploration_elbow_left_functionalLimitation             string `json:"dolor4,omitempty" bson:"dolor4,omitempty"`
	JointsExploration_elbow_left_radiatingPain                    string `json:"irrad4,omitempty" bson:"irrad4,omitempty"`
	JointsExploration_elbow_left_muscleMassAlterations            string `json:"alt_masa4,omitempty" bson:"alt_masa4,omitempty"`
	JointsExploration_wrist_right_abduction                       string `json:"abdu5,omitempty" bson:"abdu5,omitempty"`
	JointsExploration_wrist_right_adduction                       string `json:"adducc5,omitempty" bson:"adducc5,omitempty"`
	JointsExploration_wrist_right_flexion                         string `json:"flex5,omitempty" bson:"flex5,omitempty"`
	JointsExploration_wrist_right_extension                       string `json:"ext5,omitempty" bson:"ext5,omitempty"`
	JointsExploration_wrist_right_externalRotation                string `json:"rot_ext5,omitempty" bson:"rot_ext5,omitempty"`
	JointsExploration_wrist_right_internalRotation                string `json:"rot_int5,omitempty" bson:"rot_int5,omitempty"`
	JointsExploration_wrist_right_functionalLimitation            string `json:"dolor5,omitempty" bson:"dolor5,omitempty"`
	JointsExploration_wrist_right_radiatingPain                   string `json:"irrad5,omitempty" bson:"irrad5,omitempty"`
	JointsExploration_wrist_right_muscleMassAlterations           string `json:"alt_masa5,omitempty" bson:"alt_masa5,omitempty"`
	JointsExploration_wrist_left_abduction                        string `json:"abdu6,omitempty" bson:"abdu6,omitempty"`
	JointsExploration_wrist_left_adduction                        string `json:"adducc6,omitempty" bson:"adducc6,omitempty"`
	JointsExploration_wrist_left_flexion                          string `json:"flex6,omitempty" bson:"flex6,omitempty"`
	JointsExploration_wrist_left_extension                        string `json:"ext6,omitempty" bson:"ext6,omitempty"`
	JointsExploration_wrist_left_externalRotation                 string `json:"rot_ext6,omitempty" bson:"rot_ext6,omitempty"`
	JointsExploration_wrist_left_internalRotation                 string `json:"rot_int6,omitempty" bson:"rot_int6,omitempty"`
	JointsExploration_wrist_left_functionalLimitation             string `json:"dolor6,omitempty" bson:"dolor6,omitempty"`
	JointsExploration_wrist_left_radiatingPain                    string `json:"irrad6,omitempty" bson:"irrad6,omitempty"`
	JointsExploration_wrist_left_muscleMassAlterations            string `json:"alt_masa6,omitempty" bson:"alt_masa6,omitempty"`
	JointsExploration_handsAndFingers_right_abduction             string `json:"abdu7,omitempty" bson:"abdu7,omitempty"`
	JointsExploration_handsAndFingers_right_adduction             string `json:"adducc7,omitempty" bson:"adducc7,omitempty"`
	JointsExploration_handsAndFingers_right_flexion               string `json:"flex7,omitempty" bson:"flex7,omitempty"`
	JointsExploration_handsAndFingers_right_extension             string `json:"ext7,omitempty" bson:"ext7,omitempty"`
	JointsExploration_handsAndFingers_right_externalRotation      string `json:"rot_ext7,omitempty" bson:"rot_ext7,omitempty"`
	JointsExploration_handsAndFingers_right_internalRotation      string `json:"rot_int7,omitempty" bson:"rot_int7,omitempty"`
	JointsExploration_handsAndFingers_right_functionalLimitation  string `json:"dolor7,omitempty" bson:"dolor7,omitempty"`
	JointsExploration_handsAndFingers_right_radiatingPain         string `json:"irrad7,omitempty" bson:"irrad7,omitempty"`
	JointsExploration_handsAndFingers_right_muscleMassAlterations string `json:"alt_masa7,omitempty" bson:"alt_masa7,omitempty"`
	JointsExploration_handsAndFingers_left_abduction              string `json:"abdu8,omitempty" bson:"abdu8,omitempty"`
	JointsExploration_handsAndFingers_left_adduction              string `json:"adducc8,omitempty" bson:"adducc8,omitempty"`
	JointsExploration_handsAndFingers_left_flexion                string `json:"flex8,omitempty" bson:"flex8,omitempty"`
	JointsExploration_handsAndFingers_left_extension              string `json:"ext8,omitempty" bson:"ext8,omitempty"`
	JointsExploration_handsAndFingers_left_externalRotation       string `json:"rot_ext8,omitempty" bson:"rot_ext8,omitempty"`
	JointsExploration_handsAndFingers_left_internalRotation       string `json:"rot_int8,omitempty" bson:"rot_int8,omitempty"`
	JointsExploration_handsAndFingers_left_functionalLimitation   string `json:"dolor8,omitempty" bson:"dolor8,omitempty"`
	JointsExploration_handsAndFingers_left_radiatingPain          string `json:"irrad8,omitempty" bson:"irrad8,omitempty"`
	JointsExploration_handsAndFingers_left_muscleMassAlterations  string `json:"alt_masa8,omitempty" bson:"alt_masa8,omitempty"`
	JointsExploration_hips_right_abduction                        string `json:"abdu9,omitempty" bson:"abdu9,omitempty"`
	JointsExploration_hips_right_adduction                        string `json:"adducc9,omitempty" bson:"adducc9,omitempty"`
	JointsExploration_hips_right_flexion                          string `json:"flex9,omitempty" bson:"flex9,omitempty"`
	JointsExploration_hips_right_extension                        string `json:"ext9,omitempty" bson:"ext9,omitempty"`
	JointsExploration_hips_right_externalRotation                 string `json:"rot_ext9,omitempty" bson:"rot_ext9,omitempty"`
	JointsExploration_hips_right_internalRotation                 string `json:"rot_int9,omitempty" bson:"rot_int9,omitempty"`
	JointsExploration_hips_right_functionalLimitation             string `json:"dolor9,omitempty" bson:"dolor9,omitempty"`
	JointsExploration_hips_right_radiatingPain                    string `json:"irrad9,omitempty" bson:"irrad9,omitempty"`
	JointsExploration_hips_right_muscleMassAlterations            string `json:"alt_masa9,omitempty" bson:"alt_masa9,omitempty"`
	JointsExploration_hips_left_abduction                         string `json:"abdu10,omitempty" bson:"abdu10,omitempty"`
	JointsExploration_hips_left_adduction                         string `json:"adducc10,omitempty" bson:"adducc10,omitempty"`
	JointsExploration_hips_left_flexion                           string `json:"flex10,omitempty" bson:"flex10,omitempty"`
	JointsExploration_hips_left_extension                         string `json:"ext10,omitempty" bson:"ext10,omitempty"`
	JointsExploration_hips_left_externalRotation                  string `json:"rot_ext10,omitempty" bson:"rot_ext10,omitempty"`
	JointsExploration_hips_left_internalRotation                  string `json:"rot_int10,omitempty" bson:"rot_int10,omitempty"`
	JointsExploration_hips_left_functionalLimitation              string `json:"dolor10,omitempty" bson:"dolor10,omitempty"`
	JointsExploration_hips_left_radiatingPain                     string `json:"irrad10,omitempty" bson:"irrad10,omitempty"`
	JointsExploration_hips_left_muscleMassAlterations             string `json:"alt_masa10,omitempty" bson:"alt_masa10,omitempty"`
	JointsExploration_knee_right_abduction                        string `json:"abdu11,omitempty" bson:"abdu11,omitempty"`
	JointsExploration_knee_right_adduction                        string `json:"adducc11,omitempty" bson:"adducc11,omitempty"`
	JointsExploration_knee_right_flexion                          string `json:"flex11,omitempty" bson:"flex11,omitempty"`
	JointsExploration_knee_right_extension                        string `json:"ext11,omitempty" bson:"ext11,omitempty"`
	JointsExploration_knee_right_externalRotation                 string `json:"rot_ext11,omitempty" bson:"rot_ext11,omitempty"`
	JointsExploration_knee_right_internalRotation                 string `json:"rot_int11,omitempty" bson:"rot_int11,omitempty"`
	JointsExploration_knee_right_functionalLimitation             string `json:"dolor11,omitempty" bson:"dolor11,omitempty"`
	JointsExploration_knee_right_radiatingPain                    string `json:"irrad11,omitempty" bson:"irrad11,omitempty"`
	JointsExploration_knee_right_muscleMassAlterations            string `json:"alt_masa11,omitempty" bson:"alt_masa11,omitempty"`
	JointsExploration_knee_left_abduction                         string `json:"abdu12,omitempty" bson:"abdu12,omitempty"`
	JointsExploration_knee_left_adduction                         string `json:"adducc12,omitempty" bson:"adducc12,omitempty"`
	JointsExploration_knee_left_flexion                           string `json:"flex12,omitempty" bson:"flex12,omitempty"`
	JointsExploration_knee_left_extension                         string `json:"ext12,omitempty" bson:"ext12,omitempty"`
	JointsExploration_knee_left_externalRotation                  string `json:"rot_ext12,omitempty" bson:"rot_ext12,omitempty"`
	JointsExploration_knee_left_internalRotation                  string `json:"rot_int12,omitempty" bson:"rot_int12,omitempty"`
	JointsExploration_knee_left_functionalLimitation              string `json:"dolor12,omitempty" bson:"dolor12,omitempty"`
	JointsExploration_knee_left_radiatingPain                     string `json:"irrad12,omitempty" bson:"irrad12,omitempty"`
	JointsExploration_knee_left_muscleMassAlterations             string `json:"alt_masa12,omitempty" bson:"alt_masa12,omitempty"`
	JointsExploration_ankle_right_abduction                       string `json:"abdu13,omitempty" bson:"abdu13,omitempty"`
	JointsExploration_ankle_right_adduction                       string `json:"adducc13,omitempty" bson:"adducc13,omitempty"`
	JointsExploration_ankle_right_flexion                         string `json:"flex13,omitempty" bson:"flex13,omitempty"`
	JointsExploration_ankle_right_extension                       string `json:"ext13,omitempty" bson:"ext13,omitempty"`
	JointsExploration_ankle_right_externalRotation                string `json:"rot_ext13,omitempty" bson:"rot_ext13,omitempty"`
	JointsExploration_ankle_right_internalRotation                string `json:"rot_int13,omitempty" bson:"rot_int13,omitempty"`
	JointsExploration_ankle_right_functionalLimitation            string `json:"dolor13,omitempty" bson:"dolor13,omitempty"`
	JointsExploration_ankle_right_radiatingPain                   string `json:"irrad13,omitempty" bson:"irrad13,omitempty"`
	JointsExploration_ankle_right_muscleMassAlterations           string `json:"alt_masa13,omitempty" bson:"alt_masa13,omitempty"`
	JointsExploration_ankle_left_abduction                        string `json:"abdu14,omitempty" bson:"abdu14,omitempty"`
	JointsExploration_ankle_left_adduction                        string `json:"adducc14,omitempty" bson:"adducc14,omitempty"`
	JointsExploration_ankle_left_flexion                          string `json:"flex14,omitempty" bson:"flex14,omitempty"`
	JointsExploration_ankle_left_extension                        string `json:"ext14,omitempty" bson:"ext14,omitempty"`
	JointsExploration_ankle_left_externalRotation                 string `json:"rot_ext14,omitempty" bson:"rot_ext14,omitempty"`
	JointsExploration_ankle_left_internalRotation                 string `json:"rot_int14,omitempty" bson:"rot_int14,omitempty"`
	JointsExploration_ankle_left_functionalLimitation             string `json:"dolor14,omitempty" bson:"dolor14,omitempty"`
	JointsExploration_ankle_left_radiatingPain                    string `json:"irrad14,omitempty" bson:"irrad14,omitempty"`
	JointsExploration_ankle_left_muscleMassAlterations            string `json:"alt_masa14,omitempty" bson:"alt_masa14,omitempty"`
	IsDeleted                                                     string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog                                                         string
}

func (m *SkeletalMuscleConsumer) SetSkeletalMuscleTest() models.SkeletalMuscleTest {
	var newTest models.SkeletalMuscleTest
	newTest.GeneralPain = cases.Title(language.Und).String(m.GeneralPain)
	newTest.StaticEvaluation = m.SetStaticEvaluation()
	newTest.PainMobility = m.SetPainMobility()
	newTest.BackExploration = m.SetBackExploration()
	newTest.BackPalpation = m.SetBackPalpation()
	newTest.JointsExploration = m.SetJointsExploration()
	newTest.Status = m.Status
	newTest.ExternalId = strconv.Itoa(m.ID)
	newTest.ExternalSystem = "MEDIWEB"
	newTest.BackFindings = newTest.SetBackFindings()

	return newTest
}

func (m *SkeletalMuscleConsumer) SetStaticEvaluation() *models.StaticEvaluation {
	var staticEvaluation models.StaticEvaluation

	switch m.StaticEvaluation_adamsForwardBendTest {
	case "1":
		staticEvaluation.AdamsForwardBendTest = "Negativo"
	case "2":
		staticEvaluation.AdamsForwardBendTest = "Convexidad dorsal derecha"
	case "3":
		staticEvaluation.AdamsForwardBendTest = "Convexidad dorsal izquierda"
	case "4":
		staticEvaluation.AdamsForwardBendTest = "Convexidad lumbar derecha"
	case "5":
		staticEvaluation.AdamsForwardBendTest = "Convexidad lumbar izquierda"
	case "6":
		staticEvaluation.AdamsForwardBendTest = "Convexidad dorsal derecha y lumbar izquierda"
	case "7":
		staticEvaluation.AdamsForwardBendTest = "Convexidad dorsal izquierda y lumbar derecha"
	case "8":
		staticEvaluation.AdamsForwardBendTest = "Convexidad dorsolumbar derecha"
	case "9":
		staticEvaluation.AdamsForwardBendTest = "Convexidad dorsolumbar izquierda"
	case "10":
		staticEvaluation.AdamsForwardBendTest = "No evaluable"
	}

	var feet models.Feet

	if m.StaticEvaluation_Feet_feetDiagnostic == "on" {
		feet.FeetDiagnostic = "Normal"
	} else if m.StaticEvaluation_Feet_feetDiagnostic == "" {
		feet.FeetDiagnostic = "Anormal"
	}

	switch m.StaticEvaluation_Feet_pesCavus {
	case "1":
		feet.PesCavus = "No"
	case "2":
		feet.PesCavus = "Derecho"
	case "3":
		feet.PesCavus = "Izquierdo"
	case "4":
		feet.PesCavus = "Ambos"
	}

	switch m.StaticEvaluation_Feet_flatFoot {
	case "1":
		feet.FlatFoot = "No"
	case "2":
		feet.FlatFoot = "Derecho"
	case "3":
		feet.FlatFoot = "Izquierdo"
	case "4":
		feet.FlatFoot = "Ambos"
	}

	staticEvaluation.Feet = &feet

	var physiologicalCurvatures models.PhysiologicalCurvatures

	switch m.StaticEvaluation_physiologicalCurvatures_cervical {
	case "1":
		physiologicalCurvatures.Cervical = "Normal"
	case "2":
		physiologicalCurvatures.Cervical = "Aumentada"
	case "3":
		physiologicalCurvatures.Cervical = "Disminuida"
	}

	switch m.StaticEvaluation_physiologicalCurvatures_dorsal {
	case "1":
		physiologicalCurvatures.Dorsal = "Normal"
	case "2":
		physiologicalCurvatures.Dorsal = "Aumentada"
	case "3":
		physiologicalCurvatures.Dorsal = "Disminuida"
	}

	switch m.StaticEvaluation_physiologicalCurvatures_lumbar {
	case "1":
		physiologicalCurvatures.Lumbar = "Normal"
	case "2":
		physiologicalCurvatures.Lumbar = "Aumentada"
	case "3":
		physiologicalCurvatures.Lumbar = "Disminuida"
	}

	staticEvaluation.PhysiologicalCurvatures = &physiologicalCurvatures

	return &staticEvaluation
}

func (m *SkeletalMuscleConsumer) SetPainMobility() *models.PainMobility {
	var painMobility models.PainMobility

	var cervical models.PainMobilityFields
	cervical.Flexion = cases.Title(language.Und).String(m.PainMobility_cervical_flexion)
	cervical.Extension = cases.Title(language.Und).String(m.PainMobility_cervical_extension)
	cervical.RightLateralization = cases.Title(language.Und).String(m.PainMobility_cervical_rightLateralization)
	cervical.LeftLateralization = cases.Title(language.Und).String(m.PainMobility_cervical_leftLateralization)
	cervical.RightRotation = cases.Title(language.Und).String(m.PainMobility_cervical_flexion)
	cervical.LeftRotation = cases.Title(language.Und).String(m.PainMobility_cervical_rightRotation)
	cervical.FunctionalLimitation = cases.Title(language.Und).String(m.PainMobility_cervical_functionalLimitation)
	cervical.RadiatingPain = cases.Title(language.Und).String(m.PainMobility_cervical_radiatingPain)
	painMobility.Cervical = &cervical

	var dorsal models.PainMobilityFields
	dorsal.Flexion = cases.Title(language.Und).String(m.PainMobility_dorsal_flexion)
	dorsal.Extension = cases.Title(language.Und).String(m.PainMobility_dorsal_extension)
	dorsal.RightLateralization = cases.Title(language.Und).String(m.PainMobility_dorsal_rightLateralization)
	dorsal.LeftLateralization = cases.Title(language.Und).String(m.PainMobility_dorsal_leftLateralization)
	dorsal.RightRotation = cases.Title(language.Und).String(m.PainMobility_dorsal_flexion)
	dorsal.LeftRotation = cases.Title(language.Und).String(m.PainMobility_dorsal_rightRotation)
	dorsal.FunctionalLimitation = cases.Title(language.Und).String(m.PainMobility_dorsal_functionalLimitation)
	dorsal.RadiatingPain = cases.Title(language.Und).String(m.PainMobility_dorsal_radiatingPain)
	painMobility.Dorsal = &dorsal

	var lumbar models.PainMobilityFields
	lumbar.Flexion = cases.Title(language.Und).String(m.PainMobility_lumbar_flexion)
	lumbar.Extension = cases.Title(language.Und).String(m.PainMobility_lumbar_extension)
	lumbar.RightLateralization = cases.Title(language.Und).String(m.PainMobility_lumbar_rightLateralization)
	lumbar.LeftLateralization = cases.Title(language.Und).String(m.PainMobility_lumbar_leftLateralization)
	lumbar.RightRotation = cases.Title(language.Und).String(m.PainMobility_lumbar_flexion)
	lumbar.LeftRotation = cases.Title(language.Und).String(m.PainMobility_lumbar_rightRotation)
	lumbar.FunctionalLimitation = cases.Title(language.Und).String(m.PainMobility_lumbar_functionalLimitation)
	lumbar.RadiatingPain = cases.Title(language.Und).String(m.PainMobility_lumbar_radiatingPain)
	painMobility.Lumbar = &lumbar

	return &painMobility
}

func (m *SkeletalMuscleConsumer) SetBackExploration() *models.BackExploration {
	var backExploration models.BackExploration
	if m.BackExploration_schoberTest == "+" {
		backExploration.SchoberTest = "Positivo"
	} else if m.BackExploration_schoberTest == "-" {
		backExploration.SchoberTest = "Negativo"
	}
	var lassegueTest models.LassegueTest
	if m.BackExploration_lassegueTest_right == "+" {
		lassegueTest.Right = "Positivo"
	} else if m.BackExploration_lassegueTest_right == "-" {
		lassegueTest.Right = "Negativo"
	}
	if m.BackExploration_lassegueTest_left == "+" {
		lassegueTest.Left = "Positivo"
	} else if m.BackExploration_lassegueTest_left == "-" {
		lassegueTest.Left = "Negativo"
	}
	backExploration.LassegueTest = &lassegueTest

	return &backExploration
}

func (m *SkeletalMuscleConsumer) SetBackPalpation() *models.BackPalpation {
	var backPalpation models.BackPalpation

	var cervicalBackPalpation models.CervicalBackPalpation
	if m.BackPalpation_cervical_painfulSpinousApophysis == "1" {
		cervicalBackPalpation.PainfulSpinousApophysis = "Negativo"
	} else if m.BackPalpation_cervical_painfulSpinousApophysis == "2" {
		cervicalBackPalpation.PainfulSpinousApophysis = "Positivo"
	}
	if m.BackPalpation_cervical_muscleContracture == "1" {
		cervicalBackPalpation.MuscleContracture = "Negativo"
	} else if m.BackPalpation_cervical_muscleContracture == "2" {
		cervicalBackPalpation.MuscleContracture = "Positivo"
	}
	backPalpation.Cervical = &cervicalBackPalpation

	var dorsalBackPalpation models.DorsalBackPalpation
	if m.BackPalpation_dorsal_painfulSpinousApophysis == "1" {
		dorsalBackPalpation.PainfulSpinousApophysis = "Negativo"
	} else if m.BackPalpation_dorsal_painfulSpinousApophysis == "2" {
		dorsalBackPalpation.PainfulSpinousApophysis = "Positivo"
	}
	if m.BackPalpation_dorsal_muscleContracture == "1" {
		dorsalBackPalpation.MuscleContracture = "Negativo"
	} else if m.BackPalpation_dorsal_muscleContracture == "2" {
		dorsalBackPalpation.MuscleContracture = "Positivo"
	}
	backPalpation.Dorsal = &dorsalBackPalpation

	var lumbarBackPalpation models.LumbarBackPalpation
	if m.BackPalpation_lumbar_painfulSpinousApophysis == "1" {
		lumbarBackPalpation.PainfulSpinousApophysis = "Negativo"
	} else if m.BackPalpation_lumbar_painfulSpinousApophysis == "2" {
		lumbarBackPalpation.PainfulSpinousApophysis = "Positivo"
	}
	if m.BackPalpation_lumbar_muscleContracture == "1" {
		lumbarBackPalpation.MuscleContracture = "Negativo"
	} else if m.BackPalpation_lumbar_muscleContracture == "2" {
		lumbarBackPalpation.MuscleContracture = "Positivo"
	}
	backPalpation.Lumbar = &lumbarBackPalpation
	return &backPalpation
}
func (m *SkeletalMuscleConsumer) SetJointsExploration() *models.JointsExploration {
	var jointsExploration models.JointsExploration

	var shoulder models.JointsExplorationDetail
	var rightShoulder models.JointsExplorationFields
	rightShoulder.Abduction = m.JointsExploration_shoulder_right_abduction
	rightShoulder.Adduction = m.JointsExploration_shoulder_right_adduction
	rightShoulder.Flexion = m.JointsExploration_shoulder_right_flexion
	rightShoulder.Extension = m.JointsExploration_shoulder_right_extension
	rightShoulder.ExternalRotation = m.JointsExploration_shoulder_right_externalRotation
	rightShoulder.InternalRotation = m.JointsExploration_shoulder_right_internalRotation
	rightShoulder.FunctionalLimitation = cases.Title(language.Und).String(m.JointsExploration_shoulder_right_functionalLimitation)
	rightShoulder.RadiatingPain = cases.Title(language.Und).String(m.JointsExploration_shoulder_right_radiatingPain)
	rightShoulder.MuscleMassAlterations = cases.Title(language.Und).String(m.JointsExploration_shoulder_right_muscleMassAlterations)
	shoulder.Right = &rightShoulder
	var leftShoulder models.JointsExplorationFields
	leftShoulder.Abduction = m.JointsExploration_shoulder_left_abduction
	leftShoulder.Adduction = m.JointsExploration_shoulder_left_adduction
	leftShoulder.Flexion = m.JointsExploration_shoulder_left_flexion
	leftShoulder.Extension = m.JointsExploration_shoulder_left_extension
	leftShoulder.ExternalRotation = m.JointsExploration_shoulder_left_externalRotation
	leftShoulder.InternalRotation = m.JointsExploration_shoulder_left_internalRotation
	leftShoulder.FunctionalLimitation = cases.Title(language.Und).String(m.JointsExploration_shoulder_left_functionalLimitation)
	leftShoulder.RadiatingPain = cases.Title(language.Und).String(m.JointsExploration_shoulder_left_radiatingPain)
	leftShoulder.MuscleMassAlterations = cases.Title(language.Und).String(m.JointsExploration_shoulder_left_muscleMassAlterations)
	shoulder.Left = &leftShoulder
	jointsExploration.Shoulder = &shoulder

	var elbow models.JointsExplorationDetail
	var rightElbow models.JointsExplorationFields
	rightElbow.Abduction = m.JointsExploration_elbow_right_abduction
	rightElbow.Adduction = m.JointsExploration_elbow_right_adduction
	rightElbow.Flexion = m.JointsExploration_elbow_right_flexion
	rightElbow.Extension = m.JointsExploration_elbow_right_extension
	rightElbow.ExternalRotation = m.JointsExploration_elbow_right_externalRotation
	rightElbow.InternalRotation = m.JointsExploration_elbow_right_internalRotation
	rightElbow.FunctionalLimitation = cases.Title(language.Und).String(m.JointsExploration_elbow_right_functionalLimitation)
	rightElbow.RadiatingPain = cases.Title(language.Und).String(m.JointsExploration_elbow_right_radiatingPain)
	rightElbow.MuscleMassAlterations = cases.Title(language.Und).String(m.JointsExploration_elbow_right_muscleMassAlterations)
	elbow.Right = &rightElbow
	var leftElbow models.JointsExplorationFields
	leftElbow.Abduction = m.JointsExploration_elbow_left_abduction
	leftElbow.Adduction = m.JointsExploration_elbow_left_adduction
	leftElbow.Flexion = m.JointsExploration_elbow_left_flexion
	leftElbow.Extension = m.JointsExploration_elbow_left_extension
	leftElbow.ExternalRotation = m.JointsExploration_elbow_left_externalRotation
	leftElbow.InternalRotation = m.JointsExploration_elbow_left_internalRotation
	leftElbow.FunctionalLimitation = cases.Title(language.Und).String(m.JointsExploration_elbow_left_functionalLimitation)
	leftElbow.RadiatingPain = cases.Title(language.Und).String(m.JointsExploration_elbow_left_radiatingPain)
	leftElbow.MuscleMassAlterations = cases.Title(language.Und).String(m.JointsExploration_elbow_left_muscleMassAlterations)
	elbow.Left = &leftElbow
	jointsExploration.Elbow = &elbow

	var wrist models.JointsExplorationDetail
	var rightWrist models.JointsExplorationFields
	rightWrist.Abduction = m.JointsExploration_wrist_right_abduction
	rightWrist.Adduction = m.JointsExploration_wrist_right_adduction
	rightWrist.Flexion = m.JointsExploration_wrist_right_flexion
	rightWrist.Extension = m.JointsExploration_wrist_right_extension
	rightWrist.ExternalRotation = m.JointsExploration_wrist_right_externalRotation
	rightWrist.InternalRotation = m.JointsExploration_wrist_right_internalRotation
	rightWrist.FunctionalLimitation = cases.Title(language.Und).String(m.JointsExploration_wrist_right_functionalLimitation)
	rightWrist.RadiatingPain = cases.Title(language.Und).String(m.JointsExploration_wrist_right_radiatingPain)
	rightWrist.MuscleMassAlterations = cases.Title(language.Und).String(m.JointsExploration_wrist_right_muscleMassAlterations)
	wrist.Right = &rightWrist
	var leftWrist models.JointsExplorationFields
	leftWrist.Abduction = m.JointsExploration_wrist_left_abduction
	leftWrist.Adduction = m.JointsExploration_wrist_left_adduction
	leftWrist.Flexion = m.JointsExploration_wrist_left_flexion
	leftWrist.Extension = m.JointsExploration_wrist_left_extension
	leftWrist.ExternalRotation = m.JointsExploration_wrist_left_externalRotation
	leftWrist.InternalRotation = m.JointsExploration_wrist_left_internalRotation
	leftWrist.FunctionalLimitation = cases.Title(language.Und).String(m.JointsExploration_wrist_left_functionalLimitation)
	leftWrist.RadiatingPain = cases.Title(language.Und).String(m.JointsExploration_wrist_left_radiatingPain)
	leftWrist.MuscleMassAlterations = cases.Title(language.Und).String(m.JointsExploration_wrist_left_muscleMassAlterations)
	wrist.Left = &leftWrist
	jointsExploration.Wrist = &wrist

	var handsAndFingers models.JointsExplorationDetail
	var rightHandsAndFingers models.JointsExplorationFields
	rightHandsAndFingers.Abduction = m.JointsExploration_handsAndFingers_right_abduction
	rightHandsAndFingers.Adduction = m.JointsExploration_handsAndFingers_right_adduction
	rightHandsAndFingers.Flexion = m.JointsExploration_handsAndFingers_right_flexion
	rightHandsAndFingers.Extension = m.JointsExploration_handsAndFingers_right_extension
	rightHandsAndFingers.ExternalRotation = m.JointsExploration_handsAndFingers_right_externalRotation
	rightHandsAndFingers.InternalRotation = m.JointsExploration_handsAndFingers_right_internalRotation
	rightHandsAndFingers.FunctionalLimitation = cases.Title(language.Und).String(m.JointsExploration_handsAndFingers_right_functionalLimitation)
	rightHandsAndFingers.RadiatingPain = cases.Title(language.Und).String(m.JointsExploration_handsAndFingers_right_radiatingPain)
	rightHandsAndFingers.MuscleMassAlterations = cases.Title(language.Und).String(m.JointsExploration_handsAndFingers_right_muscleMassAlterations)
	handsAndFingers.Right = &rightHandsAndFingers
	var leftHandsAndFingers models.JointsExplorationFields
	leftHandsAndFingers.Abduction = m.JointsExploration_handsAndFingers_left_abduction
	leftHandsAndFingers.Adduction = m.JointsExploration_handsAndFingers_left_adduction
	leftHandsAndFingers.Flexion = m.JointsExploration_handsAndFingers_left_flexion
	leftHandsAndFingers.Extension = m.JointsExploration_handsAndFingers_left_extension
	leftHandsAndFingers.ExternalRotation = m.JointsExploration_handsAndFingers_left_externalRotation
	leftHandsAndFingers.InternalRotation = m.JointsExploration_handsAndFingers_left_internalRotation
	leftHandsAndFingers.FunctionalLimitation = cases.Title(language.Und).String(m.JointsExploration_handsAndFingers_left_functionalLimitation)
	leftHandsAndFingers.RadiatingPain = cases.Title(language.Und).String(m.JointsExploration_handsAndFingers_left_radiatingPain)
	leftHandsAndFingers.MuscleMassAlterations = cases.Title(language.Und).String(m.JointsExploration_handsAndFingers_left_muscleMassAlterations)
	handsAndFingers.Left = &leftHandsAndFingers
	jointsExploration.HandsAndFingers = &handsAndFingers

	var hips models.JointsExplorationDetail
	var rightHips models.JointsExplorationFields
	rightHips.Abduction = m.JointsExploration_hips_right_abduction
	rightHips.Adduction = m.JointsExploration_hips_right_adduction
	rightHips.Flexion = m.JointsExploration_hips_right_flexion
	rightHips.Extension = m.JointsExploration_hips_right_extension
	rightHips.ExternalRotation = m.JointsExploration_hips_right_externalRotation
	rightHips.InternalRotation = m.JointsExploration_hips_right_internalRotation
	rightHips.FunctionalLimitation = cases.Title(language.Und).String(m.JointsExploration_hips_right_functionalLimitation)
	rightHips.RadiatingPain = cases.Title(language.Und).String(m.JointsExploration_hips_right_radiatingPain)
	rightHips.MuscleMassAlterations = cases.Title(language.Und).String(m.JointsExploration_hips_right_muscleMassAlterations)
	hips.Right = &rightHips
	var leftHips models.JointsExplorationFields
	leftHips.Abduction = m.JointsExploration_hips_left_abduction
	leftHips.Adduction = m.JointsExploration_hips_left_adduction
	leftHips.Flexion = m.JointsExploration_hips_left_flexion
	leftHips.Extension = m.JointsExploration_hips_left_extension
	leftHips.ExternalRotation = m.JointsExploration_hips_left_externalRotation
	leftHips.InternalRotation = m.JointsExploration_hips_left_internalRotation
	leftHips.FunctionalLimitation = cases.Title(language.Und).String(m.JointsExploration_hips_left_functionalLimitation)
	leftHips.RadiatingPain = cases.Title(language.Und).String(m.JointsExploration_hips_left_radiatingPain)
	leftHips.MuscleMassAlterations = cases.Title(language.Und).String(m.JointsExploration_hips_left_muscleMassAlterations)
	hips.Left = &leftHips
	jointsExploration.Hips = &hips

	var knee models.JointsExplorationDetail
	var rightKnee models.JointsExplorationFields
	rightKnee.Abduction = m.JointsExploration_knee_right_abduction
	rightKnee.Adduction = m.JointsExploration_knee_right_adduction
	rightKnee.Flexion = m.JointsExploration_knee_right_flexion
	rightKnee.Extension = m.JointsExploration_knee_right_extension
	rightKnee.ExternalRotation = m.JointsExploration_knee_right_externalRotation
	rightKnee.InternalRotation = m.JointsExploration_knee_right_internalRotation
	rightKnee.FunctionalLimitation = cases.Title(language.Und).String(m.JointsExploration_knee_right_functionalLimitation)
	rightKnee.RadiatingPain = cases.Title(language.Und).String(m.JointsExploration_knee_right_radiatingPain)
	rightKnee.MuscleMassAlterations = cases.Title(language.Und).String(m.JointsExploration_knee_right_muscleMassAlterations)
	knee.Right = &rightKnee
	var leftKnee models.JointsExplorationFields
	leftKnee.Abduction = m.JointsExploration_knee_left_abduction
	leftKnee.Adduction = m.JointsExploration_knee_left_adduction
	leftKnee.Flexion = m.JointsExploration_knee_left_flexion
	leftKnee.Extension = m.JointsExploration_knee_left_extension
	leftKnee.ExternalRotation = m.JointsExploration_knee_left_externalRotation
	leftKnee.InternalRotation = m.JointsExploration_knee_left_internalRotation
	leftKnee.FunctionalLimitation = cases.Title(language.Und).String(m.JointsExploration_knee_left_functionalLimitation)
	leftKnee.RadiatingPain = cases.Title(language.Und).String(m.JointsExploration_knee_left_radiatingPain)
	leftKnee.MuscleMassAlterations = cases.Title(language.Und).String(m.JointsExploration_knee_left_muscleMassAlterations)
	knee.Left = &leftKnee
	jointsExploration.Knee = &knee

	var ankle models.JointsExplorationDetail
	var rightAnkle models.JointsExplorationFields
	rightAnkle.Abduction = m.JointsExploration_ankle_right_abduction
	rightAnkle.Adduction = m.JointsExploration_ankle_right_adduction
	rightAnkle.Flexion = m.JointsExploration_ankle_right_flexion
	rightAnkle.Extension = m.JointsExploration_ankle_right_extension
	rightAnkle.ExternalRotation = m.JointsExploration_ankle_right_externalRotation
	rightAnkle.InternalRotation = m.JointsExploration_ankle_right_internalRotation
	rightAnkle.FunctionalLimitation = cases.Title(language.Und).String(m.JointsExploration_ankle_right_functionalLimitation)
	rightAnkle.RadiatingPain = cases.Title(language.Und).String(m.JointsExploration_ankle_right_radiatingPain)
	rightAnkle.MuscleMassAlterations = cases.Title(language.Und).String(m.JointsExploration_ankle_right_muscleMassAlterations)
	ankle.Right = &rightAnkle
	var leftAnkle models.JointsExplorationFields
	leftAnkle.Abduction = m.JointsExploration_ankle_left_abduction
	leftAnkle.Adduction = m.JointsExploration_ankle_left_adduction
	leftAnkle.Flexion = m.JointsExploration_ankle_left_flexion
	leftAnkle.Extension = m.JointsExploration_ankle_left_extension
	leftAnkle.ExternalRotation = m.JointsExploration_ankle_left_externalRotation
	leftAnkle.InternalRotation = m.JointsExploration_ankle_left_internalRotation
	leftAnkle.FunctionalLimitation = cases.Title(language.Und).String(m.JointsExploration_ankle_left_functionalLimitation)
	leftAnkle.RadiatingPain = cases.Title(language.Und).String(m.JointsExploration_ankle_left_radiatingPain)
	leftAnkle.MuscleMassAlterations = cases.Title(language.Und).String(m.JointsExploration_ankle_left_muscleMassAlterations)
	ankle.Left = &leftAnkle
	jointsExploration.Ankle = &ankle

	return &jointsExploration
}
