package consumer_models

type StressTestConsumer struct {
	ID          int    `json:"idprueba_esf,omitempty" bson:"idprueba_esf,omitempty"`
	AttentionId int    `json:"idcomprobante,omitempty" bson:"idcomprobante,omitempty"`
	Result      string `json:"conclusiones,omitempty" bson:"conclusiones,omitempty"`
	Status      string `json:"estado" bson:"estado"`
	IsDeleted   string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog       string
}
