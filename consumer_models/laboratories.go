package consumer_models

import (
	"desktop-middleware/models"
	"strconv"
)

type LaboratoryConsumer struct {
	ID                               int     `json:"idanalisis_cli_arca,omitempty" bson:"idanalisis_cli_arca,omitempty"`
	AttentionId                      int     `json:"idcomprobante,omitempty" bson:"idcomprobante,omitempty"`
	BloodCountResult                 string  `json:"hemograma_c,omitempty" bson:"hemograma_c,omitempty"`
	LeukocyteCount                   string  `json:"leucocito,omitempty" bson:"leucocito,omitempty"`
	HemoglobinCount                  string  `json:"hemoglobina,omitempty" bson:"hemoglobina,omitempty"`
	HematocritCount                  string  `json:"hematocrito,omitempty" bson:"hematocrito,omitempty"`
	RedCellsCount                    string  `json:"hematies,omitempty" bson:"hematies,omitempty"`
	BandNeutrophilsCount             string  `json:"abastonado_abs,omitempty" bson:"abastonado_abs,omitempty"`
	SegmentedCount                   string  `json:"segmentado_abs,omitempty" bson:"segmentado_abs,omitempty"`
	EosinophilCount                  string  `json:"eosinofilos_abs,omitempty" bson:"eosinofilos_abs,omitempty"`
	BasophilsCount                   string  `json:"basofilos_abs,omitempty" bson:"basofilos_abs,omitempty"`
	MonocytesCount                   string  `json:"monocitos_abs,omitempty" bson:"monocitos_abs,omitempty"`
	LymphocytesCount                 string  `json:"linfocitos_abs,omitempty" bson:"linfocitos_abs,omitempty"`
	VcmCount                         string  `json:"vcm,omitempty" bson:"vcm,omitempty"`
	HcmCount                         string  `json:"hcm,omitempty" bson:"hcm,omitempty"`
	PlateletsCount                   string  `json:"plaquetas,omitempty" bson:"plaquetas,omitempty"`
	BloodType                        string  `json:"grupo,omitempty" bson:"grupo,omitempty"`
	BloodFactor                      string  `json:"factor,omitempty" bson:"factor,omitempty"`
	UrineTestResult                  string  `json:"exaco_orina,omitempty" bson:"exaco_orina,omitempty"`
	CocaineResult                    string  `json:"coca_droga,omitempty" bson:"coca_droga,omitempty"`
	MarijuanaResult                  string  `json:"mari_droga,omitempty" bson:"mari_droga,omitempty"`
	AmphetamineResult                string  `json:"anfetaminad,omitempty" bson:"anfetaminad,omitempty"`
	MethamphetamineResult            string  `json:"metanfetamina,omitempty" bson:"metanfetamina,omitempty"`
	BenzodiazepineResult             string  `json:"benzodizepina,omitempty" bson:"benzodizepina,omitempty"`
	EcstasyResult                    string  `json:"extasis_orina,omitempty" bson:"extasis_orina,omitempty"`
	AlcoholResult                    string  `json:"alcohol_cuali,omitempty" bson:"alcohol_cuali,omitempty"`
	LeadResult                       string  `json:"plomo_sangre,omitempty" bson:"plomo_sangre,omitempty"`
	SputumSampleResult1              string  `json:"bk_esputo1,omitempty" bson:"bk_esputo1,omitempty"`
	SputumSampleResult2              string  `json:"bk_esputo2,omitempty" bson:"bk_esputo2,omitempty"`
	SputumSampleResult3              string  `json:"bk_esputo3,omitempty" bson:"bk_esputo3,omitempty"`
	ParasiteResult1                  string  `json:"parasito_1,omitempty" bson:"parasito_1,omitempty"`
	ParasiteResult2                  string  `json:"parasito_2,omitempty" bson:"parasito_2,omitempty"`
	ParasiteResult3                  string  `json:"parasito_3,omitempty" bson:"parasito_3,omitempty"`
	StoolCultureResult               string  `json:"coloracion_gram,omitempty" bson:"coloracion_gram,omitempty"`
	UrineCultureResult               string  `json:"colo_gram_urocult,omitempty" bson:"colo_gram_urocult,omitempty"`
	PharyngealSecretionCultureResult string  `json:"coloracion_gram2,omitempty" bson:"coloracion_gram2,omitempty"`
	TgoCount                         string  `json:"tgo,omitempty" bson:"tgo,omitempty"`
	TgpCount                         string  `json:"tgp,omitempty" bson:"tgp,omitempty"`
	UreaCount                        string  `json:"urea,omitempty" bson:"urea,omitempty"`
	CreatinineCount                  string  `json:"creatinina,omitempty" bson:"creatinina,omitempty"`
	GlucoseCount                     string  `json:"glucosa_otro,omitempty" bson:"glucosa_otro,omitempty"`
	CholesterolCount                 string  `json:"colesterol,omitempty" bson:"colesterol,omitempty"`
	TriglycerideCount                string  `json:"trigliceridos,omitempty" bson:"trigliceridos,omitempty"`
	HdlCount                         string  `json:"hdl,omitempty" bson:"hdl,omitempty"`
	LdlCount                         string  `json:"ldl,omitempty" bson:"ldl,omitempty"`
	HaigmResult                      string  `json:"haigm,omitempty" bson:"haigm,omitempty"`
	HaiggResult                      string  `json:"haigg,omitempty" bson:"haigg,omitempty"`
	HbsagResult                      string  `json:"hbsag,omitempty" bson:"hbsag,omitempty"`
	HepatitisCResult                 string  `json:"hepatitis_c,omitempty" bson:"hepatitis_c,omitempty"`
	TificooResult                    string  `json:"tifico_o,omitempty" bson:"tifico_o,omitempty"`
	TificohCResult                   string  `json:"tifico_h,omitempty" bson:"tifico_h,omitempty"`
	ParatificoaResult                string  `json:"paratifico_a,omitempty" bson:"paratifico_a,omitempty"`
	ParatificobResult                string  `json:"paratifico_b,omitempty" bson:"paratifico_b,omitempty"`
	BrucellasSspResult               string  `json:"brucellas_ssp,omitempty" bson:"brucellas_ssp,omitempty"`
	PsaResult                        string  `json:"psa_libre,omitempty" bson:"psa_libre,omitempty"`
	VdrlResult                       string  `json:"vdrl,omitempty" bson:"vdrl,omitempty"`
	RprResult                        string  `json:"reaccion_lues,omitempty" bson:"reaccion_lues,omitempty"`
	SerologicalTestIgmResult         string  `json:"igm_sars_cov2,omitempty" bson:"igm_sars_cov2,omitempty"`
	SerologicalTestIggResult         string  `json:"igg_sars_cov2,omitempty" bson:"igg_sars_cov2,omitempty"`
	MolecularTestResult              string  `json:"bio_sars_cov2,omitempty" bson:"bio_sars_cov2,omitempty"`
	QuantitativeAntigenTestResult    string  `json:"bio_covid_19_anti,omitempty" bson:"bio_covid_19_anti,omitempty"`
	EliseTestIgaResult               string  `json:"bio_sars_cov2_anti_iga,omitempty" bson:"bio_sars_cov2_anti_iga,omitempty"`
	EliseTestIggResult               string  `json:"bio_sars_cov2_anti_igg,omitempty" bson:"bio_sars_cov2_anti_igg,omitempty"`
	QualitativeAntigenTestResult     string  `json:"antigeno,omitempty" bson:"antigeno,omitempty"`
	RocheTestResult                  string  `json:"bio_sars_cov2_anti,omitempty" bson:"bio_sars_cov2_anti,omitempty"`
	CovneuTestResult                 string  `json:"covneu,omitempty" bson:"covneu,omitempty"`
	SalivaAntigenTestResult          string  `json:"cboAntigenoSaliva,omitempty" bson:"cboAntigenoSaliva,omitempty"`
	Diagnostic1                      string  `json:"diag,omitempty" bson:"diag,omitempty"`
	Diagnostic2                      string  `json:"diag2,omitempty" bson:"diag2,omitempty"`
	Diagnostic3                      string  `json:"diag3,omitempty" bson:"diag3,omitempty"`
	Diagnostic4                      string  `json:"diag4,omitempty" bson:"diag4,omitempty"`
	Diagnostic5                      string  `json:"diag5,omitempty" bson:"diag5,omitempty"`
	Status                           string  `json:"estado" bson:"estado"`
	Reviewed                         *string `json:"revisadofh03,omitempty" bson:"revisadofh03,omitempty"`
	IsDeleted                        string  `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog                            string
}

func (m *LaboratoryConsumer) SetHematologyTest(attentionId, serviceAreaId, serviceAreaCode, examId string) models.HematologyTest {
	var newTest models.HematologyTest
	newTest.AttentionId = attentionId
	newTest.ServiceAreaID = serviceAreaId
	newTest.ServiceAreaCode = serviceAreaCode
	newTest.ExamID = examId
	if m.BloodCountResult == "1" {
		newTest.BloodCountResult = "Normal"
	} else if m.BloodCountResult == "2" {
		newTest.BloodCountResult = "Anormal"
	} else {
		newTest.BloodCountResult = ""
	}
	if val, err := strconv.ParseFloat(m.LeukocyteCount, 64); err == nil {
		newTest.LeukocyteCount = val
	}
	if val, err := strconv.ParseFloat(m.HemoglobinCount, 64); err == nil {
		newTest.HemoglobinCount = val
	}
	if val, err := strconv.ParseFloat(m.HematocritCount, 64); err == nil {
		newTest.HematocritCount = val
	}
	if val, err := strconv.ParseFloat(m.RedCellsCount, 64); err == nil {
		newTest.RedCellsCount = val
	}
	if val, err := strconv.ParseFloat(m.SegmentedCount, 64); err == nil {
		newTest.SegmentedCount = val
	}
	if val, err := strconv.ParseFloat(m.EosinophilCount, 64); err == nil {
		newTest.EosinophilCount = val
	}
	if val, err := strconv.ParseFloat(m.BasophilsCount, 64); err == nil {
		newTest.BasophilsCount = val
	}
	if val, err := strconv.ParseFloat(m.MonocytesCount, 64); err == nil {
		newTest.MonocytesCount = val
	}
	if val, err := strconv.ParseFloat(m.LymphocytesCount, 64); err == nil {
		newTest.LymphocytesCount = val
	}
	if val, err := strconv.ParseFloat(m.VcmCount, 64); err == nil {
		newTest.VcmCount = val
	}
	if val, err := strconv.ParseFloat(m.HcmCount, 64); err == nil {
		newTest.HcmCount = val
	}
	if val, err := strconv.ParseFloat(m.PlateletsCount, 64); err == nil {
		newTest.PlateletsCount = val
	}
	newTest.BandNeutrophilsCount = m.BandNeutrophilsCount
	newTest.BloodType = m.BloodType
	newTest.BloodFactor = m.BloodFactor
	status, _ := strconv.Atoi(m.Status)
	newTest.Status = status
	newTest.ExternalId = strconv.Itoa(m.ID)
	newTest.ExternalSystem = "MEDIWEB"
	return newTest
}

func (m *LaboratoryConsumer) SetUrineTest(attentionId, serviceAreaId, serviceAreaCode, examId string) models.UrineTest {
	var newTest models.UrineTest
	newTest.AttentionId = attentionId
	newTest.ServiceAreaID = serviceAreaId
	newTest.ServiceAreaCode = serviceAreaCode
	newTest.ExamID = examId
	if m.UrineTestResult == "1" {
		newTest.Result = "Normal"
	} else if m.UrineTestResult == "2" {
		newTest.Result = "Anormal"
	} else {
		newTest.Result = ""
	}
	status, _ := strconv.Atoi(m.Status)
	newTest.Status = status
	newTest.ExternalId = strconv.Itoa(m.ID)
	newTest.ExternalSystem = "MEDIWEB"
	return newTest
}

func (m *LaboratoryConsumer) SetDrugTest(attentionId, serviceAreaId, serviceAreaCode, examId string) models.DrugTest {
	var newTest models.DrugTest
	newTest.AttentionId = attentionId
	newTest.ServiceAreaID = serviceAreaId
	newTest.ServiceAreaCode = serviceAreaCode
	newTest.ExamID = examId
	newTest.CocaineResult = m.CocaineResult
	newTest.MarijuanaResult = m.MarijuanaResult
	newTest.AmphetamineResult = m.AmphetamineResult
	newTest.MethamphetamineResult = m.MethamphetamineResult
	newTest.BenzodiazepineResult = m.BenzodiazepineResult
	newTest.EcstasyResult = m.EcstasyResult
	newTest.AlcoholResult = m.AlcoholResult
	newTest.LeadResult = m.LeadResult
	status, _ := strconv.Atoi(m.Status)
	newTest.Status = status
	newTest.ExternalId = strconv.Itoa(m.ID)
	newTest.ExternalSystem = "MEDIWEB"
	return newTest
}

func (m *LaboratoryConsumer) SetMicrobiologyTest(attentionId, serviceAreaId, serviceAreaCode, examId string) models.MicrobiologyTest {
	var newTest models.MicrobiologyTest
	var sputumSampleResult []string
	var parasiteResult []string
	newTest.AttentionId = attentionId
	newTest.ServiceAreaID = serviceAreaId
	newTest.ServiceAreaCode = serviceAreaCode
	newTest.ExamID = examId
	if m.SputumSampleResult1 != "" {
		sputumSampleResult = append(sputumSampleResult, m.SputumSampleResult1)
	}
	if m.SputumSampleResult2 != "" {
		sputumSampleResult = append(sputumSampleResult, m.SputumSampleResult2)
	}
	if m.SputumSampleResult3 != "" {
		sputumSampleResult = append(sputumSampleResult, m.SputumSampleResult3)
	}
	if m.ParasiteResult1 != "" {
		parasiteResult = append(parasiteResult, m.ParasiteResult1)
	}
	if m.ParasiteResult2 != "" {
		parasiteResult = append(parasiteResult, m.ParasiteResult2)
	}
	if m.ParasiteResult3 != "" {
		parasiteResult = append(parasiteResult, m.ParasiteResult3)
	}
	newTest.ParasiteResult = parasiteResult
	newTest.SputumSampleResult = sputumSampleResult
	status, _ := strconv.Atoi(m.Status)
	newTest.Status = status
	newTest.ExternalId = strconv.Itoa(m.ID)
	newTest.ExternalSystem = "MEDIWEB"
	return newTest
}

func (m *LaboratoryConsumer) SetBiochemistryTest(attentionId, serviceAreaId, serviceAreaCode, examId string) models.BiochemistryTest {
	var newTest models.BiochemistryTest
	newTest.AttentionId = attentionId
	newTest.ServiceAreaID = serviceAreaId
	newTest.ServiceAreaCode = serviceAreaCode
	newTest.ExamID = examId
	if val, err := strconv.ParseFloat(m.TgoCount, 64); err == nil {
		newTest.TgoCount = val
	}
	if val, err := strconv.ParseFloat(m.TgpCount, 64); err == nil {
		newTest.TgpCount = val
	}
	if val, err := strconv.ParseFloat(m.UreaCount, 64); err == nil {
		newTest.UreaCount = val
	}
	if val, err := strconv.ParseFloat(m.CreatinineCount, 64); err == nil {
		newTest.CreatinineCount = val
	}
	if val, err := strconv.ParseFloat(m.GlucoseCount, 64); err == nil {
		newTest.GlucoseCount = val
	}
	if val, err := strconv.ParseFloat(m.CholesterolCount, 64); err == nil {
		newTest.CholesterolCount = val
	}
	if val, err := strconv.ParseFloat(m.TriglycerideCount, 64); err == nil {
		newTest.TriglycerideCount = val
	}
	if val, err := strconv.ParseFloat(m.HdlCount, 64); err == nil {
		newTest.HdlCount = val
	}
	if val, err := strconv.ParseFloat(m.LdlCount, 64); err == nil {
		newTest.LdlCount = val
	}
	status, _ := strconv.Atoi(m.Status)
	newTest.Status = status
	newTest.ExternalId = strconv.Itoa(m.ID)
	newTest.ExternalSystem = "MEDIWEB"
	return newTest
}

func (m *LaboratoryConsumer) SetImmunologyTest(attentionId, serviceAreaId, serviceAreaCode, examId string) models.ImmunologyTest {
	var newTest models.ImmunologyTest
	newTest.AttentionId = attentionId
	newTest.ServiceAreaID = serviceAreaId
	newTest.ServiceAreaCode = serviceAreaCode
	newTest.ExamID = examId
	newTest.HaigmResult = m.HaigmResult
	newTest.HaiggResult = m.HaiggResult
	newTest.HbsagResult = m.HbsagResult
	newTest.HepatitisCResult = m.HepatitisCResult
	newTest.TificooResult = m.TificooResult
	newTest.TificohCResult = m.TificohCResult
	newTest.ParatificoaResult = m.ParatificoaResult
	newTest.ParatificobResult = m.ParatificobResult
	newTest.BrucellasSspResult = m.BrucellasSspResult
	newTest.PsaResult = m.PsaResult
	newTest.VdrlResult = m.VdrlResult
	newTest.RprResult = m.RprResult
	status, _ := strconv.Atoi(m.Status)
	newTest.Status = status
	newTest.ExternalId = strconv.Itoa(m.ID)
	newTest.ExternalSystem = "MEDIWEB"
	return newTest
}

func (m *LaboratoryConsumer) SetCovidTest(attentionId, serviceAreaId, serviceAreaCode, examId string) models.CovidTest {
	var newTest models.CovidTest
	newTest.AttentionId = attentionId
	newTest.ServiceAreaID = serviceAreaId
	newTest.ServiceAreaCode = serviceAreaCode
	newTest.ExamID = examId
	newTest.SerologicalTestIgmResult = m.SerologicalTestIgmResult
	newTest.SerologicalTestIggResult = m.SerologicalTestIggResult
	newTest.MolecularTestResult = m.MolecularTestResult
	newTest.QuantitativeAntigenTestResult = m.QuantitativeAntigenTestResult
	newTest.EliseTestIgaResult = m.EliseTestIgaResult
	newTest.EliseTestIggResult = m.EliseTestIggResult
	newTest.QualitativeAntigenTestResult = m.QualitativeAntigenTestResult
	newTest.RocheTestResult = m.RocheTestResult
	newTest.CovneuTestResult = m.CovneuTestResult
	newTest.SalivaAntigenTestResult = m.SalivaAntigenTestResult
	status, _ := strconv.Atoi(m.Status)
	newTest.Status = status
	newTest.ExternalId = strconv.Itoa(m.ID)
	newTest.ExternalSystem = "MEDIWEB"
	return newTest
}
