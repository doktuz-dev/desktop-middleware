package consumer_models

type XRayLDConsumer struct {
	ID          int    `json:"idrayosxc,omitempty" bson:"idrayosxc,omitempty"`
	AttentionId string `json:"idcomprobante,omitempty" bson:"idcomprobante,omitempty"`
	FileName1   string `json:"descripcion,omitempty" bson:"descripcion,omitempty"`
	FileName2   string `json:"descripcion2,omitempty" bson:"descripcion2,omitempty"`
	FileName3   string `json:"descripcion3,omitempty" bson:"descripcion3,omitempty"`
	FileName4   string `json:"descripcion4,omitempty" bson:"descripcion4,omitempty"`
	FileName5   string `json:"descripcion5,omitempty" bson:"descripcion5,omitempty"`
	FileName6   string `json:"descripcion6,omitempty" bson:"descripcion6,omitempty"`
	FileName7   string `json:"descripcion7,omitempty" bson:"descripcion7,omitempty"`
	FileName8   string `json:"descripcion8,omitempty" bson:"descripcion8,omitempty"`
	FileName9   string `json:"descripcion9,omitempty" bson:"descripcion9,omitempty"`
	FileName10  string `json:"descripcion10,omitempty" bson:"descripcion10,omitempty"`
	FileName11  string `json:"descripcion11,omitempty" bson:"descripcion11,omitempty"`
	FileName12  string `json:"descripcion12,omitempty" bson:"descripcion12,omitempty"`
	FileName13  string `json:"descripcion13,omitempty" bson:"descripcion13,omitempty"`
	FileName14  string `json:"descripcion14,omitempty" bson:"descripcion14,omitempty"`
	FileName15  string `json:"descripcion15,omitempty" bson:"descripcion15,omitempty"`
	Result      string `json:"conclusion,omitempty" bson:"conclusion,omitempty"`
	Diagnostic1 string `json:"diag,omitempty" bson:"diag,omitempty"`
	Diagnostic2 string `json:"diag2,omitempty" bson:"diag2,omitempty"`
	Diagnostic3 string `json:"diag3,omitempty" bson:"diag3,omitempty"`
	Status      int    `json:"estado" bson:"estado"`
	IsDeleted   string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog       string
}
