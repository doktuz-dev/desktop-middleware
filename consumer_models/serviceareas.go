package consumer_models

type ServiceAreaConsumer struct {
	ID        int    `json:"idseccion,omitempty" bson:"idseccion,omitempty"`
	Name      string `json:"descripcion,omitempty" bson:"descripcion,omitempty"`
	Status    string `json:"estado" bson:"estado"`
	IsDeleted string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog     string
}
