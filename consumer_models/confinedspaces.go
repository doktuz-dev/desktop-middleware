package consumer_models

type ConfinedSpacesConsumer struct {
	ID          int    `json:"idespacio_confi,omitempty" bson:"idespacio_confi,omitempty"`
	AttentionId int    `json:"idcomprobante,omitempty" bson:"idcomprobante,omitempty"`
	Result      string `json:"aptitud,omitempty" bson:"aptitud,omitempty"`
	Status      string `json:"estado" bson:"estado"`
	IsDeleted   string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog       string
}
