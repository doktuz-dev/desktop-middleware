package consumer_models

type ExamTypeConsumer struct {
	ID        string `json:"idtipo,omitempty" bson:"idtipo,omitempty"`
	Name      string `json:"descripcion,omitempty" bson:"descripcion,omitempty"`
	Status    int    `json:"estado" bson:"estado"`
	IsDeleted string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog     string
}
