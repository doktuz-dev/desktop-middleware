package consumer_models

import (
	"time"

	"github.com/araddon/dateparse"
)

type EvaluationConsumer struct {
	ID             int    `json:"id,omitempty" bson:"id,omitempty"`
	AttentionId    int    `json:"ticketId,omitempty" bson:"ticketId,omitempty"`
	EvaluationDate string `json:"createdAt,omitempty" bson:"createdAt,omitempty"`
	Result         int    `json:"answer,omitempty" bson:"answer,omitempty"`
	Comments       string `json:"comments,omitempty" bson:"comments,omitempty"`
	WasRejected    int    `json:"wasrejected" bson:"wasrejected"`
	IsDeleted      string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog          string
}

func (a *EvaluationConsumer) SetEvaluationDate() time.Time {
	attentionDate, _ := dateparse.ParseLocal(a.EvaluationDate)
	return attentionDate
}
