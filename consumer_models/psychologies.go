package consumer_models

import (
	"desktop-middleware/models"
	"strconv"
)

type PsychologyConsumer struct {
	ID                 int    `json:"idpsicologia,omitempty" bson:"idrayosx,omitempty"`
	AttentionId        int    `json:"idcomprobante,omitempty" bson:"idcomprobante,omitempty"`
	FatigueTest        string `json:"fatiga_sueno,omitempty" bson:"fatiga_sueno,omitempty"`
	AcrophobiaTest     string `json:"recuestio_acro,omitempty" bson:"recuestio_acro,omitempty"`
	ClaustrophobiaTest string `json:"recuestio_cla,omitempty" bson:"recuestio_cla,omitempty"`
	Result             string `json:"aptopsico,omitempty" bson:"aptopsico,omitempty"`
	Recommendation1    string `json:"psi_reco1,omitempty" bson:"psi_reco1,omitempty"`
	Recommendation2    string `json:"psi_reco2,omitempty" bson:"psi_reco2,omitempty"`
	Recommendation3    string `json:"psi_reco3,omitempty" bson:"psi_reco3,omitempty"`
	Status             string `json:"estado" bson:"estado"`
	IsDeleted          string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog              string
}

func (m *PsychologyConsumer) SetPsychologyTest() models.PsychologyTest {
	var newTest models.PsychologyTest
	newTest.AcrophobiaTest = m.AcrophobiaTest
	newTest.ClaustrophobiaTest = m.ClaustrophobiaTest
	newTest.FatigueTest = m.FatigueTest
	status, _ := strconv.Atoi(m.Status)
	newTest.Status = status
	newTest.ExternalId = strconv.Itoa(m.ID)
	newTest.ExternalSystem = "MEDIWEB"
	return newTest
}
