package consumer_models

type ExamResultConsumer struct {
	ID            int    `json:"idformato_x_orden,omitempty" bson:"idformato_x_orden,omitempty"`
	AttentionId   int    `json:"idcomprobante,omitempty" bson:"idcomprobante,omitempty"`
	ServiceAreaId int    `json:"idseccion,omitempty" bson:"idseccion,omitempty"`
	PrintId       int    `json:"idformato,omitempty" bson:"idformato,omitempty"`
	Filename      string `json:"pagina_imp,omitempty" bson:"pagina_imp,omitempty"`
	Reviewed      string `json:"revisadofh02,omitempty" bson:"revisadofh02,omitempty"`
	Status        int    `json:"estado" bson:"estado"`
	IsDeleted     string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog         string
}
