package consumer_models

import "time"

type WorkProfileConsumer struct {
	ID        int    `json:"idobra,omitempty" bson:"idobra,omitempty"`
	Name      string `json:"descripcion,omitempty" bson:"descripcion,omitempty"`
	IsDeleted string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	Blocked   string `json:"bloquear,omitempty" bson:"bloquear,omitempty"`
	IdLog     string
}

type WorkProfileDetailConsumer struct {
	ID         int    `json:"idobra,omitempty" bson:"idobra,omitempty"`
	ProtocolID int    `json:"idprotocolo,omitempty" bson:"idprotocolo,omitempty"`
	CustomerID int    `json:"idempresa,omitempty" bson:"idempresa,omitempty"`
	EndDate    int32  `json:"fechafinal,omitempty" bson:"fechafinal,omitempty"`
	Status2    int32  `json:"estado,omitempty" bson:"estado,omitempty"`
	Status     string `json:"validar_protocolo,omitempty" bson:"validar_protocolo,omitempty"`
	Version    int    `json:"version,omitempty" bson:"version,omitempty"`
	ClinicId   int    `json:"centromed_protocolo,omitempty" bson:"centromed_protocolo,omitempty"`
	CheckOcu   string `json:"check_focu,omitempty" bson:"check_focu,omitempty"`
	CheckPre   string `json:"check_fpre,omitempty" bson:"check_fpre,omitempty"`
	CheckRet   string `json:"check_fret,omitempty" bson:"check_fret,omitempty"`
	CheckVis   string `json:"check_fvis,omitempty" bson:"check_fvis,omitempty"`
	CheckAlt   string `json:"check_falt,omitempty" bson:"check_falt,omitempty"`
	IsDeleted  string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog      string
}

func (w WorkProfileDetailConsumer) AddDateEndDate() time.Time {
	start := time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC)
	return start.AddDate(0, 0, int(w.EndDate))
}
