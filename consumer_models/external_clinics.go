package consumer_models

type ExternalClinicConsumer struct {
	ID        int    `json:"idclinica,omitempty" bson:"idclinica,omitempty"`
	Name      string `json:"descripcion,omitempty" bson:"descripcion,omitempty"`
	Status    int    `json:"estado" bson:"estado"`
	IsDeleted string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog     string
}
