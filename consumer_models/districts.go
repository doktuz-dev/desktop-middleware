package consumer_models

type DistrictConsumer struct {
	ID           int    `json:"iddistrito,omitempty"`
	DepartmentID int    `json:"iddepartamento,omitempty"`
	ProvinceID   int    `json:"idprovincia,omitempty"`
	Name         string `json:"nombre,omitempty"`
	IsDeleted    string `json:"__deleted,omitempty"`
	IdLog        string
}
