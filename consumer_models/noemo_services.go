package consumer_models

type NoEmoServiceConsumer struct {
	ID        int    `json:"idprocedimiento,omitempty" bson:"idprocedimiento,omitempty"`
	Code      string `json:"codigo,omitempty" bson:"codigo,omitempty"`
	Name      string `json:"descripcion,omitempty" bson:"descripcion,omitempty"`
	Status    int    `json:"estado" bson:"estado"`
	Blocked   string `json:"bloq_exa" bson:"bloq_exa"`
	IsDeleted string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog     string
}
