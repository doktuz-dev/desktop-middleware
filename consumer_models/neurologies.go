package consumer_models

type NeurologyConsumer struct {
	ID          int    `json:"idneurologico,omitempty" bson:"idneurologico,omitempty"`
	AttentionId int    `json:"idcomprobante,omitempty" bson:"idcomprobante,omitempty"`
	Diagnostic1 string `json:"diag,omitempty" bson:"diag,omitempty"`
	Diagnostic2 string `json:"diag2,omitempty" bson:"diag2,omitempty"`
	Diagnostic3 string `json:"diag3,omitempty" bson:"diag3,omitempty"`
	Status      string `json:"estado" bson:"estado"`
	IsDeleted   string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog       string
}
