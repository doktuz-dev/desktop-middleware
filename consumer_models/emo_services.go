package consumer_models

type EmoServiceConsumer struct {
	ID        int    `json:"idexamenocu,omitempty" bson:"idexamenocu,omitempty"`
	Code      string `json:"codigo,omitempty" bson:"codigo,omitempty"`
	Name      string `json:"descripcion,omitempty" bson:"descripcion,omitempty"`
	Status    int    `json:"estado" bson:"estado"`
	IsDeleted string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog     string
}
