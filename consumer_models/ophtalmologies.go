package consumer_models

import (
	"desktop-middleware/models"
	"strconv"
)

type OphtalmologyConsumer struct {
	ID                         int    `json:"idoftalmologica,omitempty" bson:"idoftalmologica,omitempty"`
	AttentionId                int    `json:"idcomprobante,omitempty" bson:"idcomprobante,omitempty"`
	DistanceNoCorrectorRight   string `json:"aguvisual_lejos_od,omitempty" bson:"aguvisual_lejos_od,omitempty"`
	DistanceNoCorrectorLeft    string `json:"aguvisual_lejos_oi,omitempty" bson:"aguvisual_lejos_oi,omitempty"`
	DistanceWithCorrectorRight string `json:"aguvisual_lejos_od3,omitempty" bson:"aguvisual_lejos_od3,omitempty"`
	DistanceWithCorrectorLeft  string `json:"aguvisual_lejos_oi3,omitempty" bson:"aguvisual_lejos_oi3,omitempty"`
	CloseNoCorrectorRight      string `json:"aguvisual_cerca_od,omitempty" bson:"aguvisual_cerca_od,omitempty"`
	CloseNoCorrectorLeft       string `json:"aguvisual_cerca_oi,omitempty" bson:"aguvisual_cerca_oi,omitempty"`
	CloseWithCorrectorRight    string `json:"aguvisual_cerca_od3,omitempty" bson:"aguvisual_cerca_od3,omitempty"`
	CloseWithCorrectorLeft     string `json:"aguvisual_cerca_oi3,omitempty" bson:"aguvisual_cerca_oi3,omitempty"`
	Stereopsis                 string `json:"esteropsia,omitempty" bson:"esteropsia,omitempty"`
	Diagnostic1                string `json:"diag,omitempty" bson:"diag,omitempty"`
	Diagnostic2                string `json:"diag2,omitempty" bson:"diag2,omitempty"`
	Diagnostic3                string `json:"diag3,omitempty" bson:"diag3,omitempty"`
	Diagnostic4                string `json:"diag4,omitempty" bson:"diag4,omitempty"`
	Diagnostic5                string `json:"diag5,omitempty" bson:"diag5,omitempty"`
	Status                     string `json:"estado" bson:"estado"`
	IsDeleted                  string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog                      string
}

func (m *OphtalmologyConsumer) SetOphtalmologyTest() models.OphtalmologyTest {
	var newTest models.OphtalmologyTest
	newTest.DistanceNoCorrector = m.SetDistanceNoCorrector()
	newTest.CloseNoCorrector = m.SetCloseNoCorrector()
	newTest.DistanceWithCorrector = m.SetDistanceWithCorrector()
	newTest.CloseWithCorrector = m.SetCloseWithCorrector()
	newTest.Stereopsis = m.Stereopsis
	status, _ := strconv.Atoi(m.Status)
	newTest.Status = status
	newTest.ExternalId = strconv.Itoa(m.ID)
	newTest.ExternalSystem = "MEDIWEB"
	return newTest
}

func (m *OphtalmologyConsumer) SetDistanceNoCorrector() *models.VisualAcuity {
	var cond models.VisualAcuity
	cond.Right = m.DistanceNoCorrectorRight
	cond.Left = m.DistanceNoCorrectorLeft
	return &cond
}

func (m *OphtalmologyConsumer) SetCloseNoCorrector() *models.VisualAcuity {
	var cond models.VisualAcuity
	cond.Right = m.CloseNoCorrectorRight
	cond.Left = m.CloseNoCorrectorLeft
	return &cond
}

func (m *OphtalmologyConsumer) SetDistanceWithCorrector() *models.VisualAcuity {
	var cond models.VisualAcuity
	cond.Right = m.DistanceWithCorrectorRight
	cond.Left = m.DistanceWithCorrectorLeft
	return &cond
}

func (m *OphtalmologyConsumer) SetCloseWithCorrector() *models.VisualAcuity {
	var cond models.VisualAcuity
	cond.Right = m.CloseWithCorrectorRight
	cond.Left = m.CloseWithCorrectorLeft
	return &cond
}
