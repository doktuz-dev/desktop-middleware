package consumer_models

type PsychosensometricConsumer struct {
	ID          int    `json:"idresultado_psico,omitempty" bson:"idresultado_psico,omitempty"`
	AttentionId string `json:"idcomprobante,omitempty" bson:"idcomprobante,omitempty"`
	Result      string `json:"conclu1,omitempty" bson:"conclu1,omitempty"`
	Status      int    `json:"estado" bson:"estado"`
	IsDeleted   string `json:"__deleted,omitempty" bson:"__deleted,omitempty"`
	IdLog       string
}
