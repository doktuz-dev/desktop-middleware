package consumer_models

type DepartmentConsumer struct {
	ID        int    `json:"iddepartamento,omitempty"`
	Name      string `json:"nombre,omitempty"`
	IsDeleted string `json:"__deleted,omitempty"`
	IdLog     string
}
