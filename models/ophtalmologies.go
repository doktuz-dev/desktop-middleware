package models

import "time"

type Ophtalmology struct {
	ID                    string        `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionId           string        `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	ServiceAreaID         string        `json:"serviceAreaId,omitempty" bson:"serviceAreaId,omitempty"`
	ServiceAreaCode       string        `json:"serviceAreaCode,omitempty" bson:"serviceAreaCode,omitempty"`
	DistanceNoCorrector   *VisualAcuity `json:"distanceNoCorrector,omitempty" bson:"distanceNoCorrector,omitempty"`
	CloseNoCorrector      *VisualAcuity `json:"closeNoCorrector,omitempty" bson:"closeNoCorrector,omitempty"`
	DistanceWithCorrector *VisualAcuity `json:"distanceWithCorrector,omitempty" bson:"distanceWithCorrector,omitempty"`
	CloseWithCorrector    *VisualAcuity `json:"closeWithCorrector,omitempty" bson:"closeWithCorrector,omitempty"`
	Stereopsis            string        `json:"stereopsis,omitempty" bson:"stereopsis,omitempty"`
	Diagnostic1           string        `json:"diagnostic1,omitempty" bson:"diagnostic1,omitempty"`
	Diagnostic2           string        `json:"diagnostic2,omitempty" bson:"diagnostic2,omitempty"`
	Diagnostic3           string        `json:"diagnostic3,omitempty" bson:"diagnostic3,omitempty"`
	Diagnostic4           string        `json:"diagnostic4,omitempty" bson:"diagnostic4,omitempty"`
	Diagnostic5           string        `json:"diagnostic5,omitempty" bson:"diagnostic5,omitempty"`
	Status                int           `json:"status" bson:"status"`
}

type OphtalmologyTest struct {
	ID                    string        `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionId           string        `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	ServiceAreaID         string        `json:"serviceAreaId,omitempty" bson:"serviceAreaId,omitempty"`
	ServiceAreaCode       string        `json:"serviceAreaCode,omitempty" bson:"serviceAreaCode,omitempty"`
	ExamId                string        `json:"examId,omitempty" bson:"examId,omitempty"`
	DistanceNoCorrector   *VisualAcuity `json:"distanceNoCorrector,omitempty" bson:"distanceNoCorrector,omitempty"`
	CloseNoCorrector      *VisualAcuity `json:"closeNoCorrector,omitempty" bson:"closeNoCorrector,omitempty"`
	DistanceWithCorrector *VisualAcuity `json:"distanceWithCorrector,omitempty" bson:"distanceWithCorrector,omitempty"`
	CloseWithCorrector    *VisualAcuity `json:"closeWithCorrector,omitempty" bson:"closeWithCorrector,omitempty"`
	Stereopsis            string        `json:"stereopsis,omitempty" bson:"stereopsis,omitempty"`
	Status                int           `json:"status" bson:"status"`
	ExternalId            string        `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem        string        `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate           time.Time     `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate       time.Time     `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}

type VisualAcuity struct {
	Right string `json:"right,omitempty" bson:"right,omitempty"`
	Left  string `json:"left,omitempty" bson:"left,omitempty"`
}
