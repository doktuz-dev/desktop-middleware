package models

type XrayLD struct {
	ID              string `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionID     string `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	ServiceAreaID   string `json:"serviceAreaId,omitempty" bson:"serviceAreaId,omitempty"`
	ServiceAreaCode string `json:"serviceAreaCode,omitempty" bson:"serviceAreaCode,omitempty"`
	Description1    string `json:"description1,omitempty" bson:"description1,omitempty"`
	Description2    string `json:"description2,omitempty" bson:"description2,omitempty"`
	Description3    string `json:"description3,omitempty" bson:"description3,omitempty"`
	Description4    string `json:"description4,omitempty" bson:"description4,omitempty"`
	Description5    string `json:"description5,omitempty" bson:"description5,omitempty"`
	Description6    string `json:"description6,omitempty" bson:"description6,omitempty"`
	Description7    string `json:"description7,omitempty" bson:"description7,omitempty"`
	Description8    string `json:"description8,omitempty" bson:"description8,omitempty"`
	Description9    string `json:"description9,omitempty" bson:"description9,omitempty"`
	Description10   string `json:"description10,omitempty" bson:"description10,omitempty"`
	Description11   string `json:"description11,omitempty" bson:"description11,omitempty"`
	Description12   string `json:"description12,omitempty" bson:"description12,omitempty"`
	Description13   string `json:"description13,omitempty" bson:"description13,omitempty"`
	Description14   string `json:"description14,omitempty" bson:"description14,omitempty"`
	Description15   string `json:"description15,omitempty" bson:"description15,omitempty"`
	Result          string `json:"result,omitempty" bson:"result,omitempty"`
	Diagnostic1     string `json:"diagnostic1,omitempty" bson:"diagnostic1,omitempty"`
	Diagnostic2     string `json:"diagnostic2,omitempty" bson:"diagnostic2,omitempty"`
	Diagnostic3     string `json:"diagnostic3,omitempty" bson:"diagnostic3,omitempty"`
	Status          int    `json:"status" bson:"status"`
}
