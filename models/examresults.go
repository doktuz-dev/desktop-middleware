package models

import "time"

type ExamResult struct {
	ID              string    `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionId     string    `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	ServiceAreaID   string    `json:"serviceAreaId,omitempty" bson:"serviceAreaId,omitempty"`
	ServiceAreaCode string    `json:"serviceAreaCode,omitempty" bson:"serviceAreaCode,omitempty"`
	Filename        string    `json:"filename,omitempty" bson:"filename,omitempty"`
	PrintId         string    `json:"printId,omitempty" bson:"printId,omitempty"`
	Reviewed        bool      `json:"reviewed,omitempty" bson:"reviewed,omitempty"`
	Status          int       `json:"status" bson:"status"`
	ExternalId      string    `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem  string    `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate     time.Time `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate time.Time `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}
