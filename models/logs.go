package models

import "time"

type Log struct {
	ID              string      `json:"_id" bson:"_id"`
	FromSystem      string      `json:"fromSystem" bson:"fromSystem"`
	ToSystem        string      `json:"toSystem" bson:"toSystem"`
	Collection      string      `json:"collection" bson:"collection"`
	Body            interface{} `json:"body" bson:"body"`
	Operation       string      `json:"operation" bson:"operation"`
	Error           string      `json:"error,omitempty" bson:"error,omitempty"`
	Status          string      `json:"status" bson:"status"`
	CreatedDate     time.Time   `json:"createdDate" bson:"createdDate"`
	LastUpdatedDate time.Time   `json:"lastUpdatedDate" bson:"lastUpdatedDate"`
}
