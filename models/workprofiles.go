package models

import "time"

type WorkProfile struct {
	ID                 string          `json:"_id,omitempty" bson:"_id,omitempty"`
	CustomerId         string          `json:"customerId,omitempty" bson:"customerId,omitempty"`
	Name               string          `json:"name,omitempty" bson:"name,omitempty"`
	EndDate            time.Time       `json:"endDate,omitempty" bson:"endDate,omitempty"`
	Version            int             `json:"version" bson:"version"`
	Status             int             `json:"status" bson:"status"`
	Blocked            int             `json:"blocked" bson:"blocked"`
	ExternalId         string          `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalProtocolId string          `json:"externalProtocolId,omitempty" bson:"externalProtocolId,omitempty"`
	ExternalSystem     string          `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate        time.Time       `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate    time.Time       `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
	Healthcentres      []*HealthCentre `json:"healthcentres,omitempty" bson:"healthcentres,omitempty"`
}
