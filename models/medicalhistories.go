package models

import "time"

type MedicalHistory struct {
	ID                     string    `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionId            string    `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	PersonId               string    `json:"personId,omitempty" bson:"personId,omitempty"`
	PatientId              string    `json:"patientId,omitempty" bson:"patientId,omitempty"`
	Anamnesis              string    `json:"anamnesis,omitempty" bson:"anamnesis,omitempty"`
	Skin                   string    `json:"skin,omitempty" bson:"skin,omitempty"`
	Head                   string    `json:"head,omitempty" bson:"head,omitempty"`
	Neck                   string    `json:"neck,omitempty" bson:"neck,omitempty"`
	Nose                   string    `json:"nose,omitempty" bson:"nose,omitempty"`
	Mouth                  string    `json:"mouth,omitempty" bson:"mouth,omitempty"`
	Pharynx                string    `json:"pharynx,omitempty" bson:"pharynx,omitempty"`
	Larynx                 string    `json:"larynx,omitempty" bson:"larynx,omitempty"`
	Heart                  string    `json:"heart,omitempty" bson:"heart,omitempty"`
	Lungs                  string    `json:"lungs,omitempty" bson:"lungs,omitempty"`
	Belly                  string    `json:"belly,omitempty" bson:"belly,omitempty"`
	Miemsd                 string    `json:"miemsd,omitempty" bson:"miemsd,omitempty"`
	Miemid                 string    `json:"miemid,omitempty" bson:"miemid,omitempty"`
	Backbone               string    `json:"backbone,omitempty" bson:"backbone,omitempty"`
	Breast                 string    `json:"breast,omitempty" bson:"breast,omitempty"`
	Genitals               string    `json:"genitals,omitempty" bson:"genitals,omitempty"`
	Glands                 string    `json:"glands,omitempty" bson:"glands,omitempty"`
	March                  string    `json:"march,omitempty" bson:"march,omitempty"`
	OsteotendinousReflexes string    `json:"osteotendinousReflexes,omitempty" bson:"osteotendinousReflexes,omitempty"`
	Language               string    `json:"language,omitempty" bson:"language,omitempty"`
	Hernia                 string    `json:"hernia,omitempty" bson:"hernia,omitempty"`
	VaricoseVein           string    `json:"varicoseVein,omitempty" bson:"varicoseVein,omitempty"`
	NervousSistem          string    `json:"nervousSistem,omitempty" bson:"nervousSistem,omitempty"`
	LymphaticSistem        string    `json:"lymphaticSistem,omitempty" bson:"lymphaticSistem,omitempty"`
	Status                 int       `json:"status" bson:"status"`
	ExternalId             string    `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem         string    `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate            time.Time `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate        time.Time `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}
