package models

import "time"

type ServiceArea struct {
	ID              string    `json:"_id,omitempty" bson:"_id,omitempty"`
	Code            string    `json:"code,omitempty" bson:"code,omitempty"`
	Name            string    `json:"name,omitempty" bson:"name,omitempty"`
	Description     string    `json:"description,omitempty" bson:"description,omitempty"`
	Status          int       `json:"status" bson:"status"`
	ExternalId      string    `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem  string    `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate     time.Time `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate time.Time `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}
