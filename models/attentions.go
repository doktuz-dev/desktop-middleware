package models

import "time"

type Attention struct {
	ID                 string     `json:"_id,omitempty" bson:"_id,omitempty"`
	CustomerId         string     `json:"customerId,omitempty" bson:"customerId,omitempty"`
	CustomerCode       string     `json:"customerCode,omitempty" bson:"customerCode,omitempty"`
	CustomerName       string     `json:"customerName,omitempty" bson:"customerName,omitempty"`
	DepartmentId       string     `json:"departmentId,omitempty" bson:"departmentId,omitempty"`
	DepartmentName     string     `json:"departmentName,omitempty" bson:"departmentName,omitempty"`
	DistrictId         string     `json:"districtId,omitempty" bson:"districtId,omitempty"`
	DistrictName       string     `json:"districtName,omitempty" bson:"districtName,omitempty"`
	PersonId           string     `json:"personId,omitempty" bson:"personId,omitempty"`
	PatientId          string     `json:"patientId,omitempty" bson:"patientId,omitempty"`
	AttentionTypeId    string     `json:"attentionTypeId,omitempty" bson:"attentionTypeId,omitempty"`
	AttentionTypeName  string     `json:"attentionTypeName,omitempty" bson:"attentionTypeName,omitempty"`
	DeliveryTypeId     string     `json:"deliveryTypeId,omitempty" bson:"deliveryTypeId,omitempty"`
	DeliveryTypeName   string     `json:"deliveryTypeName,omitempty" bson:"deliveryTypeName,omitempty"`
	HealthCentreId     string     `json:"healthCentreId,omitempty" bson:"healthCentreId,omitempty"`
	HealthCentreName   string     `json:"healthCentreName,omitempty" bson:"healthCentreName,omitempty"`
	WorkStationId      string     `json:"workStationId,omitempty" bson:"workStationId,omitempty"`
	WorkStationName    string     `json:"workStationName,omitempty" bson:"workStationName,omitempty"`
	WorkProfileId      string     `json:"workProfileId,omitempty" bson:"workProfileId,omitempty"`
	WorkProfileName    string     `json:"workProfileName,omitempty" bson:"workProfileName,omitempty"`
	WorkingArea        string     `json:"workingArea,omitempty" bson:"workingArea,omitempty"`
	ExamTypeId         string     `json:"examTypeId,omitempty" bson:"examTypeId,omitempty"`
	ExamTypeName       string     `json:"examTypeName,omitempty" bson:"examTypeName,omitempty"`
	ExternalClinicID   string     `json:"externalClinicID,omitempty" bson:"externalClinicID,omitempty"`
	ExternalClinicName string     `json:"externalClinicName,omitempty" bson:"externalClinicName,omitempty"`
	JobTitle           string     `json:"JobTitle,omitempty" bson:"JobTitle,omitempty"`
	AttentionDate      time.Time  `json:"attentionDate,omitempty" bson:"attentionDate,omitempty"`
	MailStatus         string     `json:"mailStatus,omitempty" bson:"mailStatus,omitempty"`
	MailSentDateTime   *time.Time `json:"mailSentDateTime,omitempty" bson:"mailSentDateTime,omitempty"`
	MailOpenedDateTime *time.Time `json:"mailOpenedDateTime,omitempty" bson:"mailOpenedDateTime,omitempty"`
	Status             int        `json:"status" bson:"status"`
	WasAttended        bool       `json:"wasAttended" bson:"wasAttended"`
	ExternalClinicFlag bool       `json:"externalClinicFlag" bson:"externalClinicFlag"`
	ExternalId         string     `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem     string     `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate        time.Time  `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate    time.Time  `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}
