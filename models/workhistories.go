package models

import "time"

type WorkHistory struct {
	ID              string    `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionId     string    `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	Filename        []string  `json:"filename,omitempty" bson:"filename,omitempty"`
	PersonId        string    `json:"personId,omitempty" bson:"personId,omitempty"`
	PatientId       string    `json:"patientId,omitempty" bson:"patientId,omitempty"`
	Company         string    `json:"company,omitempty" bson:"company,omitempty"`
	JobTitle        string    `json:"jobTitle,omitempty" bson:"jobTitle,omitempty"`
	Dangers         *Dangers  `json:"dangers,omitempty" bson:"dangers,omitempty"`
	EppType         *EppType  `json:"eppType,omitempty" bson:"eppType,omitempty"`
	SequenceNumber  int       `json:"sequenceNumber,omitempty" bson:"sequenceNumber,omitempty"`
	Status          int       `json:"status" bson:"status"`
	ExternalId      string    `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem  string    `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate     time.Time `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate time.Time `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}

type Dangers struct {
	Dust               bool `json:"dust,omitempty" bson:"dust,omitempty"`
	Noise              bool `json:"noise,omitempty" bson:"noise,omitempty"`
	RepetitiveMovement bool `json:"repetitiveMovement,omitempty" bson:"repetitiveMovement,omitempty"`
	SegmentalVibration bool `json:"segmentalVibration,omitempty" bson:"segmentalVibration,omitempty"`
	HeavyMetals        bool `json:"heavyMetals,omitempty" bson:"heavyMetals,omitempty"`
	Ergonomic          bool `json:"ergonomic,omitempty" bson:"ergonomic,omitempty"`
	TotalVibrations    bool `json:"totalVibrations,omitempty" bson:"totalVibrations,omitempty"`
	Carcinogenic       bool `json:"carcinogenic,omitempty" bson:"carcinogenic,omitempty"`
	Mutagenic          bool `json:"mutagenic,omitempty" bson:"mutagenic,omitempty"`
	Shifts             bool `json:"shifts,omitempty" bson:"shifts,omitempty"`
	Disergonomic       bool `json:"disergonomic,omitempty" bson:"disergonomic,omitempty"`
	Positions          bool `json:"positions,omitempty" bson:"positions,omitempty"`
	Falls              bool `json:"falls,omitempty" bson:"falls,omitempty"`
	Solvent            bool `json:"solvent,omitempty" bson:"solvent,omitempty"`
	NightShifts        bool `json:"nightShifts,omitempty" bson:"nightShifts,omitempty"`
	HighTemperature    bool `json:"highTemperature,omitempty" bson:"highTemperature,omitempty"`
	Biological         bool `json:"biological,omitempty" bson:"biological,omitempty"`
	Loads              bool `json:"loads,omitempty" bson:"loads,omitempty"`
	Pvd                bool `json:"pvd,omitempty" bson:"pvd,omitempty"`
	Chemical           bool `json:"chemical,omitempty" bson:"chemical,omitempty"`
	Others             bool `json:"others,omitempty" bson:"others,omitempty"`
}

type EppType struct {
	Gloves      bool `json:"gloves" bson:"gloves,omitempty"`
	Earplug     bool `json:"earplug" bson:"earplug,omitempty"`
	Earmuffs    bool `json:"earmuffs" bson:"earmuffs,omitempty"`
	Helmet      bool `json:"helmet" bson:"helmet,omitempty"`
	Mask        bool `json:"mask" bson:"mask,omitempty"`
	Boots       bool `json:"boots" bson:"boots,omitempty"`
	Harness     bool `json:"harness" bson:"harness,omitempty"`
	Glasses     bool `json:"glasses" bson:"glasses,omitempty"`
	Clothes     bool `json:"clothes" bson:"clothes,omitempty"`
	Respirators bool `json:"respirators" bson:"respirators,omitempty"`
	Others      bool `json:"others,omitempty" bson:"others,omitempty"`
}
