package models

import "time"

type Consultation struct {
	ID              string    `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionId     string    `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	PersonID        string    `json:"personId,omitempty" bson:"personId,omitempty"`
	SpecialityID    string    `json:"specialityId,omitempty" bson:"specialityId,omitempty"`
	PrintId         string    `json:"printId,omitempty" bson:"printId,omitempty"`
	Replied         bool      `json:"replied" bson:"replied"`
	Status          int       `json:"status" bson:"status"`
	ExternalId      string    `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem  string    `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate     time.Time `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate time.Time `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}
