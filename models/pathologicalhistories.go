package models

import "time"

type PathologicalHistory struct {
	ID                        string                         `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionId               string                         `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	PersonId                  string                         `json:"personId,omitempty" bson:"personId,omitempty"`
	PatientId                 string                         `json:"patientId,omitempty" bson:"patientId,omitempty"`
	MedicalHistoryDescription string                         `json:"medicalHistoryDescription,omitempty" bson:"medicalHistoryDescription,omitempty"`
	MedicalHistoryAntecedent  string                         `json:"medicalHistoryAntecedent,omitempty" bson:"medicalHistoryAntecedent,omitempty"`
	FatherDisease             *PathologicalDisease           `json:"fatherDisease,omitempty" bson:"fatherDisease,omitempty"`
	MotherDisease             *PathologicalDisease           `json:"motherDisease,omitempty" bson:"motherDisease,omitempty"`
	BrotherDisease            *PathologicalDisease           `json:"brotherDisease,omitempty" bson:"brotherDisease,omitempty"`
	GrandParentDisease        *PathologicalDisease           `json:"grandParentDisease,omitempty" bson:"grandParentDisease,omitempty"`
	SpouseDisease             *PathologicalDisease           `json:"spouseDisease,omitempty" bson:"spouseDisease,omitempty"`
	Alergies                  bool                           `json:"alergies,omitempty" bson:"alergies,omitempty"`
	AlergiesDescription       string                         `json:"alergiesDescription,omitempty" bson:"alergiesDescription,omitempty"`
	Cigarrete                 *PathologicalConsume           `json:"cigarrete,omitempty" bson:"cigarrete,omitempty"`
	Alcohol                   *PathologicalConsume           `json:"alcohol,omitempty" bson:"alcohol,omitempty"`
	Drug                      *PathologicalConsume           `json:"drug,omitempty" bson:"drug,omitempty"`
	CurrentMedicine           string                         `json:"currentMedicine,omitempty" bson:"currentMedicine,omitempty"`
	JobTitle                  string                         `json:"jobTitle,omitempty" bson:"jobTitle,omitempty"`
	Vaccines                  *Vaccines                      `json:"vaccines,omitempty" bson:"vaccines,omitempty"`
	DiseaseHistory            *DiseaseHistory                `json:"diseaseHistory,omitempty" bson:"diseaseHistory,omitempty"`
	AptitudeCertificate       string                         `json:"aptitudeCertificate,omitempty" bson:"aptitudeCertificate,omitempty"`
	RecordCertificate         string                         `json:"recordCertificate,omitempty" bson:"recordCertificate,omitempty"`
	MedicalReport             string                         `json:"medicalReport,omitempty" bson:"medicalReport,omitempty"`
	MedicalHistory            string                         `json:"medicalHistory,omitempty" bson:"medicalHistory,omitempty"`
	Consultation              string                         `json:"consultation,omitempty" bson:"consultation,omitempty"`
	Status                    int                            `json:"status" bson:"status"`
	ExternalId                string                         `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem            string                         `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate               time.Time                      `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate           time.Time                      `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
	Injuries                  []*PathologicalHistoryInjuries `json:"injuries,omitempty" bson:"injuries,omitempty"`
}

type PathologicalDisease struct {
	HTA               bool   `json:"hta,omitempty" bson:"hta,omitempty"`
	DM                bool   `json:"dm,omitempty" bson:"dm,omitempty"`
	Cancer            bool   `json:"cancer,omitempty" bson:"cancer,omitempty"`
	CancerDescription string `json:"cancerDescription,omitempty" bson:"cancerDescription,omitempty"`
	RheumatoidDisease bool   `json:"rheumatoidDisease,omitempty" bson:"rheumatoidDisease,omitempty"`
	TBC               bool   `json:"tbc,omitempty" bson:"tbc,omitempty"`
	NR                bool   `json:"nr,omitempty" bson:"nr,omitempty"`
	Other             bool   `json:"other,omitempty" bson:"other,omitempty"`
	OtherDescription  string `json:"otherDescription,omitempty" bson:"otherDescription,omitempty"`
}

type PathologicalConsume struct {
	Consume          string `json:"consume,omitempty" bson:"consume,omitempty"`
	Type             string `json:"type,omitempty" bson:"type,omitempty"`
	Amount           string `json:"amount,omitempty" bson:"amount,omitempty"`
	ConsumptionLevel string `json:"consumptionLevel,omitempty" bson:"consumptionLevel,omitempty"`
}

type Vaccines struct {
	Tetanus        bool `json:"tetanus" bson:"tetanus,omitempty"`
	YellowFever    bool `json:"yellowFever" bson:"yellowFever,omitempty"`
	Flu            bool `json:"flu" bson:"flu,omitempty"`
	Hepatitisb     bool `json:"hepatitisb" bson:"hepatitisb,omitempty"`
	Mmr            bool `json:"mmr" bson:"mmr,omitempty"`
	Polio          bool `json:"polio" bson:"polio,omitempty"`
	Rabies         bool `json:"rabies" bson:"rabies,omitempty"`
	Influenza      bool `json:"influenza2" bson:"influenza2,omitempty"`
	Hepatitisa     bool `json:"hepatitisa" bson:"hepatitisa,omitempty"`
	Meningococcus  bool `json:"meningococcus" bson:"meningococcus,omitempty"`
	HumanPapilloma bool `json:"humanPapilloma" bson:"humanPapilloma,omitempty"`
	Bcg            bool `json:"bcg" bson:"bcg,omitempty"`
	Ppd            bool `json:"ppd" bson:"ppd,omitempty"`
	Infancy        bool `json:"infancy" bson:"infancy,omitempty"`
	Others         bool `json:"others" bson:"others,omitempty"`
}

type DiseaseHistory struct {
	Asthma       bool   `json:"asthma" bson:"asthma,omitempty"`
	AsthmaDetail string `json:"asthmaDetail,omitempty" bson:"asthmaDetail,omitempty"`
}

type PathologicalHistoryInjuries struct {
	ID                    string    `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionId           string    `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	PersonId              string    `json:"personId,omitempty" bson:"personId,omitempty"`
	PatientId             string    `json:"patientId,omitempty" bson:"patientId,omitempty"`
	PathologicalHistoryId string    `json:"pathologicalHistoryId,omitempty" bson:"pathologicalHistoryId,omitempty"`
	Description           string    `json:"description,omitempty" bson:"description,omitempty"`
	RelatedWithWork       bool      `json:"relatedWithWork,omitempty" bson:"relatedWithWork,omitempty"`
	Year                  string    `json:"year,omitempty" bson:"year,omitempty"`
	RestDays              string    `json:"restDays,omitempty" bson:"restDays,omitempty"`
	SequenceNumber        int       `json:"sequenceNumber,omitempty" bson:"sequenceNumber,omitempty"`
	Status                int       `json:"status" bson:"status"`
	ExternalId            string    `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem        string    `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate           time.Time `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate       time.Time `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}
