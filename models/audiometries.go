package models

import "time"

type Audiometry struct {
	ID                  string      `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionId         string      `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	ServiceAreaID       string      `json:"serviceAreaId,omitempty" bson:"serviceAreaId,omitempty"`
	ServiceAreaCode     string      `json:"serviceAreaCode,omitempty" bson:"serviceAreaCode,omitempty"`
	RightAirConduction  *Conduction `json:"rightAirConduction,omitempty" bson:"rightAirConduction,omitempty"`
	LeftAirConduction   *Conduction `json:"leftAirConduction,omitempty" bson:"leftAirConduction,omitempty"`
	RightBoneConduction *Conduction `json:"rightBoneConduction,omitempty" bson:"rightBoneConduction,omitempty"`
	LeftBoneConduction  *Conduction `json:"leftBoneConduction,omitempty" bson:"leftBoneConduction,omitempty"`
	Result              float64     `json:"result,omitempty" bson:"result,omitempty"`
	OtoscopyRight       string      `json:"otoscopyRight,omitempty" bson:"otoscopyRight,omitempty"`
	OtoscopyLeft        string      `json:"otoscopyLeft,omitempty" bson:"otoscopyLeft,omitempty"`
	ExposureHour        string      `json:"exposureHour,omitempty" bson:"exposureHour,omitempty"`
	WorkingYears        string      `json:"workingYears,omitempty" bson:"workingYears,omitempty"`
	Earplug             bool        `json:"earplug" bson:"earplug,omitempty"`
	Earmuffs            bool        `json:"earmuffs" bson:"earmuffs,omitempty"`
	Diagnostic1         string      `json:"diagnostic1,omitempty" bson:"diagnostic1,omitempty"`
	Diagnostic2         string      `json:"diagnostic2,omitempty" bson:"diagnostic2,omitempty"`
	Status              int         `json:"status" bson:"status"`
}

type AudiometryTest struct {
	ID                  string      `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionId         string      `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	ServiceAreaID       string      `json:"serviceAreaId,omitempty" bson:"serviceAreaId,omitempty"`
	ServiceAreaCode     string      `json:"serviceAreaCode,omitempty" bson:"serviceAreaCode,omitempty"`
	ExamId              string      `json:"examId,omitempty" bson:"examId,omitempty"`
	RightAirConduction  *Conduction `json:"rightAirConduction,omitempty" bson:"rightAirConduction,omitempty"`
	LeftAirConduction   *Conduction `json:"leftAirConduction,omitempty" bson:"leftAirConduction,omitempty"`
	RightBoneConduction *Conduction `json:"rightBoneConduction,omitempty" bson:"rightBoneConduction,omitempty"`
	LeftBoneConduction  *Conduction `json:"leftBoneConduction,omitempty" bson:"leftBoneConduction,omitempty"`
	Result              float64     `json:"result,omitempty" bson:"result,omitempty"`
	OtoscopyRight       string      `json:"otoscopyRight,omitempty" bson:"otoscopyRight,omitempty"`
	OtoscopyLeft        string      `json:"otoscopyLeft,omitempty" bson:"otoscopyLeft,omitempty"`
	ExposureHour        string      `json:"exposureHour,omitempty" bson:"exposureHour,omitempty"`
	WorkingYears        string      `json:"workingYears,omitempty" bson:"workingYears,omitempty"`
	Earplug             bool        `json:"earplug" bson:"earplug,omitempty"`
	Earmuffs            bool        `json:"earmuffs" bson:"earmuffs,omitempty"`
	Status              int         `json:"status" bson:"status"`
	ExternalId          string      `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem      string      `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate         time.Time   `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate     time.Time   `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}

type Conduction struct {
	R250  *float64 `json:"r250,omitempty" bson:"r250,omitempty"`
	R500  *float64 `json:"r500,omitempty" bson:"r500,omitempty"`
	R1000 *float64 `json:"r1000,omitempty" bson:"r1000,omitempty"`
	R2000 *float64 `json:"r2000,omitempty" bson:"r2000,omitempty"`
	R3000 *float64 `json:"r3000,omitempty" bson:"r3000,omitempty"`
	R4000 *float64 `json:"r4000,omitempty" bson:"r4000,omitempty"`
	R6000 *float64 `json:"r6000,omitempty" bson:"r6000,omitempty"`
	R8000 *float64 `json:"r8000,omitempty" bson:"r8000,omitempty"`
}
