package models

import "time"

type Person struct {
	ID                    string    `json:"_id" bson:"_id,omitempty"`
	PatientId             string    `json:"patientId,omitempty" bson:"patientId,omitempty"`
	FirstName             string    `json:"firstName,omitempty" bson:"firstName,omitempty"`
	LastName              string    `json:"lastName,omitempty" bson:"lastName,omitempty"`
	BirthDate             time.Time `json:"birthDate,omitempty" bson:"birthDate,omitempty"`
	Sex                   string    `json:"sex,omitempty" bson:"sex,omitempty"`
	DocumentTypeID        int       `json:"documentTypeId,omitempty" bson:"documentTypeId,omitempty"`
	DocumentNumber        string    `json:"documentNumber,omitempty" bson:"documentNumber,omitempty"`
	MaritalStatusId       string    `json:"maritalStatusId,omitempty" bson:"maritalStatusId,omitempty"`
	MaritalStatusName     string    `json:"maritalStatusName,omitempty" bson:"maritalStatusName,omitempty"`
	DegreeInstructionId   string    `json:"degreeInstructionId,omitempty" bson:"degreeInstructionId,omitempty"`
	DegreeInstructionName string    `json:"degreeInstructionName,omitempty" bson:"degreeInstructionName,omitempty"`
	Email                 string    `json:"email,omitempty" bson:"email,omitempty"`
	ZipCode               string    `json:"zipCode,omitempty" bson:"zipCode,omitempty"`
	PhoneNumber           string    `json:"phoneNumber,omitempty" bson:"phoneNumber,omitempty"`
	CompanyID             int       `json:"companyId,omitempty" bson:"companyId,omitempty"`
	CustomerId            string    `json:"customerId,omitempty" bson:"customerId,omitempty"`
	WorkPlace             string    `json:"workPlace,omitempty" bson:"workPlace,omitempty"`
	AdmissionDate         time.Time `json:"admissionDate,omitempty" bson:"admissionDate,omitempty"`
	ProjectID             int       `json:"projectId" bson:"projectId"`
	WorkStationId         string    `json:"workStationId,omitempty" bson:"workStationId,omitempty"`
	DistrictId            string    `json:"districtId,omitempty" bson:"districtId,omitempty"`
	DistrictName          string    `json:"districtName,omitempty" bson:"districtName,omitempty"`
	CreatedDate           time.Time `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate       time.Time `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}

type Patient struct {
	ID                    string    `json:"_id" bson:"_id,omitempty"`
	PersonID              string    `json:"personId,omitempty" bson:"personId,omitempty"`
	DocumentTypeID        int       `json:"documentTypeId,omitempty" bson:"documentTypeId,omitempty"`
	DocumentNumber        string    `json:"documentNumber,omitempty" bson:"documentNumber,omitempty"`
	FirstName             string    `json:"firstName,omitempty" bson:"firstName,omitempty"`
	LastName              string    `json:"lastName,omitempty" bson:"lastName,omitempty"`
	BirthDate             time.Time `json:"birthDate,omitempty" bson:"birthDate,omitempty"`
	Sex                   string    `json:"sex,omitempty" bson:"sex,omitempty"`
	Email                 string    `json:"email,omitempty" bson:"email,omitempty"`
	PhoneNumber           string    `json:"phoneNumber,omitempty" bson:"phoneNumber,omitempty"`
	MaritalStatusId       string    `json:"maritalStatusId,omitempty" bson:"maritalStatusId,omitempty"`
	MaritalStatusName     string    `json:"maritalStatusName,omitempty" bson:"maritalStatusName,omitempty"`
	DegreeInstructionId   string    `json:"degreeInstructionId,omitempty" bson:"degreeInstructionId,omitempty"`
	DegreeInstructionName string    `json:"degreeInstructionName,omitempty" bson:"degreeInstructionName,omitempty"`
	DistrictId            string    `json:"districtId,omitempty" bson:"districtId,omitempty"`
	DistrictName          string    `json:"districtName,omitempty" bson:"districtName,omitempty"`
	ExternalId            string    `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem        string    `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate           time.Time `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate       time.Time `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}
