package models

import "time"

type WorkingArea struct {
	ID              string    `json:"_id" bson:"_id"`
	CustomerID      string    `json:"customerId,omitempty" bson:"customerId,omitempty"`
	Description     string    `json:"description,omitempty" bson:"description,omitempty"`
	Enabled         bool      `json:"enabled" bson:"enabled"`
	ExternalId      string    `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem  string    `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate     time.Time `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate time.Time `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}
