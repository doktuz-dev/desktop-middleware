package models

import "time"

type Spirometry struct {
	ID              string `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionID     string `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	ServiceAreaID   string `json:"serviceAreaId,omitempty" bson:"serviceAreaId,omitempty"`
	ServiceAreaCode string `json:"serviceAreaCode,omitempty" bson:"serviceAreaCode,omitempty"`
	FileName1       string `json:"fileName1,omitempty" bson:"fileName1,omitempty"`
	Result          string `json:"result,omitempty" bson:"result,omitempty"`
	Quality         string `json:"quality,omitempty" bson:"quality,omitempty"`
	Diagnostic1     string `json:"diagnostic1,omitempty" bson:"diagnostic1,omitempty"`
	Diagnostic2     string `json:"diagnostic2,omitempty" bson:"diagnostic2,omitempty"`
	Diagnostic3     string `json:"diagnostic3,omitempty" bson:"diagnostic3,omitempty"`
	Status          int    `json:"status" bson:"status"`
}

type SpirometryTest struct {
	ID                string    `json:"_id,omitempty" bson:"_id,omitempty"`
	AttentionId       string    `json:"attentionId,omitempty" bson:"attentionId,omitempty"`
	ServiceAreaID     string    `json:"serviceAreaId,omitempty" bson:"serviceAreaId,omitempty"`
	ServiceAreaCode   string    `json:"serviceAreaCode,omitempty" bson:"serviceAreaCode,omitempty"`
	ExamId            string    `json:"examId,omitempty" bson:"examId,omitempty"`
	Result            string    `json:"result,omitempty" bson:"result,omitempty"`
	FVC               float64   `json:"fvc,omitempty" bson:"fvc,omitempty"`
	FEV               float64   `json:"fev,omitempty" bson:"fev,omitempty"`
	FEV_FVC           float64   `json:"fev_fvc,omitempty" bson:"fev_fvc,omitempty"`
	FEF               float64   `json:"fef,omitempty" bson:"fef,omitempty"`
	FVCPercentage     float64   `json:"fvcPercentage,omitempty" bson:"fvcPercentage,omitempty"`
	FEVPercentage     float64   `json:"fevPercentage,omitempty" bson:"fevPercentage,omitempty"`
	FEV_FVCPercentage float64   `json:"fev_fvcPercentage,omitempty" bson:"fev_fvcPercentage,omitempty"`
	FEFPercentage     float64   `json:"fefPercentage,omitempty" bson:"fefPercentage,omitempty"`
	Status            int       `json:"status" bson:"status"`
	ExternalId        string    `json:"externalId,omitempty" bson:"externalId,omitempty"`
	ExternalSystem    string    `json:"externalSystem,omitempty" bson:"externalSystem,omitempty"`
	CreatedDate       time.Time `json:"createdDate,omitempty" bson:"createdDate,omitempty"`
	LastUpdatedDate   time.Time `json:"lastUpdatedDate,omitempty" bson:"lastUpdatedDate,omitempty"`
}
